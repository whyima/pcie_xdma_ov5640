﻿#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <Qtqml>
#include "pcie_speed.h"
#include "pcie_memory.h"
#include "pcie_gpio.h"
#include "pcie_interrupt.h"
#include "pcie_state.h"
#include "vediorect.h"

bool pcie_state::pcieIsOk = false; // 静态成员变量必须先初始化
int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQuickStyle::setStyle("Material");

    QQmlApplicationEngine engine;

    qmlRegisterSingletonType(QUrl("qrc:/common/SkinModel.qml"), "Qt.Singleton", 1, 0, "SkinSingleton");
    qmlRegisterType<pcie_speed>("PcieSpObj",1,0,"PcieSpeed");
    qmlRegisterType<pcie_memory>("PcieMemObj",1,0,"PcieMemory");
    qmlRegisterType<pcie_gpio>("PcieGpioObj",1,0,"PcieGpio");
    qmlRegisterType<pcie_interrupt>("PcieIntrObj",1,0,"PcieIntr");
    qmlRegisterType<pcie_state>("PcieObj",1,0,"Pcie");
    qmlRegisterType<VedioRect>("Area",1,0,"VedioRect");

    const QUrl url(u"qrc:/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject *obj, const QUrl &objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
