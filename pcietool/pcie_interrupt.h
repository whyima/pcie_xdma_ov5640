﻿#ifndef PCIE_INTERRUPT_H
#define PCIE_INTERRUPT_H

#include <QObject>
#include <QThread>
#include <QDebug>

class eventThread : public QThread
{
    Q_OBJECT
public:
    eventThread(QWidget *parent = nullptr) {
        Q_UNUSED(parent);
    }
    virtual void run() override; //线程重写
signals:
    void intrInfo();
};


class pcie_interrupt : public QObject
{
    Q_OBJECT
public:
    explicit pcie_interrupt(QObject *parent = nullptr);
    ~pcie_interrupt();

public:
    eventThread *eventthread = new eventThread();
    void extracted(uint32_t &led_val);
    void intrHandler(); //中 断处理函数



signals:

};

#endif // PCIE_INTERRUPT_H
