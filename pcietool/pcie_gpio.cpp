﻿#include "pcie_gpio.h"
#include "pcie_dev.h"
#include "pcie_state.h"

pcie_gpio::pcie_gpio(QObject *parent)
    : QObject{parent}
{
    keyCal = new QTimer(this);
    connect(keyCal,&QTimer::timeout,this,[=](){
        keyRead();
    });

    keyCal->start(1000);
}

void pcie_gpio::ledChosse(uint32_t val)
{
    val = val & 0x0000000F;
    write_user(0,val); //前者地址偏移量，后者值，对于板上对应的一个AXIGPIO通道，至多操作四位地址，即32个GPIO
}

void pcie_gpio::keyRead()
{
    if(pcie_state::pcieIsOk){
        uint32_t key_val_temp;
        read_user(8,&key_val_temp);//(offset,uint32_t *)
        setKey_val(key_val_temp & 0x000f);
    }
    else {

    }
}






//=============================================================================================
uint32_t pcie_gpio::getKey_val() const
{
    return key_val;
}

void pcie_gpio::setKey_val(uint32_t newKey_val)
{
    if (key_val == newKey_val)
        return;
    key_val = newKey_val;
    emit key_valChanged();
}
