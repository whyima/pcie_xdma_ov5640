﻿import QtQuick 2.12
import QtQuick.Controls 2.5
import Qt5Compat.GraphicalEffects
Button {
    id: container
    property string bckcolor: "#1AAD19"
    property string bckHoverColor: Qt.lighter(bckcolor, 0.8)
    property string disabledColor: Qt.lighter(bckcolor, 0.3)
    property int backRadius: 2

    font.family: "Microsoft Yahei"
    font.pixelSize: 20
    implicitWidth: text.contentWidth + 24
    implicitHeight: text.contentHeight + 8
    contentItem: Text {
        id: text
        text: container.text
        font: container.font
        color: container.enabled ? "#fff" : "80FFFFFF"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
    background: Rectangle {
        anchors.fill: parent
        radius: backRadius
        color: !container.enabled ? disabledColor:
                container.down ? bckcolor :
                container.hovered ? bckHoverColor : bckcolor

        layer.enabled: true
        layer.effect: DropShadow {
           color: container.enabled ? bckcolor:disabledColor
        }
    }
}
