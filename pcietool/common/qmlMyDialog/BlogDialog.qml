﻿import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14
import QtLocation 6.6
import QtQuick.Controls.Material 2.12
import Qt.labs.platform 1.0
import "qrc:/common"

DropPopup {
    id: skinDialog


    readonly property string typeQuestion : "qrc:/sources/question.png"
    readonly property string typeError : "qrc:/sources/error.png"
    readonly property string typeInformation : "qrc:/sources/info.png"
    readonly property string typeSuccess : "qrc:/sources/ok.png"

    enum DialogType {
        DialogQuestion,
        DialogError,
        DialogInformation,
        DialogSuccess
    }

    z: 100

    width: contentLayout.width+80 < 340 ? 340 : contentLayout.width + 80
    height: !title.length ? contentLayout.height + 60 : contentLayout.height + 80


    property alias title: _title.text
    property alias icon: _icon.source

    property string accentColorStr: accentColor
    function dialogOpen() {

        icon = typeInformation
        rejectVisible =   false // 不显示取消按钮
        open()
    }


    Item {
        anchors.fill: parent

        ImageButton {
            anchors.right: parent.right
            imageSrc: "qrc:/sources/close2.png"
            hoverimageSrc:"qrc:/sources/close_hover.png"
            backHoverColor: "#FA5151"
            BaseToolTip { //鼠标停留显示，属于自定义基本组件
                 visible: parent.hovered
                 text: "关闭"
                 font.pixelSize: 12
                 delay: 1000
            }
            onClicked: close();
        }

        ColumnLayout {
            id: dialogLayout
            width: skinDialog.width
            spacing: 0
            RowLayout {
                visible: title.length
                Layout.leftMargin: 6
                Layout.preferredHeight: 28
                YaheiText {
                    id: _title
                    font.pixelSize: 16
                    color: tingeColor
                    text: "关于QianWindow"
                }
            }
            Rectangle {
                visible: title.length
                height: 1
                color: tingeOpacityColor
                Layout.fillWidth: true
            }
            ColumnLayout {
                id: contentLayout
                Layout.margins: 20
                Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                RowLayout {
                        Layout.topMargin: title != "" ? 0 : 20
                        Layout.bottomMargin: 20
                        Image {
                            id: _icon
                            antialiasing: true
                            source: "qrc:/sources/info.png"
                            Layout.preferredWidth: 40
                            Layout.preferredHeight: 40
                            fillMode: Image.PreserveAspectFit
                        }

                        YaheiText {
//                            text: `最新版本视频地址： <font color="${accentColorStr}"><a href="https://space.bilibili.com/82157618/channel/collectiondetail?sid=1360570&ctype=0">B站演示入口</a></font>`
                            text:'      Xdma Pcie小助手'
                            font.pixelSize: 15
                            color: tingeColor
                            onLinkActivated:  Qt.openUrlExternally(link);
                        }

               }

             }
         }
    }

}
