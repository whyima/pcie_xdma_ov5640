﻿import QtQuick 2.12
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14
import QtQml.Models 2.12
import Qt.Singleton 1.0
import "qrc:/common"

Item {
    signal switchPage(string name)
    property int strechCurrentIndex: 0
    property int strechLength: 0

    property var itemsArrays: [
        {
            name: "基础测试",
            stretch: true,
            subItem: [
                {
                    name: "PCIe 测速",
                },
                {
                    name: "读写测试",
                },
                {
                    name: "GPIO 实验",
                }
           ]
        },

        {
            name: "个人工程",
            icon: "",
            stretch: false,
            subItem: [
                {
                    name: "视频采集",
                }/*,
                {
                    name: "图片预览器",
                },
                {
                    name: "DragView",
                }*/
            ]
        },

        {
            name: "组件参考",
            icon: "",
            stretch: false,
            subItem: [
                {
                    name: "Button",
                },
                {
                    name: "Other",
                },
                {
                    name: "提示",
                }
            ]
        }
    ]


    ListModel {
        id: stretchModel
//        dynamicRoles : true
    }


    property int itemHeight: 60   // 184 * 60
    property string strechCurrentName:  ""
    property var alarmObjItem: null



    // 校验未知
    function checkUnknown(field, defaultData) {
        return field == undefined ? defaultData : field
    }

    function createModel(target, tier) {


        let idx = -999
        if( target.subItem == undefined  || !target.subItem.length) {
            idx = strechLength
            strechLength += 1
        }


        return {
            'name': target.name,
            'icon': checkUnknown(target.icon, ""),
            'hintNumber': checkUnknown(target.hintNumber, -1),
            'stretch':  checkUnknown(target.stretch, false),
            'tier': tier,
            'subItem' : [],
            'toIndex' :  idx,
        };
    }

    function createModel2(name, icon, tier, toIndex, hintNumber) {

        return {
            'name': name,
            'icon': icon,
            'hintNumber': hintNumber,
            'stretch': false,
            'tier': tier,
            'subItem' : [],
            'toIndex' :  toIndex,
        };
    }

    function appendSubItem(parent, subItem, tier) {
        if (subItem == undefined || subItem.length == 0) return;



        for(let i = 0; i < subItem.length; i++) {

            let model = createModel(subItem[i], tier);

            parent.subItem.append(model);

            let count = appendSubItem(parent.subItem.get(parent.subItem.count-1), subItem[i].subItem, tier+1);
        }
    }


    function searchItemByToIndex(parent, toIndex) {

        for(let i = 0; i < parent.count; i++) {

            let model = parent.get(i);

            if(model.toIndex === toIndex)
                return model;

            if(model.subItem.count > 0) {
                let ret = searchItemByToIndex(model.subItem, toIndex);
                if(ret !== null) {
                    return ret;
                }
            }
        }
        return null;
    }


    Component.onCompleted: {
        for(let i = 0; i < itemsArrays.length; i++) {

            let tier = 0;
            let model = createModel(itemsArrays[i], tier);
            stretchModel.append(model);
            appendSubItem(stretchModel.get(stretchModel.count-1), itemsArrays[i].subItem, tier+1);

        }

    }


    property color btnColor: pcieIsOK?"#80ADFF2F":"#804785FF"
    property string btnText: pcieIsOK?"关闭Xdma":"初始化Xdma"
    property bool btnEnb: true
    Timer{ //定时器,防止用户乱操作
        id: timer_pcieInit;
        interval: 100;//设置定时器定时时间为500ms,默认1000ms
        repeat: false //是否重复定时,默认为false
        running: false //是否开启定时，默认是false，当为true的时候，进入此界面就开始定时
        triggeredOnStart: false // 是否开启定时就触发onTriggered，一些特殊用户可以用来设置初始值。
        onTriggered: {
            if(!pcieIsOK){
                pcie.pcie_sinit();
                btnEnb = true
            }
            else {
                pcie.pcie_btnclose();  //定时触发槽,定时完成一次就进入一次
                btnEnb = true
            }

        }
        //restart ,start,stop,定时器的调用方式，顾名思义
    }


    ColumnLayout {
        spacing: 10
        anchors.fill: parent
        anchors.centerIn: parent
        anchors.bottomMargin: 10 //控件底边距
        ListView {
            id: stretchList
            Layout.fillHeight: true
            Layout.topMargin: 8
            Layout.bottomMargin: 8
            Layout.rightMargin: skin.gradSupport  ? 0 :6
            Layout.fillWidth: true
            clip: true
            currentIndex: -1
            model: stretchModel



            delegate: QianStretchDalegate {
            }

            displaced: Transition {
                     NumberAnimation { properties: "x,y"; duration: 90 }
            }

            ScrollBar.vertical: ScrollBar {
                id: scroll1
                policy: size<1.0? ScrollBar.AlwaysOn : ScrollBar.AlwaysOff
                x: skin.gradSupport  ? 8 : stretchList.width-8

                contentItem: Rectangle {
                     implicitWidth: 8
                     implicitHeight: 100
                     radius: width / 2
                     color: skin.light ? skin.stretchSelectBackColor : accentOpacityHoverColor
                }
                width: 8
                background: Item {

                }
           }
        }

        ProgressBar {
          Layout.alignment: Qt.AlignCenter
          indeterminate: !btnEnb
          implicitWidth: parent.width
        }

        BaseButton{
            text: btnText
            enabled: btnEnb
            Layout.alignment: Qt.AlignCenter
//            padding: 10 //自身边框到自身内部另一个容器边框之间的距离，属于容器内距离
            Layout.preferredHeight: 75
            Layout.preferredWidth: 75
            font.pixelSize:  12
            backRadius: 100
            bckcolor: btnColor
            onClicked: {
                btnEnb = false
                timer_pcieInit.start();
            }
        }
    }
}
