﻿import QtQuick 2.12
import QtQuick.Particles

ParticleSystem {
    id: particles
    width: 100
    height: 100
    property bool mouse_effect: true   //指示鼠标点击爆炸是否有效
    property color rect_bckcolor: "#8F0f0f0f"   //指示鼠标点击爆炸是否有效

    function particles_bust(){
        burstEmitter2.burst(500, particles.width/2, particles.height/2);   //Emitter::burst(int count, int x, int y)会在（x,y）出瞬间发射出count个粒子
        imageParticle.color = Qt.rgba(Math.random(),Math.random(),Math.random(),1)
    }
    //-----------------------------------------------------------------
    //爆炸的粒子画笔
    ImageParticle {
        id: imageParticle;
        x: 400;
        y: 480;
        groups: ["stage"];                             //分组为stage
        source: "qrc:///particleresources/star.png";    //这是QML内置的一个路径,能够在源代码包内搜索particlesources这个目录
        alpha: 0;
        colorVariation: 0.2;                //颜色误差
        entryEffect: ImageParticle.Scale;   //粒子效果为放缩
        rotation: 60;                       //旋转角度
        rotationVariation: 30;              //旋转角度误差
        rotationVelocity: 45;               //旋转速率
        rotationVelocityVariation: 15;      //旋转速率误差
    }

    //爆炸的粒子发射器
    Emitter {
        id: burstEmitter2;
        group: "stage";    //分组为stage3
        emitRate: 4000;     //发射速率为4000个每秒
        lifeSpan: 4000;     //生命周期为3000ms
        size: 30;           //粒子大小
        endSize: 5;         //粒子生命周期结束时大小
        sizeVariation:10;   //粒子大小误差
        enabled: false;     //发射器不起作用
        velocity: CumulativeDirection {     //粒子速率，CumulativeDirection将多个方向合一
            AngleDirection {
                angleVariation: 360;
                magnitudeVariation: 500;
            }
            PointDirection {
                y: 0;
            }
        }
        acceleration: PointDirection {      //粒子加速度
            y: 0;
        }
    }
    //-----------------------------------------------------------------

    Rectangle{
        width: particles.width
        height: particles.height
        color: rect_bckcolor
        border.color: "black"
        z: parent.z - 1
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(mouse_effect){
                    burstEmitter2.burst(500, mouseX, mouseY);   //Emitter::burst(int count, int x, int y)会在（x,y）出瞬间发射出count个粒子
                    imageParticle.color = Qt.rgba(Math.random(),Math.random(),Math.random(),1)
                }
            }
        }
    }

}
