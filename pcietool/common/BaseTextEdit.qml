﻿import QtQuick
import QtQuick.Controls
import Qt5Compat.GraphicalEffects

TextEdit{ //确定按键enter换行
    id:mytext
    height: 50
    width: parent.width
//    anchors.centerIn: parent
    color: "red"
    font.pixelSize: 15
    font.bold: true //加粗，默认false
    font.italic: false //是否用斜体,默认false
    font.letterSpacing: 0 //字母之间距离，正表示增加，负表示缩小，0表示默认距离
    font.wordSpacing: 0 //单词之间的距离，说明同上行
    font.strikeout: false //设置字体是否有删除线，默认false
    font.underline: false //设置字体是否有下划线，默认false
    activeFocusOnPress: true //默认true,鼠标点击是否能输入。
    //readOnly: true  //设置只读
    //cursorDelegate: comp_text//光标也就是输入区域的高显，该委托起点是输入的终点
    //cursorVisible: false
    //cursorPosition: 200
    //设置文本的大小写
    //font.capitalization: Font.MixedCase //不使用大小写改变
    //font.capitalization: Font.AllUppercase //所有的都大写
    //font.capitalization: Font.AllLowercase //所有的都小写
    //font.capitalization: Font.SmallCaps //使用小大写，
    //font.capitalization: Font.Capitalize  //单词的第一个字母大写
    //font.weight: Font.Light
    //font.weight: Font.Normal
    //font.weight: Font.DemiBold
    //font.weight: Font.Bold
    //font.weight: Font.Black
    //文本的对齐方式，顾名思义
    //horizontalAlignment: TextInput.AlignHCenter
    //horizontalAlignment: TextInput.AlignLeft
    //horizontalAlignment: TextInput.AlignRight
     selectByMouse: true //是否可以选择文本
     //选择文本的方式，只有selectByMouse为真才有效
    //mouseSelectionMode: TextInput.SelectCharacters //一个字符为单位选择，默认
    //mouseSelectionMode: TextInput.SelectWords      //一个单词为单位选择
     selectedTextColor: "black" //设置选择文本的字体颜色
     selectionColor: accentOpacityColor    //设置选择框的颜色
     //text:"hello" //输入文本默认显示的，可以修改和增加
     //换行设置
     //wrapMode: TextEdit.NoWrap //不执行自动换行，默认
     //wrapMode: TextEdit.WordWrap //根据单词换行
     //wrapMode: TextEdit.WrapAnywhere //任意处换行，即达到显示长度就换行
     //wrapMode: TextEdit.Wrap //自动合适换行，已单词优先
     //文本的显示方式
     //textFormat: TextEdit.AutoText
     //textFormat: TextEdit.PlainText
     //textFormat: TextEdit.RichText
     onLinkActivated: console.log("onLinkActivated");//链接或富文本点击时执行
}
