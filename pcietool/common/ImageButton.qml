﻿/*
    此组件为自定义按钮
*/
import QtQuick 2.12
import QtQuick.Controls 2.5

Button {
    id: container
    implicitWidth: 38
    implicitHeight: 24
    property string imageSrc: ''            //图片资源
    property var hoverimageSrc: null
    property string backHoverColor: 'transparent'
    property string backColor: 'transparent'
    property alias radius: back.radius
    property int btnRadius: 2

    clip: true
    padding: 0
    background: Rectangle {
       id: back
       radius: btnRadius
       anchors.fill: parent
       color: container.hovered ? backHoverColor : backColor
       Image {
           anchors.centerIn: parent
           antialiasing: true
           source: container.hovered ? (hoverimageSrc == null? imageSrc : hoverimageSrc)
                                  : imageSrc
           fillMode: Image.PreserveAspectFit

       }
   }
}
