﻿import QtQuick
import "qrc:/common"

Rectangle{
    id: prect
    color: tingeOpacityColor
    property int rectWidth: 300
    property int rectHeight: 200
    property int fontsize: 15
    width: rectWidth
    height: rectHeight

    function getEditText(){ //获取文本，从开头到结尾，限制了1024个字节
        return edit.getText(0,1024)
    }

    function editAppend(str){ //补充
        edit.append(str)
    }

    function editClear(){ //清空编辑框
        edit.clear()
    }

    function getEditLength(){ //获取编辑框文本长度
        return edit.length
    }

    Flickable {
        id: flick
        width: prect.width;
        height: prect.height;

        contentWidth: edit.paintedWidth
        contentHeight: edit.paintedHeight
        clip: true

        function ensureVisible(r)
        {
            if (contentX >= r.x)
                contentX = r.x;
            else if (contentX+width <= r.x+r.width)
                contentX = r.x+r.width-width;
            if (contentY >= r.y)
                contentY = r.y;
            else if (contentY+height <= r.y+r.height)
                contentY = r.y+r.height-height;
        }

        BaseTextEdit{
            id: edit
            color: accentColor
            width: flick.width
            height: flick.height
            focus: true
            wrapMode: TextEdit.Wrap
            onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
            font.pixelSize: fontsize
        }

    }
}
