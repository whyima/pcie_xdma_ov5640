﻿import QtQuick 2.14
import QtQuick.Controls 2.14
import "qrc:/common"



Rectangle{
    property int st_width: 300
    property int st_height: 300
    property int car_width: 20
    property int car_vaule
    property color car_color: rgb(0, 0, 200)

    id:car
    width: st_width
    height: st_height
    color: "transparent"

    function rgb(r, g, b){
//        var ret = (r << 16 | g << 8 | b)
        var rs,gs,bs;
        if(r<16){rs = "0" + r.toString(16) }
        else {rs = r.toString(16)}
        if(g<16){gs = "0" + g.toString(16) }
        else {gs = g.toString(16)}
        if(b<16){bs = "0" + b.toString(16) }
        else {bs = b.toString(16)}
        return ("#"+rs+gs+bs)
    }

    BaseCar {
//        anchors.fill: parent
        id: speed_car
        x: 0
        y: 0
        //        x: 175
        //        y: 93
        top_backgroundColor: car_color
        width: 291
        height: 228
        dial_addR: -6 //控制刻度盘圆弧与底层圆弧的距离
        dial_longNum: 10 // 刻度盘长刻度线的数量
        dial_longLen: 12  // 刻度盘长刻度线的长度
        dial_lineWidth: 3
        btm_lineWidth: 22
        top_lineWidth: 10
//        top_endAngle: slider.value*1.3+140
        top_endAngle: car_vaule*0.257+140   //顺时针画，从start划到end，0刻度是x轴正半,圆盘缺了107度，剩下257度整除要的总刻度
        top_startAngle: 140
        btm_endAngle: 400
        btm_startAngle: 140
        btm_r: 100
        top_r: 100

        Text {
            id: speed
            x: 104
            y: 116
            width: 89
            height: 44
            color:rgb(168,168,168)
            text: car_vaule.toString()
            style: Text.Normal
            font.weight: Font.ExtraBold
            font.capitalization: Font.MixedCase
            font.pixelSize: 35
            font.bold: true
            font.family: "Verdana"
            horizontalAlignment: Text.AlignHCenter
                }

        Label {
            id: speed_label
            x: 123
            y: 152
            width: 45
            height: 30
            text: qsTr("MB/s")
            font.pointSize: 11
            font.bold: true
            verticalAlignment: Text.AlignBottom
        }

        Label {
            id: label1
            x: 26
            y: 224
            width: 23
            height: 25
            text: qsTr("0")
            font.weight: Font.Normal
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 14
        }

        Label {
            id: label2
            x: 244
            y: 227
            width: 33
            height: 25
            text: qsTr("1000")
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 14
            font.weight: Font.Normal
        }

        Label {
            id: label3
            x: -6
            y: 167
            width: 23
            height: 25
            text: qsTr("100")
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 14
            font.weight: Font.Normal
        }

        Label {
                id: label4
                x: 266
                y: 167
                width: 42
                height: 25
                text: qsTr("900")
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 14
                font.weight: Font.Normal
            }

            Label {
                id: label5
                x: 0
                y: 102
                width: 23
                height: 25
                text: qsTr("200")
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 14
                font.weight: Font.Normal
            }

            Label {
                id: label6
                x: 276
                y: 102
                width: 23
                height: 25
                text: qsTr("800")
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 14
                font.weight: Font.Normal
            }

            Label {
                id: label7
                x: 19
                y: 41
                width: 23
                height: 25
                text: qsTr("300")
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 14
                font.weight: Font.Normal
            }

            Label {
                id: label8
                x: 243
                y: 41
                width: 35
                height: 25
                text: qsTr("700")
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 14
                font.weight: Font.Normal
            }

            Label {
                id: label9
                x: 74
                y: 8
                width: 23
                height: 25
                text: qsTr("400")
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 14
                font.weight: Font.Normal
            }

            Label {
                id: label10
                x: 187
                y: 8
                width: 36
                height: 25
                text: qsTr("600")
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 14
                font.weight: Font.Normal
            }

            Label {
                id: label11
                x: 134
                y: -6
                width: 23
                height: 25
                text: qsTr("500")
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 14
                font.weight: Font.Normal
            }

    }

}

