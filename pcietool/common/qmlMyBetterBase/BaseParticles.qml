﻿import QtQuick 2.14
import QtQuick.Particles


//粒子系统
ParticleSystem {
    id: particles;
    anchors.fill: parent;

    //-----------------------------------------------------------------
    //发射出去的粒子画笔
    ImageParticle {
        groups: ["stage1"];                             //分组为stage1
        source: "qrc:///particleresources/star.png";    //这是QML内置的一个路径,能够在源代码包内搜索particlesources这个目录
        alphaVariation: 0.4;                            //透明度误差
        colorVariation: 0.9;                            //颜色误差
    }

    //发射出去的粒子发射器
    Emitter {
        id: burstEmitter;
        x: 400;
        y: 480;
        group: "stage1";    //分组为stage1
        emitRate: 3;        //发射速率为1秒5个
        lifeSpan: 1500;     //粒子生命周期为1500ms
        size: 50;           //粒子大小为50px
        endSize: 10;        //粒子消亡时大小为10px
        sizeVariation: 30;  //粒子大小误差
        acceleration: PointDirection {  //粒子加速度
            y: 100;
        }
        velocity: AngleDirection {      //粒子速率
            angle: 270;             //粒子发射角度
            magnitude: 400;         //粒子速率为400像素/秒
            angleVariation: 40;     //角度误差
            magnitudeVariation: 50; //速率误差
        }
    }
    //-----------------------------------------------------------------

    //-----------------------------------------------------------------
    //发射出去的粒子后面烟气效果粒子画笔
    ImageParticle {
        groups: ["stage2"];                                 //分组为stage2
        source: "qrc:///particleresources/glowdot.png";     //这是QML内置的一个路径,能够在源代码包内搜索particlesources这个目录
        color: "#11111111";                                 //粒子颜色
    }

    //发射出去的粒子后面烟气效果粒子发射器
    TrailEmitter {
        group: "stage2";    //分组为stage2
        follow: "stage1";   //跟随stage1
        emitRatePerParticle: 100;
        lifeSpan: 2400;             //粒子生命周期为2400ms
        lifeSpanVariation: 400;     //粒子生命周期误差
        size: 16;                   //粒子大小
        endSize: 0;                 //粒子生命周期结束时大小
        sizeVariation: 8;           //粒子大小误差
        acceleration: PointDirection {  //粒子加速度
            y: -60;
        }
        velocity: AngleDirection {      //粒子速率
            angle: 270;             //粒子发射角度
            magnitude: 40;          //粒子速率为40像素/秒
            angleVariation: 22;     //角度误差
            magnitudeVariation: 5;  //速率误差
        }
    }
    //-----------------------------------------------------------------

    //-----------------------------------------------------------------
    //爆炸的粒子画笔
    ImageParticle {
        id: imageParticle;
        groups: ["stage3"];                             //分组为stage3
        source: "qrc:///particleresources/star.png";    //这是QML内置的一个路径,能够在源代码包内搜索particlesources这个目录
        alpha: 0;
        colorVariation: 0.2;                //颜色误差
        entryEffect: ImageParticle.Scale;   //粒子效果为放缩
        rotation: 60;                       //旋转角度
        rotationVariation: 30;              //旋转角度误差
        rotationVelocity: 45;               //旋转速率
        rotationVelocityVariation: 15;      //旋转速率误差
    }

    //爆炸的粒子发射器
    Emitter {
        id: burstEmitter2;
        group: "stage3";    //分组为stage3
        emitRate: 4000;     //发射速率为4000个每秒
        lifeSpan: 3000;     //生命周期为3000ms
        size: 30;           //粒子大小
        endSize: 5;         //粒子生命周期结束时大小
        sizeVariation:10;   //粒子大小误差
        enabled: false;     //发射器不起作用
        velocity: CumulativeDirection {     //粒子速率，CumulativeDirection将多个方向合一
            AngleDirection {
                angleVariation: 360;
                magnitudeVariation: 80;
            }
            PointDirection {
                y: 20;
            }
        }
        acceleration: PointDirection {      //粒子加速度
            y: 10;
        }
    }
    //-----------------------------------------------------------------

    //-----------------------------------------------------------------
    //粒子控制器
    Affector {
        system: particles
        width: parent.width
        height: 10;
        y: 90;
        once: true;             //只影响粒子一次
        groups: "stage1";       //影响分组stage1的粒子
        onAffectParticles: {
            for (var i=0; i<particles.length; i++) {
                burstEmitter2.burst(500, particles[i].x, particles[i].y);   //Emitter::burst(int count, int x, int y)会在（x,y）出瞬间发射出count个粒子
                imageParticle.color = Qt.rgba(particles[i].red, //设置爆炸粒子画笔的颜色
                                            particles[i].green,
                                            particles[i].blue,
                                            particles[i].alpha)
            }
        }

    }

}


