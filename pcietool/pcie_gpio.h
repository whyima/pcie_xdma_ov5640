﻿#ifndef PCIE_GPIO_H
#define PCIE_GPIO_H

#include <QObject>
#include <QTimer>
#include <QDebug>

class pcie_gpio : public QObject
{
    Q_OBJECT
public:
    explicit pcie_gpio(QObject *parent = nullptr);

    Q_INVOKABLE void ledChosse(uint32_t val);
    Q_INVOKABLE void keyRead();

    QTimer * keyCal;
    uint32_t key_val = 0x000f;


    uint32_t getKey_val() const;
    void setKey_val(uint32_t newKey_val);

signals:
    void keyVaild(int val);

    void key_valChanged();
private:
    Q_PROPERTY(uint32_t key_val READ getKey_val WRITE setKey_val NOTIFY key_valChanged FINAL)
};

#endif // PCIE_GPIO_H
