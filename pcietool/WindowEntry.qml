﻿import QtQuick 6.6
import Qt.Singleton 1.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.14
import QtQuick 2.12
//import Qt5Compat.GraphicalEffects

import "qrc:/common"
import "qrc:/common/qmlMyHints"
import "qrc:/common/qmlMyDialog"
Rectangle {

    id: windowEntry
    color: skin.mainColor
    gradient: skin.gradient


    Message{ //Message是顶部的提示框，这里设置Message的基本属性
        id:messageTip
        z: 1
        parent: Overlay.overlay
    }
//    function message(type, message) { //打开提示框，对输入做一些基本的过滤
//        if(type!=='success'&&type!=='error'&&type!=='info'){
//            return false
//        }
//        messageTip.open(type, message)
//    }

    BlogDialog {  //弹出窗
        id: skinQianDialog
        backParent: windowEntry
        parent: Overlay.overlay
        onAccept: { //信号槽的回调函数
           message('success', "You clicked the accept button!")
           skinQianDialog.close();
        }
    }

    Component.onCompleted: {   //组件初始化完成时发出提示框
        skinQianDialog.dialogOpen() //调用了窗口打开的函数
    }

    ColumnLayout { // QtQuick.Layouts import
        anchors.fill: parent
        spacing: 0
        WindowTilte {
            color: skin.titleColor
            Layout.fillWidth: true
            Layout.preferredHeight: contentList.fullscreen ? 0 :  42
            Layout.alignment: Qt.AlignTop
            Behavior on Layout.preferredHeight {
                NumberAnimation { duration: 300 }
            }
            clip: true

        }
        ContentList {
            id: contentList
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
}
