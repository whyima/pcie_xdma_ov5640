﻿#ifndef PCIE_STATE_H
#define PCIE_STATE_H

#include <QObject>
#include <QString>

//此文件用于全局pcie的状态

class pcie_state : public QObject
{
    Q_OBJECT
public:
    explicit pcie_state(QObject *parent = nullptr);
    ~pcie_state();

    static pcie_state *getInstance();

    Q_INVOKABLE void pcie_sinit();//设备初始化
    Q_INVOKABLE void pcie_btnclose();//设备初始化




    //如果不加const，静态值初始化就必须在类外
//    const static bool pcieIsOk = false;
    static bool pcieIsOk;
    bool busy;
    bool getPcieIsOk() const;
    void setPcieIsOk(bool newPcieIsOk);
    bool getBusy() const;
    void setBusy(bool newBusy);


signals:
    void pcie_init_info(QString info,QString message);
    void textMessage(QString message);
    void xdmaInfo(QString message);


    void pcieIsOkChanged();

    void busyChanged();

private slots:

private:
    Q_PROPERTY(bool pcieIsOk READ getPcieIsOk WRITE setPcieIsOk NOTIFY pcieIsOkChanged FINAL)
    Q_PROPERTY(bool busy READ getBusy WRITE setBusy NOTIFY busyChanged FINAL)
};

#endif // PCIE_STATE_H
