﻿#ifndef PCIE_SPEED_H
#define PCIE_SPEED_H

#include <QObject>
#include <QDebug>
#include <QTimer>
#include <QTime>
#include <QString>
#include "pcie_state.h"

class pcie_speed : public QObject
{
    Q_OBJECT
public:
    explicit pcie_speed(QObject *parent = nullptr);
    ~pcie_speed();


    /* 暴露给QML对象的接口 */
    Q_INVOKABLE void pcie_read_btnclk(void);    //读取速率测试
    Q_INVOKABLE void pcie_write_btnclk(void);   //写入速率测试

    unsigned int c2h_transfer_size;
    unsigned int h2c_transfer_size;
    QTimer *qtime_h2c;//定时器 每100ms
    QTimer *qtime_c2h;

    //给qml端调用的变量
    unsigned int rd_rate; //读写速率
    unsigned int wr_rate;

    //以下自动生成
    bool getPcieIsOk() const;
    void setPcieIsOk(bool newPcieIsOk);
    unsigned int getRd_rate() const;
    void setRd_rate(unsigned int newRd_rate);

    unsigned int getWr_rate() const;
    void setWr_rate(unsigned int newWr_rate);

signals:
//    void pcie_init_info(QString info,QString message);
//    void textMessage(QString message);
//    void xdmaInfo(QString message);
    //以下是提供给qml端的变量信号
    void rd_rateChanged();
    void wr_rateChanged();

private slots:
    void rd_rate_test();
    void wr_rate_test();

private:


    Q_PROPERTY(unsigned int rd_rate READ getRd_rate WRITE setRd_rate NOTIFY rd_rateChanged FINAL)
    Q_PROPERTY(unsigned int wr_rate READ getWr_rate WRITE setWr_rate NOTIFY wr_rateChanged FINAL)
};

#endif // PCIE_SPEED_H
