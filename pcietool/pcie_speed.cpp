﻿#include "pcie_speed.h"
#include <iostream>
#include "pcie_dev.h"




pcie_speed::pcie_speed(QObject *parent)
    : QObject{parent}
{
    c2h_transfer_size = 0x800000;
    h2c_transfer_size = 0x800000;
    //
    qtime_c2h = new QTimer(this);
    qtime_h2c = new QTimer(this);

    connect(qtime_c2h, SIGNAL(timeout()), this, SLOT(rd_rate_test()));
    connect(qtime_h2c, SIGNAL(timeout()), this, SLOT(wr_rate_test()));
}

pcie_speed::~pcie_speed()
{
    delete qtime_c2h;
    delete qtime_h2c;

}




void pcie_speed::rd_rate_test()
{
    setRd_rate(c2h_transfer_rate(c2h_transfer_size));// 调用驱动里的api
}

void pcie_speed::wr_rate_test()
{
    setWr_rate(h2c_transfer_rate(h2c_transfer_size));
}



//===========================以下函数供qml调用==================================//
void pcie_speed::pcie_read_btnclk() //连接读开关就是开关定时器
{
    if(pcie_state::pcieIsOk){
        if (!qtime_c2h->isActive())
            qtime_c2h->start(100);
        else
            qtime_c2h->stop();
    }
    else {
//                emit pcie_init_info("info","请先初始化pcie设备");
    }
}

void pcie_speed::pcie_write_btnclk() //连接写开关
{
    if(pcie_state::pcieIsOk){
        if (!qtime_h2c->isActive())
            qtime_h2c->start(100);
        else
            qtime_h2c->stop();
    }
    else {
        //        emit pcie_init_info("info","请先初始化pcie设备");
    }
}
//===========================自动生成的，用于将变量提供给qml使用==================================//
unsigned int pcie_speed::getRd_rate() const
{
    return rd_rate;
}

void pcie_speed::setRd_rate(unsigned int newRd_rate)
{
    if (rd_rate == newRd_rate)
        return;
    rd_rate = newRd_rate;
    emit rd_rateChanged();
}


unsigned int pcie_speed::getWr_rate() const
{
    return wr_rate;
}
void pcie_speed::setWr_rate(unsigned int newWr_rate)
{
    if (wr_rate == newWr_rate)
        return;
    wr_rate = newWr_rate;
    emit wr_rateChanged();
}
