QT += quick
QT += quickcontrols2
QT += widgets
CONFIG += c++11
RC_ICONS = zhen4_min_icon.ico

SOURCES += \
        common.cpp \
        main.cpp \
        pcie_dev.c \
        pcie_gpio.cpp \
        pcie_interrupt.cpp \
        pcie_memory.cpp \
        pcie_speed.cpp \
        pcie_state.cpp \
        vediorect.cpp

#resources.files = main.qml WindowEntry.qml
#resources.prefix = /$${TARGET}
RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    common.h \
    pcie_dev.h \
    pcie_gpio.h \
    pcie_interrupt.h \
    pcie_memory.h \
    pcie_speed.h \
    pcie_state.h \
    vediorect.h \
    xdma_public.h
