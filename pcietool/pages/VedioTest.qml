﻿import QtQuick 2.14
import QtQuick.Layouts
import "qrc:/common"

import Area

Item {

    property bool btntxtsel: false
    property string btntext: btntxtsel ? "关闭测试..." : "启动！"



    ColumnLayout {
        anchors.fill: parent
        anchors.rightMargin: 60
        anchors.topMargin: 30
        anchors.bottomMargin: 30
        anchors.leftMargin: 60
        spacing: 10

        Rectangle{
            width: 800 + 500
            height: 600
            color: "yellow"
            VedioRect{
                    id:videorect
                    anchors.fill: parent
                    MouseArea{
                        anchors.fill: parent
                        onPressed: {
                            console.log("x=",mouse.x,"  y=",mouse.y);
                        }

                    }
                }
        }



        BaseButton {
            text: btntext
            Layout.preferredHeight: 28
            Layout.preferredWidth: 78
            font.pixelSize:  14
            backRadius: 4
            bckcolor: "#4785FF"
            onClicked: {
                btntxtsel = !btntxtsel
                videorect.btnHander()
            }
        }

    }

}
