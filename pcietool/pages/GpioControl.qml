﻿import QtQuick
import QtQuick.Layouts
import "qrc:/common/qmlMyHints"
import "qrc:/common"
import "qrc:/common/qmlMyBetterBase"

import PcieGpioObj 1.0
import PcieIntrObj 1.0

Item {

    property int leftWidth: 182
    property int fontsize: 19

    property bool imgState0: false
    property bool imgState1: false
    property bool imgState2: false
    property int val_send: 0

    property var val_temp: [1,1,1,1]

    PcieIntr{
        id: pcie_intr


    }


    PcieGpio{
        id: pcie_io

        onKey_valChanged:{
            console.log("pcie_io.key_val = ",pcie_io.key_val&0x0001);
            val_temp[0] = pcie_io.key_val&0x0001;
            val_temp[1] = pcie_io.key_val&0x0002;
            val_temp[2] = pcie_io.key_val&0x0004;
            val_temp[3] = pcie_io.key_val&0x0008;
            if(val_temp[0] === 0){
                effect1.particles_bust()
            }
            if(val_temp[1] === 0){
                effect2.particles_bust()
            }
            if(val_temp[2] === 0){
                effect3.particles_bust()
            }
            if(val_temp[3] === 0){
                effect4.particles_bust()
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.rightMargin: 60
        anchors.topMargin: 30
        anchors.bottomMargin: 30
        anchors.leftMargin: 60
        spacing: 10

        YaheiText {
            text: "灯的控制"
            font.pixelSize: fontsize
            Layout.preferredWidth: leftWidth
            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
        }
        RowLayout{
            spacing: 120
            Layout.leftMargin: 120
            ImageButton{
                id: led0
                implicitWidth: 100
                implicitHeight: 130
                imageSrc: "qrc:/sources/light_close.png"           //图片资源
//                backHoverColor: 'white'
                backColor: 'transparent'
                btnRadius: 10
                onClicked: {
                    if(pcieIsOK){
                        if(!imgState0){
                            val_send = val_send | 0x1
                            pcie_io.ledChosse(val_send)
                            imgState0 = true
                            led0.imageSrc= "qrc:/sources/light_open.png"
                        }
                        else {
                            val_send = val_send & 0xE
                            pcie_io.ledChosse(val_send)
                            imgState0 = false
                            led0.imageSrc= "qrc:/sources/light_close.png"
                        }
                    }
                    else{
                        message("info","XDMA设备还未准备好！")
                    }
                }
            }
            ImageButton{
                id: led1
                implicitWidth: 100
                implicitHeight: 130
                imageSrc: "qrc:/sources/light_close.png"           //图片资源
//                backHoverColor: 'white'
                backColor: 'transparent'
                btnRadius: 10
                onClicked: {
                    if(pcieIsOK){
                        if(!imgState1){
                            val_send = val_send | 0x2
                            pcie_io.ledChosse(val_send)
                            imgState1 = true
                            led1.imageSrc= "qrc:/sources/light_open.png"
                        }
                        else {
                            val_send = val_send & 0xD
                            pcie_io.ledChosse(val_send)
                            imgState1 = false
                            led1.imageSrc= "qrc:/sources/light_close.png"
                        }
                    }
                    else{
                        message("info","XDMA设备还未准备好！")
                    }
                }
            }
            ImageButton{
                id: led2
                implicitWidth: 100
                implicitHeight: 130
                imageSrc: "qrc:/sources/light_close.png"           //图片资源
//                backHoverColor: 'white'
                backColor: 'transparent'
                btnRadius: 10
                onClicked: {
                    if(pcieIsOK){
                        if(!imgState2){
                            val_send = val_send | 0x4
                            pcie_io.ledChosse(val_send)
                            imgState2 = true
                            led2.imageSrc= "qrc:/sources/light_open.png"
                        }
                        else {
                            val_send = val_send & 0xB
                            pcie_io.ledChosse(val_send)
                            imgState2 = false
                            led2.imageSrc= "qrc:/sources/light_close.png"
                        }
                    }
                    else{
                        message("info","XDMA设备还未准备好！")
                    }
                }
            }
        }
        Rectangle {
            height: 1
            color: tingeOpacityColor
            Layout.fillWidth: true
            Layout.topMargin: 12
        }

        Rectangle { //实现分隔线的效果
            height: 1
            color: tingeOpacityColor
            Layout.fillWidth: true
        }
        Row{
            spacing:20
            BaseButton {
                text: "test"
                Layout.preferredHeight: 28
                Layout.preferredWidth: 78
                font.pixelSize:  14
                backRadius: 4
                bckcolor: "#4785FF"
                onClicked: {
                    effect1.particles_bust()
                }
            }
            QianQuestionMark {
                hint: "只是单纯的测试一下动画效果"
            }
        }

        RowLayout{
            spacing: 100
            Layout.leftMargin: 50
                BaseParticlesBasic{
                    id: effect1
                    mouse_effect: false

                }
                BaseParticlesBasic{
                    id: effect2
                    mouse_effect: false

                }
                BaseParticlesBasic{
                    id: effect3
                    mouse_effect: false

                }
                BaseParticlesBasic{
                    id: effect4
                    mouse_effect: false

                }

        }
    }
}
