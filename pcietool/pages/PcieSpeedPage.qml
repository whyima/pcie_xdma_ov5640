﻿import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.14
import QtQuick.Controls.Material 2.12
import "qrc:/common"
import "qrc:/common/qmlMyHints"
import "qrc:/common/qmlMyBetterBase"

import PcieObj 1.0
import PcieSpObj 1.0


Item {
    property int leftWidth: 182
    property int fontsize: 19

    property string line_message: "--------------------" // 连接信息显示
    property int max_rate_r: 0
    property int max_rate_w: 0
    property string dev_path: "--------------------"

    property int car_vaule_r            //给刻度盘用的值
    property int car_vaule_w
    property bool sw_r_en: pcieIsOK        //读速开关使能
    property bool sw_w_en: pcieIsOK        //写速开关使能



    PcieSpeed{ //定义c++那的对象，并对传递过来的变量进行一个处理
        id: pcie_sp

        onRd_rateChanged:{
            car_vaule_r = pcie_sp.rd_rate
            if(max_rate_r<car_vaule_r) {max_rate_r = car_vaule_r}
        }
        onWr_rateChanged:{
            car_vaule_w = pcie_sp.wr_rate
            if(max_rate_w<car_vaule_w) {max_rate_w = car_vaule_w}
        }
    }


    ColumnLayout {
        anchors.fill: parent
        anchors.rightMargin: 60
        anchors.topMargin: 30
        anchors.bottomMargin: 30
        anchors.leftMargin: 60
        spacing: 10

        Row {
            spacing: 50
            Column{
                spacing: 50
                Row{
                    YaheiText {
                        text: "读写测速"
                        font.pixelSize: fontsize
                        Layout.preferredWidth: leftWidth
                        Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                    }
                    QianQuestionMark {
                        hint: "目前最高只测到1000MB/s"
                    }
                }
            }
            ColumnLayout {
                spacing: 10
                RowLayout {
                    spacing: 20

                }

                Row {
                    spacing: 80
                    Column{
                        spacing: 5
                        CarSample{ //暂时不能改宽高,修改的是边框的大小
                            property int r1: 0+(255-0)*(car_vaule_r)/1000
                            property int g1: 255+(0-255)*(car_vaule_r)/1000
                            property int b1: 128
                            property color car_color1: car_1.rgb(r1, g1, b1)
                            id:car_1
                            st_width: 300
                            st_height: 250
                            car_vaule: car_vaule_r
                            car_color:car_color1
                        }
                        Switch {
                          id: pcie_sw_rd
                          checked: false
                          enabled: sw_r_en
                          x:100
                          font.pixelSize: fontsize - 2
                          indicator.height: 18
                          font.family: "Microsoft Yahei"
                          text: "读"
                          Material.accent: "#4785FF"
                          onClicked: {
                                pcie_sp.pcie_read_btnclk();
                          }
                        }

                    }


                    Column{
                        spacing: 5
                        CarSample{ //暂时不能改宽高,修改的是边框的大小
                            property int r1: 0+(255-0)*(car_vaule_w)/1000
                            property int g1: 255+(0-255)*(car_vaule_w)/1000
                            property int b1: 128
                            property color car_color2: car_2.rgb(r1, g1, b1)
                            id:car_2
                            st_width: 300
                            st_height: 250
                            car_vaule: car_vaule_w
                            car_color:car_color2
                        }
                        Switch {
                          id: pcie_sw_wr
                          enabled: sw_w_en
                          x:110
                          font.pixelSize: fontsize - 2
                          indicator.height: 18
                          font.family: "Microsoft Yahei"
                          text: "写"
                          Material.accent: "#EC3315"
                          onClicked: {
                                pcie_sp.pcie_write_btnclk();
                          }
                        }
                    }



                }

            }
        }

        Rectangle {
            height: 1
            color: tingeOpacityColor
            Layout.fillWidth: true
            Layout.topMargin: 12
        }



        Rectangle { //实现分隔线的效果
            height: 1
            color: tingeOpacityColor
            Layout.fillWidth: true
        }
        RowLayout {
            YaheiText {
                text: "信息一览"
                font.pixelSize: fontsize
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 12
            }
        }
        Column {
            spacing: 10
        }
        RowLayout{
            YaheiText {
                text: "连接信息： "
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }
            YaheiText {
                text: line_message
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }
        }
        RowLayout{
            YaheiText {
                text: "最高读速度： "
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }
            YaheiText {
                text: max_rate_r.toString()+"MB/s"
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }
            YaheiText {
                text: "最高写速度： "
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }
            YaheiText {
                text: max_rate_w.toString()+"MB/s"
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }
        }

        RowLayout{
            YaheiText {
                text: "设备路径： "
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }
            YaheiText {
                text: dev_path
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }

        }

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

    }

    Connections{  //这里专门对C++端发送过来的信号进行处理（带参数的。。）
        target: pcie //对象是pcie_sp
        function onTextMessage(str) {
            line_message = str
        }
        function onXdmaInfo(str) {
            dev_path = str
        }
    }


}
