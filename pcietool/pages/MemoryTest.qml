﻿import QtQuick
import QtQuick.Controls
import Qt5Compat.GraphicalEffects
import QtQuick.Layouts
import "qrc:/common"
import "qrc:/common/qmlMyBetterBase"

import PcieObj 1.0
import PcieMemObj

Item {

//    Rectangle{
//        anchors.fill: parent
////        color: "white"
//    }

    property int leftWidth: 182
    property int fontsize: 19
    property string infoText: "---------------------------------------------------"

    property bool btn_enb_w: pcieIsOK
    property bool btn_enb_r: false



    PcieMemory{
        id: pcie_mem

    }

    ColumnLayout {
        anchors.fill: parent
        anchors.rightMargin: 60
        anchors.topMargin: 30
        anchors.bottomMargin: 30
        anchors.leftMargin: 60
        spacing: 10



        YaheiText {
            text: "读出数据"
            font.pixelSize: fontsize
            Layout.preferredWidth: leftWidth
            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
        }
        EditSample{
            id: edit_r
            rectWidth: 800
            rectHeight: 250
            fontsize: 15
        }


        Rectangle {
            height: 1
            color: tingeOpacityColor
            Layout.fillWidth: true
            Layout.topMargin: 12
        }

        Rectangle { //实现分隔线的效果
            height: 1
            color: tingeOpacityColor
            Layout.fillWidth: true
        }

        YaheiText {
            text: "写入数据"
            font.pixelSize: fontsize
            Layout.preferredWidth: leftWidth
            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
        }
        EditSample{
            id: edit_w
            rectWidth: 800
            rectHeight: 150
            fontsize: 15
        }

        RowLayout{
            spacing: 200
            BaseButton {
                text: "读取"
                enabled: btn_enb_r
                Layout.preferredHeight: 28
                Layout.preferredWidth: 78
                font.pixelSize:  14
                backRadius: 4
                bckcolor: "#4785FF"
                onClicked: {
                    var str_r = edit_w.getEditText()
                    edit_r.editAppend(str_r)
                }
            }
            BaseButton {
                text: "写入"
                enabled: btn_enb_w
                Layout.preferredHeight: 28
                Layout.preferredWidth: 78
                font.pixelSize:  14
                backRadius: 4
                bckcolor: "#4785FF"
                onClicked: {
                    if(edit_w.getEditLength() > 1024)
                    {
                        console.log("no")
                        message("info", "限定一次最多写入1024个字节")
                    }
                    else {
                        var str = edit_w.getEditText()
                        if(pcie_mem.mem_send(str)){
                            if(pcie_mem.mem_read()){
                                message("error","读取失败")
                            }
                        }
                        else {message("info","你还没写入任何要发送的值！")}
                    }
                }
            }
            BaseButton {
                text: "清空全部"
                Layout.preferredHeight: 28
                Layout.preferredWidth: 78
                font.pixelSize:  14
                backRadius: 4
                bckcolor: "#4785FF"
                onClicked: {
                    edit_r.editClear()
                    edit_w.editClear()
                }
            }
        }

        RowLayout{
            YaheiText {
                text: "info： "
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }
            YaheiText {
                text: infoText
                font.pixelSize: 12
                Layout.preferredWidth: leftWidth
                Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                Layout.topMargin: 10
            }
        }

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

    }


    Connections{
        target: pcie_mem
        function onMemMessage(str){
            infoText = str
        }
        function onMemRcvData(str){
            edit_r.editAppend(str)
//            console.log("111")
        }
    }

}
