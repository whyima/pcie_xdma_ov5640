﻿#ifndef PCIE_DEV_H
#define PCIE_DEV_H

#ifdef __cplusplus
extern "C" {
#endif

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <strsafe.h>
#include <stdint.h>
#include <tchar.h>
#include <Windows.h>
#include <SetupAPI.h>
#include <INITGUID.H>
#include <WinIoCtl.h>


#include "xdma_public.h"

int pcie_init();
int pcie_open();
void pcie_close();
//测速
unsigned int h2c_transfer_rate(unsigned long size);
unsigned int c2h_transfer_rate(unsigned long size);
//读写测试
long h2c_transfer(long offset, unsigned long size, char* snd_content);
long c2h_transfer(long offset, unsigned long size, char* rcv_content);
//用户逻辑
int write_user(long offset, uint32_t value);
int read_user(long offset, uint32_t* value);

//AXI_GPIO中断初始化
int open_intr_dev();
void close_intr_dev();
void intr_enable();
void intr_clear();
int wait_event0();
//
void c2h_transfer_img(unsigned long size,PBYTE *buffer_img);



extern TCHAR * device_name_glb;
extern TCHAR * base_path_glb;


#ifdef __cplusplus
}
#endif

#endif // PCIE_DEV_H
