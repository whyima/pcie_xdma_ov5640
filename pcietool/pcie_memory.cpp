﻿#include "pcie_memory.h"
#include "pcie_dev.h"
#include "pcie_state.h"

pcie_memory::pcie_memory(QObject *parent)
    : QObject{parent}
{

}




int pcie_memory::mem_send(QString str)
{
    if(pcie_state::pcieIsOk){
        QByteArray snd_byteArray = str.toUtf8();
        if(*snd_byteArray.data()) //返回一个常量字符指针，指向QByteArray对象内部存储数据的首地址。
            data_size = h2c_transfer(0,snd_byteArray.size(),snd_byteArray.data());
        else
            return 0;
        qDebug("此次传输 %d 个字节,str = %s",data_size,str.toStdString().data());
        emit memMessage("此次传输了"+QString::number(data_size)+"个字节");
        return 1;
    }
    else{
        qDebug("请先初始化pcie设备");
        return 0;
    }
}

int pcie_memory::mem_read()
{
    QByteArray rcv_byteArray;
    rcv_byteArray.resize(data_size);
    c2h_transfer(0, data_size, rcv_byteArray.data());
    QString tmp = rcv_byteArray.data();
    emit memRcvData(tmp);//发送接收到的数据到qml端
    return 0;
}



