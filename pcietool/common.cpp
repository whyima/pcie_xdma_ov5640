﻿#include "common.h"

QString WcharToChar(const TCHAR *wp, size_t codePage)
{
    QString str;
    int len = WideCharToMultiByte(codePage, 0, wp, wcslen(wp), NULL, 0, NULL, NULL);
    char *p = new char[len + 1];
    memset(p, 0, len + 1);
    WideCharToMultiByte(codePage, 0, wp, wcslen(wp), p, len, NULL, NULL);
    p[len] = '\0';
    str = QString(p);
    delete []p;
    p = NULL;
    return str;
}
