﻿#include "vediorect.h"
#include "pcie_dev.h"
#include <QDebug>

#define IMG_V 600
#define IMG_H 800
#define IMG_V_1 1080 / 2 //因为FPGA底层是隔行读取，所以这里只剩一半行数
#define IMG_H_1 1920
#define RGB888_BWIDTH 4 //内存中一个像素用了32bit来存储，因此给其4
#define EXT_MEM_SIEZ 200000 //因为两段内存不是连到一起的，中间空了一段，先给加上，防溢出

typedef struct {
    PBYTE buffer_img;
    DWORD buf_img_size;
    HANDLE imgHand;  //用于存放文件句柄
} image;

image img;
VedioRect::VedioRect()
{
    m_rect = QRectF(); //构造一个null矩形
    img.buf_img_size = (IMG_H * IMG_V * RGB888_BWIDTH) + (IMG_H_1 * IMG_V_1 * RGB888_BWIDTH) + EXT_MEM_SIEZ; // 24位真彩,存了两张图片
    img.buffer_img = (PBYTE)_aligned_malloc(img.buf_img_size, 1024); //512 | 1024


    QString file_path = "E:\\Workspace\\Qt\\video_demo\\video_demo\\veido_demo\\zhen_800_600_x2.bin";
    img.imgHand = CreateFile(file_path.toStdWString().data(), GENERIC_READ,
                             0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if(img.imgHand==INVALID_HANDLE_VALUE)
    {
        qDebug("CreateFile error.\n");
        return ;
    }
    DWORD num_bytes_read = 0;
    if (INVALID_SET_FILE_POINTER == SetFilePointer(img.imgHand, 0, NULL, FILE_BEGIN)) {
        qDebug("Error setting file pointer");
        return ;
    }


    if (!ReadFile(img.imgHand, (LPVOID)img.buffer_img, img.buf_img_size, (LPDWORD)&num_bytes_read, NULL)) {
        qDebug("ReadFile to device failed");
        return;
    }

    //定时器
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, [=](){
        pciRefresh();
    });

}

VedioRect::~VedioRect(){
    delete timer;
}


void VedioRect::btnHander()
{
    if (!timer->isActive())
        timer->start(32);   //60+hz
    else
        timer->stop();
}

void VedioRect::pciRefresh()
{

    c2h_transfer_img(img.buf_img_size,&img.buffer_img);
    update();
}

void VedioRect::paint(QPainter *painter)
{
    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->drawRect(m_rect);

    //参考 https://blog.csdn.net/houxian1103/article/details/130497033
    // 利用QImage对像素进行修改
    QImage image(IMG_H+500, IMG_V, QImage::Format_RGB888);
    //将图片背景填充为白色
    image.fill(Qt::black);
    //改变指定区域的像素点的值
    uint8_t r;
    uint8_t g;
    uint8_t b;
    for(int i=0; i<IMG_V; i+=1)
    {
        for(int j=0; j<IMG_H; j = j+1)
        {
//            if(ix%2 == 0){
//                ix = 0;
                r = img.buffer_img[i*IMG_H*RGB888_BWIDTH+j*RGB888_BWIDTH+2];
                g = img.buffer_img[i*IMG_H*RGB888_BWIDTH+j*RGB888_BWIDTH+1];
                b = img.buffer_img[i*IMG_H*RGB888_BWIDTH+j*RGB888_BWIDTH];
//            }
//            else{
//                r = img.buffer_img[i*IMG_H*RGB888_BWIDTH+j*RGB888_BWIDTH + IMG_H*IMG_V*3];
//                g = img.buffer_img[i*IMG_H*RGB888_BWIDTH+j*RGB888_BWIDTH+1 + IMG_H*IMG_V*3];
//                b = img.buffer_img[i*IMG_H*RGB888_BWIDTH+j*RGB888_BWIDTH+2 + IMG_H*IMG_V*3];
//            }

            //            uint8_t r = 255;
            //            uint8_t g = 0;
            //            uint8_t b = 0;
            QRgb value = qRgb(r, g, b);
            image.setPixel(j, i, value);
        }
    }

    for(int i=0; i<IMG_V_1; i+=1)
    {
        for(int j=0; j<IMG_H_1; j = j+1)
        {
            //2097152是第二图像的起始地址
            r = img.buffer_img[2097152+i*IMG_H_1*RGB888_BWIDTH+j*RGB888_BWIDTH+2];
            g = img.buffer_img[2097152+i*IMG_H_1*RGB888_BWIDTH+j*RGB888_BWIDTH+1];
            b = img.buffer_img[2097152+i*IMG_H_1*RGB888_BWIDTH+j*RGB888_BWIDTH];

            QRgb value = qRgb(r, g, b);
//            if(i<600&&j<480)
            image.setPixel(800+20+j/4, i/2, value); //缩小四倍，因为底层行已经舍弃了一半行，这里行只需再缩小一半就行
        }
    }

    //将图片绘制到窗口中
    painter->drawImage(QPoint(0, 0), image);
    if (img.imgHand) CloseHandle(img.imgHand);

}
