﻿import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import Qt.labs.settings
import Qt.Singleton 1.0 //这个是从main.cpp里定义来的,也许是方便调用？
import "qrc:/common/qmlMyHints"

import PcieObj 1.0
//import QtQuick.Window
//import Qt.Window
//import Qt5Compat.GraphicalEffects
//import QtQuick.Controls.Material 2.12
//import Qt.labs.platform 1.1

Window {
    id: rootWindow
    property string titleStr: "Pcie Helper" //标题

    property var areas: [                   //var 可以根据值的类型自动推断出变量的类型
        Qt.rect(0, 0, 99999, 42)
    ]

    property color accentColor: SkinSingleton.skins[setting.skinIndex].accentColor              // 皮肤深色
    property color tingeColor: skin.light ? "#555" : "#b1b1b1"                      // 皮肤淡色(白色皮肤为浅黑   黑色皮肤为灰色)
    property var skin: SkinSingleton.skins[setting.skinIndex]

    property color accentContrlColor: SkinSingleton.skins[setting.skinIndex].accentContrlColor  // 皮肤控件深色
    property color tingeOpacityColor: skin.light ? "#11000000" : "#11FFFFFF"        // 皮肤谈透明色
    property color mainColor: !skin.gradSupport && !skin.imageSupport ? skin.mainColor :
                              !skin.light  ? Qt.rgba(0,0,0, skin.gradMainOpacity - setting.skinOpacity * 0.48)    : Qt.rgba(1,1,1, skin.gradMainOpacity + setting.skinOpacity * 0.28)
    property color accentOpacityColor: Qt.rgba(accentColor.r, accentColor.g, accentColor.b, 0.5)        // 皮肤控件深色透明 (一般用在滚动条)
    property color accentOpacityHoverColor: Qt.rgba(accentColor.r, accentColor.g, accentColor.b, 0.45)  // 皮肤控件深色透明 淡色 (一般用在滚动条鼠标徘徊)

    //Material 是一种控件风格属性，
    //要使用需先在.pro 加入QT += quickcontrols2 然后在cpp 添加QQuickStyle::setStyle("Material"); （头文件 #include <QQuickStyle>）
    Material.accent: accentColor
    Material.theme:  skin.light ? Material.Light : Material.Dark
    Material.foreground: tingeColor
    width: 1200
    height: 800
    visible: true
    title: titleStr
    color: "transparent"
    flags: Qt.FramelessWindowHint | Qt.Window   //这个用于去掉框的标题栏 后面这个是让任务栏图标不被隐藏了
//    resizable: true
//    moveArea:  areas
    minimumWidth: 1150
    minimumHeight: 780

    function closeFunc() { // 用于WindowTitle 的关闭窗口功能
        close();
    }

    WindowEntry { //窗体入口，在这里面开始布局各个窗口
        anchors.fill: parent
        anchors.margins: rootWindow.maximized ? 0 : 8
        radius: rootWindow.maximized ? 0 : 4
    }


    Settings {
        id: setting
        property int skinIndex: 1
        property real skinOpacity: 1
        property string skinCustomFile: ""
        fileName: "app.ini"
    }


//////////////////////////////////////////PCIe状态专用/////////////////////////////////////////////////
    property bool pcieIsOK: false
    property bool workBusy: false //操作忙指示变量
    Pcie{
        id: pcie

        onPcieIsOkChanged:{
            pcieIsOK = pcie.pcieIsOk
        }
        onBusyChanged:{
            workBusy = pcie.busy
        }
    }
//////////////////////////////////////////后面的page可以直接调用/////////////////////////////////////////////////
    Message{ //Message是顶部的提示框，这里设置Message的基本属性
        id:messageTip
        z: 1
        parent: Overlay.overlay
    }
    function message(type, message) { //打开提示框，对输入做一些基本的过滤
        if(type!=='success'&&type!=='error'&&type!=='info'){
            return false
        }
        messageTip.open(type, message)
    }

    Connections{  //这里专门对C++端发送过来的信号进行处理（带参数的。。）
        target: pcie //对象是pcie_sp
        function onPcie_init_info(info,str){  //用str来接收，用同名的message会和下面调用的函数冲突，离谱。。
            message(info, str) //message 可以检索到main中
//            console.log("hello")
        }
    }
///////////////////////////////////////////下面实现窗体的拖动及拉伸////////////////////////////////////////////////
    property int bw: 3
    MouseArea {
        anchors.fill: parent
        z: parent.z-1
        hoverEnabled: true
        cursorShape: {
            const p = Qt.point(mouseX, mouseY);
            const b = bw + 10; // Increase the corner size slightly
            if (p.x < b && p.y < b) return Qt.SizeFDiagCursor;
            if (p.x >= rootWindow.width - b && p.y >= rootWindow.height - b) return Qt.SizeFDiagCursor;
            if (p.x >= rootWindow.width - b && p.y < b) return Qt.SizeBDiagCursor;
            if (p.x < b && p.y >= rootWindow.height - b) return Qt.SizeBDiagCursor;
            if (p.x < b || p.x >= rootWindow.width - b) return Qt.SizeHorCursor;
            if (p.y < b || p.y >= rootWindow.height - b) return Qt.SizeVerCursor;
        }
        acceptedButtons: Qt.NoButton // don't handle actual events
    }
    DragHandler {
        id: resizeHandler
        grabPermissions: TapHandler.TakeOverForbidden
        target: null
        onActiveChanged: if (active) {
                             const p = resizeHandler.centroid.position;
                             const b = bw + 10; // Increase the corner size slightly
                             let e = 0;
                             if (p.x < b) { e |= Qt.LeftEdge }
                             if (p.x >= rootWindow.width - b) { e |= Qt.RightEdge }
                             if (p.y < b) { e |= Qt.TopEdge }
                             if (p.y >= rootWindow.height - b) { e |= Qt.BottomEdge }
                             rootWindow.startSystemResize(e);
                         }
    }
    Item {
        anchors.fill: parent
        z:parent.z - 1
        DragHandler {
            grabPermissions: TapHandler.CanTakeOverFromAnything
            onActiveChanged: if (active) { rootWindow.startSystemMove(); }
        }
    }
}

