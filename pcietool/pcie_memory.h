﻿#ifndef PCIE_MEMORY_H
#define PCIE_MEMORY_H

#include <QObject>
#include <QDebug>
#include <QString>

class pcie_memory : public QObject
{
    Q_OBJECT
public:
    explicit pcie_memory(QObject *parent = nullptr);

    QString rcv_text;
    long data_size;

    Q_INVOKABLE int mem_send(QString str);
    Q_INVOKABLE int mem_read();

signals:
    void memMessage(QString message);
    void memRcvData(QString message);

    void rcv_textChanged();
private:

};

#endif // PCIE_MEMORY_H
