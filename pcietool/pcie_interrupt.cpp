﻿#include "pcie_interrupt.h"
#include "pcie_dev.h"
#include "pcie_state.h"

uint32_t key_val;

pcie_interrupt::pcie_interrupt(QObject *parent)
    : QObject{parent}
{

    connect(eventthread,&eventThread::intrInfo,this,[=](){
        pcie_interrupt::intrHandler();
    });
    eventthread->start();
}

pcie_interrupt::~pcie_interrupt()
{

}

void pcie_interrupt::intrHandler()
{
//    static uint32_t key_val;
//    read_user(8,&key_val);
    qDebug("intr happen");
    uint32_t led_val;
    read_user(0,&led_val);
    switch (key_val) {
    case 0xe: //灯高电平点亮
        if((led_val&0x0001)==0x0000){
            write_user(0,0x0001);
        }
        else {
            write_user(0,0x0000);
        }
        break;
//    case 0xd: on_btnClrLed_2_clicked(); break;
//    case 0xb: on_btnClrLed_3_clicked(); break;
    default:;
    }
}

void eventThread::run() //线程来读取event0的中断
{
    while(1){
        if(pcie_state::pcieIsOk){
            if(wait_event0()==1 ){
                msleep(20);         //按键消抖
                read_user(8,&key_val);
                intr_clear();       //清外设中断
                if(key_val ^ 0xf){   //下降沿触发
                    qDebug("准备发送");
                    emit intrInfo();
                }
            }
        }
    }
}
