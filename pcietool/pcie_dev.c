﻿#ifdef __cplusplus
extern "C" {
#endif

#include "pcie_dev.h"
#pragma comment(lib, "setupapi.lib")

#define ONE_MB (1024UL * 1024UL)     //UL: 无符号长整型

/* helper struct to remember the Xdma device names */


typedef struct {
    TCHAR base_path[MAX_PATH + 1];  /* path to first found Xdma device */
    PBYTE buffer_c2h;   /* pointer to the allocated buffer card to host*/
    PBYTE buffer_h2c;   /* pointer to the allocated buffer host to card*/
    DWORD buf_c2h_size; /* size of the buffer c2h in bytes */
    DWORD buf_h2c_size; /* size of the buffer h2c in bytes */
    HANDLE c2h0;
    HANDLE h2c0;
    HANDLE user;
    HANDLE event0;
} xdma_device;
xdma_device xdma;
typedef struct {
    LARGE_INTEGER start;
    LARGE_INTEGER end;
    LARGE_INTEGER freq;
} Timer;

__inline static void timer_start(Timer *timer) {
    /*
     * 检索性能计数器的频率。 性能计数器的频率在系统启动时固定，并且在所有处理器中保持一致。 因此，只需在应用程序初始化时查询频率，并且可以缓存结果。
     * 应该就是查询系统频率然后存到Timer的freq属性中
    */
    QueryPerformanceFrequency(&timer->freq);
    //获取当前计数值，需要在获取频率之后，开始计时时获取一下，计时完毕再获取一下，两者只差就是计数的时间了
    QueryPerformanceCounter(&timer->start);
}

__inline static double timer_elapsed_sec(Timer *timer) {
    QueryPerformanceCounter(&timer->end);
    return ((timer->end.QuadPart - timer->start.QuadPart) / (double)timer->freq.QuadPart);
}

//查找XDMA设备
static DWORD find_devices(GUID guid, PTCHAR devpath, size_t len_devpath)
{

    HDEVINFO device_info = SetupDiGetClassDevs((LPGUID)&guid, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);//系统设备枚举
    if (device_info == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "GetDevices INVALID_HANDLE_VALUE\n");
        printf("error 1");
        exit(-1);
    }

    SP_DEVICE_INTERFACE_DATA device_interface;
    device_interface.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
//    CoCreateGuid(&device_interface.InterfaceClassGuid);
//    OLECHAR* guidString;
//    StringFromCLSID(&device_interface.InterfaceClassGuid, &guidString);
//    printf("cb_size; %lu\r\n %hu",device_interface.cbSize,guidString);

    // enumerate through devices
    DWORD index;
    for (index = 0; SetupDiEnumDeviceInterfaces(device_info, NULL, &guid, index, &device_interface); ++index) {

        // get required buffer size
        ULONG detailLength = 0;
        if (!SetupDiGetDeviceInterfaceDetail(device_info, &device_interface, NULL, 0, &detailLength, NULL) && GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
            fprintf(stderr, "SetupDiGetDeviceInterfaceDetail - get length failed\n");
            break;
        }

        // allocate space for device interface detail
        PSP_DEVICE_INTERFACE_DETAIL_DATA dev_detail = (PSP_DEVICE_INTERFACE_DETAIL_DATA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, detailLength);
        if (!dev_detail) {
            fprintf(stderr, "HeapAlloc failed\n");
            break;
        }
        dev_detail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

        // get device interface detail
        if (!SetupDiGetDeviceInterfaceDetail(device_info, &device_interface, dev_detail, detailLength, NULL, NULL)) {
            fprintf(stderr, "SetupDiGetDeviceInterfaceDetail - get detail failed\n");
            HeapFree(GetProcessHeap(), 0, dev_detail);
            break;
        }

        StringCchCopy(devpath, len_devpath, dev_detail->DevicePath);
        HeapFree(GetProcessHeap(), 0, dev_detail);
    }

    SetupDiDestroyDeviceInfoList(device_info);

    return index;
}

//初始化PCIe
int pcie_init()
{
    int status = 1;
//    printf("begin find");
    // get device path from GUID
    DWORD num_devices = find_devices(GUID_DEVINTERFACE_XDMA, &xdma.base_path[0], sizeof(xdma.base_path));
    printf("--Found %lu Xdma device%s.\n", num_devices, num_devices == 1 ? "" : "s");
    if (num_devices < 1){
//        exit(-1);
        return -1;
    }

    /* allocate buffer */
    xdma.buf_h2c_size = 12 * ONE_MB; // buffer host to card size
    xdma.buffer_h2c = (PBYTE)_aligned_malloc(xdma.buf_h2c_size, 4096);
    xdma.buf_c2h_size = 12 * ONE_MB; // buffer card to host size
    xdma.buffer_c2h = (PBYTE)_aligned_malloc(xdma.buf_c2h_size, 1024); //512 | 1024
    if (!xdma.buffer_c2h || !xdma.buffer_h2c) {
        status = -1;
        if(xdma.buffer_c2h) _aligned_free(xdma.buffer_c2h);
        if(xdma.buffer_h2c) _aligned_free(xdma.buffer_h2c);
        fprintf(stderr, "Error allocate buffer , error code: %lu@\n", GetLastError());
    }

    base_path_glb = xdma.base_path;
//    return status;
    return num_devices; //返回找到的Xdma设备数量
}

HANDLE open_device(TCHAR * device_name, TCHAR * base_path, DWORD dwDesiredAccess)
{
    HANDLE device;
    TCHAR device_path[MAX_PATH + 1];

    _tcscpy_s(device_path, sizeof device_path, base_path);
    _tcscat_s(device_path, (sizeof device_path + sizeof device_name), device_name);

//    printf("xxxxxx=%ls",base_path);
    /* open XDMA device */
    //返回值是指定文件、设备、命名管道或邮件槽的打开句柄。
    device = CreateFile(device_path, dwDesiredAccess,
                           0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);



    return device;
}

//打开PCIe设备
int pcie_open()
{
    int status = 1;

    /* open XDMA Host-to-Card 0 device */
    //理解：在这里获取文件句柄，open_device 就是创建了一个文件
    xdma.h2c0 = open_device(XDMA_FILE_H2C_0, xdma.base_path, GENERIC_READ | GENERIC_WRITE);
    if (xdma.h2c0 == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "Error opening device, win64 error code: %lu\n", GetLastError());
        _tprintf(L"Error opening device, xdma.base_path: %s@\n",xdma.base_path);
        status = -1;
        goto cleanup_handles;
    }


    /* open XDMA Card-to-Host 0 device */
    xdma.c2h0 = open_device(XDMA_FILE_C2H_0, xdma.base_path, GENERIC_READ | GENERIC_WRITE);
    if (xdma.c2h0 == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "Error opening device, win64 error code: %lu@\n", GetLastError());
        status = -1;
        goto cleanup_handles;
    }

    /* open user device */
    xdma.user = open_device(XDMA_FILE_USER, xdma.base_path, GENERIC_READ | GENERIC_WRITE);
    if (xdma.user == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "Error opening device, win64 error code: %lu@\n", GetLastError());
        status = -1;
        goto cleanup_handles;
    }

    return status;

cleanup_handles:
    if (xdma.c2h0) CloseHandle(xdma.c2h0);
    if (xdma.h2c0) CloseHandle(xdma.h2c0);
    if (xdma.user) CloseHandle(xdma.user);
    return status;
}

//关闭PCIe
void pcie_close()
{
    if (xdma.c2h0) CloseHandle(xdma.c2h0);
    if (xdma.h2c0) CloseHandle(xdma.h2c0);
    if (xdma.user) CloseHandle(xdma.user);

    if(xdma.buffer_c2h) _aligned_free(xdma.buffer_c2h);
    if(xdma.buffer_h2c) _aligned_free(xdma.buffer_h2c);
}



// ======================================功能性接口================================= //
// test transfer data from Host PC to FPGA Card speed rate
unsigned int h2c_transfer_rate(DWORD size)
{
    Timer timer;
    double time_sec;
    double transfer_rate = 0;
    DWORD num_bytes_write = 0;
    DWORD total_write = 0;


    if (INVALID_SET_FILE_POINTER == SetFilePointer(xdma.h2c0, 0, NULL, FILE_BEGIN)) { //返回除INVALID_SET_FILE_POINTER 外的值，说明设置成功
        fprintf(stderr, "Error setting file pointer, error code: %ld\n", GetLastError());
        return -1;
    }

    timer_start(&timer);

    /*
    @HANDLE hFile:  文件或 I/O 设备的句柄
    @LPCVOID lpBuffer:  指向缓冲区的指针，该缓冲区包含要写入文件或设备的数据。
    @DWORD nNumberOfBytesToWrite: 要写入文件或设备的字节数。
    @LPDWORD lpNumberOfBytesWritten: 指向变量的指针，***该变量接收使用同步 hFile 参数时写入的字节数***。 WriteFile 在执行任何工作或错误检查之前将此值设置为零。
                                     如果这是异步操作，请对此参数使用 NULL ，以避免潜在的错误结果。
    @LPOVERLAPPED lpOverlapped: 如果使用 FILE_FLAG_OVERLAPPED 打开 hFile 参数，则需要指向 OVERLAPPED 结构的指针，否则此参数可以为 NULL。
    */
    while (total_write < size) {
        if (!WriteFile(xdma.h2c0, (LPVOID)xdma.buffer_h2c, size, (LPDWORD)&num_bytes_write, NULL)) {
            fprintf(stderr, "WriteFile to device failed: %ld\n", GetLastError());
        }
        total_write += num_bytes_write;
    }

    time_sec = timer_elapsed_sec(&timer);
    transfer_rate = total_write/time_sec/ONE_MB;

    return (unsigned int)transfer_rate;
}

//test transfer data from FPGA Card to Host PC speed rate
unsigned int c2h_transfer_rate(DWORD size)
{
    Timer timer;
    double time_sec = 0.0;
    double transfer_rate = 0;
    DWORD num_bytes_read = 0;
    DWORD total_read = 0;


    if (INVALID_SET_FILE_POINTER == SetFilePointer(xdma.c2h0, 0, NULL, FILE_BEGIN)) {
        fprintf(stderr, "Error setting file pointer, error code: %ld\n", GetLastError());
        return -1;
    }

    timer_start(&timer);


    while (total_read < size) {
        if (!ReadFile(xdma.c2h0, (LPVOID)xdma.buffer_c2h, size, (LPDWORD)&num_bytes_read, NULL)) {
            fprintf(stderr, "ReadFile from device failed: %ld\n", GetLastError());
            printf("error");
        }
        total_read += num_bytes_read;

    }

    time_sec = timer_elapsed_sec(&timer);
//    printf("size = %lu,,time_sec = %f,,total_read=%lu",size,time_sec,total_read);
    transfer_rate  = total_read/time_sec/ONE_MB;

    return (unsigned int)transfer_rate ;
}

//============================================================================================//
static long write_device(HANDLE device, long offset, unsigned long size, char* snd_content)
{
    DWORD count = 0;

    if (INVALID_SET_FILE_POINTER == SetFilePointer(device, offset, NULL, FILE_BEGIN)) {
        fprintf(stderr, "Error setting file pointer, error code: %ld\n", GetLastError());
        return -1;
    }

    // write to device from allocated buffer
    if (!WriteFile(device, snd_content, size, &count, NULL)) {
        fprintf(stderr, "WriteFile to device failed with Win64 error code: %ld\n",
                GetLastError());
        return -1;
    }

    return count;
}

static long read_device(HANDLE device, long offset, unsigned long size, char * rcv_content)
{
    DWORD count=0;

    if (INVALID_SET_FILE_POINTER == SetFilePointer(device, offset, NULL, FILE_BEGIN)) {
        fprintf(stderr, "Error setting file pointer, error code: %ld\n", GetLastError());
        return -1;
    }

    // read from device into allocated buffer
    if (!ReadFile(device, rcv_content, size, &count, NULL)) {
        fprintf(stderr, "ReadFile from device failed with Win64 error code: %ld\n",
                GetLastError());
        return -1;
    }

    return count;
}

//transfer data from Host PC to FPGA Card
long h2c_transfer(long offset, unsigned long size, char* snd_content)
{
    return write_device(xdma.h2c0, offset, size, snd_content);
}

//transfer data from FPGA Card to Host PC
long c2h_transfer(long offset, unsigned long size, char* rcv_content)
{
    if(size < 5){
        read_device(xdma.c2h0, offset, 10, xdma.buffer_c2h);
        memcpy_s(rcv_content, sizeof (rcv_content), xdma.buffer_c2h, size);
        return size;
    }
    else
        return read_device(xdma.c2h0, offset, size, rcv_content);
}


//写axi lite对应的GPIO外设
int write_user(long offset, uint32_t value)
{
    return write_device(xdma.user, offset, 4, (char *)&value);
}

//读axi lite对应的GPIO外设值
int read_user(long offset, uint32_t* value)
{
    return read_device(xdma.user, offset, 4, (char *)value);
}

//打开中断设备
int open_intr_dev()
{
    /* open interrupt device event */
    xdma.event0 = open_device(XDMA_FILE_EVENT_0, xdma.base_path, GENERIC_READ);
    if (xdma.user == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "Error opening device, win64 error code: %lu@\n", GetLastError());
        return -1;
    }

    return 1;
}
//关闭PCIe 中断设备
void close_intr_dev()
{
    if (xdma.event0) CloseHandle(xdma.event0);
}

//使能AXI GPIO IP中断
void intr_enable()
{
    uint32_t val=0;
    read_device(xdma.user, 0x128, 4, (char *)&val); //偏移地址0x128寄存器用于IPIER，控制每个GPIO通道的中断使能与否，
    val |= 0x2;
    write_device(xdma.user, 0x128, 4, (char *)&val); // 到这里为止，我们使能了GPIO通道2的中断
    val = 0x80000000;
    // ox11c 用于GIER ，即全局中断的使能，其第32个bit控制全局中断使能与否，其他位不可读写，所以不需要read_device管其他位
    write_device(xdma.user, 0x11c, 4, (char *)&val);
}

//清除AXI GPIO IP中断
void intr_clear()
{
    uint32_t val=0;
    read_device(xdma.user, 0x120, 4, (char *)&val);
    val &= 0x2;
    write_device(xdma.user, 0x120, 4, (char *)&val);
}
int wait_event0()
{
    uint8_t val=0;
    read_device(xdma.event0, 0, sizeof(uint8_t),(char *)&val);
    return val;
}

//
//transfer data from FPGA Card to Host PC
void c2h_transfer_img(unsigned long size, PBYTE * buffer_img)
{
//    PBYTE buffer_img = NULL;
    DWORD num_bytes_read = 0;
    if (INVALID_SET_FILE_POINTER == SetFilePointer(xdma.c2h0, 0, NULL, FILE_BEGIN)) {
       // qDebug("Error setting file pointer");
        return ;
    }


    if (!ReadFile(xdma.c2h0, (LPVOID)*buffer_img, size, (LPDWORD)&num_bytes_read, NULL)) {
        //qDebug("ReadFile to device failed");
        return ;
    }
//    return *buffer_img;
}

#ifdef __cplusplus
}
#endif
