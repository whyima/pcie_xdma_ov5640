﻿#include "pcie_state.h"
#include "pcie_dev.h"
#include "common.h"
#include <QDebug>
#include <iostream>

TCHAR * base_path_glb;

pcie_state::pcie_state(QObject *parent)
    : QObject{parent}
{
    //    pcies = getInstance();
}

pcie_state::~pcie_state()
{
    if(pcieIsOk){
        setPcieIsOk(false);
        intr_clear();
        close_intr_dev();
        pcie_close();
    }
    else {

    }
}

pcie_state *pcie_state::getInstance()
{
    static pcie_state *obj =new pcie_state();
return obj;
}

void pcie_state::pcie_sinit()
{
    setBusy(true);
    //init PCIe
    int num_devices = pcie_init();
    if(num_devices<0)
    {
        emit pcie_init_info("info","未能搜寻到Xdma设备");
        std::cerr<<"ERROR! PCIe init error";
        return;
    }
    emit textMessage("Xdma 初始化成功");
    //open PCIe xdma device
    if(pcie_open()<0)
    {
        emit pcie_init_info("info","未能打开Xdma设备驱动");
        std::cerr<<"ERROR! PCIe open error";
        return;
    }
    QString num_devices_str =QString::number(num_devices);

    open_intr_dev(); //打开中断设备
    intr_enable();  //使能gpio中断


    emit textMessage("Xdma 设备打开成功,搜寻到的设备数；"+num_devices_str);
    emit pcie_init_info("success","Xdma设备打开成功");
    emit xdmaInfo(WcharToChar(base_path_glb,CP_UTF8));
    setPcieIsOk(true);
    setBusy(false);
}

void pcie_state::pcie_btnclose()
{
    setPcieIsOk(false);
    setBusy(true);
    intr_clear();
    close_intr_dev();
    pcie_close();
    setBusy(false);
    emit pcie_init_info("success","Xdma设备关闭");
}













//=====================================生成=========================================//
bool pcie_state::getPcieIsOk()  const
{
    return pcieIsOk;
}

void pcie_state::setPcieIsOk(bool newPcieIsOk)
{
    if (pcieIsOk == newPcieIsOk)
        return;
    pcieIsOk = newPcieIsOk;
    emit pcieIsOkChanged();
}
bool pcie_state::getBusy() const
{
    return busy;
}

void pcie_state::setBusy(bool newBusy)
{
    if (busy == newBusy)
        return;
    busy = newBusy;
//    qDebug("asd");
    emit busyChanged();
}
