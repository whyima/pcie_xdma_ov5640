﻿#ifndef VEDIORECT_H
#define VEDIORECT_H

#include <QObject>
#include <QObject>
#include <QQuickPaintedItem>
#include <QPainter>
#include <Windows.h>
#include <SetupAPI.h>
#include <QTimer.h>


class VedioRect : public QQuickPaintedItem
{
    Q_OBJECT
public:
    VedioRect();
    ~VedioRect();

    Q_INVOKABLE void btnHander();
    void pciRefresh();
    virtual void paint(QPainter *painter) override;

    long data_size;
    QTimer *timer;
private:
    QRectF  m_rect;

signals:

};


#endif // VEDIORECT_H
