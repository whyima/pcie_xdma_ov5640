//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
//Date        : Wed Dec  6 10:52:10 2023
//Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
//Command     : generate_target system_wrapper.bd
//Design      : system_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module system_wrapper
   (DDR3_addr,
    DDR3_ba,
    DDR3_cas_n,
    DDR3_ck_n,
    DDR3_ck_p,
    DDR3_cke,
    DDR3_cs_n,
    DDR3_dm,
    DDR3_dq,
    DDR3_dqs_n,
    DDR3_dqs_p,
    DDR3_odt,
    DDR3_ras_n,
    DDR3_reset_n,
    DDR3_we_n,
    key_tri_i,
    led_tri_o,
    lnk_up_led,
    ov5640_data,
    ov5640_href,
    ov5640_pclk,
    ov5640_pwdn,
    ov5640_rst_n,
    ov5640_vsync,
    pcie_mgt_rxn,
    pcie_mgt_rxp,
    pcie_mgt_txn,
    pcie_mgt_txp,
    pcie_ref_clk_n,
    pcie_ref_clk_p,
    pcie_rst_n,
    sccb_scl,
    sccb_sda,
    sys_clk,
    sys_rst_n);
  output [14:0]DDR3_addr;
  output [2:0]DDR3_ba;
  output DDR3_cas_n;
  output [0:0]DDR3_ck_n;
  output [0:0]DDR3_ck_p;
  output [0:0]DDR3_cke;
  output [0:0]DDR3_cs_n;
  output [3:0]DDR3_dm;
  inout [31:0]DDR3_dq;
  inout [3:0]DDR3_dqs_n;
  inout [3:0]DDR3_dqs_p;
  output [0:0]DDR3_odt;
  output DDR3_ras_n;
  output DDR3_reset_n;
  output DDR3_we_n;
  input [3:0]key_tri_i;
  output [2:0]led_tri_o;
  output lnk_up_led;
  input [7:0]ov5640_data;
  input ov5640_href;
  input ov5640_pclk;
  output ov5640_pwdn;
  output ov5640_rst_n;
  input ov5640_vsync;
  input [1:0]pcie_mgt_rxn;
  input [1:0]pcie_mgt_rxp;
  output [1:0]pcie_mgt_txn;
  output [1:0]pcie_mgt_txp;
  input [0:0]pcie_ref_clk_n;
  input [0:0]pcie_ref_clk_p;
  input pcie_rst_n;
  output sccb_scl;
  inout sccb_sda;
  input sys_clk;
  input sys_rst_n;

  wire [14:0]DDR3_addr;
  wire [2:0]DDR3_ba;
  wire DDR3_cas_n;
  wire [0:0]DDR3_ck_n;
  wire [0:0]DDR3_ck_p;
  wire [0:0]DDR3_cke;
  wire [0:0]DDR3_cs_n;
  wire [3:0]DDR3_dm;
  wire [31:0]DDR3_dq;
  wire [3:0]DDR3_dqs_n;
  wire [3:0]DDR3_dqs_p;
  wire [0:0]DDR3_odt;
  wire DDR3_ras_n;
  wire DDR3_reset_n;
  wire DDR3_we_n;
  wire [3:0]key_tri_i;
  wire [2:0]led_tri_o;
  wire lnk_up_led;
  wire [7:0]ov5640_data;
  wire ov5640_href;
  wire ov5640_pclk;
  wire ov5640_pwdn;
  wire ov5640_rst_n;
  wire ov5640_vsync;
  wire [1:0]pcie_mgt_rxn;
  wire [1:0]pcie_mgt_rxp;
  wire [1:0]pcie_mgt_txn;
  wire [1:0]pcie_mgt_txp;
  wire [0:0]pcie_ref_clk_n;
  wire [0:0]pcie_ref_clk_p;
  wire pcie_rst_n;
  wire sccb_scl;
  wire sccb_sda;
  wire sys_clk;
  wire sys_rst_n;

  system system_i
       (.DDR3_addr(DDR3_addr),
        .DDR3_ba(DDR3_ba),
        .DDR3_cas_n(DDR3_cas_n),
        .DDR3_ck_n(DDR3_ck_n),
        .DDR3_ck_p(DDR3_ck_p),
        .DDR3_cke(DDR3_cke),
        .DDR3_cs_n(DDR3_cs_n),
        .DDR3_dm(DDR3_dm),
        .DDR3_dq(DDR3_dq),
        .DDR3_dqs_n(DDR3_dqs_n),
        .DDR3_dqs_p(DDR3_dqs_p),
        .DDR3_odt(DDR3_odt),
        .DDR3_ras_n(DDR3_ras_n),
        .DDR3_reset_n(DDR3_reset_n),
        .DDR3_we_n(DDR3_we_n),
        .key_tri_i(key_tri_i),
        .led_tri_o(led_tri_o),
        .lnk_up_led(lnk_up_led),
        .ov5640_data(ov5640_data),
        .ov5640_href(ov5640_href),
        .ov5640_pclk(ov5640_pclk),
        .ov5640_pwdn(ov5640_pwdn),
        .ov5640_rst_n(ov5640_rst_n),
        .ov5640_vsync(ov5640_vsync),
        .pcie_mgt_rxn(pcie_mgt_rxn),
        .pcie_mgt_rxp(pcie_mgt_rxp),
        .pcie_mgt_txn(pcie_mgt_txn),
        .pcie_mgt_txp(pcie_mgt_txp),
        .pcie_ref_clk_n(pcie_ref_clk_n),
        .pcie_ref_clk_p(pcie_ref_clk_p),
        .pcie_rst_n(pcie_rst_n),
        .sccb_scl(sccb_scl),
        .sccb_sda(sccb_sda),
        .sys_clk(sys_clk),
        .sys_rst_n(sys_rst_n));
endmodule
