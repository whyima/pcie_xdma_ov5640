// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Sun Dec 24 16:01:40 2023
// Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               E:/Workspace/Vivado/2019.2/pcie_xdma_v1/vivado_prj/pcie_xdma.srcs/sources_1/bd/system/ip/system_ov5640_top_0_0/system_ov5640_top_0_0_sim_netlist.v
// Design      : system_ov5640_top_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "system_ov5640_top_0_0,ov5640_top,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "ov5640_top,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module system_ov5640_top_0_0
   (sys_clk,
    sys_rst_n,
    sys_init_done,
    ov5640_pclk,
    ov5640_href,
    ov5640_vsync,
    ov5640_data,
    ov5640_rst_n,
    ov5640_pwdn,
    sccb_scl,
    sccb_sda,
    cfg_done,
    ov5640_wr_en,
    ov5640_data_out,
    href_cnt,
    href_v_cnt);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 sys_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sys_clk, ASSOCIATED_RESET sys_rst_n, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_sys_clk, INSERT_VIP 0" *) input sys_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 sys_rst_n RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sys_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input sys_rst_n;
  input sys_init_done;
  input ov5640_pclk;
  input ov5640_href;
  input ov5640_vsync;
  input [7:0]ov5640_data;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 ov5640_rst_n RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ov5640_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) output ov5640_rst_n;
  output ov5640_pwdn;
  output sccb_scl;
  inout sccb_sda;
  output cfg_done;
  output ov5640_wr_en;
  output [31:0]ov5640_data_out;
  output [31:0]href_cnt;
  output [31:0]href_v_cnt;

  wire \<const0> ;
  wire \<const1> ;
  wire cfg_done;
  wire [31:0]href_cnt;
  wire [31:0]href_v_cnt;
  wire [7:0]ov5640_data;
  wire [23:3]\^ov5640_data_out ;
  wire ov5640_href;
  wire ov5640_pclk;
  wire ov5640_vsync;
  wire ov5640_wr_en;
  wire sccb_scl;
  wire sccb_sda;
  wire sys_clk;
  wire sys_init_done;
  wire sys_rst_n;

  assign ov5640_data_out[31] = \<const0> ;
  assign ov5640_data_out[30] = \<const0> ;
  assign ov5640_data_out[29] = \<const0> ;
  assign ov5640_data_out[28] = \<const0> ;
  assign ov5640_data_out[27] = \<const0> ;
  assign ov5640_data_out[26] = \<const0> ;
  assign ov5640_data_out[25] = \<const0> ;
  assign ov5640_data_out[24] = \<const0> ;
  assign ov5640_data_out[23:19] = \^ov5640_data_out [23:19];
  assign ov5640_data_out[18] = \<const0> ;
  assign ov5640_data_out[17] = \<const0> ;
  assign ov5640_data_out[16] = \<const0> ;
  assign ov5640_data_out[15:10] = \^ov5640_data_out [15:10];
  assign ov5640_data_out[9] = \<const0> ;
  assign ov5640_data_out[8] = \<const0> ;
  assign ov5640_data_out[7:3] = \^ov5640_data_out [7:3];
  assign ov5640_data_out[2] = \<const0> ;
  assign ov5640_data_out[1] = \<const0> ;
  assign ov5640_data_out[0] = \<const0> ;
  assign ov5640_pwdn = \<const0> ;
  assign ov5640_rst_n = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  system_ov5640_top_0_0_ov5640_top inst
       (.cfg_done_reg(cfg_done),
        .href_cnt(href_cnt),
        .href_v_cnt(href_v_cnt),
        .ov5640_data(ov5640_data),
        .ov5640_data_out({\^ov5640_data_out [23:19],\^ov5640_data_out [15:10],\^ov5640_data_out [7:3]}),
        .ov5640_href(ov5640_href),
        .ov5640_pclk(ov5640_pclk),
        .ov5640_vsync(ov5640_vsync),
        .ov5640_wr_en(ov5640_wr_en),
        .sccb_scl(sccb_scl),
        .sccb_sda(sccb_sda),
        .sys_clk(sys_clk),
        .sys_init_done(sys_init_done),
        .sys_rst_n(sys_rst_n));
endmodule

(* ORIG_REF_NAME = "iic_ctrl" *) 
module system_ov5640_top_0_0_iic_ctrl
   (E,
    i2c_clk,
    Q,
    \cnt_bit_reg[0]_0 ,
    \cnt_bit_reg[0]_1 ,
    \cnt_bit_reg[1]_0 ,
    \cnt_bit_reg[2]_0 ,
    sccb_scl,
    \cnt_bit_reg[0]_2 ,
    \cnt_bit_reg[1]_1 ,
    sccb_sda,
    \cnt_i2c_clk_reg[0]_0 ,
    sys_clk,
    i2c_sda_reg_reg_0,
    i2c_sda_reg_reg_1,
    i2c_sda_reg_reg_2,
    i2c_sda_reg_reg_i_40,
    cfg_start);
  output [0:0]E;
  output i2c_clk;
  output [2:0]Q;
  output \cnt_bit_reg[0]_0 ;
  output \cnt_bit_reg[0]_1 ;
  output \cnt_bit_reg[1]_0 ;
  output \cnt_bit_reg[2]_0 ;
  output sccb_scl;
  output \cnt_bit_reg[0]_2 ;
  output \cnt_bit_reg[1]_1 ;
  inout sccb_sda;
  input \cnt_i2c_clk_reg[0]_0 ;
  input sys_clk;
  input i2c_sda_reg_reg_0;
  input i2c_sda_reg_reg_1;
  input i2c_sda_reg_reg_2;
  input i2c_sda_reg_reg_i_40;
  input cfg_start;

  wire [0:0]E;
  wire \FSM_onehot_state[0]_i_1_n_0 ;
  wire \FSM_onehot_state[10]_i_1_n_0 ;
  wire \FSM_onehot_state[11]_i_1_n_0 ;
  wire \FSM_onehot_state[12]_i_1_n_0 ;
  wire \FSM_onehot_state[13]_i_1_n_0 ;
  wire \FSM_onehot_state[14]_i_1_n_0 ;
  wire \FSM_onehot_state[15]_i_1_n_0 ;
  wire \FSM_onehot_state[15]_i_2_n_0 ;
  wire \FSM_onehot_state[15]_i_3_n_0 ;
  wire \FSM_onehot_state[1]_i_1_n_0 ;
  wire \FSM_onehot_state[2]_i_1_n_0 ;
  wire \FSM_onehot_state[3]_i_1_n_0 ;
  wire \FSM_onehot_state[6]_i_1_n_0 ;
  wire \FSM_onehot_state_reg_n_0_[11] ;
  wire \FSM_onehot_state_reg_n_0_[14] ;
  wire \FSM_onehot_state_reg_n_0_[15] ;
  wire \FSM_onehot_state_reg_n_0_[3] ;
  wire \FSM_onehot_state_reg_n_0_[6] ;
  wire [2:0]Q;
  wire ack;
  wire ack__0;
  wire ack_reg_i_1_n_0;
  wire ack_reg_i_3_n_0;
  wire ack_reg_i_4_n_0;
  wire cfg_start;
  wire cnt_bit12_out__0;
  wire \cnt_bit[0]_i_1_n_0 ;
  wire \cnt_bit[1]_i_1_n_0 ;
  wire \cnt_bit[2]_i_1_n_0 ;
  wire \cnt_bit[2]_i_2_n_0 ;
  wire \cnt_bit_reg[0]_0 ;
  wire \cnt_bit_reg[0]_1 ;
  wire \cnt_bit_reg[0]_2 ;
  wire \cnt_bit_reg[1]_0 ;
  wire \cnt_bit_reg[1]_1 ;
  wire \cnt_bit_reg[2]_0 ;
  wire [7:0]cnt_clk;
  wire \cnt_clk[0]_i_2_n_0 ;
  wire \cnt_clk[4]_i_2_n_0 ;
  wire \cnt_clk[7]_i_2_n_0 ;
  wire [7:0]cnt_clk_0;
  wire [1:0]cnt_i2c_clk;
  wire \cnt_i2c_clk[0]_i_1_n_0 ;
  wire \cnt_i2c_clk[1]_i_1_n_0 ;
  wire cnt_i2c_clk_en;
  wire cnt_i2c_clk_en1;
  wire cnt_i2c_clk_en_i_1_n_0;
  wire \cnt_i2c_clk_reg[0]_0 ;
  wire i2c_clk;
  wire i2c_clk_i_1_n_0;
  wire i2c_clk_i_2_n_0;
  wire i2c_sda_reg;
  wire i2c_sda_reg__0;
  wire i2c_sda_reg_reg_0;
  wire i2c_sda_reg_reg_1;
  wire i2c_sda_reg_reg_2;
  wire i2c_sda_reg_reg_i_18_n_0;
  wire i2c_sda_reg_reg_i_1_n_0;
  wire i2c_sda_reg_reg_i_40;
  wire i2c_sda_reg_reg_i_6_n_0;
  wire i2c_sda_reg_reg_i_7_n_0;
  wire p_1_in;
  wire p_3_in;
  wire p_5_in;
  wire sccb_scl;
  wire sccb_scl_INST_0_i_1_n_0;
  wire sccb_scl_INST_0_i_2_n_0;
  wire sccb_scl_INST_0_i_3_n_0;
  wire sccb_sda;
  wire sccb_sda_INST_0_i_1_n_0;
  wire sys_clk;

  LUT6 #(
    .INIT(64'hFFFF8FFF88888888)) 
    \FSM_onehot_state[0]_i_1 
       (.I0(cnt_bit12_out__0),
        .I1(\FSM_onehot_state_reg_n_0_[6] ),
        .I2(cnt_i2c_clk[0]),
        .I3(cnt_i2c_clk[1]),
        .I4(ack),
        .I5(p_3_in),
        .O(\FSM_onehot_state[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h8FFF8888)) 
    \FSM_onehot_state[10]_i_1 
       (.I0(cfg_start),
        .I1(\FSM_onehot_state_reg_n_0_[11] ),
        .I2(cnt_i2c_clk[1]),
        .I3(cnt_i2c_clk[0]),
        .I4(p_1_in),
        .O(\FSM_onehot_state[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0100FFFF01000100)) 
    \FSM_onehot_state[11]_i_1 
       (.I0(\cnt_bit_reg[2]_0 ),
        .I1(\FSM_onehot_state[15]_i_3_n_0 ),
        .I2(\FSM_onehot_state[15]_i_2_n_0 ),
        .I3(\FSM_onehot_state_reg_n_0_[15] ),
        .I4(cfg_start),
        .I5(\FSM_onehot_state_reg_n_0_[11] ),
        .O(\FSM_onehot_state[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF700F700F700)) 
    \FSM_onehot_state[12]_i_1 
       (.I0(cnt_i2c_clk[0]),
        .I1(cnt_i2c_clk[1]),
        .I2(ack),
        .I3(p_5_in),
        .I4(cnt_bit12_out__0),
        .I5(Q[1]),
        .O(\FSM_onehot_state[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4444F44444444444)) 
    \FSM_onehot_state[13]_i_1 
       (.I0(cnt_bit12_out__0),
        .I1(Q[2]),
        .I2(cnt_i2c_clk[0]),
        .I3(cnt_i2c_clk[1]),
        .I4(ack),
        .I5(p_5_in),
        .O(\FSM_onehot_state[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF700F700F700)) 
    \FSM_onehot_state[14]_i_1 
       (.I0(cnt_i2c_clk[0]),
        .I1(cnt_i2c_clk[1]),
        .I2(ack),
        .I3(\FSM_onehot_state_reg_n_0_[14] ),
        .I4(cnt_bit12_out__0),
        .I5(Q[2]),
        .O(\FSM_onehot_state[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \FSM_onehot_state[14]_i_2 
       (.I0(cnt_i2c_clk[1]),
        .I1(cnt_i2c_clk[0]),
        .I2(\cnt_bit_reg[2]_0 ),
        .I3(\cnt_bit_reg[0]_1 ),
        .I4(\cnt_bit_reg[1]_0 ),
        .O(cnt_bit12_out__0));
  LUT6 #(
    .INIT(64'hF0E0F0FFF0E0F0E0)) 
    \FSM_onehot_state[15]_i_1 
       (.I0(\cnt_bit_reg[2]_0 ),
        .I1(\FSM_onehot_state[15]_i_2_n_0 ),
        .I2(\FSM_onehot_state_reg_n_0_[15] ),
        .I3(\FSM_onehot_state[15]_i_3_n_0 ),
        .I4(ack),
        .I5(\FSM_onehot_state_reg_n_0_[14] ),
        .O(\FSM_onehot_state[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \FSM_onehot_state[15]_i_2 
       (.I0(\cnt_bit_reg[1]_0 ),
        .I1(\cnt_bit_reg[0]_1 ),
        .O(\FSM_onehot_state[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \FSM_onehot_state[15]_i_3 
       (.I0(cnt_i2c_clk[1]),
        .I1(cnt_i2c_clk[0]),
        .O(\FSM_onehot_state[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4444F44444444444)) 
    \FSM_onehot_state[1]_i_1 
       (.I0(cnt_bit12_out__0),
        .I1(Q[0]),
        .I2(cnt_i2c_clk[0]),
        .I3(cnt_i2c_clk[1]),
        .I4(ack),
        .I5(p_3_in),
        .O(\FSM_onehot_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0800FFFF08000800)) 
    \FSM_onehot_state[2]_i_1 
       (.I0(cnt_i2c_clk[0]),
        .I1(cnt_i2c_clk[1]),
        .I2(ack),
        .I3(\FSM_onehot_state_reg_n_0_[3] ),
        .I4(cnt_bit12_out__0),
        .I5(Q[1]),
        .O(\FSM_onehot_state[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF700F700F700)) 
    \FSM_onehot_state[3]_i_1 
       (.I0(cnt_i2c_clk[0]),
        .I1(cnt_i2c_clk[1]),
        .I2(ack),
        .I3(\FSM_onehot_state_reg_n_0_[3] ),
        .I4(cnt_bit12_out__0),
        .I5(Q[0]),
        .O(\FSM_onehot_state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h80FF8080)) 
    \FSM_onehot_state[6]_i_1 
       (.I0(cnt_i2c_clk[1]),
        .I1(cnt_i2c_clk[0]),
        .I2(p_1_in),
        .I3(cnt_bit12_out__0),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .O(\FSM_onehot_state[6]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[0] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[0]_i_1_n_0 ),
        .Q(p_3_in));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[10] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[10]_i_1_n_0 ),
        .Q(p_1_in));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDPE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg[11] 
       (.C(i2c_clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[11]_i_1_n_0 ),
        .PRE(\cnt_i2c_clk_reg[0]_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[11] ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[12] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[12]_i_1_n_0 ),
        .Q(p_5_in));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[13] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[13]_i_1_n_0 ),
        .Q(Q[2]));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[14] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[14]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[14] ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[15] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[15]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[15] ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[1] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[1]_i_1_n_0 ),
        .Q(Q[0]));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[2] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[2]_i_1_n_0 ),
        .Q(Q[1]));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[3] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[3]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[3] ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[6] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\FSM_onehot_state[6]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[6] ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    ack_reg
       (.CLR(1'b0),
        .D(ack_reg_i_1_n_0),
        .G(ack__0),
        .GE(1'b1),
        .Q(ack));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    ack_reg_i_1
       (.I0(sccb_scl_INST_0_i_3_n_0),
        .I1(\FSM_onehot_state_reg_n_0_[6] ),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(p_1_in),
        .I5(ack_reg_i_3_n_0),
        .O(ack_reg_i_1_n_0));
  LUT5 #(
    .INIT(32'hFEFEFEFF)) 
    ack_reg_i_2
       (.I0(ack_reg_i_4_n_0),
        .I1(Q[2]),
        .I2(\FSM_onehot_state_reg_n_0_[11] ),
        .I3(cnt_i2c_clk[1]),
        .I4(cnt_i2c_clk[0]),
        .O(ack__0));
  LUT3 #(
    .INIT(8'hFE)) 
    ack_reg_i_3
       (.I0(Q[2]),
        .I1(\FSM_onehot_state_reg_n_0_[11] ),
        .I2(sccb_sda),
        .O(ack_reg_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    ack_reg_i_4
       (.I0(p_1_in),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\FSM_onehot_state_reg_n_0_[6] ),
        .I4(sccb_scl_INST_0_i_3_n_0),
        .O(ack_reg_i_4_n_0));
  LUT5 #(
    .INIT(32'h00F70008)) 
    \cnt_bit[0]_i_1 
       (.I0(cnt_i2c_clk[1]),
        .I1(cnt_i2c_clk[0]),
        .I2(\FSM_onehot_state_reg_n_0_[11] ),
        .I3(\cnt_bit[2]_i_2_n_0 ),
        .I4(\cnt_bit_reg[0]_1 ),
        .O(\cnt_bit[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000FF7F00000080)) 
    \cnt_bit[1]_i_1 
       (.I0(\cnt_bit_reg[0]_1 ),
        .I1(cnt_i2c_clk[1]),
        .I2(cnt_i2c_clk[0]),
        .I3(\FSM_onehot_state_reg_n_0_[11] ),
        .I4(\cnt_bit[2]_i_2_n_0 ),
        .I5(\cnt_bit_reg[1]_0 ),
        .O(\cnt_bit[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFBF00000040)) 
    \cnt_bit[2]_i_1 
       (.I0(\FSM_onehot_state[15]_i_2_n_0 ),
        .I1(cnt_i2c_clk[1]),
        .I2(cnt_i2c_clk[0]),
        .I3(\FSM_onehot_state_reg_n_0_[11] ),
        .I4(\cnt_bit[2]_i_2_n_0 ),
        .I5(\cnt_bit_reg[2]_0 ),
        .O(\cnt_bit[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cnt_bit[2]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[11] ),
        .I1(p_1_in),
        .I2(sccb_scl_INST_0_i_3_n_0),
        .I3(cnt_bit12_out__0),
        .O(\cnt_bit[2]_i_2_n_0 ));
  FDCE \cnt_bit_reg[0] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\cnt_bit[0]_i_1_n_0 ),
        .Q(\cnt_bit_reg[0]_1 ));
  FDCE \cnt_bit_reg[1] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\cnt_bit[1]_i_1_n_0 ),
        .Q(\cnt_bit_reg[1]_0 ));
  FDCE \cnt_bit_reg[2] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\cnt_bit[2]_i_1_n_0 ),
        .Q(\cnt_bit_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h00FF00F7)) 
    \cnt_clk[0]_i_1 
       (.I0(cnt_clk[4]),
        .I1(cnt_clk[3]),
        .I2(cnt_clk[1]),
        .I3(cnt_clk[0]),
        .I4(\cnt_clk[0]_i_2_n_0 ),
        .O(cnt_clk_0[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cnt_clk[0]_i_2 
       (.I0(cnt_clk[5]),
        .I1(cnt_clk[2]),
        .I2(cnt_clk[7]),
        .I3(cnt_clk[6]),
        .O(\cnt_clk[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt_clk[1]_i_1 
       (.I0(cnt_clk[0]),
        .I1(cnt_clk[1]),
        .O(cnt_clk_0[1]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt_clk[2]_i_1 
       (.I0(cnt_clk[1]),
        .I1(cnt_clk[0]),
        .I2(cnt_clk[2]),
        .O(cnt_clk_0[2]));
  LUT6 #(
    .INIT(64'hFFFFC0001111C000)) 
    \cnt_clk[3]_i_1 
       (.I0(cnt_clk[4]),
        .I1(cnt_clk[1]),
        .I2(cnt_clk[0]),
        .I3(cnt_clk[2]),
        .I4(cnt_clk[3]),
        .I5(\cnt_clk[4]_i_2_n_0 ),
        .O(cnt_clk_0[3]));
  LUT6 #(
    .INIT(64'hFFFF800055558000)) 
    \cnt_clk[4]_i_1 
       (.I0(cnt_clk[3]),
        .I1(cnt_clk[1]),
        .I2(cnt_clk[0]),
        .I3(cnt_clk[2]),
        .I4(cnt_clk[4]),
        .I5(\cnt_clk[4]_i_2_n_0 ),
        .O(cnt_clk_0[4]));
  LUT6 #(
    .INIT(64'h55FF55FFFFFFFFFE)) 
    \cnt_clk[4]_i_2 
       (.I0(cnt_clk[1]),
        .I1(cnt_clk[6]),
        .I2(cnt_clk[7]),
        .I3(cnt_clk[2]),
        .I4(cnt_clk[5]),
        .I5(cnt_clk[0]),
        .O(\cnt_clk[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \cnt_clk[5]_i_1 
       (.I0(cnt_clk[3]),
        .I1(cnt_clk[4]),
        .I2(cnt_clk[2]),
        .I3(cnt_clk[0]),
        .I4(cnt_clk[1]),
        .I5(cnt_clk[5]),
        .O(cnt_clk_0[5]));
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    \cnt_clk[6]_i_1 
       (.I0(cnt_clk[5]),
        .I1(\cnt_clk[7]_i_2_n_0 ),
        .I2(cnt_clk[4]),
        .I3(cnt_clk[3]),
        .I4(cnt_clk[6]),
        .O(cnt_clk_0[6]));
  LUT6 #(
    .INIT(64'hFF7FFFFF00800000)) 
    \cnt_clk[7]_i_1 
       (.I0(cnt_clk[6]),
        .I1(cnt_clk[3]),
        .I2(cnt_clk[4]),
        .I3(\cnt_clk[7]_i_2_n_0 ),
        .I4(cnt_clk[5]),
        .I5(cnt_clk[7]),
        .O(cnt_clk_0[7]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \cnt_clk[7]_i_2 
       (.I0(cnt_clk[2]),
        .I1(cnt_clk[0]),
        .I2(cnt_clk[1]),
        .O(\cnt_clk[7]_i_2_n_0 ));
  FDCE \cnt_clk_reg[0] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_clk_0[0]),
        .Q(cnt_clk[0]));
  FDCE \cnt_clk_reg[1] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_clk_0[1]),
        .Q(cnt_clk[1]));
  FDCE \cnt_clk_reg[2] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_clk_0[2]),
        .Q(cnt_clk[2]));
  FDCE \cnt_clk_reg[3] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_clk_0[3]),
        .Q(cnt_clk[3]));
  FDCE \cnt_clk_reg[4] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_clk_0[4]),
        .Q(cnt_clk[4]));
  FDCE \cnt_clk_reg[5] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_clk_0[5]),
        .Q(cnt_clk[5]));
  FDCE \cnt_clk_reg[6] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_clk_0[6]),
        .Q(cnt_clk[6]));
  FDCE \cnt_clk_reg[7] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_clk_0[7]),
        .Q(cnt_clk[7]));
  LUT2 #(
    .INIT(4'h6)) 
    \cnt_i2c_clk[0]_i_1 
       (.I0(cnt_i2c_clk_en),
        .I1(cnt_i2c_clk[0]),
        .O(\cnt_i2c_clk[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt_i2c_clk[1]_i_1 
       (.I0(cnt_i2c_clk[0]),
        .I1(cnt_i2c_clk_en),
        .I2(cnt_i2c_clk[1]),
        .O(\cnt_i2c_clk[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFDFFFDFFFD0000)) 
    cnt_i2c_clk_en_i_1
       (.I0(\FSM_onehot_state_reg_n_0_[15] ),
        .I1(\cnt_bit_reg[2]_0 ),
        .I2(\FSM_onehot_state[15]_i_3_n_0 ),
        .I3(\FSM_onehot_state[15]_i_2_n_0 ),
        .I4(cfg_start),
        .I5(cnt_i2c_clk_en),
        .O(cnt_i2c_clk_en_i_1_n_0));
  FDCE cnt_i2c_clk_en_reg
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_i2c_clk_en_i_1_n_0),
        .Q(cnt_i2c_clk_en));
  FDCE \cnt_i2c_clk_reg[0] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\cnt_i2c_clk[0]_i_1_n_0 ),
        .Q(cnt_i2c_clk[0]));
  FDCE \cnt_i2c_clk_reg[1] 
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(\cnt_i2c_clk[1]_i_1_n_0 ),
        .Q(cnt_i2c_clk[1]));
  LUT5 #(
    .INIT(32'hFFBF0040)) 
    i2c_clk_i_1
       (.I0(i2c_clk_i_2_n_0),
        .I1(cnt_clk[4]),
        .I2(cnt_clk[3]),
        .I3(cnt_clk[1]),
        .I4(i2c_clk),
        .O(i2c_clk_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    i2c_clk_i_2
       (.I0(cnt_clk[6]),
        .I1(cnt_clk[7]),
        .I2(cnt_clk[2]),
        .I3(cnt_clk[5]),
        .I4(cnt_clk[0]),
        .O(i2c_clk_i_2_n_0));
  FDPE i2c_clk_reg
       (.C(sys_clk),
        .CE(1'b1),
        .D(i2c_clk_i_1_n_0),
        .PRE(\cnt_i2c_clk_reg[0]_0 ),
        .Q(i2c_clk));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    i2c_end_i_1
       (.I0(\cnt_bit_reg[1]_0 ),
        .I1(\cnt_bit_reg[0]_1 ),
        .I2(cnt_i2c_clk[0]),
        .I3(cnt_i2c_clk[1]),
        .I4(\cnt_bit_reg[2]_0 ),
        .I5(\FSM_onehot_state_reg_n_0_[15] ),
        .O(cnt_i2c_clk_en1));
  FDCE i2c_end_reg
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(\cnt_i2c_clk_reg[0]_0 ),
        .D(cnt_i2c_clk_en1),
        .Q(E));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    i2c_sda_reg_reg
       (.CLR(1'b0),
        .D(i2c_sda_reg_reg_i_1_n_0),
        .G(i2c_sda_reg__0),
        .GE(1'b1),
        .Q(i2c_sda_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    i2c_sda_reg_reg_i_1
       (.I0(sccb_scl_INST_0_i_3_n_0),
        .I1(\FSM_onehot_state_reg_n_0_[11] ),
        .I2(i2c_sda_reg_reg_0),
        .I3(i2c_sda_reg_reg_1),
        .I4(i2c_sda_reg_reg_2),
        .I5(i2c_sda_reg_reg_i_6_n_0),
        .O(i2c_sda_reg_reg_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hA00C)) 
    i2c_sda_reg_reg_i_18
       (.I0(\FSM_onehot_state_reg_n_0_[15] ),
        .I1(p_1_in),
        .I2(cnt_i2c_clk[1]),
        .I3(cnt_i2c_clk[0]),
        .O(i2c_sda_reg_reg_i_18_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    i2c_sda_reg_reg_i_2
       (.I0(i2c_sda_reg_reg_i_7_n_0),
        .I1(\FSM_onehot_state_reg_n_0_[15] ),
        .I2(\FSM_onehot_state_reg_n_0_[11] ),
        .I3(Q[2]),
        .I4(sccb_scl_INST_0_i_3_n_0),
        .I5(p_1_in),
        .O(i2c_sda_reg__0));
  LUT3 #(
    .INIT(8'h04)) 
    i2c_sda_reg_reg_i_42
       (.I0(\cnt_bit_reg[0]_1 ),
        .I1(\cnt_bit_reg[1]_0 ),
        .I2(i2c_sda_reg_reg_i_40),
        .O(\cnt_bit_reg[0]_2 ));
  LUT6 #(
    .INIT(64'hEFEFEFFCEEEEEEEC)) 
    i2c_sda_reg_reg_i_6
       (.I0(\FSM_onehot_state_reg_n_0_[15] ),
        .I1(i2c_sda_reg_reg_i_18_n_0),
        .I2(\cnt_bit_reg[2]_0 ),
        .I3(\cnt_bit_reg[1]_0 ),
        .I4(\cnt_bit_reg[0]_1 ),
        .I5(\FSM_onehot_state_reg_n_0_[6] ),
        .O(i2c_sda_reg_reg_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    i2c_sda_reg_reg_i_7
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\FSM_onehot_state_reg_n_0_[6] ),
        .O(i2c_sda_reg_reg_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h10)) 
    i2c_sda_reg_reg_i_83
       (.I0(\cnt_bit_reg[1]_0 ),
        .I1(i2c_sda_reg_reg_i_40),
        .I2(\cnt_bit_reg[0]_1 ),
        .O(\cnt_bit_reg[1]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h08)) 
    i2c_sda_reg_reg_i_87
       (.I0(\cnt_bit_reg[0]_1 ),
        .I1(\cnt_bit_reg[1]_0 ),
        .I2(i2c_sda_reg_reg_i_40),
        .O(\cnt_bit_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFBEFFFF3F00)) 
    sccb_scl_INST_0
       (.I0(\FSM_onehot_state_reg_n_0_[11] ),
        .I1(cnt_i2c_clk[1]),
        .I2(cnt_i2c_clk[0]),
        .I3(p_1_in),
        .I4(sccb_scl_INST_0_i_1_n_0),
        .I5(sccb_scl_INST_0_i_2_n_0),
        .O(sccb_scl));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000000)) 
    sccb_scl_INST_0_i_1
       (.I0(\cnt_bit_reg[2]_0 ),
        .I1(\cnt_bit_reg[1]_0 ),
        .I2(\cnt_bit_reg[0]_1 ),
        .I3(cnt_i2c_clk[1]),
        .I4(cnt_i2c_clk[0]),
        .I5(\FSM_onehot_state_reg_n_0_[15] ),
        .O(sccb_scl_INST_0_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    sccb_scl_INST_0_i_2
       (.I0(sccb_scl_INST_0_i_3_n_0),
        .I1(Q[2]),
        .I2(\FSM_onehot_state_reg_n_0_[11] ),
        .I3(\FSM_onehot_state_reg_n_0_[6] ),
        .I4(Q[1]),
        .I5(Q[0]),
        .O(sccb_scl_INST_0_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    sccb_scl_INST_0_i_3
       (.I0(\FSM_onehot_state_reg_n_0_[14] ),
        .I1(\FSM_onehot_state_reg_n_0_[3] ),
        .I2(p_3_in),
        .I3(p_5_in),
        .O(sccb_scl_INST_0_i_3_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    sccb_sda_INST_0
       (.I0(i2c_sda_reg),
        .I1(sccb_sda_INST_0_i_1_n_0),
        .O(sccb_sda));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    sccb_sda_INST_0_i_1
       (.I0(p_5_in),
        .I1(p_3_in),
        .I2(\FSM_onehot_state_reg_n_0_[3] ),
        .I3(\FSM_onehot_state_reg_n_0_[14] ),
        .O(sccb_sda_INST_0_i_1_n_0));
endmodule

(* ORIG_REF_NAME = "ov5640_cfg" *) 
module system_ov5640_top_0_0_ov5640_cfg
   (cfg_start,
    sys_rst_n_0,
    cfg_done_reg_0,
    \FSM_onehot_state_reg[13] ,
    \FSM_onehot_state_reg[13]_0 ,
    \FSM_onehot_state_reg[2] ,
    i2c_clk,
    Q,
    i2c_sda_reg_reg_i_4_0,
    i2c_sda_reg_reg_i_41_0,
    i2c_sda_reg_reg_i_4_1,
    i2c_sda_reg_reg_i_5_0,
    i2c_sda_reg_reg_i_17_0,
    i2c_sda_reg_reg_i_17_1,
    E,
    sys_rst_n);
  output cfg_start;
  output sys_rst_n_0;
  output cfg_done_reg_0;
  output \FSM_onehot_state_reg[13] ;
  output \FSM_onehot_state_reg[13]_0 ;
  output \FSM_onehot_state_reg[2] ;
  input i2c_clk;
  input [2:0]Q;
  input i2c_sda_reg_reg_i_4_0;
  input i2c_sda_reg_reg_i_41_0;
  input i2c_sda_reg_reg_i_4_1;
  input i2c_sda_reg_reg_i_5_0;
  input i2c_sda_reg_reg_i_17_0;
  input i2c_sda_reg_reg_i_17_1;
  input [0:0]E;
  input sys_rst_n;

  wire [0:0]E;
  wire \FSM_onehot_state_reg[13] ;
  wire \FSM_onehot_state_reg[13]_0 ;
  wire \FSM_onehot_state_reg[2] ;
  wire [2:0]Q;
  wire cfg_done_i_1_n_0;
  wire cfg_done_i_3_n_0;
  wire cfg_done_reg_0;
  wire cfg_start;
  wire cfg_start_0;
  wire cfg_start_i_2_n_0;
  wire cfg_start_i_3_n_0;
  wire cfg_start_i_4_n_0;
  wire cfg_start_i_5_n_0;
  wire \cnt_wait[0]_i_1_n_0 ;
  wire \cnt_wait[0]_i_3_n_0 ;
  wire \cnt_wait[0]_i_4_n_0 ;
  wire \cnt_wait[0]_i_5_n_0 ;
  wire \cnt_wait[0]_i_6_n_0 ;
  wire [15:0]cnt_wait_reg;
  wire \cnt_wait_reg[0]_i_2_n_0 ;
  wire \cnt_wait_reg[0]_i_2_n_1 ;
  wire \cnt_wait_reg[0]_i_2_n_2 ;
  wire \cnt_wait_reg[0]_i_2_n_3 ;
  wire \cnt_wait_reg[0]_i_2_n_4 ;
  wire \cnt_wait_reg[0]_i_2_n_5 ;
  wire \cnt_wait_reg[0]_i_2_n_6 ;
  wire \cnt_wait_reg[0]_i_2_n_7 ;
  wire \cnt_wait_reg[12]_i_1_n_1 ;
  wire \cnt_wait_reg[12]_i_1_n_2 ;
  wire \cnt_wait_reg[12]_i_1_n_3 ;
  wire \cnt_wait_reg[12]_i_1_n_4 ;
  wire \cnt_wait_reg[12]_i_1_n_5 ;
  wire \cnt_wait_reg[12]_i_1_n_6 ;
  wire \cnt_wait_reg[12]_i_1_n_7 ;
  wire \cnt_wait_reg[4]_i_1_n_0 ;
  wire \cnt_wait_reg[4]_i_1_n_1 ;
  wire \cnt_wait_reg[4]_i_1_n_2 ;
  wire \cnt_wait_reg[4]_i_1_n_3 ;
  wire \cnt_wait_reg[4]_i_1_n_4 ;
  wire \cnt_wait_reg[4]_i_1_n_5 ;
  wire \cnt_wait_reg[4]_i_1_n_6 ;
  wire \cnt_wait_reg[4]_i_1_n_7 ;
  wire \cnt_wait_reg[8]_i_1_n_0 ;
  wire \cnt_wait_reg[8]_i_1_n_1 ;
  wire \cnt_wait_reg[8]_i_1_n_2 ;
  wire \cnt_wait_reg[8]_i_1_n_3 ;
  wire \cnt_wait_reg[8]_i_1_n_4 ;
  wire \cnt_wait_reg[8]_i_1_n_5 ;
  wire \cnt_wait_reg[8]_i_1_n_6 ;
  wire \cnt_wait_reg[8]_i_1_n_7 ;
  wire i2c_clk;
  wire i2c_sda_reg_reg_i_100_n_0;
  wire i2c_sda_reg_reg_i_101_n_0;
  wire i2c_sda_reg_reg_i_102_n_0;
  wire i2c_sda_reg_reg_i_103_n_0;
  wire i2c_sda_reg_reg_i_104_n_0;
  wire i2c_sda_reg_reg_i_105_n_0;
  wire i2c_sda_reg_reg_i_106_n_0;
  wire i2c_sda_reg_reg_i_107_n_0;
  wire i2c_sda_reg_reg_i_108_n_0;
  wire i2c_sda_reg_reg_i_109_n_0;
  wire i2c_sda_reg_reg_i_10_n_0;
  wire i2c_sda_reg_reg_i_110_n_0;
  wire i2c_sda_reg_reg_i_111_n_0;
  wire i2c_sda_reg_reg_i_112_n_0;
  wire i2c_sda_reg_reg_i_113_n_0;
  wire i2c_sda_reg_reg_i_114_n_0;
  wire i2c_sda_reg_reg_i_115_n_0;
  wire i2c_sda_reg_reg_i_116_n_0;
  wire i2c_sda_reg_reg_i_117_n_0;
  wire i2c_sda_reg_reg_i_118_n_0;
  wire i2c_sda_reg_reg_i_119_n_0;
  wire i2c_sda_reg_reg_i_11_n_0;
  wire i2c_sda_reg_reg_i_120_n_0;
  wire i2c_sda_reg_reg_i_121_n_0;
  wire i2c_sda_reg_reg_i_122_n_0;
  wire i2c_sda_reg_reg_i_123_n_0;
  wire i2c_sda_reg_reg_i_124_n_0;
  wire i2c_sda_reg_reg_i_125_n_0;
  wire i2c_sda_reg_reg_i_126_n_0;
  wire i2c_sda_reg_reg_i_127_n_0;
  wire i2c_sda_reg_reg_i_128_n_0;
  wire i2c_sda_reg_reg_i_129_n_0;
  wire i2c_sda_reg_reg_i_12_n_0;
  wire i2c_sda_reg_reg_i_130_n_0;
  wire i2c_sda_reg_reg_i_131_n_0;
  wire i2c_sda_reg_reg_i_132_n_0;
  wire i2c_sda_reg_reg_i_133_n_0;
  wire i2c_sda_reg_reg_i_134_n_0;
  wire i2c_sda_reg_reg_i_135_n_0;
  wire i2c_sda_reg_reg_i_136_n_0;
  wire i2c_sda_reg_reg_i_137_n_0;
  wire i2c_sda_reg_reg_i_138_n_0;
  wire i2c_sda_reg_reg_i_139_n_0;
  wire i2c_sda_reg_reg_i_13_n_0;
  wire i2c_sda_reg_reg_i_140_n_0;
  wire i2c_sda_reg_reg_i_141_n_0;
  wire i2c_sda_reg_reg_i_142_n_0;
  wire i2c_sda_reg_reg_i_143_n_0;
  wire i2c_sda_reg_reg_i_144_n_0;
  wire i2c_sda_reg_reg_i_14_n_0;
  wire i2c_sda_reg_reg_i_15_n_0;
  wire i2c_sda_reg_reg_i_16_n_0;
  wire i2c_sda_reg_reg_i_17_0;
  wire i2c_sda_reg_reg_i_17_1;
  wire i2c_sda_reg_reg_i_17_n_0;
  wire i2c_sda_reg_reg_i_19_n_0;
  wire i2c_sda_reg_reg_i_20_n_0;
  wire i2c_sda_reg_reg_i_21_n_0;
  wire i2c_sda_reg_reg_i_22_n_0;
  wire i2c_sda_reg_reg_i_23_n_0;
  wire i2c_sda_reg_reg_i_24_n_0;
  wire i2c_sda_reg_reg_i_25_n_0;
  wire i2c_sda_reg_reg_i_26_n_0;
  wire i2c_sda_reg_reg_i_27_n_0;
  wire i2c_sda_reg_reg_i_28_n_0;
  wire i2c_sda_reg_reg_i_29_n_0;
  wire i2c_sda_reg_reg_i_30_n_0;
  wire i2c_sda_reg_reg_i_31_n_0;
  wire i2c_sda_reg_reg_i_32_n_0;
  wire i2c_sda_reg_reg_i_33_n_0;
  wire i2c_sda_reg_reg_i_34_n_0;
  wire i2c_sda_reg_reg_i_35_n_0;
  wire i2c_sda_reg_reg_i_36_n_0;
  wire i2c_sda_reg_reg_i_37_n_0;
  wire i2c_sda_reg_reg_i_38_n_0;
  wire i2c_sda_reg_reg_i_39_n_0;
  wire i2c_sda_reg_reg_i_40_n_0;
  wire i2c_sda_reg_reg_i_41_0;
  wire i2c_sda_reg_reg_i_41_n_0;
  wire i2c_sda_reg_reg_i_43_n_0;
  wire i2c_sda_reg_reg_i_44_n_0;
  wire i2c_sda_reg_reg_i_45_n_0;
  wire i2c_sda_reg_reg_i_46_n_0;
  wire i2c_sda_reg_reg_i_47_n_0;
  wire i2c_sda_reg_reg_i_48_n_0;
  wire i2c_sda_reg_reg_i_49_n_0;
  wire i2c_sda_reg_reg_i_4_0;
  wire i2c_sda_reg_reg_i_4_1;
  wire i2c_sda_reg_reg_i_50_n_0;
  wire i2c_sda_reg_reg_i_51_n_0;
  wire i2c_sda_reg_reg_i_52_n_0;
  wire i2c_sda_reg_reg_i_53_n_0;
  wire i2c_sda_reg_reg_i_54_n_0;
  wire i2c_sda_reg_reg_i_55_n_0;
  wire i2c_sda_reg_reg_i_56_n_0;
  wire i2c_sda_reg_reg_i_57_n_0;
  wire i2c_sda_reg_reg_i_58_n_0;
  wire i2c_sda_reg_reg_i_59_n_0;
  wire i2c_sda_reg_reg_i_5_0;
  wire i2c_sda_reg_reg_i_60_n_0;
  wire i2c_sda_reg_reg_i_61_n_0;
  wire i2c_sda_reg_reg_i_62_n_0;
  wire i2c_sda_reg_reg_i_63_n_0;
  wire i2c_sda_reg_reg_i_64_n_0;
  wire i2c_sda_reg_reg_i_65_n_0;
  wire i2c_sda_reg_reg_i_66_n_0;
  wire i2c_sda_reg_reg_i_67_n_0;
  wire i2c_sda_reg_reg_i_68_n_0;
  wire i2c_sda_reg_reg_i_69_n_0;
  wire i2c_sda_reg_reg_i_70_n_0;
  wire i2c_sda_reg_reg_i_71_n_0;
  wire i2c_sda_reg_reg_i_72_n_0;
  wire i2c_sda_reg_reg_i_73_n_0;
  wire i2c_sda_reg_reg_i_74_n_0;
  wire i2c_sda_reg_reg_i_75_n_0;
  wire i2c_sda_reg_reg_i_76_n_0;
  wire i2c_sda_reg_reg_i_77_n_0;
  wire i2c_sda_reg_reg_i_78_n_0;
  wire i2c_sda_reg_reg_i_79_n_0;
  wire i2c_sda_reg_reg_i_80_n_0;
  wire i2c_sda_reg_reg_i_81_n_0;
  wire i2c_sda_reg_reg_i_82_n_0;
  wire i2c_sda_reg_reg_i_84_n_0;
  wire i2c_sda_reg_reg_i_85_n_0;
  wire i2c_sda_reg_reg_i_86_n_0;
  wire i2c_sda_reg_reg_i_88_n_0;
  wire i2c_sda_reg_reg_i_89_n_0;
  wire i2c_sda_reg_reg_i_8_n_0;
  wire i2c_sda_reg_reg_i_90_n_0;
  wire i2c_sda_reg_reg_i_91_n_0;
  wire i2c_sda_reg_reg_i_92_n_0;
  wire i2c_sda_reg_reg_i_93_n_0;
  wire i2c_sda_reg_reg_i_94_n_0;
  wire i2c_sda_reg_reg_i_95_n_0;
  wire i2c_sda_reg_reg_i_96_n_0;
  wire i2c_sda_reg_reg_i_97_n_0;
  wire i2c_sda_reg_reg_i_98_n_0;
  wire i2c_sda_reg_reg_i_99_n_0;
  wire i2c_sda_reg_reg_i_9_n_0;
  wire [7:0]reg_num;
  wire [7:0]reg_num_reg;
  wire \reg_num_rep[0]_i_1_n_0 ;
  wire \reg_num_rep[1]_i_1_n_0 ;
  wire \reg_num_rep[2]_i_1_n_0 ;
  wire \reg_num_rep[3]_i_1_n_0 ;
  wire \reg_num_rep[4]_i_1_n_0 ;
  wire \reg_num_rep[5]_i_1_n_0 ;
  wire \reg_num_rep[6]_i_1_n_0 ;
  wire \reg_num_rep[7]_i_1_n_0 ;
  wire \reg_num_rep[7]_i_2_n_0 ;
  wire sys_rst_n;
  wire sys_rst_n_0;
  wire [3:3]\NLW_cnt_wait_reg[12]_i_1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hFFFFFFFF10000000)) 
    cfg_done_i_1
       (.I0(cfg_done_i_3_n_0),
        .I1(reg_num_reg[2]),
        .I2(E),
        .I3(reg_num_reg[1]),
        .I4(reg_num_reg[0]),
        .I5(cfg_done_reg_0),
        .O(cfg_done_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    cfg_done_i_2
       (.I0(sys_rst_n),
        .O(sys_rst_n_0));
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    cfg_done_i_3
       (.I0(reg_num_reg[6]),
        .I1(reg_num_reg[4]),
        .I2(reg_num_reg[3]),
        .I3(reg_num_reg[5]),
        .I4(reg_num_reg[7]),
        .O(cfg_done_i_3_n_0));
  FDCE cfg_done_reg
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(sys_rst_n_0),
        .D(cfg_done_i_1_n_0),
        .Q(cfg_done_reg_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFCCCC0444)) 
    cfg_start_i_1
       (.I0(reg_num_reg[2]),
        .I1(E),
        .I2(reg_num_reg[1]),
        .I3(reg_num_reg[0]),
        .I4(cfg_done_i_3_n_0),
        .I5(cfg_start_i_2_n_0),
        .O(cfg_start_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    cfg_start_i_2
       (.I0(cfg_start_i_3_n_0),
        .I1(cfg_start_i_4_n_0),
        .I2(cnt_wait_reg[0]),
        .I3(cnt_wait_reg[1]),
        .I4(cnt_wait_reg[2]),
        .I5(cfg_start_i_5_n_0),
        .O(cfg_start_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    cfg_start_i_3
       (.I0(cnt_wait_reg[13]),
        .I1(cnt_wait_reg[11]),
        .I2(cnt_wait_reg[8]),
        .O(cfg_start_i_3_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    cfg_start_i_4
       (.I0(cnt_wait_reg[7]),
        .I1(cnt_wait_reg[6]),
        .I2(cnt_wait_reg[10]),
        .I3(cnt_wait_reg[9]),
        .O(cfg_start_i_4_n_0));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    cfg_start_i_5
       (.I0(cnt_wait_reg[12]),
        .I1(cnt_wait_reg[5]),
        .I2(cnt_wait_reg[4]),
        .I3(cnt_wait_reg[3]),
        .I4(cnt_wait_reg[15]),
        .I5(cnt_wait_reg[14]),
        .O(cfg_start_i_5_n_0));
  FDCE cfg_start_reg
       (.C(i2c_clk),
        .CE(1'b1),
        .CLR(sys_rst_n_0),
        .D(cfg_start_0),
        .Q(cfg_start));
  LUT6 #(
    .INIT(64'hAAAAAABFFFFFFFFF)) 
    \cnt_wait[0]_i_1 
       (.I0(\cnt_wait[0]_i_3_n_0 ),
        .I1(cnt_wait_reg[10]),
        .I2(cnt_wait_reg[9]),
        .I3(cnt_wait_reg[13]),
        .I4(cnt_wait_reg[11]),
        .I5(\cnt_wait[0]_i_4_n_0 ),
        .O(\cnt_wait[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hABABFFABFFABFFAB)) 
    \cnt_wait[0]_i_3 
       (.I0(\cnt_wait[0]_i_6_n_0 ),
        .I1(cnt_wait_reg[12]),
        .I2(cnt_wait_reg[13]),
        .I3(cfg_start_i_3_n_0),
        .I4(cnt_wait_reg[6]),
        .I5(cnt_wait_reg[7]),
        .O(\cnt_wait[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_wait[0]_i_4 
       (.I0(cnt_wait_reg[14]),
        .I1(cnt_wait_reg[15]),
        .O(\cnt_wait[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_wait[0]_i_5 
       (.I0(cnt_wait_reg[0]),
        .O(\cnt_wait[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000015)) 
    \cnt_wait[0]_i_6 
       (.I0(cnt_wait_reg[5]),
        .I1(cnt_wait_reg[3]),
        .I2(cnt_wait_reg[4]),
        .I3(cnt_wait_reg[8]),
        .I4(cnt_wait_reg[11]),
        .I5(cnt_wait_reg[13]),
        .O(\cnt_wait[0]_i_6_n_0 ));
  FDCE \cnt_wait_reg[0] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[0]_i_2_n_7 ),
        .Q(cnt_wait_reg[0]));
  CARRY4 \cnt_wait_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_wait_reg[0]_i_2_n_0 ,\cnt_wait_reg[0]_i_2_n_1 ,\cnt_wait_reg[0]_i_2_n_2 ,\cnt_wait_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_wait_reg[0]_i_2_n_4 ,\cnt_wait_reg[0]_i_2_n_5 ,\cnt_wait_reg[0]_i_2_n_6 ,\cnt_wait_reg[0]_i_2_n_7 }),
        .S({cnt_wait_reg[3:1],\cnt_wait[0]_i_5_n_0 }));
  FDCE \cnt_wait_reg[10] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[8]_i_1_n_5 ),
        .Q(cnt_wait_reg[10]));
  FDCE \cnt_wait_reg[11] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[8]_i_1_n_4 ),
        .Q(cnt_wait_reg[11]));
  FDCE \cnt_wait_reg[12] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[12]_i_1_n_7 ),
        .Q(cnt_wait_reg[12]));
  CARRY4 \cnt_wait_reg[12]_i_1 
       (.CI(\cnt_wait_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_wait_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_wait_reg[12]_i_1_n_1 ,\cnt_wait_reg[12]_i_1_n_2 ,\cnt_wait_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_wait_reg[12]_i_1_n_4 ,\cnt_wait_reg[12]_i_1_n_5 ,\cnt_wait_reg[12]_i_1_n_6 ,\cnt_wait_reg[12]_i_1_n_7 }),
        .S(cnt_wait_reg[15:12]));
  FDCE \cnt_wait_reg[13] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[12]_i_1_n_6 ),
        .Q(cnt_wait_reg[13]));
  FDCE \cnt_wait_reg[14] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[12]_i_1_n_5 ),
        .Q(cnt_wait_reg[14]));
  FDCE \cnt_wait_reg[15] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[12]_i_1_n_4 ),
        .Q(cnt_wait_reg[15]));
  FDCE \cnt_wait_reg[1] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[0]_i_2_n_6 ),
        .Q(cnt_wait_reg[1]));
  FDCE \cnt_wait_reg[2] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[0]_i_2_n_5 ),
        .Q(cnt_wait_reg[2]));
  FDCE \cnt_wait_reg[3] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[0]_i_2_n_4 ),
        .Q(cnt_wait_reg[3]));
  FDCE \cnt_wait_reg[4] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[4]_i_1_n_7 ),
        .Q(cnt_wait_reg[4]));
  CARRY4 \cnt_wait_reg[4]_i_1 
       (.CI(\cnt_wait_reg[0]_i_2_n_0 ),
        .CO({\cnt_wait_reg[4]_i_1_n_0 ,\cnt_wait_reg[4]_i_1_n_1 ,\cnt_wait_reg[4]_i_1_n_2 ,\cnt_wait_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_wait_reg[4]_i_1_n_4 ,\cnt_wait_reg[4]_i_1_n_5 ,\cnt_wait_reg[4]_i_1_n_6 ,\cnt_wait_reg[4]_i_1_n_7 }),
        .S(cnt_wait_reg[7:4]));
  FDCE \cnt_wait_reg[5] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[4]_i_1_n_6 ),
        .Q(cnt_wait_reg[5]));
  FDCE \cnt_wait_reg[6] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[4]_i_1_n_5 ),
        .Q(cnt_wait_reg[6]));
  FDCE \cnt_wait_reg[7] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[4]_i_1_n_4 ),
        .Q(cnt_wait_reg[7]));
  FDCE \cnt_wait_reg[8] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[8]_i_1_n_7 ),
        .Q(cnt_wait_reg[8]));
  CARRY4 \cnt_wait_reg[8]_i_1 
       (.CI(\cnt_wait_reg[4]_i_1_n_0 ),
        .CO({\cnt_wait_reg[8]_i_1_n_0 ,\cnt_wait_reg[8]_i_1_n_1 ,\cnt_wait_reg[8]_i_1_n_2 ,\cnt_wait_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_wait_reg[8]_i_1_n_4 ,\cnt_wait_reg[8]_i_1_n_5 ,\cnt_wait_reg[8]_i_1_n_6 ,\cnt_wait_reg[8]_i_1_n_7 }),
        .S(cnt_wait_reg[11:8]));
  FDCE \cnt_wait_reg[9] 
       (.C(i2c_clk),
        .CE(\cnt_wait[0]_i_1_n_0 ),
        .CLR(sys_rst_n_0),
        .D(\cnt_wait_reg[8]_i_1_n_6 ),
        .Q(cnt_wait_reg[9]));
  LUT6 #(
    .INIT(64'h00000000F0AACC00)) 
    i2c_sda_reg_reg_i_10
       (.I0(i2c_sda_reg_reg_i_23_n_0),
        .I1(i2c_sda_reg_reg_i_24_n_0),
        .I2(i2c_sda_reg_reg_i_25_n_0),
        .I3(i2c_sda_reg_reg_i_4_1),
        .I4(i2c_sda_reg_reg_i_41_0),
        .I5(cfg_done_reg_0),
        .O(i2c_sda_reg_reg_i_10_n_0));
  LUT6 #(
    .INIT(64'h40554A4015881887)) 
    i2c_sda_reg_reg_i_100
       (.I0(reg_num[4]),
        .I1(reg_num[2]),
        .I2(reg_num[3]),
        .I3(reg_num[0]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_100_n_0));
  LUT6 #(
    .INIT(64'h0210602948CBD102)) 
    i2c_sda_reg_reg_i_101
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_101_n_0));
  LUT6 #(
    .INIT(64'h271040645F206273)) 
    i2c_sda_reg_reg_i_102
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_102_n_0));
  LUT6 #(
    .INIT(64'h800020009000E001)) 
    i2c_sda_reg_reg_i_103
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[1]),
        .I3(reg_num[5]),
        .I4(reg_num[2]),
        .I5(reg_num[0]),
        .O(i2c_sda_reg_reg_i_103_n_0));
  LUT6 #(
    .INIT(64'h0107010000200031)) 
    i2c_sda_reg_reg_i_104
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_104_n_0));
  LUT6 #(
    .INIT(64'h8980044AB82959C2)) 
    i2c_sda_reg_reg_i_105
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_105_n_0));
  LUT6 #(
    .INIT(64'h274513000C603771)) 
    i2c_sda_reg_reg_i_106
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_106_n_0));
  LUT6 #(
    .INIT(64'h7610FE00C401E901)) 
    i2c_sda_reg_reg_i_107
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[5]),
        .I4(reg_num[1]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_107_n_0));
  LUT6 #(
    .INIT(64'h221200502128C101)) 
    i2c_sda_reg_reg_i_108
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[5]),
        .I4(reg_num[2]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_108_n_0));
  LUT6 #(
    .INIT(64'h1080044024015110)) 
    i2c_sda_reg_reg_i_109
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_109_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFF11F1)) 
    i2c_sda_reg_reg_i_11
       (.I0(i2c_sda_reg_reg_i_26_n_0),
        .I1(i2c_sda_reg_reg_i_41_0),
        .I2(i2c_sda_reg_reg_i_4_1),
        .I3(i2c_sda_reg_reg_i_27_n_0),
        .I4(cfg_done_reg_0),
        .I5(i2c_sda_reg_reg_i_4_0),
        .O(i2c_sda_reg_reg_i_11_n_0));
  LUT6 #(
    .INIT(64'h146400A64C444031)) 
    i2c_sda_reg_reg_i_110
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[1]),
        .I4(reg_num[5]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_110_n_0));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    i2c_sda_reg_reg_i_111
       (.I0(reg_num[3]),
        .I1(reg_num[2]),
        .I2(reg_num[5]),
        .I3(reg_num[1]),
        .I4(reg_num[0]),
        .I5(reg_num[4]),
        .O(i2c_sda_reg_reg_i_111_n_0));
  LUT6 #(
    .INIT(64'h0020080012000001)) 
    i2c_sda_reg_reg_i_112
       (.I0(reg_num[4]),
        .I1(reg_num[0]),
        .I2(reg_num[2]),
        .I3(reg_num[5]),
        .I4(reg_num[1]),
        .I5(reg_num[3]),
        .O(i2c_sda_reg_reg_i_112_n_0));
  LUT6 #(
    .INIT(64'hF8000857ABD5540A)) 
    i2c_sda_reg_reg_i_113
       (.I0(reg_num[4]),
        .I1(reg_num[0]),
        .I2(reg_num[1]),
        .I3(reg_num[3]),
        .I4(reg_num[2]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_113_n_0));
  LUT6 #(
    .INIT(64'h402A00AA00AA00AA)) 
    i2c_sda_reg_reg_i_114
       (.I0(reg_num[4]),
        .I1(reg_num[0]),
        .I2(reg_num[1]),
        .I3(reg_num[5]),
        .I4(reg_num[2]),
        .I5(reg_num[3]),
        .O(i2c_sda_reg_reg_i_114_n_0));
  LUT4 #(
    .INIT(16'hAAA9)) 
    i2c_sda_reg_reg_i_115
       (.I0(reg_num[4]),
        .I1(reg_num[2]),
        .I2(reg_num[1]),
        .I3(reg_num[3]),
        .O(i2c_sda_reg_reg_i_115_n_0));
  LUT6 #(
    .INIT(64'h7002240340200402)) 
    i2c_sda_reg_reg_i_116
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[1]),
        .I3(reg_num[5]),
        .I4(reg_num[2]),
        .I5(reg_num[0]),
        .O(i2c_sda_reg_reg_i_116_n_0));
  LUT6 #(
    .INIT(64'hAA804C984C0BA114)) 
    i2c_sda_reg_reg_i_117
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[5]),
        .I4(reg_num[1]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_117_n_0));
  LUT6 #(
    .INIT(64'hB44C4CCCCCCC4CCC)) 
    i2c_sda_reg_reg_i_118
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[2]),
        .I3(reg_num[5]),
        .I4(reg_num[1]),
        .I5(reg_num[0]),
        .O(i2c_sda_reg_reg_i_118_n_0));
  LUT6 #(
    .INIT(64'hCCCCCCCCCC33CC32)) 
    i2c_sda_reg_reg_i_119
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[1]),
        .I4(reg_num[5]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_119_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF77470000)) 
    i2c_sda_reg_reg_i_12
       (.I0(i2c_sda_reg_reg_i_28_n_0),
        .I1(reg_num[6]),
        .I2(i2c_sda_reg_reg_i_29_n_0),
        .I3(reg_num[7]),
        .I4(i2c_sda_reg_reg_i_41_0),
        .I5(i2c_sda_reg_reg_i_4_1),
        .O(i2c_sda_reg_reg_i_12_n_0));
  LUT6 #(
    .INIT(64'h0355C04431154411)) 
    i2c_sda_reg_reg_i_120
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_120_n_0));
  LUT6 #(
    .INIT(64'hAA094DC658522D21)) 
    i2c_sda_reg_reg_i_121
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_121_n_0));
  LUT6 #(
    .INIT(64'h807F7FFF7F008000)) 
    i2c_sda_reg_reg_i_122
       (.I0(reg_num[2]),
        .I1(reg_num[3]),
        .I2(reg_num[4]),
        .I3(reg_num[5]),
        .I4(reg_num[0]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_122_n_0));
  LUT6 #(
    .INIT(64'h00000000FFFFFFFE)) 
    i2c_sda_reg_reg_i_123
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_123_n_0));
  LUT6 #(
    .INIT(64'h321B7760048A9E55)) 
    i2c_sda_reg_reg_i_124
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[2]),
        .I3(reg_num[0]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_124_n_0));
  LUT6 #(
    .INIT(64'hB339152828D4A322)) 
    i2c_sda_reg_reg_i_125
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_125_n_0));
  LUT6 #(
    .INIT(64'h0F7F77FF70000000)) 
    i2c_sda_reg_reg_i_126
       (.I0(reg_num[3]),
        .I1(reg_num[4]),
        .I2(reg_num[1]),
        .I3(reg_num[5]),
        .I4(reg_num[0]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_126_n_0));
  LUT6 #(
    .INIT(64'hFF00FF0000FF00FE)) 
    i2c_sda_reg_reg_i_127
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_127_n_0));
  LUT6 #(
    .INIT(64'h671F13E852820A76)) 
    i2c_sda_reg_reg_i_128
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[5]),
        .I4(reg_num[1]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_128_n_0));
  LUT6 #(
    .INIT(64'h00812198181011A5)) 
    i2c_sda_reg_reg_i_129
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_129_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF010DFFFF)) 
    i2c_sda_reg_reg_i_13
       (.I0(i2c_sda_reg_reg_i_30_n_0),
        .I1(i2c_sda_reg_reg_i_41_0),
        .I2(i2c_sda_reg_reg_i_4_1),
        .I3(i2c_sda_reg_reg_i_31_n_0),
        .I4(i2c_sda_reg_reg_i_4_0),
        .I5(cfg_done_reg_0),
        .O(i2c_sda_reg_reg_i_13_n_0));
  LUT6 #(
    .INIT(64'h5155B70110A40045)) 
    i2c_sda_reg_reg_i_130
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[1]),
        .I4(reg_num[5]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_130_n_0));
  LUT6 #(
    .INIT(64'h00110111606F7B08)) 
    i2c_sda_reg_reg_i_131
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_131_n_0));
  LUT6 #(
    .INIT(64'h038A7C2044A82002)) 
    i2c_sda_reg_reg_i_132
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[1]),
        .I3(reg_num[5]),
        .I4(reg_num[2]),
        .I5(reg_num[0]),
        .O(i2c_sda_reg_reg_i_132_n_0));
  LUT6 #(
    .INIT(64'h80340011C0200440)) 
    i2c_sda_reg_reg_i_133
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[1]),
        .I4(reg_num[5]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_133_n_0));
  LUT6 #(
    .INIT(64'h7380441E4712194B)) 
    i2c_sda_reg_reg_i_134
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_134_n_0));
  LUT6 #(
    .INIT(64'hD72F90DBCCD25C14)) 
    i2c_sda_reg_reg_i_135
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[1]),
        .I4(reg_num[5]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_135_n_0));
  LUT6 #(
    .INIT(64'h050A02D2404820C0)) 
    i2c_sda_reg_reg_i_136
       (.I0(reg_num[4]),
        .I1(reg_num[2]),
        .I2(reg_num[3]),
        .I3(reg_num[1]),
        .I4(reg_num[5]),
        .I5(reg_num[0]),
        .O(i2c_sda_reg_reg_i_136_n_0));
  LUT6 #(
    .INIT(64'h807BA3F5201710D0)) 
    i2c_sda_reg_reg_i_137
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[5]),
        .I4(reg_num[2]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_137_n_0));
  LUT6 #(
    .INIT(64'h140242CCA4620093)) 
    i2c_sda_reg_reg_i_138
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[1]),
        .I4(reg_num[5]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_138_n_0));
  LUT6 #(
    .INIT(64'hB96068FAD103B308)) 
    i2c_sda_reg_reg_i_139
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_139_n_0));
  LUT6 #(
    .INIT(64'h470000004700FF00)) 
    i2c_sda_reg_reg_i_14
       (.I0(i2c_sda_reg_reg_i_32_n_0),
        .I1(reg_num[6]),
        .I2(i2c_sda_reg_reg_i_33_n_0),
        .I3(i2c_sda_reg_reg_i_4_1),
        .I4(i2c_sda_reg_reg_i_41_0),
        .I5(i2c_sda_reg_reg_i_34_n_0),
        .O(i2c_sda_reg_reg_i_14_n_0));
  LUT6 #(
    .INIT(64'h12F5ED2256A28800)) 
    i2c_sda_reg_reg_i_140
       (.I0(reg_num[4]),
        .I1(reg_num[2]),
        .I2(reg_num[1]),
        .I3(reg_num[3]),
        .I4(reg_num[5]),
        .I5(reg_num[0]),
        .O(i2c_sda_reg_reg_i_140_n_0));
  LUT6 #(
    .INIT(64'h1360A4414324110A)) 
    i2c_sda_reg_reg_i_141
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_141_n_0));
  LUT6 #(
    .INIT(64'h676F1D0066630025)) 
    i2c_sda_reg_reg_i_142
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_142_n_0));
  LUT6 #(
    .INIT(64'h80008CA8900555C4)) 
    i2c_sda_reg_reg_i_143
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_143_n_0));
  LUT6 #(
    .INIT(64'h0000086019181705)) 
    i2c_sda_reg_reg_i_144
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[5]),
        .I3(reg_num[1]),
        .I4(reg_num[2]),
        .I5(reg_num[0]),
        .O(i2c_sda_reg_reg_i_144_n_0));
  LUT5 #(
    .INIT(32'h00AC0000)) 
    i2c_sda_reg_reg_i_15
       (.I0(i2c_sda_reg_reg_i_35_n_0),
        .I1(i2c_sda_reg_reg_i_36_n_0),
        .I2(i2c_sda_reg_reg_i_41_0),
        .I3(cfg_done_reg_0),
        .I4(i2c_sda_reg_reg_i_4_1),
        .O(i2c_sda_reg_reg_i_15_n_0));
  LUT5 #(
    .INIT(32'h00002320)) 
    i2c_sda_reg_reg_i_16
       (.I0(i2c_sda_reg_reg_i_37_n_0),
        .I1(cfg_done_reg_0),
        .I2(i2c_sda_reg_reg_i_41_0),
        .I3(i2c_sda_reg_reg_i_38_n_0),
        .I4(i2c_sda_reg_reg_i_4_1),
        .O(i2c_sda_reg_reg_i_16_n_0));
  LUT5 #(
    .INIT(32'hFFFEFEFE)) 
    i2c_sda_reg_reg_i_17
       (.I0(i2c_sda_reg_reg_i_39_n_0),
        .I1(i2c_sda_reg_reg_i_40_n_0),
        .I2(i2c_sda_reg_reg_i_41_n_0),
        .I3(i2c_sda_reg_reg_i_5_0),
        .I4(i2c_sda_reg_reg_i_43_n_0),
        .O(i2c_sda_reg_reg_i_17_n_0));
  MUXF8 i2c_sda_reg_reg_i_19
       (.I0(i2c_sda_reg_reg_i_44_n_0),
        .I1(i2c_sda_reg_reg_i_45_n_0),
        .O(i2c_sda_reg_reg_i_19_n_0),
        .S(reg_num[6]));
  MUXF8 i2c_sda_reg_reg_i_20
       (.I0(i2c_sda_reg_reg_i_46_n_0),
        .I1(i2c_sda_reg_reg_i_47_n_0),
        .O(i2c_sda_reg_reg_i_20_n_0),
        .S(reg_num[6]));
  MUXF8 i2c_sda_reg_reg_i_21
       (.I0(i2c_sda_reg_reg_i_48_n_0),
        .I1(i2c_sda_reg_reg_i_49_n_0),
        .O(i2c_sda_reg_reg_i_21_n_0),
        .S(reg_num[6]));
  MUXF8 i2c_sda_reg_reg_i_22
       (.I0(i2c_sda_reg_reg_i_50_n_0),
        .I1(i2c_sda_reg_reg_i_51_n_0),
        .O(i2c_sda_reg_reg_i_22_n_0),
        .S(reg_num[6]));
  LUT5 #(
    .INIT(32'hA0A0CFC0)) 
    i2c_sda_reg_reg_i_23
       (.I0(i2c_sda_reg_reg_i_52_n_0),
        .I1(i2c_sda_reg_reg_i_53_n_0),
        .I2(reg_num[6]),
        .I3(i2c_sda_reg_reg_i_54_n_0),
        .I4(reg_num[7]),
        .O(i2c_sda_reg_reg_i_23_n_0));
  LUT5 #(
    .INIT(32'hAFAFCFC0)) 
    i2c_sda_reg_reg_i_24
       (.I0(i2c_sda_reg_reg_i_55_n_0),
        .I1(i2c_sda_reg_reg_i_56_n_0),
        .I2(reg_num[6]),
        .I3(i2c_sda_reg_reg_i_57_n_0),
        .I4(reg_num[7]),
        .O(i2c_sda_reg_reg_i_24_n_0));
  LUT4 #(
    .INIT(16'hBBFC)) 
    i2c_sda_reg_reg_i_25
       (.I0(i2c_sda_reg_reg_i_58_n_0),
        .I1(reg_num[6]),
        .I2(i2c_sda_reg_reg_i_59_n_0),
        .I3(reg_num[7]),
        .O(i2c_sda_reg_reg_i_25_n_0));
  LUT6 #(
    .INIT(64'h0010FFFF00000000)) 
    i2c_sda_reg_reg_i_26
       (.I0(reg_num[4]),
        .I1(reg_num[0]),
        .I2(i2c_sda_reg_reg_i_60_n_0),
        .I3(reg_num[3]),
        .I4(reg_num[6]),
        .I5(reg_num[7]),
        .O(i2c_sda_reg_reg_i_26_n_0));
  MUXF8 i2c_sda_reg_reg_i_27
       (.I0(i2c_sda_reg_reg_i_61_n_0),
        .I1(i2c_sda_reg_reg_i_62_n_0),
        .O(i2c_sda_reg_reg_i_27_n_0),
        .S(reg_num[6]));
  MUXF7 i2c_sda_reg_reg_i_28
       (.I0(i2c_sda_reg_reg_i_63_n_0),
        .I1(i2c_sda_reg_reg_i_64_n_0),
        .O(i2c_sda_reg_reg_i_28_n_0),
        .S(reg_num[7]));
  LUT6 #(
    .INIT(64'h201131CC01104440)) 
    i2c_sda_reg_reg_i_29
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_29_n_0));
  LUT6 #(
    .INIT(64'h00000000FFE0E0E0)) 
    i2c_sda_reg_reg_i_3
       (.I0(i2c_sda_reg_reg_i_8_n_0),
        .I1(i2c_sda_reg_reg_i_9_n_0),
        .I2(Q[2]),
        .I3(i2c_sda_reg_reg_i_10_n_0),
        .I4(Q[0]),
        .I5(i2c_sda_reg_reg_i_4_0),
        .O(\FSM_onehot_state_reg[13]_0 ));
  MUXF8 i2c_sda_reg_reg_i_30
       (.I0(i2c_sda_reg_reg_i_65_n_0),
        .I1(i2c_sda_reg_reg_i_66_n_0),
        .O(i2c_sda_reg_reg_i_30_n_0),
        .S(reg_num[6]));
  MUXF8 i2c_sda_reg_reg_i_31
       (.I0(i2c_sda_reg_reg_i_67_n_0),
        .I1(i2c_sda_reg_reg_i_68_n_0),
        .O(i2c_sda_reg_reg_i_31_n_0),
        .S(reg_num[6]));
  MUXF7 i2c_sda_reg_reg_i_32
       (.I0(i2c_sda_reg_reg_i_69_n_0),
        .I1(i2c_sda_reg_reg_i_70_n_0),
        .O(i2c_sda_reg_reg_i_32_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_33
       (.I0(i2c_sda_reg_reg_i_71_n_0),
        .I1(i2c_sda_reg_reg_i_72_n_0),
        .O(i2c_sda_reg_reg_i_33_n_0),
        .S(reg_num[7]));
  MUXF8 i2c_sda_reg_reg_i_34
       (.I0(i2c_sda_reg_reg_i_73_n_0),
        .I1(i2c_sda_reg_reg_i_74_n_0),
        .O(i2c_sda_reg_reg_i_34_n_0),
        .S(reg_num[6]));
  MUXF8 i2c_sda_reg_reg_i_35
       (.I0(i2c_sda_reg_reg_i_75_n_0),
        .I1(i2c_sda_reg_reg_i_76_n_0),
        .O(i2c_sda_reg_reg_i_35_n_0),
        .S(reg_num[6]));
  MUXF8 i2c_sda_reg_reg_i_36
       (.I0(i2c_sda_reg_reg_i_77_n_0),
        .I1(i2c_sda_reg_reg_i_78_n_0),
        .O(i2c_sda_reg_reg_i_36_n_0),
        .S(reg_num[6]));
  MUXF8 i2c_sda_reg_reg_i_37
       (.I0(i2c_sda_reg_reg_i_79_n_0),
        .I1(i2c_sda_reg_reg_i_80_n_0),
        .O(i2c_sda_reg_reg_i_37_n_0),
        .S(reg_num[6]));
  MUXF8 i2c_sda_reg_reg_i_38
       (.I0(i2c_sda_reg_reg_i_81_n_0),
        .I1(i2c_sda_reg_reg_i_82_n_0),
        .O(i2c_sda_reg_reg_i_38_n_0),
        .S(reg_num[6]));
  LUT6 #(
    .INIT(64'hA0A0A8080000A808)) 
    i2c_sda_reg_reg_i_39
       (.I0(i2c_sda_reg_reg_i_17_0),
        .I1(i2c_sda_reg_reg_i_84_n_0),
        .I2(reg_num[7]),
        .I3(i2c_sda_reg_reg_i_85_n_0),
        .I4(reg_num[6]),
        .I5(i2c_sda_reg_reg_i_86_n_0),
        .O(i2c_sda_reg_reg_i_39_n_0));
  LUT5 #(
    .INIT(32'h111F0000)) 
    i2c_sda_reg_reg_i_4
       (.I0(i2c_sda_reg_reg_i_11_n_0),
        .I1(i2c_sda_reg_reg_i_12_n_0),
        .I2(i2c_sda_reg_reg_i_13_n_0),
        .I3(i2c_sda_reg_reg_i_14_n_0),
        .I4(Q[1]),
        .O(\FSM_onehot_state_reg[2] ));
  LUT6 #(
    .INIT(64'hA0A0A8080000A808)) 
    i2c_sda_reg_reg_i_40
       (.I0(i2c_sda_reg_reg_i_17_1),
        .I1(i2c_sda_reg_reg_i_88_n_0),
        .I2(reg_num[7]),
        .I3(i2c_sda_reg_reg_i_89_n_0),
        .I4(reg_num[6]),
        .I5(i2c_sda_reg_reg_i_90_n_0),
        .O(i2c_sda_reg_reg_i_40_n_0));
  LUT6 #(
    .INIT(64'h4040555000000000)) 
    i2c_sda_reg_reg_i_41
       (.I0(i2c_sda_reg_reg_i_4_1),
        .I1(i2c_sda_reg_reg_i_91_n_0),
        .I2(reg_num[6]),
        .I3(i2c_sda_reg_reg_i_92_n_0),
        .I4(reg_num[7]),
        .I5(i2c_sda_reg_reg_i_93_n_0),
        .O(i2c_sda_reg_reg_i_41_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    i2c_sda_reg_reg_i_43
       (.I0(i2c_sda_reg_reg_i_94_n_0),
        .I1(i2c_sda_reg_reg_i_53_n_0),
        .I2(reg_num[6]),
        .I3(i2c_sda_reg_reg_i_95_n_0),
        .I4(reg_num[7]),
        .I5(i2c_sda_reg_reg_i_96_n_0),
        .O(i2c_sda_reg_reg_i_43_n_0));
  MUXF7 i2c_sda_reg_reg_i_44
       (.I0(i2c_sda_reg_reg_i_97_n_0),
        .I1(i2c_sda_reg_reg_i_98_n_0),
        .O(i2c_sda_reg_reg_i_44_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_45
       (.I0(i2c_sda_reg_reg_i_99_n_0),
        .I1(i2c_sda_reg_reg_i_100_n_0),
        .O(i2c_sda_reg_reg_i_45_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_46
       (.I0(i2c_sda_reg_reg_i_101_n_0),
        .I1(i2c_sda_reg_reg_i_102_n_0),
        .O(i2c_sda_reg_reg_i_46_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_47
       (.I0(i2c_sda_reg_reg_i_103_n_0),
        .I1(i2c_sda_reg_reg_i_104_n_0),
        .O(i2c_sda_reg_reg_i_47_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_48
       (.I0(i2c_sda_reg_reg_i_105_n_0),
        .I1(i2c_sda_reg_reg_i_106_n_0),
        .O(i2c_sda_reg_reg_i_48_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_49
       (.I0(i2c_sda_reg_reg_i_107_n_0),
        .I1(i2c_sda_reg_reg_i_108_n_0),
        .O(i2c_sda_reg_reg_i_49_n_0),
        .S(reg_num[7]));
  LUT6 #(
    .INIT(64'hFFE00000E0E00000)) 
    i2c_sda_reg_reg_i_5
       (.I0(i2c_sda_reg_reg_i_15_n_0),
        .I1(i2c_sda_reg_reg_i_16_n_0),
        .I2(Q[2]),
        .I3(i2c_sda_reg_reg_i_17_n_0),
        .I4(i2c_sda_reg_reg_i_4_0),
        .I5(Q[0]),
        .O(\FSM_onehot_state_reg[13] ));
  MUXF7 i2c_sda_reg_reg_i_50
       (.I0(i2c_sda_reg_reg_i_109_n_0),
        .I1(i2c_sda_reg_reg_i_110_n_0),
        .O(i2c_sda_reg_reg_i_50_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_51
       (.I0(i2c_sda_reg_reg_i_111_n_0),
        .I1(i2c_sda_reg_reg_i_112_n_0),
        .O(i2c_sda_reg_reg_i_51_n_0),
        .S(reg_num[7]));
  LUT6 #(
    .INIT(64'h555F575DEAAAAAAA)) 
    i2c_sda_reg_reg_i_52
       (.I0(reg_num[4]),
        .I1(reg_num[1]),
        .I2(reg_num[2]),
        .I3(reg_num[0]),
        .I4(reg_num[3]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_52_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    i2c_sda_reg_reg_i_53
       (.I0(reg_num[3]),
        .I1(reg_num[1]),
        .I2(reg_num[5]),
        .I3(reg_num[2]),
        .I4(reg_num[4]),
        .O(i2c_sda_reg_reg_i_53_n_0));
  LUT6 #(
    .INIT(64'hADBFAFBFFFFFFFFF)) 
    i2c_sda_reg_reg_i_54
       (.I0(reg_num[2]),
        .I1(reg_num[1]),
        .I2(reg_num[5]),
        .I3(reg_num[3]),
        .I4(reg_num[0]),
        .I5(reg_num[4]),
        .O(i2c_sda_reg_reg_i_54_n_0));
  LUT6 #(
    .INIT(64'h0200A8A215555555)) 
    i2c_sda_reg_reg_i_55
       (.I0(reg_num[4]),
        .I1(reg_num[1]),
        .I2(reg_num[2]),
        .I3(reg_num[0]),
        .I4(reg_num[3]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_55_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    i2c_sda_reg_reg_i_56
       (.I0(reg_num[3]),
        .I1(reg_num[5]),
        .I2(reg_num[1]),
        .I3(reg_num[2]),
        .I4(reg_num[4]),
        .O(i2c_sda_reg_reg_i_56_n_0));
  LUT6 #(
    .INIT(64'h4640444000000000)) 
    i2c_sda_reg_reg_i_57
       (.I0(reg_num[2]),
        .I1(reg_num[5]),
        .I2(reg_num[1]),
        .I3(reg_num[3]),
        .I4(reg_num[0]),
        .I5(reg_num[4]),
        .O(i2c_sda_reg_reg_i_57_n_0));
  LUT6 #(
    .INIT(64'h575F575DFFFFFFFF)) 
    i2c_sda_reg_reg_i_58
       (.I0(reg_num[5]),
        .I1(reg_num[1]),
        .I2(reg_num[2]),
        .I3(reg_num[0]),
        .I4(reg_num[3]),
        .I5(reg_num[4]),
        .O(i2c_sda_reg_reg_i_58_n_0));
  LUT6 #(
    .INIT(64'hFDBFAFBFFFFFFFFF)) 
    i2c_sda_reg_reg_i_59
       (.I0(reg_num[2]),
        .I1(reg_num[1]),
        .I2(reg_num[5]),
        .I3(reg_num[3]),
        .I4(reg_num[0]),
        .I5(reg_num[4]),
        .O(i2c_sda_reg_reg_i_59_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i2c_sda_reg_reg_i_60
       (.I0(reg_num[1]),
        .I1(reg_num[5]),
        .I2(reg_num[2]),
        .O(i2c_sda_reg_reg_i_60_n_0));
  MUXF7 i2c_sda_reg_reg_i_61
       (.I0(i2c_sda_reg_reg_i_113_n_0),
        .I1(i2c_sda_reg_reg_i_114_n_0),
        .O(i2c_sda_reg_reg_i_61_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_62
       (.I0(i2c_sda_reg_reg_i_115_n_0),
        .I1(i2c_sda_reg_reg_i_116_n_0),
        .O(i2c_sda_reg_reg_i_62_n_0),
        .S(reg_num[7]));
  LUT5 #(
    .INIT(32'hFF00FE00)) 
    i2c_sda_reg_reg_i_63
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[2]),
        .I3(reg_num[5]),
        .I4(reg_num[1]),
        .O(i2c_sda_reg_reg_i_63_n_0));
  LUT6 #(
    .INIT(64'h010000208002080A)) 
    i2c_sda_reg_reg_i_64
       (.I0(reg_num[4]),
        .I1(reg_num[0]),
        .I2(reg_num[5]),
        .I3(reg_num[1]),
        .I4(reg_num[2]),
        .I5(reg_num[3]),
        .O(i2c_sda_reg_reg_i_64_n_0));
  MUXF7 i2c_sda_reg_reg_i_65
       (.I0(i2c_sda_reg_reg_i_117_n_0),
        .I1(i2c_sda_reg_reg_i_118_n_0),
        .O(i2c_sda_reg_reg_i_65_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_66
       (.I0(i2c_sda_reg_reg_i_119_n_0),
        .I1(i2c_sda_reg_reg_i_120_n_0),
        .O(i2c_sda_reg_reg_i_66_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_67
       (.I0(i2c_sda_reg_reg_i_121_n_0),
        .I1(i2c_sda_reg_reg_i_122_n_0),
        .O(i2c_sda_reg_reg_i_67_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_68
       (.I0(i2c_sda_reg_reg_i_123_n_0),
        .I1(i2c_sda_reg_reg_i_124_n_0),
        .O(i2c_sda_reg_reg_i_68_n_0),
        .S(reg_num[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000001)) 
    i2c_sda_reg_reg_i_69
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[2]),
        .I3(reg_num[5]),
        .I4(reg_num[1]),
        .I5(reg_num[0]),
        .O(i2c_sda_reg_reg_i_69_n_0));
  LUT6 #(
    .INIT(64'h712D1E1E63811E5B)) 
    i2c_sda_reg_reg_i_70
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_70_n_0));
  LUT6 #(
    .INIT(64'h587A623E584FD1A3)) 
    i2c_sda_reg_reg_i_71
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[5]),
        .I4(reg_num[2]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_71_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF7FFF0000)) 
    i2c_sda_reg_reg_i_72
       (.I0(reg_num[1]),
        .I1(reg_num[2]),
        .I2(reg_num[3]),
        .I3(reg_num[4]),
        .I4(reg_num[0]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_72_n_0));
  MUXF7 i2c_sda_reg_reg_i_73
       (.I0(i2c_sda_reg_reg_i_125_n_0),
        .I1(i2c_sda_reg_reg_i_126_n_0),
        .O(i2c_sda_reg_reg_i_73_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_74
       (.I0(i2c_sda_reg_reg_i_127_n_0),
        .I1(i2c_sda_reg_reg_i_128_n_0),
        .O(i2c_sda_reg_reg_i_74_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_75
       (.I0(i2c_sda_reg_reg_i_129_n_0),
        .I1(i2c_sda_reg_reg_i_130_n_0),
        .O(i2c_sda_reg_reg_i_75_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_76
       (.I0(i2c_sda_reg_reg_i_131_n_0),
        .I1(i2c_sda_reg_reg_i_132_n_0),
        .O(i2c_sda_reg_reg_i_76_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_77
       (.I0(i2c_sda_reg_reg_i_133_n_0),
        .I1(i2c_sda_reg_reg_i_134_n_0),
        .O(i2c_sda_reg_reg_i_77_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_78
       (.I0(i2c_sda_reg_reg_i_135_n_0),
        .I1(i2c_sda_reg_reg_i_136_n_0),
        .O(i2c_sda_reg_reg_i_78_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_79
       (.I0(i2c_sda_reg_reg_i_137_n_0),
        .I1(i2c_sda_reg_reg_i_138_n_0),
        .O(i2c_sda_reg_reg_i_79_n_0),
        .S(reg_num[7]));
  LUT5 #(
    .INIT(32'h00AC0000)) 
    i2c_sda_reg_reg_i_8
       (.I0(i2c_sda_reg_reg_i_19_n_0),
        .I1(i2c_sda_reg_reg_i_20_n_0),
        .I2(i2c_sda_reg_reg_i_41_0),
        .I3(cfg_done_reg_0),
        .I4(i2c_sda_reg_reg_i_4_1),
        .O(i2c_sda_reg_reg_i_8_n_0));
  MUXF7 i2c_sda_reg_reg_i_80
       (.I0(i2c_sda_reg_reg_i_139_n_0),
        .I1(i2c_sda_reg_reg_i_140_n_0),
        .O(i2c_sda_reg_reg_i_80_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_81
       (.I0(i2c_sda_reg_reg_i_141_n_0),
        .I1(i2c_sda_reg_reg_i_142_n_0),
        .O(i2c_sda_reg_reg_i_81_n_0),
        .S(reg_num[7]));
  MUXF7 i2c_sda_reg_reg_i_82
       (.I0(i2c_sda_reg_reg_i_143_n_0),
        .I1(i2c_sda_reg_reg_i_144_n_0),
        .O(i2c_sda_reg_reg_i_82_n_0),
        .S(reg_num[7]));
  LUT6 #(
    .INIT(64'h115D64E65574CCEA)) 
    i2c_sda_reg_reg_i_84
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_84_n_0));
  LUT6 #(
    .INIT(64'hEA00FFFF80000000)) 
    i2c_sda_reg_reg_i_85
       (.I0(reg_num[2]),
        .I1(reg_num[0]),
        .I2(reg_num[1]),
        .I3(reg_num[3]),
        .I4(reg_num[4]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_85_n_0));
  LUT6 #(
    .INIT(64'h060024002C022401)) 
    i2c_sda_reg_reg_i_86
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[2]),
        .I3(reg_num[5]),
        .I4(reg_num[1]),
        .I5(reg_num[0]),
        .O(i2c_sda_reg_reg_i_86_n_0));
  LUT6 #(
    .INIT(64'h000066AA00283227)) 
    i2c_sda_reg_reg_i_88
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[1]),
        .I4(reg_num[5]),
        .I5(reg_num[2]),
        .O(i2c_sda_reg_reg_i_88_n_0));
  LUT6 #(
    .INIT(64'hFFFF00007FFFFFFF)) 
    i2c_sda_reg_reg_i_89
       (.I0(reg_num[0]),
        .I1(reg_num[1]),
        .I2(reg_num[2]),
        .I3(reg_num[3]),
        .I4(reg_num[4]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_89_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h00002320)) 
    i2c_sda_reg_reg_i_9
       (.I0(i2c_sda_reg_reg_i_21_n_0),
        .I1(cfg_done_reg_0),
        .I2(i2c_sda_reg_reg_i_41_0),
        .I3(i2c_sda_reg_reg_i_22_n_0),
        .I4(i2c_sda_reg_reg_i_4_1),
        .O(i2c_sda_reg_reg_i_9_n_0));
  LUT6 #(
    .INIT(64'h06000C0015541555)) 
    i2c_sda_reg_reg_i_90
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[2]),
        .I3(reg_num[1]),
        .I4(reg_num[0]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_90_n_0));
  LUT6 #(
    .INIT(64'h0F108F0FF0F0F0C0)) 
    i2c_sda_reg_reg_i_91
       (.I0(reg_num[0]),
        .I1(reg_num[1]),
        .I2(reg_num[4]),
        .I3(reg_num[2]),
        .I4(reg_num[3]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_91_n_0));
  LUT6 #(
    .INIT(64'hDD448A00DC472200)) 
    i2c_sda_reg_reg_i_92
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_92_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h1)) 
    i2c_sda_reg_reg_i_93
       (.I0(cfg_done_reg_0),
        .I1(i2c_sda_reg_reg_i_41_0),
        .O(i2c_sda_reg_reg_i_93_n_0));
  LUT6 #(
    .INIT(64'h3438383003333330)) 
    i2c_sda_reg_reg_i_94
       (.I0(reg_num[0]),
        .I1(reg_num[4]),
        .I2(reg_num[3]),
        .I3(reg_num[2]),
        .I4(reg_num[1]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_94_n_0));
  LUT6 #(
    .INIT(64'h222A2A2A00000000)) 
    i2c_sda_reg_reg_i_95
       (.I0(reg_num[5]),
        .I1(reg_num[3]),
        .I2(reg_num[2]),
        .I3(reg_num[0]),
        .I4(reg_num[1]),
        .I5(reg_num[4]),
        .O(i2c_sda_reg_reg_i_95_n_0));
  LUT6 #(
    .INIT(64'h88891139ECCCE6EA)) 
    i2c_sda_reg_reg_i_96
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[1]),
        .I4(reg_num[2]),
        .I5(reg_num[5]),
        .O(i2c_sda_reg_reg_i_96_n_0));
  LUT6 #(
    .INIT(64'h5A7160A03CB511C3)) 
    i2c_sda_reg_reg_i_97
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[5]),
        .I4(reg_num[2]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_97_n_0));
  LUT6 #(
    .INIT(64'h4B584C34D27E26F1)) 
    i2c_sda_reg_reg_i_98
       (.I0(reg_num[4]),
        .I1(reg_num[3]),
        .I2(reg_num[0]),
        .I3(reg_num[2]),
        .I4(reg_num[5]),
        .I5(reg_num[1]),
        .O(i2c_sda_reg_reg_i_98_n_0));
  LUT6 #(
    .INIT(64'h0000000005141404)) 
    i2c_sda_reg_reg_i_99
       (.I0(reg_num[3]),
        .I1(reg_num[0]),
        .I2(reg_num[2]),
        .I3(reg_num[5]),
        .I4(reg_num[1]),
        .I5(reg_num[4]),
        .O(i2c_sda_reg_reg_i_99_n_0));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg[0] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[0]_i_1_n_0 ),
        .Q(reg_num_reg[0]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg[1] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[1]_i_1_n_0 ),
        .Q(reg_num_reg[1]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg[2] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[2]_i_1_n_0 ),
        .Q(reg_num_reg[2]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg[3] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[3]_i_1_n_0 ),
        .Q(reg_num_reg[3]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg[4] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[4]_i_1_n_0 ),
        .Q(reg_num_reg[4]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg[5] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[5]_i_1_n_0 ),
        .Q(reg_num_reg[5]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg[6] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[6]_i_1_n_0 ),
        .Q(reg_num_reg[6]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg[7] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[7]_i_1_n_0 ),
        .Q(reg_num_reg[7]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg_rep[0] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[0]_i_1_n_0 ),
        .Q(reg_num[0]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg_rep[1] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[1]_i_1_n_0 ),
        .Q(reg_num[1]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg_rep[2] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[2]_i_1_n_0 ),
        .Q(reg_num[2]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg_rep[3] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[3]_i_1_n_0 ),
        .Q(reg_num[3]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg_rep[4] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[4]_i_1_n_0 ),
        .Q(reg_num[4]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg_rep[5] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[5]_i_1_n_0 ),
        .Q(reg_num[5]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg_rep[6] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[6]_i_1_n_0 ),
        .Q(reg_num[6]));
  (* equivalent_register_removal = "no" *) 
  FDCE \reg_num_reg_rep[7] 
       (.C(i2c_clk),
        .CE(E),
        .CLR(sys_rst_n_0),
        .D(\reg_num_rep[7]_i_1_n_0 ),
        .Q(reg_num[7]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \reg_num_rep[0]_i_1 
       (.I0(reg_num_reg[0]),
        .O(\reg_num_rep[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \reg_num_rep[1]_i_1 
       (.I0(reg_num_reg[0]),
        .I1(reg_num_reg[1]),
        .O(\reg_num_rep[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \reg_num_rep[2]_i_1 
       (.I0(reg_num_reg[1]),
        .I1(reg_num_reg[0]),
        .I2(reg_num_reg[2]),
        .O(\reg_num_rep[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \reg_num_rep[3]_i_1 
       (.I0(reg_num_reg[2]),
        .I1(reg_num_reg[0]),
        .I2(reg_num_reg[1]),
        .I3(reg_num_reg[3]),
        .O(\reg_num_rep[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \reg_num_rep[4]_i_1 
       (.I0(reg_num_reg[3]),
        .I1(reg_num_reg[1]),
        .I2(reg_num_reg[0]),
        .I3(reg_num_reg[2]),
        .I4(reg_num_reg[4]),
        .O(\reg_num_rep[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \reg_num_rep[5]_i_1 
       (.I0(reg_num_reg[1]),
        .I1(reg_num_reg[0]),
        .I2(reg_num_reg[2]),
        .I3(reg_num_reg[3]),
        .I4(reg_num_reg[4]),
        .I5(reg_num_reg[5]),
        .O(\reg_num_rep[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hBFFF4000)) 
    \reg_num_rep[6]_i_1 
       (.I0(\reg_num_rep[7]_i_2_n_0 ),
        .I1(reg_num_reg[4]),
        .I2(reg_num_reg[3]),
        .I3(reg_num_reg[5]),
        .I4(reg_num_reg[6]),
        .O(\reg_num_rep[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFFFFF40000000)) 
    \reg_num_rep[7]_i_1 
       (.I0(\reg_num_rep[7]_i_2_n_0 ),
        .I1(reg_num_reg[5]),
        .I2(reg_num_reg[3]),
        .I3(reg_num_reg[4]),
        .I4(reg_num_reg[6]),
        .I5(reg_num_reg[7]),
        .O(\reg_num_rep[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \reg_num_rep[7]_i_2 
       (.I0(reg_num_reg[1]),
        .I1(reg_num_reg[0]),
        .I2(reg_num_reg[2]),
        .O(\reg_num_rep[7]_i_2_n_0 ));
endmodule

(* ORIG_REF_NAME = "ov5640_data_in" *) 
module system_ov5640_top_0_0_ov5640_data_in
   (ov5640_wr_en,
    ov5640_data_out,
    ov5640_pclk,
    sys_rst_n,
    sys_init_done,
    ov5640_href,
    ov5640_data);
  output ov5640_wr_en;
  output [15:0]ov5640_data_out;
  input ov5640_pclk;
  input sys_rst_n;
  input sys_init_done;
  input ov5640_href;
  input [7:0]ov5640_data;

  wire data_flag;
  wire data_flag_dly1_i_1_n_0;
  wire data_flag_reg_n_0;
  wire data_out_reg;
  wire [7:0]ov5640_data;
  wire [15:0]ov5640_data_out;
  wire ov5640_href;
  wire ov5640_pclk;
  wire ov5640_wr_en;
  wire [7:0]pic_data_reg;
  wire \pic_data_reg[0]_i_1_n_0 ;
  wire \pic_data_reg[1]_i_1_n_0 ;
  wire \pic_data_reg[2]_i_1_n_0 ;
  wire \pic_data_reg[3]_i_1_n_0 ;
  wire \pic_data_reg[4]_i_1_n_0 ;
  wire \pic_data_reg[5]_i_1_n_0 ;
  wire \pic_data_reg[6]_i_1_n_0 ;
  wire \pic_data_reg[7]_i_1_n_0 ;
  wire sys_init_done;
  wire sys_rst_n;

  LUT2 #(
    .INIT(4'h7)) 
    data_flag_dly1_i_1
       (.I0(sys_rst_n),
        .I1(sys_init_done),
        .O(data_flag_dly1_i_1_n_0));
  FDCE data_flag_dly1_reg
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(data_flag_reg_n_0),
        .Q(ov5640_wr_en));
  LUT2 #(
    .INIT(4'h2)) 
    data_flag_i_1
       (.I0(ov5640_href),
        .I1(data_flag_reg_n_0),
        .O(data_flag));
  FDCE data_flag_reg
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(data_flag),
        .Q(data_flag_reg_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    \data_out_reg[15]_i_1 
       (.I0(ov5640_href),
        .I1(data_flag_reg_n_0),
        .O(data_out_reg));
  FDCE \data_out_reg_reg[0] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(ov5640_data[0]),
        .Q(ov5640_data_out[0]));
  FDCE \data_out_reg_reg[10] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(pic_data_reg[2]),
        .Q(ov5640_data_out[10]));
  FDCE \data_out_reg_reg[11] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(pic_data_reg[3]),
        .Q(ov5640_data_out[11]));
  FDCE \data_out_reg_reg[12] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(pic_data_reg[4]),
        .Q(ov5640_data_out[12]));
  FDCE \data_out_reg_reg[13] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(pic_data_reg[5]),
        .Q(ov5640_data_out[13]));
  FDCE \data_out_reg_reg[14] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(pic_data_reg[6]),
        .Q(ov5640_data_out[14]));
  FDCE \data_out_reg_reg[15] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(pic_data_reg[7]),
        .Q(ov5640_data_out[15]));
  FDCE \data_out_reg_reg[1] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(ov5640_data[1]),
        .Q(ov5640_data_out[1]));
  FDCE \data_out_reg_reg[2] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(ov5640_data[2]),
        .Q(ov5640_data_out[2]));
  FDCE \data_out_reg_reg[3] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(ov5640_data[3]),
        .Q(ov5640_data_out[3]));
  FDCE \data_out_reg_reg[4] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(ov5640_data[4]),
        .Q(ov5640_data_out[4]));
  FDCE \data_out_reg_reg[5] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(ov5640_data[5]),
        .Q(ov5640_data_out[5]));
  FDCE \data_out_reg_reg[6] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(ov5640_data[6]),
        .Q(ov5640_data_out[6]));
  FDCE \data_out_reg_reg[7] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(ov5640_data[7]),
        .Q(ov5640_data_out[7]));
  FDCE \data_out_reg_reg[8] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(pic_data_reg[0]),
        .Q(ov5640_data_out[8]));
  FDCE \data_out_reg_reg[9] 
       (.C(ov5640_pclk),
        .CE(data_out_reg),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(pic_data_reg[1]),
        .Q(ov5640_data_out[9]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pic_data_reg[0]_i_1 
       (.I0(ov5640_href),
        .I1(ov5640_data[0]),
        .O(\pic_data_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pic_data_reg[1]_i_1 
       (.I0(ov5640_href),
        .I1(ov5640_data[1]),
        .O(\pic_data_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pic_data_reg[2]_i_1 
       (.I0(ov5640_href),
        .I1(ov5640_data[2]),
        .O(\pic_data_reg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pic_data_reg[3]_i_1 
       (.I0(ov5640_href),
        .I1(ov5640_data[3]),
        .O(\pic_data_reg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pic_data_reg[4]_i_1 
       (.I0(ov5640_href),
        .I1(ov5640_data[4]),
        .O(\pic_data_reg[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pic_data_reg[5]_i_1 
       (.I0(ov5640_href),
        .I1(ov5640_data[5]),
        .O(\pic_data_reg[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pic_data_reg[6]_i_1 
       (.I0(ov5640_href),
        .I1(ov5640_data[6]),
        .O(\pic_data_reg[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pic_data_reg[7]_i_1 
       (.I0(ov5640_href),
        .I1(ov5640_data[7]),
        .O(\pic_data_reg[7]_i_1_n_0 ));
  FDCE \pic_data_reg_reg[0] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(\pic_data_reg[0]_i_1_n_0 ),
        .Q(pic_data_reg[0]));
  FDCE \pic_data_reg_reg[1] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(\pic_data_reg[1]_i_1_n_0 ),
        .Q(pic_data_reg[1]));
  FDCE \pic_data_reg_reg[2] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(\pic_data_reg[2]_i_1_n_0 ),
        .Q(pic_data_reg[2]));
  FDCE \pic_data_reg_reg[3] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(\pic_data_reg[3]_i_1_n_0 ),
        .Q(pic_data_reg[3]));
  FDCE \pic_data_reg_reg[4] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(\pic_data_reg[4]_i_1_n_0 ),
        .Q(pic_data_reg[4]));
  FDCE \pic_data_reg_reg[5] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(\pic_data_reg[5]_i_1_n_0 ),
        .Q(pic_data_reg[5]));
  FDCE \pic_data_reg_reg[6] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(\pic_data_reg[6]_i_1_n_0 ),
        .Q(pic_data_reg[6]));
  FDCE \pic_data_reg_reg[7] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(data_flag_dly1_i_1_n_0),
        .D(\pic_data_reg[7]_i_1_n_0 ),
        .Q(pic_data_reg[7]));
endmodule

(* ORIG_REF_NAME = "ov5640_top" *) 
module system_ov5640_top_0_0_ov5640_top
   (href_v_cnt,
    ov5640_wr_en,
    ov5640_data_out,
    cfg_done_reg,
    sccb_scl,
    href_cnt,
    sccb_sda,
    sys_rst_n,
    sys_init_done,
    ov5640_vsync,
    ov5640_href,
    ov5640_data,
    sys_clk,
    ov5640_pclk);
  output [31:0]href_v_cnt;
  output ov5640_wr_en;
  output [15:0]ov5640_data_out;
  output cfg_done_reg;
  output sccb_scl;
  output [31:0]href_cnt;
  inout sccb_sda;
  input sys_rst_n;
  input sys_init_done;
  input ov5640_vsync;
  input ov5640_href;
  input [7:0]ov5640_data;
  input sys_clk;
  input ov5640_pclk;

  wire cfg_done_reg;
  wire cfg_start;
  wire [31:0]href_cnt;
  wire \href_cnt[11]_i_2_n_0 ;
  wire \href_cnt[11]_i_3_n_0 ;
  wire \href_cnt[11]_i_4_n_0 ;
  wire \href_cnt[11]_i_5_n_0 ;
  wire \href_cnt[15]_i_2_n_0 ;
  wire \href_cnt[15]_i_3_n_0 ;
  wire \href_cnt[15]_i_4_n_0 ;
  wire \href_cnt[15]_i_5_n_0 ;
  wire \href_cnt[19]_i_2_n_0 ;
  wire \href_cnt[19]_i_3_n_0 ;
  wire \href_cnt[19]_i_4_n_0 ;
  wire \href_cnt[19]_i_5_n_0 ;
  wire \href_cnt[23]_i_2_n_0 ;
  wire \href_cnt[23]_i_3_n_0 ;
  wire \href_cnt[23]_i_4_n_0 ;
  wire \href_cnt[23]_i_5_n_0 ;
  wire \href_cnt[27]_i_2_n_0 ;
  wire \href_cnt[27]_i_3_n_0 ;
  wire \href_cnt[27]_i_4_n_0 ;
  wire \href_cnt[27]_i_5_n_0 ;
  wire \href_cnt[31]_i_2_n_0 ;
  wire \href_cnt[31]_i_3_n_0 ;
  wire \href_cnt[31]_i_4_n_0 ;
  wire \href_cnt[31]_i_5_n_0 ;
  wire \href_cnt[3]_i_2_n_0 ;
  wire \href_cnt[3]_i_3_n_0 ;
  wire \href_cnt[3]_i_4_n_0 ;
  wire \href_cnt[3]_i_5_n_0 ;
  wire \href_cnt[7]_i_2_n_0 ;
  wire \href_cnt[7]_i_3_n_0 ;
  wire \href_cnt[7]_i_4_n_0 ;
  wire \href_cnt[7]_i_5_n_0 ;
  wire \href_cnt_reg[11]_i_1_n_0 ;
  wire \href_cnt_reg[11]_i_1_n_1 ;
  wire \href_cnt_reg[11]_i_1_n_2 ;
  wire \href_cnt_reg[11]_i_1_n_3 ;
  wire \href_cnt_reg[11]_i_1_n_4 ;
  wire \href_cnt_reg[11]_i_1_n_5 ;
  wire \href_cnt_reg[11]_i_1_n_6 ;
  wire \href_cnt_reg[11]_i_1_n_7 ;
  wire \href_cnt_reg[15]_i_1_n_0 ;
  wire \href_cnt_reg[15]_i_1_n_1 ;
  wire \href_cnt_reg[15]_i_1_n_2 ;
  wire \href_cnt_reg[15]_i_1_n_3 ;
  wire \href_cnt_reg[15]_i_1_n_4 ;
  wire \href_cnt_reg[15]_i_1_n_5 ;
  wire \href_cnt_reg[15]_i_1_n_6 ;
  wire \href_cnt_reg[15]_i_1_n_7 ;
  wire \href_cnt_reg[19]_i_1_n_0 ;
  wire \href_cnt_reg[19]_i_1_n_1 ;
  wire \href_cnt_reg[19]_i_1_n_2 ;
  wire \href_cnt_reg[19]_i_1_n_3 ;
  wire \href_cnt_reg[19]_i_1_n_4 ;
  wire \href_cnt_reg[19]_i_1_n_5 ;
  wire \href_cnt_reg[19]_i_1_n_6 ;
  wire \href_cnt_reg[19]_i_1_n_7 ;
  wire \href_cnt_reg[23]_i_1_n_0 ;
  wire \href_cnt_reg[23]_i_1_n_1 ;
  wire \href_cnt_reg[23]_i_1_n_2 ;
  wire \href_cnt_reg[23]_i_1_n_3 ;
  wire \href_cnt_reg[23]_i_1_n_4 ;
  wire \href_cnt_reg[23]_i_1_n_5 ;
  wire \href_cnt_reg[23]_i_1_n_6 ;
  wire \href_cnt_reg[23]_i_1_n_7 ;
  wire \href_cnt_reg[27]_i_1_n_0 ;
  wire \href_cnt_reg[27]_i_1_n_1 ;
  wire \href_cnt_reg[27]_i_1_n_2 ;
  wire \href_cnt_reg[27]_i_1_n_3 ;
  wire \href_cnt_reg[27]_i_1_n_4 ;
  wire \href_cnt_reg[27]_i_1_n_5 ;
  wire \href_cnt_reg[27]_i_1_n_6 ;
  wire \href_cnt_reg[27]_i_1_n_7 ;
  wire \href_cnt_reg[31]_i_1_n_1 ;
  wire \href_cnt_reg[31]_i_1_n_2 ;
  wire \href_cnt_reg[31]_i_1_n_3 ;
  wire \href_cnt_reg[31]_i_1_n_4 ;
  wire \href_cnt_reg[31]_i_1_n_5 ;
  wire \href_cnt_reg[31]_i_1_n_6 ;
  wire \href_cnt_reg[31]_i_1_n_7 ;
  wire \href_cnt_reg[3]_i_1_n_0 ;
  wire \href_cnt_reg[3]_i_1_n_1 ;
  wire \href_cnt_reg[3]_i_1_n_2 ;
  wire \href_cnt_reg[3]_i_1_n_3 ;
  wire \href_cnt_reg[3]_i_1_n_4 ;
  wire \href_cnt_reg[3]_i_1_n_5 ;
  wire \href_cnt_reg[3]_i_1_n_6 ;
  wire \href_cnt_reg[3]_i_1_n_7 ;
  wire \href_cnt_reg[7]_i_1_n_0 ;
  wire \href_cnt_reg[7]_i_1_n_1 ;
  wire \href_cnt_reg[7]_i_1_n_2 ;
  wire \href_cnt_reg[7]_i_1_n_3 ;
  wire \href_cnt_reg[7]_i_1_n_4 ;
  wire \href_cnt_reg[7]_i_1_n_5 ;
  wire \href_cnt_reg[7]_i_1_n_6 ;
  wire \href_cnt_reg[7]_i_1_n_7 ;
  wire href_reg;
  wire [31:0]href_v_cnt;
  wire \href_v_cnt[11]_i_2_n_0 ;
  wire \href_v_cnt[11]_i_3_n_0 ;
  wire \href_v_cnt[11]_i_4_n_0 ;
  wire \href_v_cnt[11]_i_5_n_0 ;
  wire \href_v_cnt[15]_i_2_n_0 ;
  wire \href_v_cnt[15]_i_3_n_0 ;
  wire \href_v_cnt[15]_i_4_n_0 ;
  wire \href_v_cnt[15]_i_5_n_0 ;
  wire \href_v_cnt[19]_i_2_n_0 ;
  wire \href_v_cnt[19]_i_3_n_0 ;
  wire \href_v_cnt[19]_i_4_n_0 ;
  wire \href_v_cnt[19]_i_5_n_0 ;
  wire \href_v_cnt[23]_i_2_n_0 ;
  wire \href_v_cnt[23]_i_3_n_0 ;
  wire \href_v_cnt[23]_i_4_n_0 ;
  wire \href_v_cnt[23]_i_5_n_0 ;
  wire \href_v_cnt[27]_i_2_n_0 ;
  wire \href_v_cnt[27]_i_3_n_0 ;
  wire \href_v_cnt[27]_i_4_n_0 ;
  wire \href_v_cnt[27]_i_5_n_0 ;
  wire \href_v_cnt[31]_i_1_n_0 ;
  wire \href_v_cnt[31]_i_3_n_0 ;
  wire \href_v_cnt[31]_i_4_n_0 ;
  wire \href_v_cnt[31]_i_5_n_0 ;
  wire \href_v_cnt[31]_i_6_n_0 ;
  wire \href_v_cnt[3]_i_2_n_0 ;
  wire \href_v_cnt[3]_i_3_n_0 ;
  wire \href_v_cnt[3]_i_4_n_0 ;
  wire \href_v_cnt[3]_i_5_n_0 ;
  wire \href_v_cnt[3]_i_6_n_0 ;
  wire \href_v_cnt[7]_i_2_n_0 ;
  wire \href_v_cnt[7]_i_3_n_0 ;
  wire \href_v_cnt[7]_i_4_n_0 ;
  wire \href_v_cnt[7]_i_5_n_0 ;
  wire \href_v_cnt_reg[11]_i_1_n_0 ;
  wire \href_v_cnt_reg[11]_i_1_n_1 ;
  wire \href_v_cnt_reg[11]_i_1_n_2 ;
  wire \href_v_cnt_reg[11]_i_1_n_3 ;
  wire \href_v_cnt_reg[11]_i_1_n_4 ;
  wire \href_v_cnt_reg[11]_i_1_n_5 ;
  wire \href_v_cnt_reg[11]_i_1_n_6 ;
  wire \href_v_cnt_reg[11]_i_1_n_7 ;
  wire \href_v_cnt_reg[15]_i_1_n_0 ;
  wire \href_v_cnt_reg[15]_i_1_n_1 ;
  wire \href_v_cnt_reg[15]_i_1_n_2 ;
  wire \href_v_cnt_reg[15]_i_1_n_3 ;
  wire \href_v_cnt_reg[15]_i_1_n_4 ;
  wire \href_v_cnt_reg[15]_i_1_n_5 ;
  wire \href_v_cnt_reg[15]_i_1_n_6 ;
  wire \href_v_cnt_reg[15]_i_1_n_7 ;
  wire \href_v_cnt_reg[19]_i_1_n_0 ;
  wire \href_v_cnt_reg[19]_i_1_n_1 ;
  wire \href_v_cnt_reg[19]_i_1_n_2 ;
  wire \href_v_cnt_reg[19]_i_1_n_3 ;
  wire \href_v_cnt_reg[19]_i_1_n_4 ;
  wire \href_v_cnt_reg[19]_i_1_n_5 ;
  wire \href_v_cnt_reg[19]_i_1_n_6 ;
  wire \href_v_cnt_reg[19]_i_1_n_7 ;
  wire \href_v_cnt_reg[23]_i_1_n_0 ;
  wire \href_v_cnt_reg[23]_i_1_n_1 ;
  wire \href_v_cnt_reg[23]_i_1_n_2 ;
  wire \href_v_cnt_reg[23]_i_1_n_3 ;
  wire \href_v_cnt_reg[23]_i_1_n_4 ;
  wire \href_v_cnt_reg[23]_i_1_n_5 ;
  wire \href_v_cnt_reg[23]_i_1_n_6 ;
  wire \href_v_cnt_reg[23]_i_1_n_7 ;
  wire \href_v_cnt_reg[27]_i_1_n_0 ;
  wire \href_v_cnt_reg[27]_i_1_n_1 ;
  wire \href_v_cnt_reg[27]_i_1_n_2 ;
  wire \href_v_cnt_reg[27]_i_1_n_3 ;
  wire \href_v_cnt_reg[27]_i_1_n_4 ;
  wire \href_v_cnt_reg[27]_i_1_n_5 ;
  wire \href_v_cnt_reg[27]_i_1_n_6 ;
  wire \href_v_cnt_reg[27]_i_1_n_7 ;
  wire \href_v_cnt_reg[31]_i_2_n_1 ;
  wire \href_v_cnt_reg[31]_i_2_n_2 ;
  wire \href_v_cnt_reg[31]_i_2_n_3 ;
  wire \href_v_cnt_reg[31]_i_2_n_4 ;
  wire \href_v_cnt_reg[31]_i_2_n_5 ;
  wire \href_v_cnt_reg[31]_i_2_n_6 ;
  wire \href_v_cnt_reg[31]_i_2_n_7 ;
  wire \href_v_cnt_reg[3]_i_1_n_0 ;
  wire \href_v_cnt_reg[3]_i_1_n_1 ;
  wire \href_v_cnt_reg[3]_i_1_n_2 ;
  wire \href_v_cnt_reg[3]_i_1_n_3 ;
  wire \href_v_cnt_reg[3]_i_1_n_4 ;
  wire \href_v_cnt_reg[3]_i_1_n_5 ;
  wire \href_v_cnt_reg[3]_i_1_n_6 ;
  wire \href_v_cnt_reg[3]_i_1_n_7 ;
  wire \href_v_cnt_reg[7]_i_1_n_0 ;
  wire \href_v_cnt_reg[7]_i_1_n_1 ;
  wire \href_v_cnt_reg[7]_i_1_n_2 ;
  wire \href_v_cnt_reg[7]_i_1_n_3 ;
  wire \href_v_cnt_reg[7]_i_1_n_4 ;
  wire \href_v_cnt_reg[7]_i_1_n_5 ;
  wire \href_v_cnt_reg[7]_i_1_n_6 ;
  wire \href_v_cnt_reg[7]_i_1_n_7 ;
  wire i2c_clk;
  wire i2c_end;
  wire iic_ctrl_n_10;
  wire iic_ctrl_n_11;
  wire iic_ctrl_n_2;
  wire iic_ctrl_n_3;
  wire iic_ctrl_n_4;
  wire iic_ctrl_n_5;
  wire iic_ctrl_n_6;
  wire iic_ctrl_n_7;
  wire iic_ctrl_n_8;
  wire ov5640_cfg_n_1;
  wire ov5640_cfg_n_3;
  wire ov5640_cfg_n_4;
  wire ov5640_cfg_n_5;
  wire [7:0]ov5640_data;
  wire [15:0]ov5640_data_out;
  wire ov5640_href;
  wire ov5640_pclk;
  wire ov5640_vsync;
  wire ov5640_wr_en;
  wire sccb_scl;
  wire sccb_sda;
  wire sys_clk;
  wire sys_init_done;
  wire sys_rst_n;
  wire [3:3]\NLW_href_cnt_reg[31]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_href_v_cnt_reg[31]_i_2_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[11]_i_2 
       (.I0(ov5640_href),
        .I1(href_cnt[11]),
        .O(\href_cnt[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[11]_i_3 
       (.I0(ov5640_href),
        .I1(href_cnt[10]),
        .O(\href_cnt[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[11]_i_4 
       (.I0(ov5640_href),
        .I1(href_cnt[9]),
        .O(\href_cnt[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[11]_i_5 
       (.I0(ov5640_href),
        .I1(href_cnt[8]),
        .O(\href_cnt[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[15]_i_2 
       (.I0(ov5640_href),
        .I1(href_cnt[15]),
        .O(\href_cnt[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[15]_i_3 
       (.I0(ov5640_href),
        .I1(href_cnt[14]),
        .O(\href_cnt[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[15]_i_4 
       (.I0(ov5640_href),
        .I1(href_cnt[13]),
        .O(\href_cnt[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[15]_i_5 
       (.I0(ov5640_href),
        .I1(href_cnt[12]),
        .O(\href_cnt[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[19]_i_2 
       (.I0(ov5640_href),
        .I1(href_cnt[19]),
        .O(\href_cnt[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[19]_i_3 
       (.I0(ov5640_href),
        .I1(href_cnt[18]),
        .O(\href_cnt[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[19]_i_4 
       (.I0(ov5640_href),
        .I1(href_cnt[17]),
        .O(\href_cnt[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[19]_i_5 
       (.I0(ov5640_href),
        .I1(href_cnt[16]),
        .O(\href_cnt[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[23]_i_2 
       (.I0(ov5640_href),
        .I1(href_cnt[23]),
        .O(\href_cnt[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[23]_i_3 
       (.I0(ov5640_href),
        .I1(href_cnt[22]),
        .O(\href_cnt[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[23]_i_4 
       (.I0(ov5640_href),
        .I1(href_cnt[21]),
        .O(\href_cnt[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[23]_i_5 
       (.I0(ov5640_href),
        .I1(href_cnt[20]),
        .O(\href_cnt[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[27]_i_2 
       (.I0(ov5640_href),
        .I1(href_cnt[27]),
        .O(\href_cnt[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[27]_i_3 
       (.I0(ov5640_href),
        .I1(href_cnt[26]),
        .O(\href_cnt[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[27]_i_4 
       (.I0(ov5640_href),
        .I1(href_cnt[25]),
        .O(\href_cnt[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[27]_i_5 
       (.I0(ov5640_href),
        .I1(href_cnt[24]),
        .O(\href_cnt[27]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[31]_i_2 
       (.I0(ov5640_href),
        .I1(href_cnt[31]),
        .O(\href_cnt[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[31]_i_3 
       (.I0(ov5640_href),
        .I1(href_cnt[30]),
        .O(\href_cnt[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[31]_i_4 
       (.I0(ov5640_href),
        .I1(href_cnt[29]),
        .O(\href_cnt[31]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[31]_i_5 
       (.I0(ov5640_href),
        .I1(href_cnt[28]),
        .O(\href_cnt[31]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[3]_i_2 
       (.I0(ov5640_href),
        .I1(href_cnt[3]),
        .O(\href_cnt[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[3]_i_3 
       (.I0(ov5640_href),
        .I1(href_cnt[2]),
        .O(\href_cnt[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[3]_i_4 
       (.I0(ov5640_href),
        .I1(href_cnt[1]),
        .O(\href_cnt[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \href_cnt[3]_i_5 
       (.I0(href_cnt[0]),
        .I1(ov5640_href),
        .O(\href_cnt[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[7]_i_2 
       (.I0(ov5640_href),
        .I1(href_cnt[7]),
        .O(\href_cnt[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[7]_i_3 
       (.I0(ov5640_href),
        .I1(href_cnt[6]),
        .O(\href_cnt[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[7]_i_4 
       (.I0(ov5640_href),
        .I1(href_cnt[5]),
        .O(\href_cnt[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \href_cnt[7]_i_5 
       (.I0(ov5640_href),
        .I1(href_cnt[4]),
        .O(\href_cnt[7]_i_5_n_0 ));
  FDCE \href_cnt_reg[0] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[3]_i_1_n_7 ),
        .Q(href_cnt[0]));
  FDCE \href_cnt_reg[10] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[11]_i_1_n_5 ),
        .Q(href_cnt[10]));
  FDCE \href_cnt_reg[11] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[11]_i_1_n_4 ),
        .Q(href_cnt[11]));
  CARRY4 \href_cnt_reg[11]_i_1 
       (.CI(\href_cnt_reg[7]_i_1_n_0 ),
        .CO({\href_cnt_reg[11]_i_1_n_0 ,\href_cnt_reg[11]_i_1_n_1 ,\href_cnt_reg[11]_i_1_n_2 ,\href_cnt_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_cnt_reg[11]_i_1_n_4 ,\href_cnt_reg[11]_i_1_n_5 ,\href_cnt_reg[11]_i_1_n_6 ,\href_cnt_reg[11]_i_1_n_7 }),
        .S({\href_cnt[11]_i_2_n_0 ,\href_cnt[11]_i_3_n_0 ,\href_cnt[11]_i_4_n_0 ,\href_cnt[11]_i_5_n_0 }));
  FDCE \href_cnt_reg[12] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[15]_i_1_n_7 ),
        .Q(href_cnt[12]));
  FDCE \href_cnt_reg[13] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[15]_i_1_n_6 ),
        .Q(href_cnt[13]));
  FDCE \href_cnt_reg[14] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[15]_i_1_n_5 ),
        .Q(href_cnt[14]));
  FDCE \href_cnt_reg[15] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[15]_i_1_n_4 ),
        .Q(href_cnt[15]));
  CARRY4 \href_cnt_reg[15]_i_1 
       (.CI(\href_cnt_reg[11]_i_1_n_0 ),
        .CO({\href_cnt_reg[15]_i_1_n_0 ,\href_cnt_reg[15]_i_1_n_1 ,\href_cnt_reg[15]_i_1_n_2 ,\href_cnt_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_cnt_reg[15]_i_1_n_4 ,\href_cnt_reg[15]_i_1_n_5 ,\href_cnt_reg[15]_i_1_n_6 ,\href_cnt_reg[15]_i_1_n_7 }),
        .S({\href_cnt[15]_i_2_n_0 ,\href_cnt[15]_i_3_n_0 ,\href_cnt[15]_i_4_n_0 ,\href_cnt[15]_i_5_n_0 }));
  FDCE \href_cnt_reg[16] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[19]_i_1_n_7 ),
        .Q(href_cnt[16]));
  FDCE \href_cnt_reg[17] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[19]_i_1_n_6 ),
        .Q(href_cnt[17]));
  FDCE \href_cnt_reg[18] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[19]_i_1_n_5 ),
        .Q(href_cnt[18]));
  FDCE \href_cnt_reg[19] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[19]_i_1_n_4 ),
        .Q(href_cnt[19]));
  CARRY4 \href_cnt_reg[19]_i_1 
       (.CI(\href_cnt_reg[15]_i_1_n_0 ),
        .CO({\href_cnt_reg[19]_i_1_n_0 ,\href_cnt_reg[19]_i_1_n_1 ,\href_cnt_reg[19]_i_1_n_2 ,\href_cnt_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_cnt_reg[19]_i_1_n_4 ,\href_cnt_reg[19]_i_1_n_5 ,\href_cnt_reg[19]_i_1_n_6 ,\href_cnt_reg[19]_i_1_n_7 }),
        .S({\href_cnt[19]_i_2_n_0 ,\href_cnt[19]_i_3_n_0 ,\href_cnt[19]_i_4_n_0 ,\href_cnt[19]_i_5_n_0 }));
  FDCE \href_cnt_reg[1] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[3]_i_1_n_6 ),
        .Q(href_cnt[1]));
  FDCE \href_cnt_reg[20] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[23]_i_1_n_7 ),
        .Q(href_cnt[20]));
  FDCE \href_cnt_reg[21] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[23]_i_1_n_6 ),
        .Q(href_cnt[21]));
  FDCE \href_cnt_reg[22] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[23]_i_1_n_5 ),
        .Q(href_cnt[22]));
  FDCE \href_cnt_reg[23] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[23]_i_1_n_4 ),
        .Q(href_cnt[23]));
  CARRY4 \href_cnt_reg[23]_i_1 
       (.CI(\href_cnt_reg[19]_i_1_n_0 ),
        .CO({\href_cnt_reg[23]_i_1_n_0 ,\href_cnt_reg[23]_i_1_n_1 ,\href_cnt_reg[23]_i_1_n_2 ,\href_cnt_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_cnt_reg[23]_i_1_n_4 ,\href_cnt_reg[23]_i_1_n_5 ,\href_cnt_reg[23]_i_1_n_6 ,\href_cnt_reg[23]_i_1_n_7 }),
        .S({\href_cnt[23]_i_2_n_0 ,\href_cnt[23]_i_3_n_0 ,\href_cnt[23]_i_4_n_0 ,\href_cnt[23]_i_5_n_0 }));
  FDCE \href_cnt_reg[24] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[27]_i_1_n_7 ),
        .Q(href_cnt[24]));
  FDCE \href_cnt_reg[25] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[27]_i_1_n_6 ),
        .Q(href_cnt[25]));
  FDCE \href_cnt_reg[26] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[27]_i_1_n_5 ),
        .Q(href_cnt[26]));
  FDCE \href_cnt_reg[27] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[27]_i_1_n_4 ),
        .Q(href_cnt[27]));
  CARRY4 \href_cnt_reg[27]_i_1 
       (.CI(\href_cnt_reg[23]_i_1_n_0 ),
        .CO({\href_cnt_reg[27]_i_1_n_0 ,\href_cnt_reg[27]_i_1_n_1 ,\href_cnt_reg[27]_i_1_n_2 ,\href_cnt_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_cnt_reg[27]_i_1_n_4 ,\href_cnt_reg[27]_i_1_n_5 ,\href_cnt_reg[27]_i_1_n_6 ,\href_cnt_reg[27]_i_1_n_7 }),
        .S({\href_cnt[27]_i_2_n_0 ,\href_cnt[27]_i_3_n_0 ,\href_cnt[27]_i_4_n_0 ,\href_cnt[27]_i_5_n_0 }));
  FDCE \href_cnt_reg[28] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[31]_i_1_n_7 ),
        .Q(href_cnt[28]));
  FDCE \href_cnt_reg[29] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[31]_i_1_n_6 ),
        .Q(href_cnt[29]));
  FDCE \href_cnt_reg[2] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[3]_i_1_n_5 ),
        .Q(href_cnt[2]));
  FDCE \href_cnt_reg[30] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[31]_i_1_n_5 ),
        .Q(href_cnt[30]));
  FDCE \href_cnt_reg[31] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[31]_i_1_n_4 ),
        .Q(href_cnt[31]));
  CARRY4 \href_cnt_reg[31]_i_1 
       (.CI(\href_cnt_reg[27]_i_1_n_0 ),
        .CO({\NLW_href_cnt_reg[31]_i_1_CO_UNCONNECTED [3],\href_cnt_reg[31]_i_1_n_1 ,\href_cnt_reg[31]_i_1_n_2 ,\href_cnt_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_cnt_reg[31]_i_1_n_4 ,\href_cnt_reg[31]_i_1_n_5 ,\href_cnt_reg[31]_i_1_n_6 ,\href_cnt_reg[31]_i_1_n_7 }),
        .S({\href_cnt[31]_i_2_n_0 ,\href_cnt[31]_i_3_n_0 ,\href_cnt[31]_i_4_n_0 ,\href_cnt[31]_i_5_n_0 }));
  FDCE \href_cnt_reg[3] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[3]_i_1_n_4 ),
        .Q(href_cnt[3]));
  CARRY4 \href_cnt_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\href_cnt_reg[3]_i_1_n_0 ,\href_cnt_reg[3]_i_1_n_1 ,\href_cnt_reg[3]_i_1_n_2 ,\href_cnt_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,ov5640_href}),
        .O({\href_cnt_reg[3]_i_1_n_4 ,\href_cnt_reg[3]_i_1_n_5 ,\href_cnt_reg[3]_i_1_n_6 ,\href_cnt_reg[3]_i_1_n_7 }),
        .S({\href_cnt[3]_i_2_n_0 ,\href_cnt[3]_i_3_n_0 ,\href_cnt[3]_i_4_n_0 ,\href_cnt[3]_i_5_n_0 }));
  FDCE \href_cnt_reg[4] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[7]_i_1_n_7 ),
        .Q(href_cnt[4]));
  FDCE \href_cnt_reg[5] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[7]_i_1_n_6 ),
        .Q(href_cnt[5]));
  FDCE \href_cnt_reg[6] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[7]_i_1_n_5 ),
        .Q(href_cnt[6]));
  FDCE \href_cnt_reg[7] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[7]_i_1_n_4 ),
        .Q(href_cnt[7]));
  CARRY4 \href_cnt_reg[7]_i_1 
       (.CI(\href_cnt_reg[3]_i_1_n_0 ),
        .CO({\href_cnt_reg[7]_i_1_n_0 ,\href_cnt_reg[7]_i_1_n_1 ,\href_cnt_reg[7]_i_1_n_2 ,\href_cnt_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_cnt_reg[7]_i_1_n_4 ,\href_cnt_reg[7]_i_1_n_5 ,\href_cnt_reg[7]_i_1_n_6 ,\href_cnt_reg[7]_i_1_n_7 }),
        .S({\href_cnt[7]_i_2_n_0 ,\href_cnt[7]_i_3_n_0 ,\href_cnt[7]_i_4_n_0 ,\href_cnt[7]_i_5_n_0 }));
  FDCE \href_cnt_reg[8] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[11]_i_1_n_7 ),
        .Q(href_cnt[8]));
  FDCE \href_cnt_reg[9] 
       (.C(ov5640_pclk),
        .CE(1'b1),
        .CLR(ov5640_cfg_n_1),
        .D(\href_cnt_reg[11]_i_1_n_6 ),
        .Q(href_cnt[9]));
  FDRE href_reg_reg
       (.C(ov5640_pclk),
        .CE(1'b1),
        .D(ov5640_href),
        .Q(href_reg),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[11]_i_2 
       (.I0(href_v_cnt[11]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[11]_i_3 
       (.I0(href_v_cnt[10]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[11]_i_4 
       (.I0(href_v_cnt[9]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[11]_i_5 
       (.I0(href_v_cnt[8]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[15]_i_2 
       (.I0(href_v_cnt[15]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[15]_i_3 
       (.I0(href_v_cnt[14]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[15]_i_4 
       (.I0(href_v_cnt[13]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[15]_i_5 
       (.I0(href_v_cnt[12]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[19]_i_2 
       (.I0(href_v_cnt[19]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[19]_i_3 
       (.I0(href_v_cnt[18]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[19]_i_4 
       (.I0(href_v_cnt[17]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[19]_i_5 
       (.I0(href_v_cnt[16]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[23]_i_2 
       (.I0(href_v_cnt[23]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[23]_i_3 
       (.I0(href_v_cnt[22]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[23]_i_4 
       (.I0(href_v_cnt[21]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[23]_i_5 
       (.I0(href_v_cnt[20]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[27]_i_2 
       (.I0(href_v_cnt[27]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[27]_i_3 
       (.I0(href_v_cnt[26]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[27]_i_4 
       (.I0(href_v_cnt[25]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[27]_i_5 
       (.I0(href_v_cnt[24]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[27]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hF4)) 
    \href_v_cnt[31]_i_1 
       (.I0(href_reg),
        .I1(ov5640_href),
        .I2(ov5640_vsync),
        .O(\href_v_cnt[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[31]_i_3 
       (.I0(href_v_cnt[31]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[31]_i_4 
       (.I0(href_v_cnt[30]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[31]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[31]_i_5 
       (.I0(href_v_cnt[29]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[31]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[31]_i_6 
       (.I0(href_v_cnt[28]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[31]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \href_v_cnt[3]_i_2 
       (.I0(ov5640_vsync),
        .O(\href_v_cnt[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[3]_i_3 
       (.I0(href_v_cnt[3]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[3]_i_4 
       (.I0(href_v_cnt[2]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[3]_i_5 
       (.I0(href_v_cnt[1]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \href_v_cnt[3]_i_6 
       (.I0(href_v_cnt[0]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[3]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[7]_i_2 
       (.I0(href_v_cnt[7]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[7]_i_3 
       (.I0(href_v_cnt[6]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[7]_i_4 
       (.I0(href_v_cnt[5]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \href_v_cnt[7]_i_5 
       (.I0(href_v_cnt[4]),
        .I1(ov5640_vsync),
        .O(\href_v_cnt[7]_i_5_n_0 ));
  FDCE \href_v_cnt_reg[0] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[3]_i_1_n_7 ),
        .Q(href_v_cnt[0]));
  FDCE \href_v_cnt_reg[10] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[11]_i_1_n_5 ),
        .Q(href_v_cnt[10]));
  FDCE \href_v_cnt_reg[11] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[11]_i_1_n_4 ),
        .Q(href_v_cnt[11]));
  CARRY4 \href_v_cnt_reg[11]_i_1 
       (.CI(\href_v_cnt_reg[7]_i_1_n_0 ),
        .CO({\href_v_cnt_reg[11]_i_1_n_0 ,\href_v_cnt_reg[11]_i_1_n_1 ,\href_v_cnt_reg[11]_i_1_n_2 ,\href_v_cnt_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_v_cnt_reg[11]_i_1_n_4 ,\href_v_cnt_reg[11]_i_1_n_5 ,\href_v_cnt_reg[11]_i_1_n_6 ,\href_v_cnt_reg[11]_i_1_n_7 }),
        .S({\href_v_cnt[11]_i_2_n_0 ,\href_v_cnt[11]_i_3_n_0 ,\href_v_cnt[11]_i_4_n_0 ,\href_v_cnt[11]_i_5_n_0 }));
  FDCE \href_v_cnt_reg[12] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[15]_i_1_n_7 ),
        .Q(href_v_cnt[12]));
  FDCE \href_v_cnt_reg[13] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[15]_i_1_n_6 ),
        .Q(href_v_cnt[13]));
  FDCE \href_v_cnt_reg[14] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[15]_i_1_n_5 ),
        .Q(href_v_cnt[14]));
  FDCE \href_v_cnt_reg[15] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[15]_i_1_n_4 ),
        .Q(href_v_cnt[15]));
  CARRY4 \href_v_cnt_reg[15]_i_1 
       (.CI(\href_v_cnt_reg[11]_i_1_n_0 ),
        .CO({\href_v_cnt_reg[15]_i_1_n_0 ,\href_v_cnt_reg[15]_i_1_n_1 ,\href_v_cnt_reg[15]_i_1_n_2 ,\href_v_cnt_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_v_cnt_reg[15]_i_1_n_4 ,\href_v_cnt_reg[15]_i_1_n_5 ,\href_v_cnt_reg[15]_i_1_n_6 ,\href_v_cnt_reg[15]_i_1_n_7 }),
        .S({\href_v_cnt[15]_i_2_n_0 ,\href_v_cnt[15]_i_3_n_0 ,\href_v_cnt[15]_i_4_n_0 ,\href_v_cnt[15]_i_5_n_0 }));
  FDCE \href_v_cnt_reg[16] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[19]_i_1_n_7 ),
        .Q(href_v_cnt[16]));
  FDCE \href_v_cnt_reg[17] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[19]_i_1_n_6 ),
        .Q(href_v_cnt[17]));
  FDCE \href_v_cnt_reg[18] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[19]_i_1_n_5 ),
        .Q(href_v_cnt[18]));
  FDCE \href_v_cnt_reg[19] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[19]_i_1_n_4 ),
        .Q(href_v_cnt[19]));
  CARRY4 \href_v_cnt_reg[19]_i_1 
       (.CI(\href_v_cnt_reg[15]_i_1_n_0 ),
        .CO({\href_v_cnt_reg[19]_i_1_n_0 ,\href_v_cnt_reg[19]_i_1_n_1 ,\href_v_cnt_reg[19]_i_1_n_2 ,\href_v_cnt_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_v_cnt_reg[19]_i_1_n_4 ,\href_v_cnt_reg[19]_i_1_n_5 ,\href_v_cnt_reg[19]_i_1_n_6 ,\href_v_cnt_reg[19]_i_1_n_7 }),
        .S({\href_v_cnt[19]_i_2_n_0 ,\href_v_cnt[19]_i_3_n_0 ,\href_v_cnt[19]_i_4_n_0 ,\href_v_cnt[19]_i_5_n_0 }));
  FDCE \href_v_cnt_reg[1] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[3]_i_1_n_6 ),
        .Q(href_v_cnt[1]));
  FDCE \href_v_cnt_reg[20] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[23]_i_1_n_7 ),
        .Q(href_v_cnt[20]));
  FDCE \href_v_cnt_reg[21] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[23]_i_1_n_6 ),
        .Q(href_v_cnt[21]));
  FDCE \href_v_cnt_reg[22] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[23]_i_1_n_5 ),
        .Q(href_v_cnt[22]));
  FDCE \href_v_cnt_reg[23] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[23]_i_1_n_4 ),
        .Q(href_v_cnt[23]));
  CARRY4 \href_v_cnt_reg[23]_i_1 
       (.CI(\href_v_cnt_reg[19]_i_1_n_0 ),
        .CO({\href_v_cnt_reg[23]_i_1_n_0 ,\href_v_cnt_reg[23]_i_1_n_1 ,\href_v_cnt_reg[23]_i_1_n_2 ,\href_v_cnt_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_v_cnt_reg[23]_i_1_n_4 ,\href_v_cnt_reg[23]_i_1_n_5 ,\href_v_cnt_reg[23]_i_1_n_6 ,\href_v_cnt_reg[23]_i_1_n_7 }),
        .S({\href_v_cnt[23]_i_2_n_0 ,\href_v_cnt[23]_i_3_n_0 ,\href_v_cnt[23]_i_4_n_0 ,\href_v_cnt[23]_i_5_n_0 }));
  FDCE \href_v_cnt_reg[24] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[27]_i_1_n_7 ),
        .Q(href_v_cnt[24]));
  FDCE \href_v_cnt_reg[25] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[27]_i_1_n_6 ),
        .Q(href_v_cnt[25]));
  FDCE \href_v_cnt_reg[26] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[27]_i_1_n_5 ),
        .Q(href_v_cnt[26]));
  FDCE \href_v_cnt_reg[27] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[27]_i_1_n_4 ),
        .Q(href_v_cnt[27]));
  CARRY4 \href_v_cnt_reg[27]_i_1 
       (.CI(\href_v_cnt_reg[23]_i_1_n_0 ),
        .CO({\href_v_cnt_reg[27]_i_1_n_0 ,\href_v_cnt_reg[27]_i_1_n_1 ,\href_v_cnt_reg[27]_i_1_n_2 ,\href_v_cnt_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_v_cnt_reg[27]_i_1_n_4 ,\href_v_cnt_reg[27]_i_1_n_5 ,\href_v_cnt_reg[27]_i_1_n_6 ,\href_v_cnt_reg[27]_i_1_n_7 }),
        .S({\href_v_cnt[27]_i_2_n_0 ,\href_v_cnt[27]_i_3_n_0 ,\href_v_cnt[27]_i_4_n_0 ,\href_v_cnt[27]_i_5_n_0 }));
  FDCE \href_v_cnt_reg[28] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[31]_i_2_n_7 ),
        .Q(href_v_cnt[28]));
  FDCE \href_v_cnt_reg[29] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[31]_i_2_n_6 ),
        .Q(href_v_cnt[29]));
  FDCE \href_v_cnt_reg[2] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[3]_i_1_n_5 ),
        .Q(href_v_cnt[2]));
  FDCE \href_v_cnt_reg[30] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[31]_i_2_n_5 ),
        .Q(href_v_cnt[30]));
  FDCE \href_v_cnt_reg[31] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[31]_i_2_n_4 ),
        .Q(href_v_cnt[31]));
  CARRY4 \href_v_cnt_reg[31]_i_2 
       (.CI(\href_v_cnt_reg[27]_i_1_n_0 ),
        .CO({\NLW_href_v_cnt_reg[31]_i_2_CO_UNCONNECTED [3],\href_v_cnt_reg[31]_i_2_n_1 ,\href_v_cnt_reg[31]_i_2_n_2 ,\href_v_cnt_reg[31]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_v_cnt_reg[31]_i_2_n_4 ,\href_v_cnt_reg[31]_i_2_n_5 ,\href_v_cnt_reg[31]_i_2_n_6 ,\href_v_cnt_reg[31]_i_2_n_7 }),
        .S({\href_v_cnt[31]_i_3_n_0 ,\href_v_cnt[31]_i_4_n_0 ,\href_v_cnt[31]_i_5_n_0 ,\href_v_cnt[31]_i_6_n_0 }));
  FDCE \href_v_cnt_reg[3] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[3]_i_1_n_4 ),
        .Q(href_v_cnt[3]));
  CARRY4 \href_v_cnt_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\href_v_cnt_reg[3]_i_1_n_0 ,\href_v_cnt_reg[3]_i_1_n_1 ,\href_v_cnt_reg[3]_i_1_n_2 ,\href_v_cnt_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\href_v_cnt[3]_i_2_n_0 }),
        .O({\href_v_cnt_reg[3]_i_1_n_4 ,\href_v_cnt_reg[3]_i_1_n_5 ,\href_v_cnt_reg[3]_i_1_n_6 ,\href_v_cnt_reg[3]_i_1_n_7 }),
        .S({\href_v_cnt[3]_i_3_n_0 ,\href_v_cnt[3]_i_4_n_0 ,\href_v_cnt[3]_i_5_n_0 ,\href_v_cnt[3]_i_6_n_0 }));
  FDCE \href_v_cnt_reg[4] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[7]_i_1_n_7 ),
        .Q(href_v_cnt[4]));
  FDCE \href_v_cnt_reg[5] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[7]_i_1_n_6 ),
        .Q(href_v_cnt[5]));
  FDCE \href_v_cnt_reg[6] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[7]_i_1_n_5 ),
        .Q(href_v_cnt[6]));
  FDCE \href_v_cnt_reg[7] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[7]_i_1_n_4 ),
        .Q(href_v_cnt[7]));
  CARRY4 \href_v_cnt_reg[7]_i_1 
       (.CI(\href_v_cnt_reg[3]_i_1_n_0 ),
        .CO({\href_v_cnt_reg[7]_i_1_n_0 ,\href_v_cnt_reg[7]_i_1_n_1 ,\href_v_cnt_reg[7]_i_1_n_2 ,\href_v_cnt_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\href_v_cnt_reg[7]_i_1_n_4 ,\href_v_cnt_reg[7]_i_1_n_5 ,\href_v_cnt_reg[7]_i_1_n_6 ,\href_v_cnt_reg[7]_i_1_n_7 }),
        .S({\href_v_cnt[7]_i_2_n_0 ,\href_v_cnt[7]_i_3_n_0 ,\href_v_cnt[7]_i_4_n_0 ,\href_v_cnt[7]_i_5_n_0 }));
  FDCE \href_v_cnt_reg[8] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[11]_i_1_n_7 ),
        .Q(href_v_cnt[8]));
  FDCE \href_v_cnt_reg[9] 
       (.C(ov5640_pclk),
        .CE(\href_v_cnt[31]_i_1_n_0 ),
        .CLR(ov5640_cfg_n_1),
        .D(\href_v_cnt_reg[11]_i_1_n_6 ),
        .Q(href_v_cnt[9]));
  system_ov5640_top_0_0_iic_ctrl iic_ctrl
       (.E(i2c_end),
        .Q({iic_ctrl_n_2,iic_ctrl_n_3,iic_ctrl_n_4}),
        .cfg_start(cfg_start),
        .\cnt_bit_reg[0]_0 (iic_ctrl_n_5),
        .\cnt_bit_reg[0]_1 (iic_ctrl_n_6),
        .\cnt_bit_reg[0]_2 (iic_ctrl_n_10),
        .\cnt_bit_reg[1]_0 (iic_ctrl_n_7),
        .\cnt_bit_reg[1]_1 (iic_ctrl_n_11),
        .\cnt_bit_reg[2]_0 (iic_ctrl_n_8),
        .\cnt_i2c_clk_reg[0]_0 (ov5640_cfg_n_1),
        .i2c_clk(i2c_clk),
        .i2c_sda_reg_reg_0(ov5640_cfg_n_4),
        .i2c_sda_reg_reg_1(ov5640_cfg_n_5),
        .i2c_sda_reg_reg_2(ov5640_cfg_n_3),
        .i2c_sda_reg_reg_i_40(cfg_done_reg),
        .sccb_scl(sccb_scl),
        .sccb_sda(sccb_sda),
        .sys_clk(sys_clk));
  system_ov5640_top_0_0_ov5640_cfg ov5640_cfg
       (.E(i2c_end),
        .\FSM_onehot_state_reg[13] (ov5640_cfg_n_3),
        .\FSM_onehot_state_reg[13]_0 (ov5640_cfg_n_4),
        .\FSM_onehot_state_reg[2] (ov5640_cfg_n_5),
        .Q({iic_ctrl_n_2,iic_ctrl_n_3,iic_ctrl_n_4}),
        .cfg_done_reg_0(cfg_done_reg),
        .cfg_start(cfg_start),
        .i2c_clk(i2c_clk),
        .i2c_sda_reg_reg_i_17_0(iic_ctrl_n_11),
        .i2c_sda_reg_reg_i_17_1(iic_ctrl_n_5),
        .i2c_sda_reg_reg_i_41_0(iic_ctrl_n_7),
        .i2c_sda_reg_reg_i_4_0(iic_ctrl_n_8),
        .i2c_sda_reg_reg_i_4_1(iic_ctrl_n_6),
        .i2c_sda_reg_reg_i_5_0(iic_ctrl_n_10),
        .sys_rst_n(sys_rst_n),
        .sys_rst_n_0(ov5640_cfg_n_1));
  system_ov5640_top_0_0_ov5640_data_in ov5640_data_in
       (.ov5640_data(ov5640_data),
        .ov5640_data_out(ov5640_data_out),
        .ov5640_href(ov5640_href),
        .ov5640_pclk(ov5640_pclk),
        .ov5640_wr_en(ov5640_wr_en),
        .sys_init_done(sys_init_done),
        .sys_rst_n(sys_rst_n));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
