-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
-- Date        : Sun Dec 24 16:01:40 2023
-- Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               E:/Workspace/Vivado/2019.2/pcie_xdma_v1/vivado_prj/pcie_xdma.srcs/sources_1/bd/system/ip/system_ov5640_top_0_0/system_ov5640_top_0_0_stub.vhdl
-- Design      : system_ov5640_top_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity system_ov5640_top_0_0 is
  Port ( 
    sys_clk : in STD_LOGIC;
    sys_rst_n : in STD_LOGIC;
    sys_init_done : in STD_LOGIC;
    ov5640_pclk : in STD_LOGIC;
    ov5640_href : in STD_LOGIC;
    ov5640_vsync : in STD_LOGIC;
    ov5640_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ov5640_rst_n : out STD_LOGIC;
    ov5640_pwdn : out STD_LOGIC;
    sccb_scl : out STD_LOGIC;
    sccb_sda : inout STD_LOGIC;
    cfg_done : out STD_LOGIC;
    ov5640_wr_en : out STD_LOGIC;
    ov5640_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    href_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 );
    href_v_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end system_ov5640_top_0_0;

architecture stub of system_ov5640_top_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "sys_clk,sys_rst_n,sys_init_done,ov5640_pclk,ov5640_href,ov5640_vsync,ov5640_data[7:0],ov5640_rst_n,ov5640_pwdn,sccb_scl,sccb_sda,cfg_done,ov5640_wr_en,ov5640_data_out[31:0],href_cnt[31:0],href_v_cnt[31:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "ov5640_top,Vivado 2019.2";
begin
end;
