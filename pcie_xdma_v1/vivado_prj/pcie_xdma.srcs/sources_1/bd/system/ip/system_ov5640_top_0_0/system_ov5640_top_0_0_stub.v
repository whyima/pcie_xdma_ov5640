// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Sun Dec 24 16:01:40 2023
// Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               E:/Workspace/Vivado/2019.2/pcie_xdma_v1/vivado_prj/pcie_xdma.srcs/sources_1/bd/system/ip/system_ov5640_top_0_0/system_ov5640_top_0_0_stub.v
// Design      : system_ov5640_top_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ov5640_top,Vivado 2019.2" *)
module system_ov5640_top_0_0(sys_clk, sys_rst_n, sys_init_done, ov5640_pclk, 
  ov5640_href, ov5640_vsync, ov5640_data, ov5640_rst_n, ov5640_pwdn, sccb_scl, sccb_sda, 
  cfg_done, ov5640_wr_en, ov5640_data_out, href_cnt, href_v_cnt)
/* synthesis syn_black_box black_box_pad_pin="sys_clk,sys_rst_n,sys_init_done,ov5640_pclk,ov5640_href,ov5640_vsync,ov5640_data[7:0],ov5640_rst_n,ov5640_pwdn,sccb_scl,sccb_sda,cfg_done,ov5640_wr_en,ov5640_data_out[31:0],href_cnt[31:0],href_v_cnt[31:0]" */;
  input sys_clk;
  input sys_rst_n;
  input sys_init_done;
  input ov5640_pclk;
  input ov5640_href;
  input ov5640_vsync;
  input [7:0]ov5640_data;
  output ov5640_rst_n;
  output ov5640_pwdn;
  output sccb_scl;
  inout sccb_sda;
  output cfg_done;
  output ov5640_wr_en;
  output [31:0]ov5640_data_out;
  output [31:0]href_cnt;
  output [31:0]href_v_cnt;
endmodule
