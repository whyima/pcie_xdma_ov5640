-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
-- Date        : Sun Dec 24 16:01:40 2023
-- Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/Workspace/Vivado/2019.2/pcie_xdma_v1/vivado_prj/pcie_xdma.srcs/sources_1/bd/system/ip/system_ov5640_top_0_0/system_ov5640_top_0_0_sim_netlist.vhdl
-- Design      : system_ov5640_top_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_ov5640_top_0_0_iic_ctrl is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    i2c_clk : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \cnt_bit_reg[0]_0\ : out STD_LOGIC;
    \cnt_bit_reg[0]_1\ : out STD_LOGIC;
    \cnt_bit_reg[1]_0\ : out STD_LOGIC;
    \cnt_bit_reg[2]_0\ : out STD_LOGIC;
    sccb_scl : out STD_LOGIC;
    \cnt_bit_reg[0]_2\ : out STD_LOGIC;
    \cnt_bit_reg[1]_1\ : out STD_LOGIC;
    sccb_sda : inout STD_LOGIC;
    \cnt_i2c_clk_reg[0]_0\ : in STD_LOGIC;
    sys_clk : in STD_LOGIC;
    i2c_sda_reg_reg_0 : in STD_LOGIC;
    i2c_sda_reg_reg_1 : in STD_LOGIC;
    i2c_sda_reg_reg_2 : in STD_LOGIC;
    i2c_sda_reg_reg_i_40 : in STD_LOGIC;
    cfg_start : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_ov5640_top_0_0_iic_ctrl : entity is "iic_ctrl";
end system_ov5640_top_0_0_iic_ctrl;

architecture STRUCTURE of system_ov5640_top_0_0_iic_ctrl is
  signal \FSM_onehot_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[10]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[11]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[12]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[13]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[14]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[15]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[15]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[15]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[6]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[11]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[14]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[15]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[3]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[6]\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal ack : STD_LOGIC;
  signal \ack__0\ : STD_LOGIC;
  signal ack_reg_i_1_n_0 : STD_LOGIC;
  signal ack_reg_i_3_n_0 : STD_LOGIC;
  signal ack_reg_i_4_n_0 : STD_LOGIC;
  signal \cnt_bit12_out__0\ : STD_LOGIC;
  signal \cnt_bit[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_bit[1]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_bit[2]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_bit[2]_i_2_n_0\ : STD_LOGIC;
  signal \^cnt_bit_reg[0]_1\ : STD_LOGIC;
  signal \^cnt_bit_reg[1]_0\ : STD_LOGIC;
  signal \^cnt_bit_reg[2]_0\ : STD_LOGIC;
  signal cnt_clk : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \cnt_clk[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk[4]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk[7]_i_2_n_0\ : STD_LOGIC;
  signal cnt_clk_0 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal cnt_i2c_clk : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \cnt_i2c_clk[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_i2c_clk[1]_i_1_n_0\ : STD_LOGIC;
  signal cnt_i2c_clk_en : STD_LOGIC;
  signal cnt_i2c_clk_en1 : STD_LOGIC;
  signal cnt_i2c_clk_en_i_1_n_0 : STD_LOGIC;
  signal \^i2c_clk\ : STD_LOGIC;
  signal i2c_clk_i_1_n_0 : STD_LOGIC;
  signal i2c_clk_i_2_n_0 : STD_LOGIC;
  signal i2c_sda_reg : STD_LOGIC;
  signal \i2c_sda_reg__0\ : STD_LOGIC;
  signal i2c_sda_reg_reg_i_18_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_1_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_6_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_7_n_0 : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal p_3_in : STD_LOGIC;
  signal p_5_in : STD_LOGIC;
  signal sccb_scl_INST_0_i_1_n_0 : STD_LOGIC;
  signal sccb_scl_INST_0_i_2_n_0 : STD_LOGIC;
  signal sccb_scl_INST_0_i_3_n_0 : STD_LOGIC;
  signal sccb_sda_INST_0_i_1_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state[14]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_state[15]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_state[15]_i_3\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_onehot_state[6]_i_1\ : label is "soft_lutpair0";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[10]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[11]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[12]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[13]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[14]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[15]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[1]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[3]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[6]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ack_reg : label is "LD";
  attribute SOFT_HLUTNM of ack_reg_i_4 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt_clk[0]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cnt_clk[0]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt_clk[1]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cnt_clk[2]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \cnt_clk[7]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \cnt_i2c_clk[1]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of i2c_clk_i_2 : label is "soft_lutpair2";
  attribute XILINX_LEGACY_PRIM of i2c_sda_reg_reg : label is "LD";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_18 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_7 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_83 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_87 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of sccb_scl_INST_0_i_3 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of sccb_sda_INST_0_i_1 : label is "soft_lutpair6";
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
  \cnt_bit_reg[0]_1\ <= \^cnt_bit_reg[0]_1\;
  \cnt_bit_reg[1]_0\ <= \^cnt_bit_reg[1]_0\;
  \cnt_bit_reg[2]_0\ <= \^cnt_bit_reg[2]_0\;
  i2c_clk <= \^i2c_clk\;
\FSM_onehot_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF8FFF88888888"
    )
        port map (
      I0 => \cnt_bit12_out__0\,
      I1 => \FSM_onehot_state_reg_n_0_[6]\,
      I2 => cnt_i2c_clk(0),
      I3 => cnt_i2c_clk(1),
      I4 => ack,
      I5 => p_3_in,
      O => \FSM_onehot_state[0]_i_1_n_0\
    );
\FSM_onehot_state[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"8FFF8888"
    )
        port map (
      I0 => cfg_start,
      I1 => \FSM_onehot_state_reg_n_0_[11]\,
      I2 => cnt_i2c_clk(1),
      I3 => cnt_i2c_clk(0),
      I4 => p_1_in,
      O => \FSM_onehot_state[10]_i_1_n_0\
    );
\FSM_onehot_state[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100FFFF01000100"
    )
        port map (
      I0 => \^cnt_bit_reg[2]_0\,
      I1 => \FSM_onehot_state[15]_i_3_n_0\,
      I2 => \FSM_onehot_state[15]_i_2_n_0\,
      I3 => \FSM_onehot_state_reg_n_0_[15]\,
      I4 => cfg_start,
      I5 => \FSM_onehot_state_reg_n_0_[11]\,
      O => \FSM_onehot_state[11]_i_1_n_0\
    );
\FSM_onehot_state[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF700F700F700"
    )
        port map (
      I0 => cnt_i2c_clk(0),
      I1 => cnt_i2c_clk(1),
      I2 => ack,
      I3 => p_5_in,
      I4 => \cnt_bit12_out__0\,
      I5 => \^q\(1),
      O => \FSM_onehot_state[12]_i_1_n_0\
    );
\FSM_onehot_state[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444F44444444444"
    )
        port map (
      I0 => \cnt_bit12_out__0\,
      I1 => \^q\(2),
      I2 => cnt_i2c_clk(0),
      I3 => cnt_i2c_clk(1),
      I4 => ack,
      I5 => p_5_in,
      O => \FSM_onehot_state[13]_i_1_n_0\
    );
\FSM_onehot_state[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF700F700F700"
    )
        port map (
      I0 => cnt_i2c_clk(0),
      I1 => cnt_i2c_clk(1),
      I2 => ack,
      I3 => \FSM_onehot_state_reg_n_0_[14]\,
      I4 => \cnt_bit12_out__0\,
      I5 => \^q\(2),
      O => \FSM_onehot_state[14]_i_1_n_0\
    );
\FSM_onehot_state[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => cnt_i2c_clk(1),
      I1 => cnt_i2c_clk(0),
      I2 => \^cnt_bit_reg[2]_0\,
      I3 => \^cnt_bit_reg[0]_1\,
      I4 => \^cnt_bit_reg[1]_0\,
      O => \cnt_bit12_out__0\
    );
\FSM_onehot_state[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0E0F0FFF0E0F0E0"
    )
        port map (
      I0 => \^cnt_bit_reg[2]_0\,
      I1 => \FSM_onehot_state[15]_i_2_n_0\,
      I2 => \FSM_onehot_state_reg_n_0_[15]\,
      I3 => \FSM_onehot_state[15]_i_3_n_0\,
      I4 => ack,
      I5 => \FSM_onehot_state_reg_n_0_[14]\,
      O => \FSM_onehot_state[15]_i_1_n_0\
    );
\FSM_onehot_state[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^cnt_bit_reg[1]_0\,
      I1 => \^cnt_bit_reg[0]_1\,
      O => \FSM_onehot_state[15]_i_2_n_0\
    );
\FSM_onehot_state[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => cnt_i2c_clk(1),
      I1 => cnt_i2c_clk(0),
      O => \FSM_onehot_state[15]_i_3_n_0\
    );
\FSM_onehot_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444F44444444444"
    )
        port map (
      I0 => \cnt_bit12_out__0\,
      I1 => \^q\(0),
      I2 => cnt_i2c_clk(0),
      I3 => cnt_i2c_clk(1),
      I4 => ack,
      I5 => p_3_in,
      O => \FSM_onehot_state[1]_i_1_n_0\
    );
\FSM_onehot_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0800FFFF08000800"
    )
        port map (
      I0 => cnt_i2c_clk(0),
      I1 => cnt_i2c_clk(1),
      I2 => ack,
      I3 => \FSM_onehot_state_reg_n_0_[3]\,
      I4 => \cnt_bit12_out__0\,
      I5 => \^q\(1),
      O => \FSM_onehot_state[2]_i_1_n_0\
    );
\FSM_onehot_state[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF700F700F700"
    )
        port map (
      I0 => cnt_i2c_clk(0),
      I1 => cnt_i2c_clk(1),
      I2 => ack,
      I3 => \FSM_onehot_state_reg_n_0_[3]\,
      I4 => \cnt_bit12_out__0\,
      I5 => \^q\(0),
      O => \FSM_onehot_state[3]_i_1_n_0\
    );
\FSM_onehot_state[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80FF8080"
    )
        port map (
      I0 => cnt_i2c_clk(1),
      I1 => cnt_i2c_clk(0),
      I2 => p_1_in,
      I3 => \cnt_bit12_out__0\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      O => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[0]_i_1_n_0\,
      Q => p_3_in
    );
\FSM_onehot_state_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[10]_i_1_n_0\,
      Q => p_1_in
    );
\FSM_onehot_state_reg[11]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      D => \FSM_onehot_state[11]_i_1_n_0\,
      PRE => \cnt_i2c_clk_reg[0]_0\,
      Q => \FSM_onehot_state_reg_n_0_[11]\
    );
\FSM_onehot_state_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[12]_i_1_n_0\,
      Q => p_5_in
    );
\FSM_onehot_state_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[13]_i_1_n_0\,
      Q => \^q\(2)
    );
\FSM_onehot_state_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[14]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[14]\
    );
\FSM_onehot_state_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[15]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[15]\
    );
\FSM_onehot_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[1]_i_1_n_0\,
      Q => \^q\(0)
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[2]_i_1_n_0\,
      Q => \^q\(1)
    );
\FSM_onehot_state_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[3]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[3]\
    );
\FSM_onehot_state_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \FSM_onehot_state[6]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[6]\
    );
ack_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => ack_reg_i_1_n_0,
      G => \ack__0\,
      GE => '1',
      Q => ack
    );
ack_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFD"
    )
        port map (
      I0 => sccb_scl_INST_0_i_3_n_0,
      I1 => \FSM_onehot_state_reg_n_0_[6]\,
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => p_1_in,
      I5 => ack_reg_i_3_n_0,
      O => ack_reg_i_1_n_0
    );
ack_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFEFEFF"
    )
        port map (
      I0 => ack_reg_i_4_n_0,
      I1 => \^q\(2),
      I2 => \FSM_onehot_state_reg_n_0_[11]\,
      I3 => cnt_i2c_clk(1),
      I4 => cnt_i2c_clk(0),
      O => \ack__0\
    );
ack_reg_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^q\(2),
      I1 => \FSM_onehot_state_reg_n_0_[11]\,
      I2 => sccb_sda,
      O => ack_reg_i_3_n_0
    );
ack_reg_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEFFFF"
    )
        port map (
      I0 => p_1_in,
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \FSM_onehot_state_reg_n_0_[6]\,
      I4 => sccb_scl_INST_0_i_3_n_0,
      O => ack_reg_i_4_n_0
    );
\cnt_bit[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00F70008"
    )
        port map (
      I0 => cnt_i2c_clk(1),
      I1 => cnt_i2c_clk(0),
      I2 => \FSM_onehot_state_reg_n_0_[11]\,
      I3 => \cnt_bit[2]_i_2_n_0\,
      I4 => \^cnt_bit_reg[0]_1\,
      O => \cnt_bit[0]_i_1_n_0\
    );
\cnt_bit[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FF7F00000080"
    )
        port map (
      I0 => \^cnt_bit_reg[0]_1\,
      I1 => cnt_i2c_clk(1),
      I2 => cnt_i2c_clk(0),
      I3 => \FSM_onehot_state_reg_n_0_[11]\,
      I4 => \cnt_bit[2]_i_2_n_0\,
      I5 => \^cnt_bit_reg[1]_0\,
      O => \cnt_bit[1]_i_1_n_0\
    );
\cnt_bit[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFBF00000040"
    )
        port map (
      I0 => \FSM_onehot_state[15]_i_2_n_0\,
      I1 => cnt_i2c_clk(1),
      I2 => cnt_i2c_clk(0),
      I3 => \FSM_onehot_state_reg_n_0_[11]\,
      I4 => \cnt_bit[2]_i_2_n_0\,
      I5 => \^cnt_bit_reg[2]_0\,
      O => \cnt_bit[2]_i_1_n_0\
    );
\cnt_bit[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[11]\,
      I1 => p_1_in,
      I2 => sccb_scl_INST_0_i_3_n_0,
      I3 => \cnt_bit12_out__0\,
      O => \cnt_bit[2]_i_2_n_0\
    );
\cnt_bit_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \cnt_bit[0]_i_1_n_0\,
      Q => \^cnt_bit_reg[0]_1\
    );
\cnt_bit_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \cnt_bit[1]_i_1_n_0\,
      Q => \^cnt_bit_reg[1]_0\
    );
\cnt_bit_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \cnt_bit[2]_i_1_n_0\,
      Q => \^cnt_bit_reg[2]_0\
    );
\cnt_clk[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00FF00F7"
    )
        port map (
      I0 => cnt_clk(4),
      I1 => cnt_clk(3),
      I2 => cnt_clk(1),
      I3 => cnt_clk(0),
      I4 => \cnt_clk[0]_i_2_n_0\,
      O => cnt_clk_0(0)
    );
\cnt_clk[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_clk(5),
      I1 => cnt_clk(2),
      I2 => cnt_clk(7),
      I3 => cnt_clk(6),
      O => \cnt_clk[0]_i_2_n_0\
    );
\cnt_clk[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_clk(0),
      I1 => cnt_clk(1),
      O => cnt_clk_0(1)
    );
\cnt_clk[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt_clk(1),
      I1 => cnt_clk(0),
      I2 => cnt_clk(2),
      O => cnt_clk_0(2)
    );
\cnt_clk[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFC0001111C000"
    )
        port map (
      I0 => cnt_clk(4),
      I1 => cnt_clk(1),
      I2 => cnt_clk(0),
      I3 => cnt_clk(2),
      I4 => cnt_clk(3),
      I5 => \cnt_clk[4]_i_2_n_0\,
      O => cnt_clk_0(3)
    );
\cnt_clk[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF800055558000"
    )
        port map (
      I0 => cnt_clk(3),
      I1 => cnt_clk(1),
      I2 => cnt_clk(0),
      I3 => cnt_clk(2),
      I4 => cnt_clk(4),
      I5 => \cnt_clk[4]_i_2_n_0\,
      O => cnt_clk_0(4)
    );
\cnt_clk[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55FF55FFFFFFFFFE"
    )
        port map (
      I0 => cnt_clk(1),
      I1 => cnt_clk(6),
      I2 => cnt_clk(7),
      I3 => cnt_clk(2),
      I4 => cnt_clk(5),
      I5 => cnt_clk(0),
      O => \cnt_clk[4]_i_2_n_0\
    );
\cnt_clk[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => cnt_clk(3),
      I1 => cnt_clk(4),
      I2 => cnt_clk(2),
      I3 => cnt_clk(0),
      I4 => cnt_clk(1),
      I5 => cnt_clk(5),
      O => cnt_clk_0(5)
    );
\cnt_clk[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => cnt_clk(5),
      I1 => \cnt_clk[7]_i_2_n_0\,
      I2 => cnt_clk(4),
      I3 => cnt_clk(3),
      I4 => cnt_clk(6),
      O => cnt_clk_0(6)
    );
\cnt_clk[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF7FFFFF00800000"
    )
        port map (
      I0 => cnt_clk(6),
      I1 => cnt_clk(3),
      I2 => cnt_clk(4),
      I3 => \cnt_clk[7]_i_2_n_0\,
      I4 => cnt_clk(5),
      I5 => cnt_clk(7),
      O => cnt_clk_0(7)
    );
\cnt_clk[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => cnt_clk(2),
      I1 => cnt_clk(0),
      I2 => cnt_clk(1),
      O => \cnt_clk[7]_i_2_n_0\
    );
\cnt_clk_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_clk_0(0),
      Q => cnt_clk(0)
    );
\cnt_clk_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_clk_0(1),
      Q => cnt_clk(1)
    );
\cnt_clk_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_clk_0(2),
      Q => cnt_clk(2)
    );
\cnt_clk_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_clk_0(3),
      Q => cnt_clk(3)
    );
\cnt_clk_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_clk_0(4),
      Q => cnt_clk(4)
    );
\cnt_clk_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_clk_0(5),
      Q => cnt_clk(5)
    );
\cnt_clk_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_clk_0(6),
      Q => cnt_clk(6)
    );
\cnt_clk_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_clk_0(7),
      Q => cnt_clk(7)
    );
\cnt_i2c_clk[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_i2c_clk_en,
      I1 => cnt_i2c_clk(0),
      O => \cnt_i2c_clk[0]_i_1_n_0\
    );
\cnt_i2c_clk[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt_i2c_clk(0),
      I1 => cnt_i2c_clk_en,
      I2 => cnt_i2c_clk(1),
      O => \cnt_i2c_clk[1]_i_1_n_0\
    );
cnt_i2c_clk_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFDFFFDFFFD0000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[15]\,
      I1 => \^cnt_bit_reg[2]_0\,
      I2 => \FSM_onehot_state[15]_i_3_n_0\,
      I3 => \FSM_onehot_state[15]_i_2_n_0\,
      I4 => cfg_start,
      I5 => cnt_i2c_clk_en,
      O => cnt_i2c_clk_en_i_1_n_0
    );
cnt_i2c_clk_en_reg: unisim.vcomponents.FDCE
     port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_i2c_clk_en_i_1_n_0,
      Q => cnt_i2c_clk_en
    );
\cnt_i2c_clk_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \cnt_i2c_clk[0]_i_1_n_0\,
      Q => cnt_i2c_clk(0)
    );
\cnt_i2c_clk_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => \cnt_i2c_clk[1]_i_1_n_0\,
      Q => cnt_i2c_clk(1)
    );
i2c_clk_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0040"
    )
        port map (
      I0 => i2c_clk_i_2_n_0,
      I1 => cnt_clk(4),
      I2 => cnt_clk(3),
      I3 => cnt_clk(1),
      I4 => \^i2c_clk\,
      O => i2c_clk_i_1_n_0
    );
i2c_clk_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt_clk(6),
      I1 => cnt_clk(7),
      I2 => cnt_clk(2),
      I3 => cnt_clk(5),
      I4 => cnt_clk(0),
      O => i2c_clk_i_2_n_0
    );
i2c_clk_reg: unisim.vcomponents.FDPE
     port map (
      C => sys_clk,
      CE => '1',
      D => i2c_clk_i_1_n_0,
      PRE => \cnt_i2c_clk_reg[0]_0\,
      Q => \^i2c_clk\
    );
i2c_end_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \^cnt_bit_reg[1]_0\,
      I1 => \^cnt_bit_reg[0]_1\,
      I2 => cnt_i2c_clk(0),
      I3 => cnt_i2c_clk(1),
      I4 => \^cnt_bit_reg[2]_0\,
      I5 => \FSM_onehot_state_reg_n_0_[15]\,
      O => cnt_i2c_clk_en1
    );
i2c_end_reg: unisim.vcomponents.FDCE
     port map (
      C => \^i2c_clk\,
      CE => '1',
      CLR => \cnt_i2c_clk_reg[0]_0\,
      D => cnt_i2c_clk_en1,
      Q => E(0)
    );
i2c_sda_reg_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => i2c_sda_reg_reg_i_1_n_0,
      G => \i2c_sda_reg__0\,
      GE => '1',
      Q => i2c_sda_reg
    );
i2c_sda_reg_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => sccb_scl_INST_0_i_3_n_0,
      I1 => \FSM_onehot_state_reg_n_0_[11]\,
      I2 => i2c_sda_reg_reg_0,
      I3 => i2c_sda_reg_reg_1,
      I4 => i2c_sda_reg_reg_2,
      I5 => i2c_sda_reg_reg_i_6_n_0,
      O => i2c_sda_reg_reg_i_1_n_0
    );
i2c_sda_reg_reg_i_18: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A00C"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[15]\,
      I1 => p_1_in,
      I2 => cnt_i2c_clk(1),
      I3 => cnt_i2c_clk(0),
      O => i2c_sda_reg_reg_i_18_n_0
    );
i2c_sda_reg_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_7_n_0,
      I1 => \FSM_onehot_state_reg_n_0_[15]\,
      I2 => \FSM_onehot_state_reg_n_0_[11]\,
      I3 => \^q\(2),
      I4 => sccb_scl_INST_0_i_3_n_0,
      I5 => p_1_in,
      O => \i2c_sda_reg__0\
    );
i2c_sda_reg_reg_i_42: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^cnt_bit_reg[0]_1\,
      I1 => \^cnt_bit_reg[1]_0\,
      I2 => i2c_sda_reg_reg_i_40,
      O => \cnt_bit_reg[0]_2\
    );
i2c_sda_reg_reg_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFEFEFFCEEEEEEEC"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[15]\,
      I1 => i2c_sda_reg_reg_i_18_n_0,
      I2 => \^cnt_bit_reg[2]_0\,
      I3 => \^cnt_bit_reg[1]_0\,
      I4 => \^cnt_bit_reg[0]_1\,
      I5 => \FSM_onehot_state_reg_n_0_[6]\,
      O => i2c_sda_reg_reg_i_6_n_0
    );
i2c_sda_reg_reg_i_7: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \FSM_onehot_state_reg_n_0_[6]\,
      O => i2c_sda_reg_reg_i_7_n_0
    );
i2c_sda_reg_reg_i_83: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => \^cnt_bit_reg[1]_0\,
      I1 => i2c_sda_reg_reg_i_40,
      I2 => \^cnt_bit_reg[0]_1\,
      O => \cnt_bit_reg[1]_1\
    );
i2c_sda_reg_reg_i_87: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^cnt_bit_reg[0]_1\,
      I1 => \^cnt_bit_reg[1]_0\,
      I2 => i2c_sda_reg_reg_i_40,
      O => \cnt_bit_reg[0]_0\
    );
sccb_scl_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFBEFFFF3F00"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[11]\,
      I1 => cnt_i2c_clk(1),
      I2 => cnt_i2c_clk(0),
      I3 => p_1_in,
      I4 => sccb_scl_INST_0_i_1_n_0,
      I5 => sccb_scl_INST_0_i_2_n_0,
      O => sccb_scl
    );
sccb_scl_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000000"
    )
        port map (
      I0 => \^cnt_bit_reg[2]_0\,
      I1 => \^cnt_bit_reg[1]_0\,
      I2 => \^cnt_bit_reg[0]_1\,
      I3 => cnt_i2c_clk(1),
      I4 => cnt_i2c_clk(0),
      I5 => \FSM_onehot_state_reg_n_0_[15]\,
      O => sccb_scl_INST_0_i_1_n_0
    );
sccb_scl_INST_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => sccb_scl_INST_0_i_3_n_0,
      I1 => \^q\(2),
      I2 => \FSM_onehot_state_reg_n_0_[11]\,
      I3 => \FSM_onehot_state_reg_n_0_[6]\,
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => sccb_scl_INST_0_i_2_n_0
    );
sccb_scl_INST_0_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[14]\,
      I1 => \FSM_onehot_state_reg_n_0_[3]\,
      I2 => p_3_in,
      I3 => p_5_in,
      O => sccb_scl_INST_0_i_3_n_0
    );
sccb_sda_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i2c_sda_reg,
      I1 => sccb_sda_INST_0_i_1_n_0,
      O => sccb_sda
    );
sccb_sda_INST_0_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => p_5_in,
      I1 => p_3_in,
      I2 => \FSM_onehot_state_reg_n_0_[3]\,
      I3 => \FSM_onehot_state_reg_n_0_[14]\,
      O => sccb_sda_INST_0_i_1_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_ov5640_top_0_0_ov5640_cfg is
  port (
    cfg_start : out STD_LOGIC;
    sys_rst_n_0 : out STD_LOGIC;
    cfg_done_reg_0 : out STD_LOGIC;
    \FSM_onehot_state_reg[13]\ : out STD_LOGIC;
    \FSM_onehot_state_reg[13]_0\ : out STD_LOGIC;
    \FSM_onehot_state_reg[2]\ : out STD_LOGIC;
    i2c_clk : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    i2c_sda_reg_reg_i_4_0 : in STD_LOGIC;
    i2c_sda_reg_reg_i_41_0 : in STD_LOGIC;
    i2c_sda_reg_reg_i_4_1 : in STD_LOGIC;
    i2c_sda_reg_reg_i_5_0 : in STD_LOGIC;
    i2c_sda_reg_reg_i_17_0 : in STD_LOGIC;
    i2c_sda_reg_reg_i_17_1 : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    sys_rst_n : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_ov5640_top_0_0_ov5640_cfg : entity is "ov5640_cfg";
end system_ov5640_top_0_0_ov5640_cfg;

architecture STRUCTURE of system_ov5640_top_0_0_ov5640_cfg is
  signal cfg_done_i_1_n_0 : STD_LOGIC;
  signal cfg_done_i_3_n_0 : STD_LOGIC;
  signal \^cfg_done_reg_0\ : STD_LOGIC;
  signal cfg_start_0 : STD_LOGIC;
  signal cfg_start_i_2_n_0 : STD_LOGIC;
  signal cfg_start_i_3_n_0 : STD_LOGIC;
  signal cfg_start_i_4_n_0 : STD_LOGIC;
  signal cfg_start_i_5_n_0 : STD_LOGIC;
  signal \cnt_wait[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_wait[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_wait[0]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_wait[0]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_wait[0]_i_6_n_0\ : STD_LOGIC;
  signal cnt_wait_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_wait_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_wait_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_wait_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_wait_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_wait_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_wait_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_wait_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_wait_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_wait_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_wait_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_wait_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_wait_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_wait_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_wait_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_wait_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_wait_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_wait_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_wait_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_wait_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_wait_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_wait_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_wait_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_wait_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_wait_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_wait_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_wait_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_wait_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_wait_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_wait_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_wait_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_wait_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal i2c_sda_reg_reg_i_100_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_101_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_102_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_103_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_104_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_105_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_106_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_107_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_108_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_109_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_10_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_110_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_111_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_112_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_113_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_114_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_115_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_116_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_117_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_118_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_119_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_11_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_120_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_121_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_122_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_123_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_124_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_125_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_126_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_127_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_128_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_129_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_12_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_130_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_131_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_132_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_133_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_134_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_135_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_136_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_137_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_138_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_139_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_13_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_140_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_141_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_142_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_143_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_144_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_14_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_15_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_16_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_17_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_19_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_20_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_21_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_22_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_23_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_24_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_25_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_26_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_27_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_28_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_29_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_30_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_31_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_32_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_33_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_34_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_35_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_36_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_37_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_38_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_39_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_40_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_41_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_43_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_44_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_45_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_46_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_47_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_48_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_49_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_50_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_51_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_52_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_53_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_54_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_55_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_56_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_57_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_58_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_59_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_60_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_61_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_62_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_63_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_64_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_65_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_66_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_67_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_68_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_69_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_70_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_71_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_72_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_73_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_74_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_75_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_76_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_77_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_78_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_79_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_80_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_81_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_82_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_84_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_85_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_86_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_88_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_89_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_8_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_90_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_91_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_92_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_93_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_94_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_95_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_96_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_97_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_98_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_99_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_9_n_0 : STD_LOGIC;
  signal reg_num : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal reg_num_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \reg_num_rep[0]_i_1_n_0\ : STD_LOGIC;
  signal \reg_num_rep[1]_i_1_n_0\ : STD_LOGIC;
  signal \reg_num_rep[2]_i_1_n_0\ : STD_LOGIC;
  signal \reg_num_rep[3]_i_1_n_0\ : STD_LOGIC;
  signal \reg_num_rep[4]_i_1_n_0\ : STD_LOGIC;
  signal \reg_num_rep[5]_i_1_n_0\ : STD_LOGIC;
  signal \reg_num_rep[6]_i_1_n_0\ : STD_LOGIC;
  signal \reg_num_rep[7]_i_1_n_0\ : STD_LOGIC;
  signal \reg_num_rep[7]_i_2_n_0\ : STD_LOGIC;
  signal \^sys_rst_n_0\ : STD_LOGIC;
  signal \NLW_cnt_wait_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_53 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_56 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_9 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_93 : label is "soft_lutpair11";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of \reg_num_reg[0]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg[1]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg[2]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg[3]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg[4]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg[5]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg[6]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg[7]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg_rep[0]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg_rep[1]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg_rep[2]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg_rep[3]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg_rep[4]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg_rep[5]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg_rep[6]\ : label is "no";
  attribute equivalent_register_removal of \reg_num_reg_rep[7]\ : label is "no";
  attribute SOFT_HLUTNM of \reg_num_rep[0]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \reg_num_rep[1]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \reg_num_rep[2]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \reg_num_rep[3]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \reg_num_rep[4]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \reg_num_rep[7]_i_2\ : label is "soft_lutpair12";
begin
  cfg_done_reg_0 <= \^cfg_done_reg_0\;
  sys_rst_n_0 <= \^sys_rst_n_0\;
cfg_done_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF10000000"
    )
        port map (
      I0 => cfg_done_i_3_n_0,
      I1 => reg_num_reg(2),
      I2 => E(0),
      I3 => reg_num_reg(1),
      I4 => reg_num_reg(0),
      I5 => \^cfg_done_reg_0\,
      O => cfg_done_i_1_n_0
    );
cfg_done_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sys_rst_n,
      O => \^sys_rst_n_0\
    );
cfg_done_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => reg_num_reg(6),
      I1 => reg_num_reg(4),
      I2 => reg_num_reg(3),
      I3 => reg_num_reg(5),
      I4 => reg_num_reg(7),
      O => cfg_done_i_3_n_0
    );
cfg_done_reg: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => '1',
      CLR => \^sys_rst_n_0\,
      D => cfg_done_i_1_n_0,
      Q => \^cfg_done_reg_0\
    );
cfg_start_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFCCCC0444"
    )
        port map (
      I0 => reg_num_reg(2),
      I1 => E(0),
      I2 => reg_num_reg(1),
      I3 => reg_num_reg(0),
      I4 => cfg_done_i_3_n_0,
      I5 => cfg_start_i_2_n_0,
      O => cfg_start_0
    );
cfg_start_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => cfg_start_i_3_n_0,
      I1 => cfg_start_i_4_n_0,
      I2 => cnt_wait_reg(0),
      I3 => cnt_wait_reg(1),
      I4 => cnt_wait_reg(2),
      I5 => cfg_start_i_5_n_0,
      O => cfg_start_i_2_n_0
    );
cfg_start_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_wait_reg(13),
      I1 => cnt_wait_reg(11),
      I2 => cnt_wait_reg(8),
      O => cfg_start_i_3_n_0
    );
cfg_start_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => cnt_wait_reg(7),
      I1 => cnt_wait_reg(6),
      I2 => cnt_wait_reg(10),
      I3 => cnt_wait_reg(9),
      O => cfg_start_i_4_n_0
    );
cfg_start_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => cnt_wait_reg(12),
      I1 => cnt_wait_reg(5),
      I2 => cnt_wait_reg(4),
      I3 => cnt_wait_reg(3),
      I4 => cnt_wait_reg(15),
      I5 => cnt_wait_reg(14),
      O => cfg_start_i_5_n_0
    );
cfg_start_reg: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => '1',
      CLR => \^sys_rst_n_0\,
      D => cfg_start_0,
      Q => cfg_start
    );
\cnt_wait[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAABFFFFFFFFF"
    )
        port map (
      I0 => \cnt_wait[0]_i_3_n_0\,
      I1 => cnt_wait_reg(10),
      I2 => cnt_wait_reg(9),
      I3 => cnt_wait_reg(13),
      I4 => cnt_wait_reg(11),
      I5 => \cnt_wait[0]_i_4_n_0\,
      O => \cnt_wait[0]_i_1_n_0\
    );
\cnt_wait[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ABABFFABFFABFFAB"
    )
        port map (
      I0 => \cnt_wait[0]_i_6_n_0\,
      I1 => cnt_wait_reg(12),
      I2 => cnt_wait_reg(13),
      I3 => cfg_start_i_3_n_0,
      I4 => cnt_wait_reg(6),
      I5 => cnt_wait_reg(7),
      O => \cnt_wait[0]_i_3_n_0\
    );
\cnt_wait[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_wait_reg(14),
      I1 => cnt_wait_reg(15),
      O => \cnt_wait[0]_i_4_n_0\
    );
\cnt_wait[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_wait_reg(0),
      O => \cnt_wait[0]_i_5_n_0\
    );
\cnt_wait[0]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000015"
    )
        port map (
      I0 => cnt_wait_reg(5),
      I1 => cnt_wait_reg(3),
      I2 => cnt_wait_reg(4),
      I3 => cnt_wait_reg(8),
      I4 => cnt_wait_reg(11),
      I5 => cnt_wait_reg(13),
      O => \cnt_wait[0]_i_6_n_0\
    );
\cnt_wait_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[0]_i_2_n_7\,
      Q => cnt_wait_reg(0)
    );
\cnt_wait_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_wait_reg[0]_i_2_n_0\,
      CO(2) => \cnt_wait_reg[0]_i_2_n_1\,
      CO(1) => \cnt_wait_reg[0]_i_2_n_2\,
      CO(0) => \cnt_wait_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_wait_reg[0]_i_2_n_4\,
      O(2) => \cnt_wait_reg[0]_i_2_n_5\,
      O(1) => \cnt_wait_reg[0]_i_2_n_6\,
      O(0) => \cnt_wait_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt_wait_reg(3 downto 1),
      S(0) => \cnt_wait[0]_i_5_n_0\
    );
\cnt_wait_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[8]_i_1_n_5\,
      Q => cnt_wait_reg(10)
    );
\cnt_wait_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[8]_i_1_n_4\,
      Q => cnt_wait_reg(11)
    );
\cnt_wait_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[12]_i_1_n_7\,
      Q => cnt_wait_reg(12)
    );
\cnt_wait_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_wait_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_wait_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_wait_reg[12]_i_1_n_1\,
      CO(1) => \cnt_wait_reg[12]_i_1_n_2\,
      CO(0) => \cnt_wait_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_wait_reg[12]_i_1_n_4\,
      O(2) => \cnt_wait_reg[12]_i_1_n_5\,
      O(1) => \cnt_wait_reg[12]_i_1_n_6\,
      O(0) => \cnt_wait_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_wait_reg(15 downto 12)
    );
\cnt_wait_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[12]_i_1_n_6\,
      Q => cnt_wait_reg(13)
    );
\cnt_wait_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[12]_i_1_n_5\,
      Q => cnt_wait_reg(14)
    );
\cnt_wait_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[12]_i_1_n_4\,
      Q => cnt_wait_reg(15)
    );
\cnt_wait_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[0]_i_2_n_6\,
      Q => cnt_wait_reg(1)
    );
\cnt_wait_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[0]_i_2_n_5\,
      Q => cnt_wait_reg(2)
    );
\cnt_wait_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[0]_i_2_n_4\,
      Q => cnt_wait_reg(3)
    );
\cnt_wait_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[4]_i_1_n_7\,
      Q => cnt_wait_reg(4)
    );
\cnt_wait_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_wait_reg[0]_i_2_n_0\,
      CO(3) => \cnt_wait_reg[4]_i_1_n_0\,
      CO(2) => \cnt_wait_reg[4]_i_1_n_1\,
      CO(1) => \cnt_wait_reg[4]_i_1_n_2\,
      CO(0) => \cnt_wait_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_wait_reg[4]_i_1_n_4\,
      O(2) => \cnt_wait_reg[4]_i_1_n_5\,
      O(1) => \cnt_wait_reg[4]_i_1_n_6\,
      O(0) => \cnt_wait_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_wait_reg(7 downto 4)
    );
\cnt_wait_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[4]_i_1_n_6\,
      Q => cnt_wait_reg(5)
    );
\cnt_wait_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[4]_i_1_n_5\,
      Q => cnt_wait_reg(6)
    );
\cnt_wait_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[4]_i_1_n_4\,
      Q => cnt_wait_reg(7)
    );
\cnt_wait_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[8]_i_1_n_7\,
      Q => cnt_wait_reg(8)
    );
\cnt_wait_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_wait_reg[4]_i_1_n_0\,
      CO(3) => \cnt_wait_reg[8]_i_1_n_0\,
      CO(2) => \cnt_wait_reg[8]_i_1_n_1\,
      CO(1) => \cnt_wait_reg[8]_i_1_n_2\,
      CO(0) => \cnt_wait_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_wait_reg[8]_i_1_n_4\,
      O(2) => \cnt_wait_reg[8]_i_1_n_5\,
      O(1) => \cnt_wait_reg[8]_i_1_n_6\,
      O(0) => \cnt_wait_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_wait_reg(11 downto 8)
    );
\cnt_wait_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => \cnt_wait[0]_i_1_n_0\,
      CLR => \^sys_rst_n_0\,
      D => \cnt_wait_reg[8]_i_1_n_6\,
      Q => cnt_wait_reg(9)
    );
i2c_sda_reg_reg_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000F0AACC00"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_23_n_0,
      I1 => i2c_sda_reg_reg_i_24_n_0,
      I2 => i2c_sda_reg_reg_i_25_n_0,
      I3 => i2c_sda_reg_reg_i_4_1,
      I4 => i2c_sda_reg_reg_i_41_0,
      I5 => \^cfg_done_reg_0\,
      O => i2c_sda_reg_reg_i_10_n_0
    );
i2c_sda_reg_reg_i_100: unisim.vcomponents.LUT6
    generic map(
      INIT => X"40554A4015881887"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(2),
      I2 => reg_num(3),
      I3 => reg_num(0),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_100_n_0
    );
i2c_sda_reg_reg_i_101: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0210602948CBD102"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_101_n_0
    );
i2c_sda_reg_reg_i_102: unisim.vcomponents.LUT6
    generic map(
      INIT => X"271040645F206273"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_102_n_0
    );
i2c_sda_reg_reg_i_103: unisim.vcomponents.LUT6
    generic map(
      INIT => X"800020009000E001"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(1),
      I3 => reg_num(5),
      I4 => reg_num(2),
      I5 => reg_num(0),
      O => i2c_sda_reg_reg_i_103_n_0
    );
i2c_sda_reg_reg_i_104: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0107010000200031"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_104_n_0
    );
i2c_sda_reg_reg_i_105: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8980044AB82959C2"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_105_n_0
    );
i2c_sda_reg_reg_i_106: unisim.vcomponents.LUT6
    generic map(
      INIT => X"274513000C603771"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_106_n_0
    );
i2c_sda_reg_reg_i_107: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7610FE00C401E901"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(5),
      I4 => reg_num(1),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_107_n_0
    );
i2c_sda_reg_reg_i_108: unisim.vcomponents.LUT6
    generic map(
      INIT => X"221200502128C101"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(5),
      I4 => reg_num(2),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_108_n_0
    );
i2c_sda_reg_reg_i_109: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1080044024015110"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_109_n_0
    );
i2c_sda_reg_reg_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF11F1"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_26_n_0,
      I1 => i2c_sda_reg_reg_i_41_0,
      I2 => i2c_sda_reg_reg_i_4_1,
      I3 => i2c_sda_reg_reg_i_27_n_0,
      I4 => \^cfg_done_reg_0\,
      I5 => i2c_sda_reg_reg_i_4_0,
      O => i2c_sda_reg_reg_i_11_n_0
    );
i2c_sda_reg_reg_i_110: unisim.vcomponents.LUT6
    generic map(
      INIT => X"146400A64C444031"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(1),
      I4 => reg_num(5),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_110_n_0
    );
i2c_sda_reg_reg_i_111: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => reg_num(3),
      I1 => reg_num(2),
      I2 => reg_num(5),
      I3 => reg_num(1),
      I4 => reg_num(0),
      I5 => reg_num(4),
      O => i2c_sda_reg_reg_i_111_n_0
    );
i2c_sda_reg_reg_i_112: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020080012000001"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(0),
      I2 => reg_num(2),
      I3 => reg_num(5),
      I4 => reg_num(1),
      I5 => reg_num(3),
      O => i2c_sda_reg_reg_i_112_n_0
    );
i2c_sda_reg_reg_i_113: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8000857ABD5540A"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(0),
      I2 => reg_num(1),
      I3 => reg_num(3),
      I4 => reg_num(2),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_113_n_0
    );
i2c_sda_reg_reg_i_114: unisim.vcomponents.LUT6
    generic map(
      INIT => X"402A00AA00AA00AA"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(0),
      I2 => reg_num(1),
      I3 => reg_num(5),
      I4 => reg_num(2),
      I5 => reg_num(3),
      O => i2c_sda_reg_reg_i_114_n_0
    );
i2c_sda_reg_reg_i_115: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA9"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(2),
      I2 => reg_num(1),
      I3 => reg_num(3),
      O => i2c_sda_reg_reg_i_115_n_0
    );
i2c_sda_reg_reg_i_116: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7002240340200402"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(1),
      I3 => reg_num(5),
      I4 => reg_num(2),
      I5 => reg_num(0),
      O => i2c_sda_reg_reg_i_116_n_0
    );
i2c_sda_reg_reg_i_117: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA804C984C0BA114"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(5),
      I4 => reg_num(1),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_117_n_0
    );
i2c_sda_reg_reg_i_118: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B44C4CCCCCCC4CCC"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(2),
      I3 => reg_num(5),
      I4 => reg_num(1),
      I5 => reg_num(0),
      O => i2c_sda_reg_reg_i_118_n_0
    );
i2c_sda_reg_reg_i_119: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCCCCCCC33CC32"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(1),
      I4 => reg_num(5),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_119_n_0
    );
i2c_sda_reg_reg_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF77470000"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_28_n_0,
      I1 => reg_num(6),
      I2 => i2c_sda_reg_reg_i_29_n_0,
      I3 => reg_num(7),
      I4 => i2c_sda_reg_reg_i_41_0,
      I5 => i2c_sda_reg_reg_i_4_1,
      O => i2c_sda_reg_reg_i_12_n_0
    );
i2c_sda_reg_reg_i_120: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0355C04431154411"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_120_n_0
    );
i2c_sda_reg_reg_i_121: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA094DC658522D21"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_121_n_0
    );
i2c_sda_reg_reg_i_122: unisim.vcomponents.LUT6
    generic map(
      INIT => X"807F7FFF7F008000"
    )
        port map (
      I0 => reg_num(2),
      I1 => reg_num(3),
      I2 => reg_num(4),
      I3 => reg_num(5),
      I4 => reg_num(0),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_122_n_0
    );
i2c_sda_reg_reg_i_123: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFFFFFE"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_123_n_0
    );
i2c_sda_reg_reg_i_124: unisim.vcomponents.LUT6
    generic map(
      INIT => X"321B7760048A9E55"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(2),
      I3 => reg_num(0),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_124_n_0
    );
i2c_sda_reg_reg_i_125: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B339152828D4A322"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_125_n_0
    );
i2c_sda_reg_reg_i_126: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F7F77FF70000000"
    )
        port map (
      I0 => reg_num(3),
      I1 => reg_num(4),
      I2 => reg_num(1),
      I3 => reg_num(5),
      I4 => reg_num(0),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_126_n_0
    );
i2c_sda_reg_reg_i_127: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF0000FF00FE"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_127_n_0
    );
i2c_sda_reg_reg_i_128: unisim.vcomponents.LUT6
    generic map(
      INIT => X"671F13E852820A76"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(5),
      I4 => reg_num(1),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_128_n_0
    );
i2c_sda_reg_reg_i_129: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00812198181011A5"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_129_n_0
    );
i2c_sda_reg_reg_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF010DFFFF"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_30_n_0,
      I1 => i2c_sda_reg_reg_i_41_0,
      I2 => i2c_sda_reg_reg_i_4_1,
      I3 => i2c_sda_reg_reg_i_31_n_0,
      I4 => i2c_sda_reg_reg_i_4_0,
      I5 => \^cfg_done_reg_0\,
      O => i2c_sda_reg_reg_i_13_n_0
    );
i2c_sda_reg_reg_i_130: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5155B70110A40045"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(1),
      I4 => reg_num(5),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_130_n_0
    );
i2c_sda_reg_reg_i_131: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00110111606F7B08"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_131_n_0
    );
i2c_sda_reg_reg_i_132: unisim.vcomponents.LUT6
    generic map(
      INIT => X"038A7C2044A82002"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(1),
      I3 => reg_num(5),
      I4 => reg_num(2),
      I5 => reg_num(0),
      O => i2c_sda_reg_reg_i_132_n_0
    );
i2c_sda_reg_reg_i_133: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80340011C0200440"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(1),
      I4 => reg_num(5),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_133_n_0
    );
i2c_sda_reg_reg_i_134: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7380441E4712194B"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_134_n_0
    );
i2c_sda_reg_reg_i_135: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D72F90DBCCD25C14"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(1),
      I4 => reg_num(5),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_135_n_0
    );
i2c_sda_reg_reg_i_136: unisim.vcomponents.LUT6
    generic map(
      INIT => X"050A02D2404820C0"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(2),
      I2 => reg_num(3),
      I3 => reg_num(1),
      I4 => reg_num(5),
      I5 => reg_num(0),
      O => i2c_sda_reg_reg_i_136_n_0
    );
i2c_sda_reg_reg_i_137: unisim.vcomponents.LUT6
    generic map(
      INIT => X"807BA3F5201710D0"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(5),
      I4 => reg_num(2),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_137_n_0
    );
i2c_sda_reg_reg_i_138: unisim.vcomponents.LUT6
    generic map(
      INIT => X"140242CCA4620093"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(1),
      I4 => reg_num(5),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_138_n_0
    );
i2c_sda_reg_reg_i_139: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B96068FAD103B308"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_139_n_0
    );
i2c_sda_reg_reg_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"470000004700FF00"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_32_n_0,
      I1 => reg_num(6),
      I2 => i2c_sda_reg_reg_i_33_n_0,
      I3 => i2c_sda_reg_reg_i_4_1,
      I4 => i2c_sda_reg_reg_i_41_0,
      I5 => i2c_sda_reg_reg_i_34_n_0,
      O => i2c_sda_reg_reg_i_14_n_0
    );
i2c_sda_reg_reg_i_140: unisim.vcomponents.LUT6
    generic map(
      INIT => X"12F5ED2256A28800"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(2),
      I2 => reg_num(1),
      I3 => reg_num(3),
      I4 => reg_num(5),
      I5 => reg_num(0),
      O => i2c_sda_reg_reg_i_140_n_0
    );
i2c_sda_reg_reg_i_141: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1360A4414324110A"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_141_n_0
    );
i2c_sda_reg_reg_i_142: unisim.vcomponents.LUT6
    generic map(
      INIT => X"676F1D0066630025"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_142_n_0
    );
i2c_sda_reg_reg_i_143: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80008CA8900555C4"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_143_n_0
    );
i2c_sda_reg_reg_i_144: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000086019181705"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(5),
      I3 => reg_num(1),
      I4 => reg_num(2),
      I5 => reg_num(0),
      O => i2c_sda_reg_reg_i_144_n_0
    );
i2c_sda_reg_reg_i_15: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00AC0000"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_35_n_0,
      I1 => i2c_sda_reg_reg_i_36_n_0,
      I2 => i2c_sda_reg_reg_i_41_0,
      I3 => \^cfg_done_reg_0\,
      I4 => i2c_sda_reg_reg_i_4_1,
      O => i2c_sda_reg_reg_i_15_n_0
    );
i2c_sda_reg_reg_i_16: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002320"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_37_n_0,
      I1 => \^cfg_done_reg_0\,
      I2 => i2c_sda_reg_reg_i_41_0,
      I3 => i2c_sda_reg_reg_i_38_n_0,
      I4 => i2c_sda_reg_reg_i_4_1,
      O => i2c_sda_reg_reg_i_16_n_0
    );
i2c_sda_reg_reg_i_17: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFEFEFE"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_39_n_0,
      I1 => i2c_sda_reg_reg_i_40_n_0,
      I2 => i2c_sda_reg_reg_i_41_n_0,
      I3 => i2c_sda_reg_reg_i_5_0,
      I4 => i2c_sda_reg_reg_i_43_n_0,
      O => i2c_sda_reg_reg_i_17_n_0
    );
i2c_sda_reg_reg_i_19: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_44_n_0,
      I1 => i2c_sda_reg_reg_i_45_n_0,
      O => i2c_sda_reg_reg_i_19_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_20: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_46_n_0,
      I1 => i2c_sda_reg_reg_i_47_n_0,
      O => i2c_sda_reg_reg_i_20_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_21: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_48_n_0,
      I1 => i2c_sda_reg_reg_i_49_n_0,
      O => i2c_sda_reg_reg_i_21_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_22: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_50_n_0,
      I1 => i2c_sda_reg_reg_i_51_n_0,
      O => i2c_sda_reg_reg_i_22_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_23: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_52_n_0,
      I1 => i2c_sda_reg_reg_i_53_n_0,
      I2 => reg_num(6),
      I3 => i2c_sda_reg_reg_i_54_n_0,
      I4 => reg_num(7),
      O => i2c_sda_reg_reg_i_23_n_0
    );
i2c_sda_reg_reg_i_24: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFAFCFC0"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_55_n_0,
      I1 => i2c_sda_reg_reg_i_56_n_0,
      I2 => reg_num(6),
      I3 => i2c_sda_reg_reg_i_57_n_0,
      I4 => reg_num(7),
      O => i2c_sda_reg_reg_i_24_n_0
    );
i2c_sda_reg_reg_i_25: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BBFC"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_58_n_0,
      I1 => reg_num(6),
      I2 => i2c_sda_reg_reg_i_59_n_0,
      I3 => reg_num(7),
      O => i2c_sda_reg_reg_i_25_n_0
    );
i2c_sda_reg_reg_i_26: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010FFFF00000000"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(0),
      I2 => i2c_sda_reg_reg_i_60_n_0,
      I3 => reg_num(3),
      I4 => reg_num(6),
      I5 => reg_num(7),
      O => i2c_sda_reg_reg_i_26_n_0
    );
i2c_sda_reg_reg_i_27: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_61_n_0,
      I1 => i2c_sda_reg_reg_i_62_n_0,
      O => i2c_sda_reg_reg_i_27_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_28: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_63_n_0,
      I1 => i2c_sda_reg_reg_i_64_n_0,
      O => i2c_sda_reg_reg_i_28_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_29: unisim.vcomponents.LUT6
    generic map(
      INIT => X"201131CC01104440"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_29_n_0
    );
i2c_sda_reg_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFE0E0E0"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_8_n_0,
      I1 => i2c_sda_reg_reg_i_9_n_0,
      I2 => Q(2),
      I3 => i2c_sda_reg_reg_i_10_n_0,
      I4 => Q(0),
      I5 => i2c_sda_reg_reg_i_4_0,
      O => \FSM_onehot_state_reg[13]_0\
    );
i2c_sda_reg_reg_i_30: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_65_n_0,
      I1 => i2c_sda_reg_reg_i_66_n_0,
      O => i2c_sda_reg_reg_i_30_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_31: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_67_n_0,
      I1 => i2c_sda_reg_reg_i_68_n_0,
      O => i2c_sda_reg_reg_i_31_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_32: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_69_n_0,
      I1 => i2c_sda_reg_reg_i_70_n_0,
      O => i2c_sda_reg_reg_i_32_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_33: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_71_n_0,
      I1 => i2c_sda_reg_reg_i_72_n_0,
      O => i2c_sda_reg_reg_i_33_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_34: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_73_n_0,
      I1 => i2c_sda_reg_reg_i_74_n_0,
      O => i2c_sda_reg_reg_i_34_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_35: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_75_n_0,
      I1 => i2c_sda_reg_reg_i_76_n_0,
      O => i2c_sda_reg_reg_i_35_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_36: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_77_n_0,
      I1 => i2c_sda_reg_reg_i_78_n_0,
      O => i2c_sda_reg_reg_i_36_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_37: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_79_n_0,
      I1 => i2c_sda_reg_reg_i_80_n_0,
      O => i2c_sda_reg_reg_i_37_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_38: unisim.vcomponents.MUXF8
     port map (
      I0 => i2c_sda_reg_reg_i_81_n_0,
      I1 => i2c_sda_reg_reg_i_82_n_0,
      O => i2c_sda_reg_reg_i_38_n_0,
      S => reg_num(6)
    );
i2c_sda_reg_reg_i_39: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A8080000A808"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_17_0,
      I1 => i2c_sda_reg_reg_i_84_n_0,
      I2 => reg_num(7),
      I3 => i2c_sda_reg_reg_i_85_n_0,
      I4 => reg_num(6),
      I5 => i2c_sda_reg_reg_i_86_n_0,
      O => i2c_sda_reg_reg_i_39_n_0
    );
i2c_sda_reg_reg_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"111F0000"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_11_n_0,
      I1 => i2c_sda_reg_reg_i_12_n_0,
      I2 => i2c_sda_reg_reg_i_13_n_0,
      I3 => i2c_sda_reg_reg_i_14_n_0,
      I4 => Q(1),
      O => \FSM_onehot_state_reg[2]\
    );
i2c_sda_reg_reg_i_40: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A0A0A8080000A808"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_17_1,
      I1 => i2c_sda_reg_reg_i_88_n_0,
      I2 => reg_num(7),
      I3 => i2c_sda_reg_reg_i_89_n_0,
      I4 => reg_num(6),
      I5 => i2c_sda_reg_reg_i_90_n_0,
      O => i2c_sda_reg_reg_i_40_n_0
    );
i2c_sda_reg_reg_i_41: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4040555000000000"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_4_1,
      I1 => i2c_sda_reg_reg_i_91_n_0,
      I2 => reg_num(6),
      I3 => i2c_sda_reg_reg_i_92_n_0,
      I4 => reg_num(7),
      I5 => i2c_sda_reg_reg_i_93_n_0,
      O => i2c_sda_reg_reg_i_41_n_0
    );
i2c_sda_reg_reg_i_43: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_94_n_0,
      I1 => i2c_sda_reg_reg_i_53_n_0,
      I2 => reg_num(6),
      I3 => i2c_sda_reg_reg_i_95_n_0,
      I4 => reg_num(7),
      I5 => i2c_sda_reg_reg_i_96_n_0,
      O => i2c_sda_reg_reg_i_43_n_0
    );
i2c_sda_reg_reg_i_44: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_97_n_0,
      I1 => i2c_sda_reg_reg_i_98_n_0,
      O => i2c_sda_reg_reg_i_44_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_45: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_99_n_0,
      I1 => i2c_sda_reg_reg_i_100_n_0,
      O => i2c_sda_reg_reg_i_45_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_46: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_101_n_0,
      I1 => i2c_sda_reg_reg_i_102_n_0,
      O => i2c_sda_reg_reg_i_46_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_47: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_103_n_0,
      I1 => i2c_sda_reg_reg_i_104_n_0,
      O => i2c_sda_reg_reg_i_47_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_48: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_105_n_0,
      I1 => i2c_sda_reg_reg_i_106_n_0,
      O => i2c_sda_reg_reg_i_48_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_49: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_107_n_0,
      I1 => i2c_sda_reg_reg_i_108_n_0,
      O => i2c_sda_reg_reg_i_49_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFE00000E0E00000"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_15_n_0,
      I1 => i2c_sda_reg_reg_i_16_n_0,
      I2 => Q(2),
      I3 => i2c_sda_reg_reg_i_17_n_0,
      I4 => i2c_sda_reg_reg_i_4_0,
      I5 => Q(0),
      O => \FSM_onehot_state_reg[13]\
    );
i2c_sda_reg_reg_i_50: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_109_n_0,
      I1 => i2c_sda_reg_reg_i_110_n_0,
      O => i2c_sda_reg_reg_i_50_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_51: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_111_n_0,
      I1 => i2c_sda_reg_reg_i_112_n_0,
      O => i2c_sda_reg_reg_i_51_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_52: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555F575DEAAAAAAA"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(1),
      I2 => reg_num(2),
      I3 => reg_num(0),
      I4 => reg_num(3),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_52_n_0
    );
i2c_sda_reg_reg_i_53: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => reg_num(3),
      I1 => reg_num(1),
      I2 => reg_num(5),
      I3 => reg_num(2),
      I4 => reg_num(4),
      O => i2c_sda_reg_reg_i_53_n_0
    );
i2c_sda_reg_reg_i_54: unisim.vcomponents.LUT6
    generic map(
      INIT => X"ADBFAFBFFFFFFFFF"
    )
        port map (
      I0 => reg_num(2),
      I1 => reg_num(1),
      I2 => reg_num(5),
      I3 => reg_num(3),
      I4 => reg_num(0),
      I5 => reg_num(4),
      O => i2c_sda_reg_reg_i_54_n_0
    );
i2c_sda_reg_reg_i_55: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0200A8A215555555"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(1),
      I2 => reg_num(2),
      I3 => reg_num(0),
      I4 => reg_num(3),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_55_n_0
    );
i2c_sda_reg_reg_i_56: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => reg_num(3),
      I1 => reg_num(5),
      I2 => reg_num(1),
      I3 => reg_num(2),
      I4 => reg_num(4),
      O => i2c_sda_reg_reg_i_56_n_0
    );
i2c_sda_reg_reg_i_57: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4640444000000000"
    )
        port map (
      I0 => reg_num(2),
      I1 => reg_num(5),
      I2 => reg_num(1),
      I3 => reg_num(3),
      I4 => reg_num(0),
      I5 => reg_num(4),
      O => i2c_sda_reg_reg_i_57_n_0
    );
i2c_sda_reg_reg_i_58: unisim.vcomponents.LUT6
    generic map(
      INIT => X"575F575DFFFFFFFF"
    )
        port map (
      I0 => reg_num(5),
      I1 => reg_num(1),
      I2 => reg_num(2),
      I3 => reg_num(0),
      I4 => reg_num(3),
      I5 => reg_num(4),
      O => i2c_sda_reg_reg_i_58_n_0
    );
i2c_sda_reg_reg_i_59: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDBFAFBFFFFFFFFF"
    )
        port map (
      I0 => reg_num(2),
      I1 => reg_num(1),
      I2 => reg_num(5),
      I3 => reg_num(3),
      I4 => reg_num(0),
      I5 => reg_num(4),
      O => i2c_sda_reg_reg_i_59_n_0
    );
i2c_sda_reg_reg_i_60: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => reg_num(1),
      I1 => reg_num(5),
      I2 => reg_num(2),
      O => i2c_sda_reg_reg_i_60_n_0
    );
i2c_sda_reg_reg_i_61: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_113_n_0,
      I1 => i2c_sda_reg_reg_i_114_n_0,
      O => i2c_sda_reg_reg_i_61_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_62: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_115_n_0,
      I1 => i2c_sda_reg_reg_i_116_n_0,
      O => i2c_sda_reg_reg_i_62_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_63: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF00FE00"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(2),
      I3 => reg_num(5),
      I4 => reg_num(1),
      O => i2c_sda_reg_reg_i_63_n_0
    );
i2c_sda_reg_reg_i_64: unisim.vcomponents.LUT6
    generic map(
      INIT => X"010000208002080A"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(0),
      I2 => reg_num(5),
      I3 => reg_num(1),
      I4 => reg_num(2),
      I5 => reg_num(3),
      O => i2c_sda_reg_reg_i_64_n_0
    );
i2c_sda_reg_reg_i_65: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_117_n_0,
      I1 => i2c_sda_reg_reg_i_118_n_0,
      O => i2c_sda_reg_reg_i_65_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_66: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_119_n_0,
      I1 => i2c_sda_reg_reg_i_120_n_0,
      O => i2c_sda_reg_reg_i_66_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_67: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_121_n_0,
      I1 => i2c_sda_reg_reg_i_122_n_0,
      O => i2c_sda_reg_reg_i_67_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_68: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_123_n_0,
      I1 => i2c_sda_reg_reg_i_124_n_0,
      O => i2c_sda_reg_reg_i_68_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_69: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000001"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(2),
      I3 => reg_num(5),
      I4 => reg_num(1),
      I5 => reg_num(0),
      O => i2c_sda_reg_reg_i_69_n_0
    );
i2c_sda_reg_reg_i_70: unisim.vcomponents.LUT6
    generic map(
      INIT => X"712D1E1E63811E5B"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_70_n_0
    );
i2c_sda_reg_reg_i_71: unisim.vcomponents.LUT6
    generic map(
      INIT => X"587A623E584FD1A3"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(5),
      I4 => reg_num(2),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_71_n_0
    );
i2c_sda_reg_reg_i_72: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF7FFF0000"
    )
        port map (
      I0 => reg_num(1),
      I1 => reg_num(2),
      I2 => reg_num(3),
      I3 => reg_num(4),
      I4 => reg_num(0),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_72_n_0
    );
i2c_sda_reg_reg_i_73: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_125_n_0,
      I1 => i2c_sda_reg_reg_i_126_n_0,
      O => i2c_sda_reg_reg_i_73_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_74: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_127_n_0,
      I1 => i2c_sda_reg_reg_i_128_n_0,
      O => i2c_sda_reg_reg_i_74_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_75: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_129_n_0,
      I1 => i2c_sda_reg_reg_i_130_n_0,
      O => i2c_sda_reg_reg_i_75_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_76: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_131_n_0,
      I1 => i2c_sda_reg_reg_i_132_n_0,
      O => i2c_sda_reg_reg_i_76_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_77: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_133_n_0,
      I1 => i2c_sda_reg_reg_i_134_n_0,
      O => i2c_sda_reg_reg_i_77_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_78: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_135_n_0,
      I1 => i2c_sda_reg_reg_i_136_n_0,
      O => i2c_sda_reg_reg_i_78_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_79: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_137_n_0,
      I1 => i2c_sda_reg_reg_i_138_n_0,
      O => i2c_sda_reg_reg_i_79_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_8: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00AC0000"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_19_n_0,
      I1 => i2c_sda_reg_reg_i_20_n_0,
      I2 => i2c_sda_reg_reg_i_41_0,
      I3 => \^cfg_done_reg_0\,
      I4 => i2c_sda_reg_reg_i_4_1,
      O => i2c_sda_reg_reg_i_8_n_0
    );
i2c_sda_reg_reg_i_80: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_139_n_0,
      I1 => i2c_sda_reg_reg_i_140_n_0,
      O => i2c_sda_reg_reg_i_80_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_81: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_141_n_0,
      I1 => i2c_sda_reg_reg_i_142_n_0,
      O => i2c_sda_reg_reg_i_81_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_82: unisim.vcomponents.MUXF7
     port map (
      I0 => i2c_sda_reg_reg_i_143_n_0,
      I1 => i2c_sda_reg_reg_i_144_n_0,
      O => i2c_sda_reg_reg_i_82_n_0,
      S => reg_num(7)
    );
i2c_sda_reg_reg_i_84: unisim.vcomponents.LUT6
    generic map(
      INIT => X"115D64E65574CCEA"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_84_n_0
    );
i2c_sda_reg_reg_i_85: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EA00FFFF80000000"
    )
        port map (
      I0 => reg_num(2),
      I1 => reg_num(0),
      I2 => reg_num(1),
      I3 => reg_num(3),
      I4 => reg_num(4),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_85_n_0
    );
i2c_sda_reg_reg_i_86: unisim.vcomponents.LUT6
    generic map(
      INIT => X"060024002C022401"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(2),
      I3 => reg_num(5),
      I4 => reg_num(1),
      I5 => reg_num(0),
      O => i2c_sda_reg_reg_i_86_n_0
    );
i2c_sda_reg_reg_i_88: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000066AA00283227"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(1),
      I4 => reg_num(5),
      I5 => reg_num(2),
      O => i2c_sda_reg_reg_i_88_n_0
    );
i2c_sda_reg_reg_i_89: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF00007FFFFFFF"
    )
        port map (
      I0 => reg_num(0),
      I1 => reg_num(1),
      I2 => reg_num(2),
      I3 => reg_num(3),
      I4 => reg_num(4),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_89_n_0
    );
i2c_sda_reg_reg_i_9: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002320"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_21_n_0,
      I1 => \^cfg_done_reg_0\,
      I2 => i2c_sda_reg_reg_i_41_0,
      I3 => i2c_sda_reg_reg_i_22_n_0,
      I4 => i2c_sda_reg_reg_i_4_1,
      O => i2c_sda_reg_reg_i_9_n_0
    );
i2c_sda_reg_reg_i_90: unisim.vcomponents.LUT6
    generic map(
      INIT => X"06000C0015541555"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(2),
      I3 => reg_num(1),
      I4 => reg_num(0),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_90_n_0
    );
i2c_sda_reg_reg_i_91: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0F108F0FF0F0F0C0"
    )
        port map (
      I0 => reg_num(0),
      I1 => reg_num(1),
      I2 => reg_num(4),
      I3 => reg_num(2),
      I4 => reg_num(3),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_91_n_0
    );
i2c_sda_reg_reg_i_92: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DD448A00DC472200"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_92_n_0
    );
i2c_sda_reg_reg_i_93: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^cfg_done_reg_0\,
      I1 => i2c_sda_reg_reg_i_41_0,
      O => i2c_sda_reg_reg_i_93_n_0
    );
i2c_sda_reg_reg_i_94: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3438383003333330"
    )
        port map (
      I0 => reg_num(0),
      I1 => reg_num(4),
      I2 => reg_num(3),
      I3 => reg_num(2),
      I4 => reg_num(1),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_94_n_0
    );
i2c_sda_reg_reg_i_95: unisim.vcomponents.LUT6
    generic map(
      INIT => X"222A2A2A00000000"
    )
        port map (
      I0 => reg_num(5),
      I1 => reg_num(3),
      I2 => reg_num(2),
      I3 => reg_num(0),
      I4 => reg_num(1),
      I5 => reg_num(4),
      O => i2c_sda_reg_reg_i_95_n_0
    );
i2c_sda_reg_reg_i_96: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88891139ECCCE6EA"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(1),
      I4 => reg_num(2),
      I5 => reg_num(5),
      O => i2c_sda_reg_reg_i_96_n_0
    );
i2c_sda_reg_reg_i_97: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5A7160A03CB511C3"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(5),
      I4 => reg_num(2),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_97_n_0
    );
i2c_sda_reg_reg_i_98: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4B584C34D27E26F1"
    )
        port map (
      I0 => reg_num(4),
      I1 => reg_num(3),
      I2 => reg_num(0),
      I3 => reg_num(2),
      I4 => reg_num(5),
      I5 => reg_num(1),
      O => i2c_sda_reg_reg_i_98_n_0
    );
i2c_sda_reg_reg_i_99: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000005141404"
    )
        port map (
      I0 => reg_num(3),
      I1 => reg_num(0),
      I2 => reg_num(2),
      I3 => reg_num(5),
      I4 => reg_num(1),
      I5 => reg_num(4),
      O => i2c_sda_reg_reg_i_99_n_0
    );
\reg_num_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[0]_i_1_n_0\,
      Q => reg_num_reg(0)
    );
\reg_num_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[1]_i_1_n_0\,
      Q => reg_num_reg(1)
    );
\reg_num_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[2]_i_1_n_0\,
      Q => reg_num_reg(2)
    );
\reg_num_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[3]_i_1_n_0\,
      Q => reg_num_reg(3)
    );
\reg_num_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[4]_i_1_n_0\,
      Q => reg_num_reg(4)
    );
\reg_num_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[5]_i_1_n_0\,
      Q => reg_num_reg(5)
    );
\reg_num_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[6]_i_1_n_0\,
      Q => reg_num_reg(6)
    );
\reg_num_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[7]_i_1_n_0\,
      Q => reg_num_reg(7)
    );
\reg_num_reg_rep[0]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[0]_i_1_n_0\,
      Q => reg_num(0)
    );
\reg_num_reg_rep[1]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[1]_i_1_n_0\,
      Q => reg_num(1)
    );
\reg_num_reg_rep[2]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[2]_i_1_n_0\,
      Q => reg_num(2)
    );
\reg_num_reg_rep[3]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[3]_i_1_n_0\,
      Q => reg_num(3)
    );
\reg_num_reg_rep[4]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[4]_i_1_n_0\,
      Q => reg_num(4)
    );
\reg_num_reg_rep[5]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[5]_i_1_n_0\,
      Q => reg_num(5)
    );
\reg_num_reg_rep[6]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[6]_i_1_n_0\,
      Q => reg_num(6)
    );
\reg_num_reg_rep[7]\: unisim.vcomponents.FDCE
     port map (
      C => i2c_clk,
      CE => E(0),
      CLR => \^sys_rst_n_0\,
      D => \reg_num_rep[7]_i_1_n_0\,
      Q => reg_num(7)
    );
\reg_num_rep[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reg_num_reg(0),
      O => \reg_num_rep[0]_i_1_n_0\
    );
\reg_num_rep[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => reg_num_reg(0),
      I1 => reg_num_reg(1),
      O => \reg_num_rep[1]_i_1_n_0\
    );
\reg_num_rep[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => reg_num_reg(1),
      I1 => reg_num_reg(0),
      I2 => reg_num_reg(2),
      O => \reg_num_rep[2]_i_1_n_0\
    );
\reg_num_rep[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => reg_num_reg(2),
      I1 => reg_num_reg(0),
      I2 => reg_num_reg(1),
      I3 => reg_num_reg(3),
      O => \reg_num_rep[3]_i_1_n_0\
    );
\reg_num_rep[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => reg_num_reg(3),
      I1 => reg_num_reg(1),
      I2 => reg_num_reg(0),
      I3 => reg_num_reg(2),
      I4 => reg_num_reg(4),
      O => \reg_num_rep[4]_i_1_n_0\
    );
\reg_num_rep[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => reg_num_reg(1),
      I1 => reg_num_reg(0),
      I2 => reg_num_reg(2),
      I3 => reg_num_reg(3),
      I4 => reg_num_reg(4),
      I5 => reg_num_reg(5),
      O => \reg_num_rep[5]_i_1_n_0\
    );
\reg_num_rep[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFF4000"
    )
        port map (
      I0 => \reg_num_rep[7]_i_2_n_0\,
      I1 => reg_num_reg(4),
      I2 => reg_num_reg(3),
      I3 => reg_num_reg(5),
      I4 => reg_num_reg(6),
      O => \reg_num_rep[6]_i_1_n_0\
    );
\reg_num_rep[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF40000000"
    )
        port map (
      I0 => \reg_num_rep[7]_i_2_n_0\,
      I1 => reg_num_reg(5),
      I2 => reg_num_reg(3),
      I3 => reg_num_reg(4),
      I4 => reg_num_reg(6),
      I5 => reg_num_reg(7),
      O => \reg_num_rep[7]_i_1_n_0\
    );
\reg_num_rep[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => reg_num_reg(1),
      I1 => reg_num_reg(0),
      I2 => reg_num_reg(2),
      O => \reg_num_rep[7]_i_2_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_ov5640_top_0_0_ov5640_data_in is
  port (
    ov5640_wr_en : out STD_LOGIC;
    ov5640_data_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ov5640_pclk : in STD_LOGIC;
    sys_rst_n : in STD_LOGIC;
    sys_init_done : in STD_LOGIC;
    ov5640_href : in STD_LOGIC;
    ov5640_data : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_ov5640_top_0_0_ov5640_data_in : entity is "ov5640_data_in";
end system_ov5640_top_0_0_ov5640_data_in;

architecture STRUCTURE of system_ov5640_top_0_0_ov5640_data_in is
  signal data_flag : STD_LOGIC;
  signal data_flag_dly1_i_1_n_0 : STD_LOGIC;
  signal data_flag_reg_n_0 : STD_LOGIC;
  signal data_out_reg : STD_LOGIC;
  signal pic_data_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \pic_data_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \pic_data_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \pic_data_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \pic_data_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \pic_data_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \pic_data_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \pic_data_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \pic_data_reg[7]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \pic_data_reg[0]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \pic_data_reg[1]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \pic_data_reg[2]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \pic_data_reg[3]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \pic_data_reg[4]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \pic_data_reg[5]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \pic_data_reg[6]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \pic_data_reg[7]_i_1\ : label is "soft_lutpair14";
begin
data_flag_dly1_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => sys_rst_n,
      I1 => sys_init_done,
      O => data_flag_dly1_i_1_n_0
    );
data_flag_dly1_reg: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => data_flag_reg_n_0,
      Q => ov5640_wr_en
    );
data_flag_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ov5640_href,
      I1 => data_flag_reg_n_0,
      O => data_flag
    );
data_flag_reg: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => data_flag,
      Q => data_flag_reg_n_0
    );
\data_out_reg[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => data_flag_reg_n_0,
      O => data_out_reg
    );
\data_out_reg_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => ov5640_data(0),
      Q => ov5640_data_out(0)
    );
\data_out_reg_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => pic_data_reg(2),
      Q => ov5640_data_out(10)
    );
\data_out_reg_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => pic_data_reg(3),
      Q => ov5640_data_out(11)
    );
\data_out_reg_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => pic_data_reg(4),
      Q => ov5640_data_out(12)
    );
\data_out_reg_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => pic_data_reg(5),
      Q => ov5640_data_out(13)
    );
\data_out_reg_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => pic_data_reg(6),
      Q => ov5640_data_out(14)
    );
\data_out_reg_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => pic_data_reg(7),
      Q => ov5640_data_out(15)
    );
\data_out_reg_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => ov5640_data(1),
      Q => ov5640_data_out(1)
    );
\data_out_reg_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => ov5640_data(2),
      Q => ov5640_data_out(2)
    );
\data_out_reg_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => ov5640_data(3),
      Q => ov5640_data_out(3)
    );
\data_out_reg_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => ov5640_data(4),
      Q => ov5640_data_out(4)
    );
\data_out_reg_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => ov5640_data(5),
      Q => ov5640_data_out(5)
    );
\data_out_reg_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => ov5640_data(6),
      Q => ov5640_data_out(6)
    );
\data_out_reg_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => ov5640_data(7),
      Q => ov5640_data_out(7)
    );
\data_out_reg_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => pic_data_reg(0),
      Q => ov5640_data_out(8)
    );
\data_out_reg_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => data_out_reg,
      CLR => data_flag_dly1_i_1_n_0,
      D => pic_data_reg(1),
      Q => ov5640_data_out(9)
    );
\pic_data_reg[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => ov5640_data(0),
      O => \pic_data_reg[0]_i_1_n_0\
    );
\pic_data_reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => ov5640_data(1),
      O => \pic_data_reg[1]_i_1_n_0\
    );
\pic_data_reg[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => ov5640_data(2),
      O => \pic_data_reg[2]_i_1_n_0\
    );
\pic_data_reg[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => ov5640_data(3),
      O => \pic_data_reg[3]_i_1_n_0\
    );
\pic_data_reg[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => ov5640_data(4),
      O => \pic_data_reg[4]_i_1_n_0\
    );
\pic_data_reg[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => ov5640_data(5),
      O => \pic_data_reg[5]_i_1_n_0\
    );
\pic_data_reg[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => ov5640_data(6),
      O => \pic_data_reg[6]_i_1_n_0\
    );
\pic_data_reg[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => ov5640_data(7),
      O => \pic_data_reg[7]_i_1_n_0\
    );
\pic_data_reg_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => \pic_data_reg[0]_i_1_n_0\,
      Q => pic_data_reg(0)
    );
\pic_data_reg_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => \pic_data_reg[1]_i_1_n_0\,
      Q => pic_data_reg(1)
    );
\pic_data_reg_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => \pic_data_reg[2]_i_1_n_0\,
      Q => pic_data_reg(2)
    );
\pic_data_reg_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => \pic_data_reg[3]_i_1_n_0\,
      Q => pic_data_reg(3)
    );
\pic_data_reg_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => \pic_data_reg[4]_i_1_n_0\,
      Q => pic_data_reg(4)
    );
\pic_data_reg_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => \pic_data_reg[5]_i_1_n_0\,
      Q => pic_data_reg(5)
    );
\pic_data_reg_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => \pic_data_reg[6]_i_1_n_0\,
      Q => pic_data_reg(6)
    );
\pic_data_reg_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => data_flag_dly1_i_1_n_0,
      D => \pic_data_reg[7]_i_1_n_0\,
      Q => pic_data_reg(7)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_ov5640_top_0_0_ov5640_top is
  port (
    href_v_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ov5640_wr_en : out STD_LOGIC;
    ov5640_data_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    cfg_done_reg : out STD_LOGIC;
    sccb_scl : out STD_LOGIC;
    href_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 );
    sccb_sda : inout STD_LOGIC;
    sys_rst_n : in STD_LOGIC;
    sys_init_done : in STD_LOGIC;
    ov5640_vsync : in STD_LOGIC;
    ov5640_href : in STD_LOGIC;
    ov5640_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    sys_clk : in STD_LOGIC;
    ov5640_pclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_ov5640_top_0_0_ov5640_top : entity is "ov5640_top";
end system_ov5640_top_0_0_ov5640_top;

architecture STRUCTURE of system_ov5640_top_0_0_ov5640_top is
  signal \^cfg_done_reg\ : STD_LOGIC;
  signal cfg_start : STD_LOGIC;
  signal \^href_cnt\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \href_cnt[11]_i_2_n_0\ : STD_LOGIC;
  signal \href_cnt[11]_i_3_n_0\ : STD_LOGIC;
  signal \href_cnt[11]_i_4_n_0\ : STD_LOGIC;
  signal \href_cnt[11]_i_5_n_0\ : STD_LOGIC;
  signal \href_cnt[15]_i_2_n_0\ : STD_LOGIC;
  signal \href_cnt[15]_i_3_n_0\ : STD_LOGIC;
  signal \href_cnt[15]_i_4_n_0\ : STD_LOGIC;
  signal \href_cnt[15]_i_5_n_0\ : STD_LOGIC;
  signal \href_cnt[19]_i_2_n_0\ : STD_LOGIC;
  signal \href_cnt[19]_i_3_n_0\ : STD_LOGIC;
  signal \href_cnt[19]_i_4_n_0\ : STD_LOGIC;
  signal \href_cnt[19]_i_5_n_0\ : STD_LOGIC;
  signal \href_cnt[23]_i_2_n_0\ : STD_LOGIC;
  signal \href_cnt[23]_i_3_n_0\ : STD_LOGIC;
  signal \href_cnt[23]_i_4_n_0\ : STD_LOGIC;
  signal \href_cnt[23]_i_5_n_0\ : STD_LOGIC;
  signal \href_cnt[27]_i_2_n_0\ : STD_LOGIC;
  signal \href_cnt[27]_i_3_n_0\ : STD_LOGIC;
  signal \href_cnt[27]_i_4_n_0\ : STD_LOGIC;
  signal \href_cnt[27]_i_5_n_0\ : STD_LOGIC;
  signal \href_cnt[31]_i_2_n_0\ : STD_LOGIC;
  signal \href_cnt[31]_i_3_n_0\ : STD_LOGIC;
  signal \href_cnt[31]_i_4_n_0\ : STD_LOGIC;
  signal \href_cnt[31]_i_5_n_0\ : STD_LOGIC;
  signal \href_cnt[3]_i_2_n_0\ : STD_LOGIC;
  signal \href_cnt[3]_i_3_n_0\ : STD_LOGIC;
  signal \href_cnt[3]_i_4_n_0\ : STD_LOGIC;
  signal \href_cnt[3]_i_5_n_0\ : STD_LOGIC;
  signal \href_cnt[7]_i_2_n_0\ : STD_LOGIC;
  signal \href_cnt[7]_i_3_n_0\ : STD_LOGIC;
  signal \href_cnt[7]_i_4_n_0\ : STD_LOGIC;
  signal \href_cnt[7]_i_5_n_0\ : STD_LOGIC;
  signal \href_cnt_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \href_cnt_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \href_cnt_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \href_cnt_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \href_cnt_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \href_cnt_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \href_cnt_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \href_cnt_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \href_cnt_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \href_cnt_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \href_cnt_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \href_cnt_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \href_cnt_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \href_cnt_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \href_cnt_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \href_cnt_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \href_cnt_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \href_cnt_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \href_cnt_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \href_cnt_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \href_cnt_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \href_cnt_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \href_cnt_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \href_cnt_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \href_cnt_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \href_cnt_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \href_cnt_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \href_cnt_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \href_cnt_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \href_cnt_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \href_cnt_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \href_cnt_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \href_cnt_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \href_cnt_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \href_cnt_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \href_cnt_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \href_cnt_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \href_cnt_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \href_cnt_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \href_cnt_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \href_cnt_reg[31]_i_1_n_1\ : STD_LOGIC;
  signal \href_cnt_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \href_cnt_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \href_cnt_reg[31]_i_1_n_4\ : STD_LOGIC;
  signal \href_cnt_reg[31]_i_1_n_5\ : STD_LOGIC;
  signal \href_cnt_reg[31]_i_1_n_6\ : STD_LOGIC;
  signal \href_cnt_reg[31]_i_1_n_7\ : STD_LOGIC;
  signal \href_cnt_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \href_cnt_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \href_cnt_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \href_cnt_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \href_cnt_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \href_cnt_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \href_cnt_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \href_cnt_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \href_cnt_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \href_cnt_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \href_cnt_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \href_cnt_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \href_cnt_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \href_cnt_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \href_cnt_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \href_cnt_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal href_reg : STD_LOGIC;
  signal \^href_v_cnt\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \href_v_cnt[11]_i_2_n_0\ : STD_LOGIC;
  signal \href_v_cnt[11]_i_3_n_0\ : STD_LOGIC;
  signal \href_v_cnt[11]_i_4_n_0\ : STD_LOGIC;
  signal \href_v_cnt[11]_i_5_n_0\ : STD_LOGIC;
  signal \href_v_cnt[15]_i_2_n_0\ : STD_LOGIC;
  signal \href_v_cnt[15]_i_3_n_0\ : STD_LOGIC;
  signal \href_v_cnt[15]_i_4_n_0\ : STD_LOGIC;
  signal \href_v_cnt[15]_i_5_n_0\ : STD_LOGIC;
  signal \href_v_cnt[19]_i_2_n_0\ : STD_LOGIC;
  signal \href_v_cnt[19]_i_3_n_0\ : STD_LOGIC;
  signal \href_v_cnt[19]_i_4_n_0\ : STD_LOGIC;
  signal \href_v_cnt[19]_i_5_n_0\ : STD_LOGIC;
  signal \href_v_cnt[23]_i_2_n_0\ : STD_LOGIC;
  signal \href_v_cnt[23]_i_3_n_0\ : STD_LOGIC;
  signal \href_v_cnt[23]_i_4_n_0\ : STD_LOGIC;
  signal \href_v_cnt[23]_i_5_n_0\ : STD_LOGIC;
  signal \href_v_cnt[27]_i_2_n_0\ : STD_LOGIC;
  signal \href_v_cnt[27]_i_3_n_0\ : STD_LOGIC;
  signal \href_v_cnt[27]_i_4_n_0\ : STD_LOGIC;
  signal \href_v_cnt[27]_i_5_n_0\ : STD_LOGIC;
  signal \href_v_cnt[31]_i_1_n_0\ : STD_LOGIC;
  signal \href_v_cnt[31]_i_3_n_0\ : STD_LOGIC;
  signal \href_v_cnt[31]_i_4_n_0\ : STD_LOGIC;
  signal \href_v_cnt[31]_i_5_n_0\ : STD_LOGIC;
  signal \href_v_cnt[31]_i_6_n_0\ : STD_LOGIC;
  signal \href_v_cnt[3]_i_2_n_0\ : STD_LOGIC;
  signal \href_v_cnt[3]_i_3_n_0\ : STD_LOGIC;
  signal \href_v_cnt[3]_i_4_n_0\ : STD_LOGIC;
  signal \href_v_cnt[3]_i_5_n_0\ : STD_LOGIC;
  signal \href_v_cnt[3]_i_6_n_0\ : STD_LOGIC;
  signal \href_v_cnt[7]_i_2_n_0\ : STD_LOGIC;
  signal \href_v_cnt[7]_i_3_n_0\ : STD_LOGIC;
  signal \href_v_cnt[7]_i_4_n_0\ : STD_LOGIC;
  signal \href_v_cnt[7]_i_5_n_0\ : STD_LOGIC;
  signal \href_v_cnt_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \href_v_cnt_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \href_v_cnt_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \href_v_cnt_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \href_v_cnt_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \href_v_cnt_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \href_v_cnt_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \href_v_cnt_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \href_v_cnt_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \href_v_cnt_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \href_v_cnt_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \href_v_cnt_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \href_v_cnt_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \href_v_cnt_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \href_v_cnt_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \href_v_cnt_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \href_v_cnt_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \href_v_cnt_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \href_v_cnt_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \href_v_cnt_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \href_v_cnt_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \href_v_cnt_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \href_v_cnt_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \href_v_cnt_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \href_v_cnt_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \href_v_cnt_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \href_v_cnt_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \href_v_cnt_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \href_v_cnt_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \href_v_cnt_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \href_v_cnt_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \href_v_cnt_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \href_v_cnt_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \href_v_cnt_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \href_v_cnt_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \href_v_cnt_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \href_v_cnt_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \href_v_cnt_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \href_v_cnt_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \href_v_cnt_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \href_v_cnt_reg[31]_i_2_n_1\ : STD_LOGIC;
  signal \href_v_cnt_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \href_v_cnt_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \href_v_cnt_reg[31]_i_2_n_4\ : STD_LOGIC;
  signal \href_v_cnt_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \href_v_cnt_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \href_v_cnt_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \href_v_cnt_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \href_v_cnt_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \href_v_cnt_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \href_v_cnt_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \href_v_cnt_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \href_v_cnt_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \href_v_cnt_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \href_v_cnt_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \href_v_cnt_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \href_v_cnt_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \href_v_cnt_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \href_v_cnt_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \href_v_cnt_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \href_v_cnt_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \href_v_cnt_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \href_v_cnt_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal i2c_clk : STD_LOGIC;
  signal i2c_end : STD_LOGIC;
  signal iic_ctrl_n_10 : STD_LOGIC;
  signal iic_ctrl_n_11 : STD_LOGIC;
  signal iic_ctrl_n_2 : STD_LOGIC;
  signal iic_ctrl_n_3 : STD_LOGIC;
  signal iic_ctrl_n_4 : STD_LOGIC;
  signal iic_ctrl_n_5 : STD_LOGIC;
  signal iic_ctrl_n_6 : STD_LOGIC;
  signal iic_ctrl_n_7 : STD_LOGIC;
  signal iic_ctrl_n_8 : STD_LOGIC;
  signal ov5640_cfg_n_1 : STD_LOGIC;
  signal ov5640_cfg_n_3 : STD_LOGIC;
  signal ov5640_cfg_n_4 : STD_LOGIC;
  signal ov5640_cfg_n_5 : STD_LOGIC;
  signal \NLW_href_cnt_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_href_v_cnt_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  cfg_done_reg <= \^cfg_done_reg\;
  href_cnt(31 downto 0) <= \^href_cnt\(31 downto 0);
  href_v_cnt(31 downto 0) <= \^href_v_cnt\(31 downto 0);
\href_cnt[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(11),
      O => \href_cnt[11]_i_2_n_0\
    );
\href_cnt[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(10),
      O => \href_cnt[11]_i_3_n_0\
    );
\href_cnt[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(9),
      O => \href_cnt[11]_i_4_n_0\
    );
\href_cnt[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(8),
      O => \href_cnt[11]_i_5_n_0\
    );
\href_cnt[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(15),
      O => \href_cnt[15]_i_2_n_0\
    );
\href_cnt[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(14),
      O => \href_cnt[15]_i_3_n_0\
    );
\href_cnt[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(13),
      O => \href_cnt[15]_i_4_n_0\
    );
\href_cnt[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(12),
      O => \href_cnt[15]_i_5_n_0\
    );
\href_cnt[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(19),
      O => \href_cnt[19]_i_2_n_0\
    );
\href_cnt[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(18),
      O => \href_cnt[19]_i_3_n_0\
    );
\href_cnt[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(17),
      O => \href_cnt[19]_i_4_n_0\
    );
\href_cnt[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(16),
      O => \href_cnt[19]_i_5_n_0\
    );
\href_cnt[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(23),
      O => \href_cnt[23]_i_2_n_0\
    );
\href_cnt[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(22),
      O => \href_cnt[23]_i_3_n_0\
    );
\href_cnt[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(21),
      O => \href_cnt[23]_i_4_n_0\
    );
\href_cnt[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(20),
      O => \href_cnt[23]_i_5_n_0\
    );
\href_cnt[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(27),
      O => \href_cnt[27]_i_2_n_0\
    );
\href_cnt[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(26),
      O => \href_cnt[27]_i_3_n_0\
    );
\href_cnt[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(25),
      O => \href_cnt[27]_i_4_n_0\
    );
\href_cnt[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(24),
      O => \href_cnt[27]_i_5_n_0\
    );
\href_cnt[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(31),
      O => \href_cnt[31]_i_2_n_0\
    );
\href_cnt[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(30),
      O => \href_cnt[31]_i_3_n_0\
    );
\href_cnt[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(29),
      O => \href_cnt[31]_i_4_n_0\
    );
\href_cnt[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(28),
      O => \href_cnt[31]_i_5_n_0\
    );
\href_cnt[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(3),
      O => \href_cnt[3]_i_2_n_0\
    );
\href_cnt[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(2),
      O => \href_cnt[3]_i_3_n_0\
    );
\href_cnt[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(1),
      O => \href_cnt[3]_i_4_n_0\
    );
\href_cnt[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \^href_cnt\(0),
      I1 => ov5640_href,
      O => \href_cnt[3]_i_5_n_0\
    );
\href_cnt[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(7),
      O => \href_cnt[7]_i_2_n_0\
    );
\href_cnt[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(6),
      O => \href_cnt[7]_i_3_n_0\
    );
\href_cnt[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(5),
      O => \href_cnt[7]_i_4_n_0\
    );
\href_cnt[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ov5640_href,
      I1 => \^href_cnt\(4),
      O => \href_cnt[7]_i_5_n_0\
    );
\href_cnt_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[3]_i_1_n_7\,
      Q => \^href_cnt\(0)
    );
\href_cnt_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[11]_i_1_n_5\,
      Q => \^href_cnt\(10)
    );
\href_cnt_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[11]_i_1_n_4\,
      Q => \^href_cnt\(11)
    );
\href_cnt_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_cnt_reg[7]_i_1_n_0\,
      CO(3) => \href_cnt_reg[11]_i_1_n_0\,
      CO(2) => \href_cnt_reg[11]_i_1_n_1\,
      CO(1) => \href_cnt_reg[11]_i_1_n_2\,
      CO(0) => \href_cnt_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_cnt_reg[11]_i_1_n_4\,
      O(2) => \href_cnt_reg[11]_i_1_n_5\,
      O(1) => \href_cnt_reg[11]_i_1_n_6\,
      O(0) => \href_cnt_reg[11]_i_1_n_7\,
      S(3) => \href_cnt[11]_i_2_n_0\,
      S(2) => \href_cnt[11]_i_3_n_0\,
      S(1) => \href_cnt[11]_i_4_n_0\,
      S(0) => \href_cnt[11]_i_5_n_0\
    );
\href_cnt_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[15]_i_1_n_7\,
      Q => \^href_cnt\(12)
    );
\href_cnt_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[15]_i_1_n_6\,
      Q => \^href_cnt\(13)
    );
\href_cnt_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[15]_i_1_n_5\,
      Q => \^href_cnt\(14)
    );
\href_cnt_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[15]_i_1_n_4\,
      Q => \^href_cnt\(15)
    );
\href_cnt_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_cnt_reg[11]_i_1_n_0\,
      CO(3) => \href_cnt_reg[15]_i_1_n_0\,
      CO(2) => \href_cnt_reg[15]_i_1_n_1\,
      CO(1) => \href_cnt_reg[15]_i_1_n_2\,
      CO(0) => \href_cnt_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_cnt_reg[15]_i_1_n_4\,
      O(2) => \href_cnt_reg[15]_i_1_n_5\,
      O(1) => \href_cnt_reg[15]_i_1_n_6\,
      O(0) => \href_cnt_reg[15]_i_1_n_7\,
      S(3) => \href_cnt[15]_i_2_n_0\,
      S(2) => \href_cnt[15]_i_3_n_0\,
      S(1) => \href_cnt[15]_i_4_n_0\,
      S(0) => \href_cnt[15]_i_5_n_0\
    );
\href_cnt_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[19]_i_1_n_7\,
      Q => \^href_cnt\(16)
    );
\href_cnt_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[19]_i_1_n_6\,
      Q => \^href_cnt\(17)
    );
\href_cnt_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[19]_i_1_n_5\,
      Q => \^href_cnt\(18)
    );
\href_cnt_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[19]_i_1_n_4\,
      Q => \^href_cnt\(19)
    );
\href_cnt_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_cnt_reg[15]_i_1_n_0\,
      CO(3) => \href_cnt_reg[19]_i_1_n_0\,
      CO(2) => \href_cnt_reg[19]_i_1_n_1\,
      CO(1) => \href_cnt_reg[19]_i_1_n_2\,
      CO(0) => \href_cnt_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_cnt_reg[19]_i_1_n_4\,
      O(2) => \href_cnt_reg[19]_i_1_n_5\,
      O(1) => \href_cnt_reg[19]_i_1_n_6\,
      O(0) => \href_cnt_reg[19]_i_1_n_7\,
      S(3) => \href_cnt[19]_i_2_n_0\,
      S(2) => \href_cnt[19]_i_3_n_0\,
      S(1) => \href_cnt[19]_i_4_n_0\,
      S(0) => \href_cnt[19]_i_5_n_0\
    );
\href_cnt_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[3]_i_1_n_6\,
      Q => \^href_cnt\(1)
    );
\href_cnt_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[23]_i_1_n_7\,
      Q => \^href_cnt\(20)
    );
\href_cnt_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[23]_i_1_n_6\,
      Q => \^href_cnt\(21)
    );
\href_cnt_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[23]_i_1_n_5\,
      Q => \^href_cnt\(22)
    );
\href_cnt_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[23]_i_1_n_4\,
      Q => \^href_cnt\(23)
    );
\href_cnt_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_cnt_reg[19]_i_1_n_0\,
      CO(3) => \href_cnt_reg[23]_i_1_n_0\,
      CO(2) => \href_cnt_reg[23]_i_1_n_1\,
      CO(1) => \href_cnt_reg[23]_i_1_n_2\,
      CO(0) => \href_cnt_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_cnt_reg[23]_i_1_n_4\,
      O(2) => \href_cnt_reg[23]_i_1_n_5\,
      O(1) => \href_cnt_reg[23]_i_1_n_6\,
      O(0) => \href_cnt_reg[23]_i_1_n_7\,
      S(3) => \href_cnt[23]_i_2_n_0\,
      S(2) => \href_cnt[23]_i_3_n_0\,
      S(1) => \href_cnt[23]_i_4_n_0\,
      S(0) => \href_cnt[23]_i_5_n_0\
    );
\href_cnt_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[27]_i_1_n_7\,
      Q => \^href_cnt\(24)
    );
\href_cnt_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[27]_i_1_n_6\,
      Q => \^href_cnt\(25)
    );
\href_cnt_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[27]_i_1_n_5\,
      Q => \^href_cnt\(26)
    );
\href_cnt_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[27]_i_1_n_4\,
      Q => \^href_cnt\(27)
    );
\href_cnt_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_cnt_reg[23]_i_1_n_0\,
      CO(3) => \href_cnt_reg[27]_i_1_n_0\,
      CO(2) => \href_cnt_reg[27]_i_1_n_1\,
      CO(1) => \href_cnt_reg[27]_i_1_n_2\,
      CO(0) => \href_cnt_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_cnt_reg[27]_i_1_n_4\,
      O(2) => \href_cnt_reg[27]_i_1_n_5\,
      O(1) => \href_cnt_reg[27]_i_1_n_6\,
      O(0) => \href_cnt_reg[27]_i_1_n_7\,
      S(3) => \href_cnt[27]_i_2_n_0\,
      S(2) => \href_cnt[27]_i_3_n_0\,
      S(1) => \href_cnt[27]_i_4_n_0\,
      S(0) => \href_cnt[27]_i_5_n_0\
    );
\href_cnt_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[31]_i_1_n_7\,
      Q => \^href_cnt\(28)
    );
\href_cnt_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[31]_i_1_n_6\,
      Q => \^href_cnt\(29)
    );
\href_cnt_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[3]_i_1_n_5\,
      Q => \^href_cnt\(2)
    );
\href_cnt_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[31]_i_1_n_5\,
      Q => \^href_cnt\(30)
    );
\href_cnt_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[31]_i_1_n_4\,
      Q => \^href_cnt\(31)
    );
\href_cnt_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_cnt_reg[27]_i_1_n_0\,
      CO(3) => \NLW_href_cnt_reg[31]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \href_cnt_reg[31]_i_1_n_1\,
      CO(1) => \href_cnt_reg[31]_i_1_n_2\,
      CO(0) => \href_cnt_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_cnt_reg[31]_i_1_n_4\,
      O(2) => \href_cnt_reg[31]_i_1_n_5\,
      O(1) => \href_cnt_reg[31]_i_1_n_6\,
      O(0) => \href_cnt_reg[31]_i_1_n_7\,
      S(3) => \href_cnt[31]_i_2_n_0\,
      S(2) => \href_cnt[31]_i_3_n_0\,
      S(1) => \href_cnt[31]_i_4_n_0\,
      S(0) => \href_cnt[31]_i_5_n_0\
    );
\href_cnt_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[3]_i_1_n_4\,
      Q => \^href_cnt\(3)
    );
\href_cnt_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \href_cnt_reg[3]_i_1_n_0\,
      CO(2) => \href_cnt_reg[3]_i_1_n_1\,
      CO(1) => \href_cnt_reg[3]_i_1_n_2\,
      CO(0) => \href_cnt_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => ov5640_href,
      O(3) => \href_cnt_reg[3]_i_1_n_4\,
      O(2) => \href_cnt_reg[3]_i_1_n_5\,
      O(1) => \href_cnt_reg[3]_i_1_n_6\,
      O(0) => \href_cnt_reg[3]_i_1_n_7\,
      S(3) => \href_cnt[3]_i_2_n_0\,
      S(2) => \href_cnt[3]_i_3_n_0\,
      S(1) => \href_cnt[3]_i_4_n_0\,
      S(0) => \href_cnt[3]_i_5_n_0\
    );
\href_cnt_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[7]_i_1_n_7\,
      Q => \^href_cnt\(4)
    );
\href_cnt_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[7]_i_1_n_6\,
      Q => \^href_cnt\(5)
    );
\href_cnt_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[7]_i_1_n_5\,
      Q => \^href_cnt\(6)
    );
\href_cnt_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[7]_i_1_n_4\,
      Q => \^href_cnt\(7)
    );
\href_cnt_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_cnt_reg[3]_i_1_n_0\,
      CO(3) => \href_cnt_reg[7]_i_1_n_0\,
      CO(2) => \href_cnt_reg[7]_i_1_n_1\,
      CO(1) => \href_cnt_reg[7]_i_1_n_2\,
      CO(0) => \href_cnt_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_cnt_reg[7]_i_1_n_4\,
      O(2) => \href_cnt_reg[7]_i_1_n_5\,
      O(1) => \href_cnt_reg[7]_i_1_n_6\,
      O(0) => \href_cnt_reg[7]_i_1_n_7\,
      S(3) => \href_cnt[7]_i_2_n_0\,
      S(2) => \href_cnt[7]_i_3_n_0\,
      S(1) => \href_cnt[7]_i_4_n_0\,
      S(0) => \href_cnt[7]_i_5_n_0\
    );
\href_cnt_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[11]_i_1_n_7\,
      Q => \^href_cnt\(8)
    );
\href_cnt_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => '1',
      CLR => ov5640_cfg_n_1,
      D => \href_cnt_reg[11]_i_1_n_6\,
      Q => \^href_cnt\(9)
    );
href_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => ov5640_pclk,
      CE => '1',
      D => ov5640_href,
      Q => href_reg,
      R => '0'
    );
\href_v_cnt[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(11),
      I1 => ov5640_vsync,
      O => \href_v_cnt[11]_i_2_n_0\
    );
\href_v_cnt[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(10),
      I1 => ov5640_vsync,
      O => \href_v_cnt[11]_i_3_n_0\
    );
\href_v_cnt[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(9),
      I1 => ov5640_vsync,
      O => \href_v_cnt[11]_i_4_n_0\
    );
\href_v_cnt[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(8),
      I1 => ov5640_vsync,
      O => \href_v_cnt[11]_i_5_n_0\
    );
\href_v_cnt[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(15),
      I1 => ov5640_vsync,
      O => \href_v_cnt[15]_i_2_n_0\
    );
\href_v_cnt[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(14),
      I1 => ov5640_vsync,
      O => \href_v_cnt[15]_i_3_n_0\
    );
\href_v_cnt[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(13),
      I1 => ov5640_vsync,
      O => \href_v_cnt[15]_i_4_n_0\
    );
\href_v_cnt[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(12),
      I1 => ov5640_vsync,
      O => \href_v_cnt[15]_i_5_n_0\
    );
\href_v_cnt[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(19),
      I1 => ov5640_vsync,
      O => \href_v_cnt[19]_i_2_n_0\
    );
\href_v_cnt[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(18),
      I1 => ov5640_vsync,
      O => \href_v_cnt[19]_i_3_n_0\
    );
\href_v_cnt[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(17),
      I1 => ov5640_vsync,
      O => \href_v_cnt[19]_i_4_n_0\
    );
\href_v_cnt[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(16),
      I1 => ov5640_vsync,
      O => \href_v_cnt[19]_i_5_n_0\
    );
\href_v_cnt[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(23),
      I1 => ov5640_vsync,
      O => \href_v_cnt[23]_i_2_n_0\
    );
\href_v_cnt[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(22),
      I1 => ov5640_vsync,
      O => \href_v_cnt[23]_i_3_n_0\
    );
\href_v_cnt[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(21),
      I1 => ov5640_vsync,
      O => \href_v_cnt[23]_i_4_n_0\
    );
\href_v_cnt[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(20),
      I1 => ov5640_vsync,
      O => \href_v_cnt[23]_i_5_n_0\
    );
\href_v_cnt[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(27),
      I1 => ov5640_vsync,
      O => \href_v_cnt[27]_i_2_n_0\
    );
\href_v_cnt[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(26),
      I1 => ov5640_vsync,
      O => \href_v_cnt[27]_i_3_n_0\
    );
\href_v_cnt[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(25),
      I1 => ov5640_vsync,
      O => \href_v_cnt[27]_i_4_n_0\
    );
\href_v_cnt[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(24),
      I1 => ov5640_vsync,
      O => \href_v_cnt[27]_i_5_n_0\
    );
\href_v_cnt[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => href_reg,
      I1 => ov5640_href,
      I2 => ov5640_vsync,
      O => \href_v_cnt[31]_i_1_n_0\
    );
\href_v_cnt[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(31),
      I1 => ov5640_vsync,
      O => \href_v_cnt[31]_i_3_n_0\
    );
\href_v_cnt[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(30),
      I1 => ov5640_vsync,
      O => \href_v_cnt[31]_i_4_n_0\
    );
\href_v_cnt[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(29),
      I1 => ov5640_vsync,
      O => \href_v_cnt[31]_i_5_n_0\
    );
\href_v_cnt[31]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(28),
      I1 => ov5640_vsync,
      O => \href_v_cnt[31]_i_6_n_0\
    );
\href_v_cnt[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ov5640_vsync,
      O => \href_v_cnt[3]_i_2_n_0\
    );
\href_v_cnt[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(3),
      I1 => ov5640_vsync,
      O => \href_v_cnt[3]_i_3_n_0\
    );
\href_v_cnt[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(2),
      I1 => ov5640_vsync,
      O => \href_v_cnt[3]_i_4_n_0\
    );
\href_v_cnt[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(1),
      I1 => ov5640_vsync,
      O => \href_v_cnt[3]_i_5_n_0\
    );
\href_v_cnt[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^href_v_cnt\(0),
      I1 => ov5640_vsync,
      O => \href_v_cnt[3]_i_6_n_0\
    );
\href_v_cnt[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(7),
      I1 => ov5640_vsync,
      O => \href_v_cnt[7]_i_2_n_0\
    );
\href_v_cnt[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(6),
      I1 => ov5640_vsync,
      O => \href_v_cnt[7]_i_3_n_0\
    );
\href_v_cnt[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(5),
      I1 => ov5640_vsync,
      O => \href_v_cnt[7]_i_4_n_0\
    );
\href_v_cnt[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^href_v_cnt\(4),
      I1 => ov5640_vsync,
      O => \href_v_cnt[7]_i_5_n_0\
    );
\href_v_cnt_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[3]_i_1_n_7\,
      Q => \^href_v_cnt\(0)
    );
\href_v_cnt_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[11]_i_1_n_5\,
      Q => \^href_v_cnt\(10)
    );
\href_v_cnt_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[11]_i_1_n_4\,
      Q => \^href_v_cnt\(11)
    );
\href_v_cnt_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_v_cnt_reg[7]_i_1_n_0\,
      CO(3) => \href_v_cnt_reg[11]_i_1_n_0\,
      CO(2) => \href_v_cnt_reg[11]_i_1_n_1\,
      CO(1) => \href_v_cnt_reg[11]_i_1_n_2\,
      CO(0) => \href_v_cnt_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_v_cnt_reg[11]_i_1_n_4\,
      O(2) => \href_v_cnt_reg[11]_i_1_n_5\,
      O(1) => \href_v_cnt_reg[11]_i_1_n_6\,
      O(0) => \href_v_cnt_reg[11]_i_1_n_7\,
      S(3) => \href_v_cnt[11]_i_2_n_0\,
      S(2) => \href_v_cnt[11]_i_3_n_0\,
      S(1) => \href_v_cnt[11]_i_4_n_0\,
      S(0) => \href_v_cnt[11]_i_5_n_0\
    );
\href_v_cnt_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[15]_i_1_n_7\,
      Q => \^href_v_cnt\(12)
    );
\href_v_cnt_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[15]_i_1_n_6\,
      Q => \^href_v_cnt\(13)
    );
\href_v_cnt_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[15]_i_1_n_5\,
      Q => \^href_v_cnt\(14)
    );
\href_v_cnt_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[15]_i_1_n_4\,
      Q => \^href_v_cnt\(15)
    );
\href_v_cnt_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_v_cnt_reg[11]_i_1_n_0\,
      CO(3) => \href_v_cnt_reg[15]_i_1_n_0\,
      CO(2) => \href_v_cnt_reg[15]_i_1_n_1\,
      CO(1) => \href_v_cnt_reg[15]_i_1_n_2\,
      CO(0) => \href_v_cnt_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_v_cnt_reg[15]_i_1_n_4\,
      O(2) => \href_v_cnt_reg[15]_i_1_n_5\,
      O(1) => \href_v_cnt_reg[15]_i_1_n_6\,
      O(0) => \href_v_cnt_reg[15]_i_1_n_7\,
      S(3) => \href_v_cnt[15]_i_2_n_0\,
      S(2) => \href_v_cnt[15]_i_3_n_0\,
      S(1) => \href_v_cnt[15]_i_4_n_0\,
      S(0) => \href_v_cnt[15]_i_5_n_0\
    );
\href_v_cnt_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[19]_i_1_n_7\,
      Q => \^href_v_cnt\(16)
    );
\href_v_cnt_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[19]_i_1_n_6\,
      Q => \^href_v_cnt\(17)
    );
\href_v_cnt_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[19]_i_1_n_5\,
      Q => \^href_v_cnt\(18)
    );
\href_v_cnt_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[19]_i_1_n_4\,
      Q => \^href_v_cnt\(19)
    );
\href_v_cnt_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_v_cnt_reg[15]_i_1_n_0\,
      CO(3) => \href_v_cnt_reg[19]_i_1_n_0\,
      CO(2) => \href_v_cnt_reg[19]_i_1_n_1\,
      CO(1) => \href_v_cnt_reg[19]_i_1_n_2\,
      CO(0) => \href_v_cnt_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_v_cnt_reg[19]_i_1_n_4\,
      O(2) => \href_v_cnt_reg[19]_i_1_n_5\,
      O(1) => \href_v_cnt_reg[19]_i_1_n_6\,
      O(0) => \href_v_cnt_reg[19]_i_1_n_7\,
      S(3) => \href_v_cnt[19]_i_2_n_0\,
      S(2) => \href_v_cnt[19]_i_3_n_0\,
      S(1) => \href_v_cnt[19]_i_4_n_0\,
      S(0) => \href_v_cnt[19]_i_5_n_0\
    );
\href_v_cnt_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[3]_i_1_n_6\,
      Q => \^href_v_cnt\(1)
    );
\href_v_cnt_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[23]_i_1_n_7\,
      Q => \^href_v_cnt\(20)
    );
\href_v_cnt_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[23]_i_1_n_6\,
      Q => \^href_v_cnt\(21)
    );
\href_v_cnt_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[23]_i_1_n_5\,
      Q => \^href_v_cnt\(22)
    );
\href_v_cnt_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[23]_i_1_n_4\,
      Q => \^href_v_cnt\(23)
    );
\href_v_cnt_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_v_cnt_reg[19]_i_1_n_0\,
      CO(3) => \href_v_cnt_reg[23]_i_1_n_0\,
      CO(2) => \href_v_cnt_reg[23]_i_1_n_1\,
      CO(1) => \href_v_cnt_reg[23]_i_1_n_2\,
      CO(0) => \href_v_cnt_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_v_cnt_reg[23]_i_1_n_4\,
      O(2) => \href_v_cnt_reg[23]_i_1_n_5\,
      O(1) => \href_v_cnt_reg[23]_i_1_n_6\,
      O(0) => \href_v_cnt_reg[23]_i_1_n_7\,
      S(3) => \href_v_cnt[23]_i_2_n_0\,
      S(2) => \href_v_cnt[23]_i_3_n_0\,
      S(1) => \href_v_cnt[23]_i_4_n_0\,
      S(0) => \href_v_cnt[23]_i_5_n_0\
    );
\href_v_cnt_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[27]_i_1_n_7\,
      Q => \^href_v_cnt\(24)
    );
\href_v_cnt_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[27]_i_1_n_6\,
      Q => \^href_v_cnt\(25)
    );
\href_v_cnt_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[27]_i_1_n_5\,
      Q => \^href_v_cnt\(26)
    );
\href_v_cnt_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[27]_i_1_n_4\,
      Q => \^href_v_cnt\(27)
    );
\href_v_cnt_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_v_cnt_reg[23]_i_1_n_0\,
      CO(3) => \href_v_cnt_reg[27]_i_1_n_0\,
      CO(2) => \href_v_cnt_reg[27]_i_1_n_1\,
      CO(1) => \href_v_cnt_reg[27]_i_1_n_2\,
      CO(0) => \href_v_cnt_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_v_cnt_reg[27]_i_1_n_4\,
      O(2) => \href_v_cnt_reg[27]_i_1_n_5\,
      O(1) => \href_v_cnt_reg[27]_i_1_n_6\,
      O(0) => \href_v_cnt_reg[27]_i_1_n_7\,
      S(3) => \href_v_cnt[27]_i_2_n_0\,
      S(2) => \href_v_cnt[27]_i_3_n_0\,
      S(1) => \href_v_cnt[27]_i_4_n_0\,
      S(0) => \href_v_cnt[27]_i_5_n_0\
    );
\href_v_cnt_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[31]_i_2_n_7\,
      Q => \^href_v_cnt\(28)
    );
\href_v_cnt_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[31]_i_2_n_6\,
      Q => \^href_v_cnt\(29)
    );
\href_v_cnt_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[3]_i_1_n_5\,
      Q => \^href_v_cnt\(2)
    );
\href_v_cnt_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[31]_i_2_n_5\,
      Q => \^href_v_cnt\(30)
    );
\href_v_cnt_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[31]_i_2_n_4\,
      Q => \^href_v_cnt\(31)
    );
\href_v_cnt_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_v_cnt_reg[27]_i_1_n_0\,
      CO(3) => \NLW_href_v_cnt_reg[31]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \href_v_cnt_reg[31]_i_2_n_1\,
      CO(1) => \href_v_cnt_reg[31]_i_2_n_2\,
      CO(0) => \href_v_cnt_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_v_cnt_reg[31]_i_2_n_4\,
      O(2) => \href_v_cnt_reg[31]_i_2_n_5\,
      O(1) => \href_v_cnt_reg[31]_i_2_n_6\,
      O(0) => \href_v_cnt_reg[31]_i_2_n_7\,
      S(3) => \href_v_cnt[31]_i_3_n_0\,
      S(2) => \href_v_cnt[31]_i_4_n_0\,
      S(1) => \href_v_cnt[31]_i_5_n_0\,
      S(0) => \href_v_cnt[31]_i_6_n_0\
    );
\href_v_cnt_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[3]_i_1_n_4\,
      Q => \^href_v_cnt\(3)
    );
\href_v_cnt_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \href_v_cnt_reg[3]_i_1_n_0\,
      CO(2) => \href_v_cnt_reg[3]_i_1_n_1\,
      CO(1) => \href_v_cnt_reg[3]_i_1_n_2\,
      CO(0) => \href_v_cnt_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \href_v_cnt[3]_i_2_n_0\,
      O(3) => \href_v_cnt_reg[3]_i_1_n_4\,
      O(2) => \href_v_cnt_reg[3]_i_1_n_5\,
      O(1) => \href_v_cnt_reg[3]_i_1_n_6\,
      O(0) => \href_v_cnt_reg[3]_i_1_n_7\,
      S(3) => \href_v_cnt[3]_i_3_n_0\,
      S(2) => \href_v_cnt[3]_i_4_n_0\,
      S(1) => \href_v_cnt[3]_i_5_n_0\,
      S(0) => \href_v_cnt[3]_i_6_n_0\
    );
\href_v_cnt_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[7]_i_1_n_7\,
      Q => \^href_v_cnt\(4)
    );
\href_v_cnt_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[7]_i_1_n_6\,
      Q => \^href_v_cnt\(5)
    );
\href_v_cnt_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[7]_i_1_n_5\,
      Q => \^href_v_cnt\(6)
    );
\href_v_cnt_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[7]_i_1_n_4\,
      Q => \^href_v_cnt\(7)
    );
\href_v_cnt_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \href_v_cnt_reg[3]_i_1_n_0\,
      CO(3) => \href_v_cnt_reg[7]_i_1_n_0\,
      CO(2) => \href_v_cnt_reg[7]_i_1_n_1\,
      CO(1) => \href_v_cnt_reg[7]_i_1_n_2\,
      CO(0) => \href_v_cnt_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \href_v_cnt_reg[7]_i_1_n_4\,
      O(2) => \href_v_cnt_reg[7]_i_1_n_5\,
      O(1) => \href_v_cnt_reg[7]_i_1_n_6\,
      O(0) => \href_v_cnt_reg[7]_i_1_n_7\,
      S(3) => \href_v_cnt[7]_i_2_n_0\,
      S(2) => \href_v_cnt[7]_i_3_n_0\,
      S(1) => \href_v_cnt[7]_i_4_n_0\,
      S(0) => \href_v_cnt[7]_i_5_n_0\
    );
\href_v_cnt_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[11]_i_1_n_7\,
      Q => \^href_v_cnt\(8)
    );
\href_v_cnt_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => ov5640_pclk,
      CE => \href_v_cnt[31]_i_1_n_0\,
      CLR => ov5640_cfg_n_1,
      D => \href_v_cnt_reg[11]_i_1_n_6\,
      Q => \^href_v_cnt\(9)
    );
iic_ctrl: entity work.system_ov5640_top_0_0_iic_ctrl
     port map (
      E(0) => i2c_end,
      Q(2) => iic_ctrl_n_2,
      Q(1) => iic_ctrl_n_3,
      Q(0) => iic_ctrl_n_4,
      cfg_start => cfg_start,
      \cnt_bit_reg[0]_0\ => iic_ctrl_n_5,
      \cnt_bit_reg[0]_1\ => iic_ctrl_n_6,
      \cnt_bit_reg[0]_2\ => iic_ctrl_n_10,
      \cnt_bit_reg[1]_0\ => iic_ctrl_n_7,
      \cnt_bit_reg[1]_1\ => iic_ctrl_n_11,
      \cnt_bit_reg[2]_0\ => iic_ctrl_n_8,
      \cnt_i2c_clk_reg[0]_0\ => ov5640_cfg_n_1,
      i2c_clk => i2c_clk,
      i2c_sda_reg_reg_0 => ov5640_cfg_n_4,
      i2c_sda_reg_reg_1 => ov5640_cfg_n_5,
      i2c_sda_reg_reg_2 => ov5640_cfg_n_3,
      i2c_sda_reg_reg_i_40 => \^cfg_done_reg\,
      sccb_scl => sccb_scl,
      sccb_sda => sccb_sda,
      sys_clk => sys_clk
    );
ov5640_cfg: entity work.system_ov5640_top_0_0_ov5640_cfg
     port map (
      E(0) => i2c_end,
      \FSM_onehot_state_reg[13]\ => ov5640_cfg_n_3,
      \FSM_onehot_state_reg[13]_0\ => ov5640_cfg_n_4,
      \FSM_onehot_state_reg[2]\ => ov5640_cfg_n_5,
      Q(2) => iic_ctrl_n_2,
      Q(1) => iic_ctrl_n_3,
      Q(0) => iic_ctrl_n_4,
      cfg_done_reg_0 => \^cfg_done_reg\,
      cfg_start => cfg_start,
      i2c_clk => i2c_clk,
      i2c_sda_reg_reg_i_17_0 => iic_ctrl_n_11,
      i2c_sda_reg_reg_i_17_1 => iic_ctrl_n_5,
      i2c_sda_reg_reg_i_41_0 => iic_ctrl_n_7,
      i2c_sda_reg_reg_i_4_0 => iic_ctrl_n_8,
      i2c_sda_reg_reg_i_4_1 => iic_ctrl_n_6,
      i2c_sda_reg_reg_i_5_0 => iic_ctrl_n_10,
      sys_rst_n => sys_rst_n,
      sys_rst_n_0 => ov5640_cfg_n_1
    );
ov5640_data_in: entity work.system_ov5640_top_0_0_ov5640_data_in
     port map (
      ov5640_data(7 downto 0) => ov5640_data(7 downto 0),
      ov5640_data_out(15 downto 0) => ov5640_data_out(15 downto 0),
      ov5640_href => ov5640_href,
      ov5640_pclk => ov5640_pclk,
      ov5640_wr_en => ov5640_wr_en,
      sys_init_done => sys_init_done,
      sys_rst_n => sys_rst_n
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_ov5640_top_0_0 is
  port (
    sys_clk : in STD_LOGIC;
    sys_rst_n : in STD_LOGIC;
    sys_init_done : in STD_LOGIC;
    ov5640_pclk : in STD_LOGIC;
    ov5640_href : in STD_LOGIC;
    ov5640_vsync : in STD_LOGIC;
    ov5640_data : in STD_LOGIC_VECTOR ( 7 downto 0 );
    ov5640_rst_n : out STD_LOGIC;
    ov5640_pwdn : out STD_LOGIC;
    sccb_scl : out STD_LOGIC;
    sccb_sda : inout STD_LOGIC;
    cfg_done : out STD_LOGIC;
    ov5640_wr_en : out STD_LOGIC;
    ov5640_data_out : out STD_LOGIC_VECTOR ( 31 downto 0 );
    href_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 );
    href_v_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_ov5640_top_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_ov5640_top_0_0 : entity is "system_ov5640_top_0_0,ov5640_top,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of system_ov5640_top_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of system_ov5640_top_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of system_ov5640_top_0_0 : entity is "ov5640_top,Vivado 2019.2";
end system_ov5640_top_0_0;

architecture STRUCTURE of system_ov5640_top_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^ov5640_data_out\ : STD_LOGIC_VECTOR ( 23 downto 3 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of ov5640_rst_n : signal is "xilinx.com:signal:reset:1.0 ov5640_rst_n RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of ov5640_rst_n : signal is "XIL_INTERFACENAME ov5640_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of sys_clk : signal is "xilinx.com:signal:clock:1.0 sys_clk CLK";
  attribute X_INTERFACE_PARAMETER of sys_clk : signal is "XIL_INTERFACENAME sys_clk, ASSOCIATED_RESET sys_rst_n, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_sys_clk, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of sys_rst_n : signal is "xilinx.com:signal:reset:1.0 sys_rst_n RST";
  attribute X_INTERFACE_PARAMETER of sys_rst_n : signal is "XIL_INTERFACENAME sys_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  ov5640_data_out(31) <= \<const0>\;
  ov5640_data_out(30) <= \<const0>\;
  ov5640_data_out(29) <= \<const0>\;
  ov5640_data_out(28) <= \<const0>\;
  ov5640_data_out(27) <= \<const0>\;
  ov5640_data_out(26) <= \<const0>\;
  ov5640_data_out(25) <= \<const0>\;
  ov5640_data_out(24) <= \<const0>\;
  ov5640_data_out(23 downto 19) <= \^ov5640_data_out\(23 downto 19);
  ov5640_data_out(18) <= \<const0>\;
  ov5640_data_out(17) <= \<const0>\;
  ov5640_data_out(16) <= \<const0>\;
  ov5640_data_out(15 downto 10) <= \^ov5640_data_out\(15 downto 10);
  ov5640_data_out(9) <= \<const0>\;
  ov5640_data_out(8) <= \<const0>\;
  ov5640_data_out(7 downto 3) <= \^ov5640_data_out\(7 downto 3);
  ov5640_data_out(2) <= \<const0>\;
  ov5640_data_out(1) <= \<const0>\;
  ov5640_data_out(0) <= \<const0>\;
  ov5640_pwdn <= \<const0>\;
  ov5640_rst_n <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.system_ov5640_top_0_0_ov5640_top
     port map (
      cfg_done_reg => cfg_done,
      href_cnt(31 downto 0) => href_cnt(31 downto 0),
      href_v_cnt(31 downto 0) => href_v_cnt(31 downto 0),
      ov5640_data(7 downto 0) => ov5640_data(7 downto 0),
      ov5640_data_out(15 downto 11) => \^ov5640_data_out\(23 downto 19),
      ov5640_data_out(10 downto 5) => \^ov5640_data_out\(15 downto 10),
      ov5640_data_out(4 downto 0) => \^ov5640_data_out\(7 downto 3),
      ov5640_href => ov5640_href,
      ov5640_pclk => ov5640_pclk,
      ov5640_vsync => ov5640_vsync,
      ov5640_wr_en => ov5640_wr_en,
      sccb_scl => sccb_scl,
      sccb_sda => sccb_sda,
      sys_clk => sys_clk,
      sys_init_done => sys_init_done,
      sys_rst_n => sys_rst_n
    );
end STRUCTURE;
