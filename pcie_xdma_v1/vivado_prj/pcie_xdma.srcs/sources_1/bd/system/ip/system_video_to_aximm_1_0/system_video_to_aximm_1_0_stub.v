// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Sun Dec  3 17:51:02 2023
// Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top system_video_to_aximm_1_0 -prefix
//               system_video_to_aximm_1_0_ system_video_to_aximm_1_0_stub.v
// Design      : system_video_to_aximm_1_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "video_to_aximm,Vivado 2019.2" *)
module system_video_to_aximm_1_0(M_AXI_ACLK, ui_rst, ov5640_vsync, rd_wdfifo_en, 
  fifo_data_rd, almost_empty, prog_empty, M_AXI_AWID, M_AXI_AWADDR, M_AXI_AWLEN, M_AXI_AWSIZE, 
  M_AXI_AWBURST, M_AXI_AWVALID, M_AXI_AWLOCK, M_AXI_AWCACHE, M_AXI_AWPROT, M_AXI_AWQOS, 
  M_AXI_AWUSER, M_AXI_AWREADY, M_AXI_WDATA, M_AXI_WLAST, M_AXI_WVALID, M_AXI_WREADY, 
  M_AXI_WSTRB, M_AXI_BREADY, M_AXI_BID, M_AXI_BVALID, M_AXI_BRESP, M_AXI_BUSER, M_AXI_ARADDR, 
  M_AXI_ARLEN, M_AXI_ARSIZE, M_AXI_ARBURST, M_AXI_ARVALID, M_AXI_ARREADY, M_AXI_ARID, 
  M_AXI_ARLOCK, M_AXI_ARCACHE, M_AXI_ARPROT, M_AXI_ARQOS, M_AXI_ARUSER, M_AXI_RREADY, 
  M_AXI_RDATA, M_AXI_RRESP, M_AXI_RLAST, M_AXI_RVALID, M_AXI_RID)
/* synthesis syn_black_box black_box_pad_pin="M_AXI_ACLK,ui_rst,ov5640_vsync,rd_wdfifo_en,fifo_data_rd[31:0],almost_empty,prog_empty,M_AXI_AWID[0:0],M_AXI_AWADDR[31:0],M_AXI_AWLEN[7:0],M_AXI_AWSIZE[2:0],M_AXI_AWBURST[1:0],M_AXI_AWVALID,M_AXI_AWLOCK,M_AXI_AWCACHE[3:0],M_AXI_AWPROT[2:0],M_AXI_AWQOS[3:0],M_AXI_AWUSER[0:0],M_AXI_AWREADY,M_AXI_WDATA[31:0],M_AXI_WLAST,M_AXI_WVALID,M_AXI_WREADY,M_AXI_WSTRB[3:0],M_AXI_BREADY,M_AXI_BID[0:0],M_AXI_BVALID,M_AXI_BRESP[1:0],M_AXI_BUSER[0:0],M_AXI_ARADDR[31:0],M_AXI_ARLEN[7:0],M_AXI_ARSIZE[2:0],M_AXI_ARBURST[1:0],M_AXI_ARVALID,M_AXI_ARREADY,M_AXI_ARID[0:0],M_AXI_ARLOCK,M_AXI_ARCACHE[3:0],M_AXI_ARPROT[2:0],M_AXI_ARQOS[3:0],M_AXI_ARUSER[0:0],M_AXI_RREADY,M_AXI_RDATA[31:0],M_AXI_RRESP[1:0],M_AXI_RLAST,M_AXI_RVALID,M_AXI_RID[0:0]" */;
  input M_AXI_ACLK;
  input ui_rst;
  input ov5640_vsync;
  output rd_wdfifo_en;
  input [31:0]fifo_data_rd;
  input almost_empty;
  input prog_empty;
  output [0:0]M_AXI_AWID;
  output [31:0]M_AXI_AWADDR;
  output [7:0]M_AXI_AWLEN;
  output [2:0]M_AXI_AWSIZE;
  output [1:0]M_AXI_AWBURST;
  output M_AXI_AWVALID;
  output M_AXI_AWLOCK;
  output [3:0]M_AXI_AWCACHE;
  output [2:0]M_AXI_AWPROT;
  output [3:0]M_AXI_AWQOS;
  output [0:0]M_AXI_AWUSER;
  input M_AXI_AWREADY;
  output [31:0]M_AXI_WDATA;
  output M_AXI_WLAST;
  output M_AXI_WVALID;
  input M_AXI_WREADY;
  output [3:0]M_AXI_WSTRB;
  output M_AXI_BREADY;
  input [0:0]M_AXI_BID;
  input M_AXI_BVALID;
  input [1:0]M_AXI_BRESP;
  input [0:0]M_AXI_BUSER;
  output [31:0]M_AXI_ARADDR;
  output [7:0]M_AXI_ARLEN;
  output [2:0]M_AXI_ARSIZE;
  output [1:0]M_AXI_ARBURST;
  output M_AXI_ARVALID;
  input M_AXI_ARREADY;
  output [0:0]M_AXI_ARID;
  output M_AXI_ARLOCK;
  output [3:0]M_AXI_ARCACHE;
  output [2:0]M_AXI_ARPROT;
  output [3:0]M_AXI_ARQOS;
  output [0:0]M_AXI_ARUSER;
  output M_AXI_RREADY;
  input [31:0]M_AXI_RDATA;
  input [1:0]M_AXI_RRESP;
  input M_AXI_RLAST;
  input M_AXI_RVALID;
  input [0:0]M_AXI_RID;
endmodule
