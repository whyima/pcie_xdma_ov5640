-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
-- Date        : Sun Dec  3 17:51:02 2023
-- Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top system_video_to_aximm_1_0 -prefix
--               system_video_to_aximm_1_0_ system_video_to_aximm_1_0_sim_netlist.vhdl
-- Design      : system_video_to_aximm_1_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_video_to_aximm_1_0_AXI_DDR_RW_v1_0_M_AXI is
  port (
    M_AXI_RREADY : out STD_LOGIC;
    M_AXI_ACLK : in STD_LOGIC;
    M_AXI_RLAST : in STD_LOGIC;
    M_AXI_RVALID : in STD_LOGIC
  );
end system_video_to_aximm_1_0_AXI_DDR_RW_v1_0_M_AXI;

architecture STRUCTURE of system_video_to_aximm_1_0_AXI_DDR_RW_v1_0_M_AXI is
  signal \^m_axi_rready\ : STD_LOGIC;
  signal axi_rready_i_1_n_0 : STD_LOGIC;
begin
  M_AXI_RREADY <= \^m_axi_rready\;
axi_rready_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7C"
    )
        port map (
      I0 => M_AXI_RLAST,
      I1 => \^m_axi_rready\,
      I2 => M_AXI_RVALID,
      O => axi_rready_i_1_n_0
    );
axi_rready_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_rready_i_1_n_0,
      Q => \^m_axi_rready\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_video_to_aximm_1_0_AXI_DDR_WD_A_v1_0_SA_AXI is
  port (
    M_AXI_BREADY : out STD_LOGIC;
    axi_awvalid_reg_0 : out STD_LOGIC;
    axi_wvalid_reg_0 : out STD_LOGIC;
    M_AXI_WLAST : out STD_LOGIC;
    M_AXI_AWADDR : out STD_LOGIC_VECTOR ( 22 downto 0 );
    rd_wdfifo_en : out STD_LOGIC;
    M_AXI_ACLK : in STD_LOGIC;
    ui_rst : in STD_LOGIC;
    M_AXI_BVALID : in STD_LOGIC;
    M_AXI_AWREADY : in STD_LOGIC;
    M_AXI_WREADY : in STD_LOGIC;
    ov5640_vsync_reg : in STD_LOGIC;
    ov5640_vsync_reg1 : in STD_LOGIC;
    prog_empty : in STD_LOGIC
  );
end system_video_to_aximm_1_0_AXI_DDR_WD_A_v1_0_SA_AXI;

architecture STRUCTURE of system_video_to_aximm_1_0_AXI_DDR_WD_A_v1_0_SA_AXI is
  signal FSM_sequential_mst_exec_state_i_1_n_0 : STD_LOGIC;
  signal \^m_axi_awaddr\ : STD_LOGIC_VECTOR ( 22 downto 0 );
  signal \^m_axi_bready\ : STD_LOGIC;
  signal \^m_axi_wlast\ : STD_LOGIC;
  signal \axi_awaddr[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_awaddr_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \axi_awaddr_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \axi_awaddr_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \axi_awaddr_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \axi_awaddr_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \axi_awaddr_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \axi_awaddr_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \axi_awaddr_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \axi_awaddr_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \axi_awaddr_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \axi_awaddr_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \axi_awaddr_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \axi_awaddr_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \axi_awaddr_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \axi_awaddr_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \axi_awaddr_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \axi_awaddr_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \axi_awaddr_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \axi_awaddr_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \axi_awaddr_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \axi_awaddr_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \axi_awaddr_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \axi_awaddr_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \axi_awaddr_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \axi_awaddr_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \axi_awaddr_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \axi_awaddr_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \axi_awaddr_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \axi_awaddr_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \axi_awaddr_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \axi_awaddr_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \axi_awaddr_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \axi_awaddr_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \axi_awaddr_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \axi_awaddr_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \axi_awaddr_reg[31]_i_3_n_2\ : STD_LOGIC;
  signal \axi_awaddr_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \axi_awaddr_reg[31]_i_3_n_5\ : STD_LOGIC;
  signal \axi_awaddr_reg[31]_i_3_n_6\ : STD_LOGIC;
  signal \axi_awaddr_reg[31]_i_3_n_7\ : STD_LOGIC;
  signal axi_awvalid0 : STD_LOGIC;
  signal axi_awvalid_i_1_n_0 : STD_LOGIC;
  signal \^axi_awvalid_reg_0\ : STD_LOGIC;
  signal axi_bready0 : STD_LOGIC;
  signal axi_wlast_i_1_n_0 : STD_LOGIC;
  signal axi_wlast_i_2_n_0 : STD_LOGIC;
  signal axi_wvalid_i_1_n_0 : STD_LOGIC;
  signal \^axi_wvalid_reg_0\ : STD_LOGIC;
  signal burst_vaild : STD_LOGIC;
  signal burst_vaild1 : STD_LOGIC;
  signal burst_vaild_i_1_n_0 : STD_LOGIC;
  signal burst_write_active : STD_LOGIC;
  signal burst_write_active_i_1_n_0 : STD_LOGIC;
  signal cnt_cp : STD_LOGIC;
  signal \cnt_cp[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_cp[0]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_cp[0]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_cp[0]_i_6_n_0\ : STD_LOGIC;
  signal \cnt_cp[0]_i_7_n_0\ : STD_LOGIC;
  signal \cnt_cp[12]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_cp[12]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_cp[12]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_cp[12]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_cp[16]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_cp[16]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_cp[16]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_cp[16]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_cp[20]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_cp[20]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_cp[20]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_cp[20]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_cp[24]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_cp[24]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_cp[24]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_cp[24]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_cp[28]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_cp[28]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_cp[28]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_cp[28]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_cp[4]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_cp[4]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_cp[4]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_cp[4]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_cp[8]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_cp[8]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_cp[8]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_cp[8]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_cp_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_cp_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_cp_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_cp_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_cp_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_cp_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_cp_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_cp_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_cp_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_cp_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_cp_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_cp_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_cp_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_cp_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_cp_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_cp_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_cp_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_cp_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_cp_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_cp_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_cp_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_cp_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_cp_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_cp_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_cp_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_cp_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_cp_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_cp_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_cp_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_cp_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_cp_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_cp_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_cp_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_cp_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_cp_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_cp_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_cp_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_cp_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_cp_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_cp_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_cp_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_cp_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_cp_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_cp_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_cp_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_cp_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_cp_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_cp_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_cp_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_cp_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_cp_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_cp_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_cp_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_cp_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_cp_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_cp_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_cp_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_cp_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_cp_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_cp_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_cp_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_cp_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_cp_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[0]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[10]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[11]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[12]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[13]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[14]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[15]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[16]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[17]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[18]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[19]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[1]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[20]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[21]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[22]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[23]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[24]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[25]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[26]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[27]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[28]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[29]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[2]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[30]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[31]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[3]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[4]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[5]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[6]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[7]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[8]\ : STD_LOGIC;
  signal \cnt_cp_reg_n_0_[9]\ : STD_LOGIC;
  signal mst_exec_state : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal start_single_burst_write_i_10_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_11_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_1_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_2_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_3_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_4_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_5_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_6_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_7_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_8_n_0 : STD_LOGIC;
  signal start_single_burst_write_i_9_n_0 : STD_LOGIC;
  signal start_single_burst_write_reg_n_0 : STD_LOGIC;
  signal write_burst_counter : STD_LOGIC;
  signal \write_burst_counter[0]_i_10_n_0\ : STD_LOGIC;
  signal \write_burst_counter[0]_i_11_n_0\ : STD_LOGIC;
  signal \write_burst_counter[0]_i_3_n_0\ : STD_LOGIC;
  signal \write_burst_counter[0]_i_4_n_0\ : STD_LOGIC;
  signal \write_burst_counter[0]_i_5_n_0\ : STD_LOGIC;
  signal \write_burst_counter[0]_i_6_n_0\ : STD_LOGIC;
  signal \write_burst_counter[0]_i_7_n_0\ : STD_LOGIC;
  signal \write_burst_counter[0]_i_8_n_0\ : STD_LOGIC;
  signal \write_burst_counter[0]_i_9_n_0\ : STD_LOGIC;
  signal write_burst_counter_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \write_burst_counter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \write_burst_counter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \write_burst_counter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \write_burst_counter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \write_burst_counter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \write_burst_counter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \write_burst_counter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \write_burst_counter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \write_burst_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \write_burst_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \write_burst_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \write_burst_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \write_burst_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \write_burst_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \write_burst_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \write_burst_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \write_burst_counter_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \write_burst_counter_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \write_burst_counter_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \write_burst_counter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \write_burst_counter_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \write_burst_counter_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \write_burst_counter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \write_burst_counter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \write_burst_counter_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \write_burst_counter_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \write_burst_counter_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \write_burst_counter_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \write_burst_counter_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \write_burst_counter_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \write_burst_counter_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \write_burst_counter_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \write_burst_counter_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \write_burst_counter_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \write_burst_counter_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \write_burst_counter_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \write_burst_counter_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \write_burst_counter_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \write_burst_counter_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \write_burst_counter_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \write_burst_counter_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \write_burst_counter_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \write_burst_counter_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \write_burst_counter_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \write_burst_counter_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \write_burst_counter_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \write_burst_counter_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \write_burst_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \write_burst_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \write_burst_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \write_burst_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \write_burst_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \write_burst_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \write_burst_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \write_burst_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \write_burst_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \write_burst_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \write_burst_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \write_burst_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \write_burst_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \write_burst_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \write_burst_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \write_burst_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal write_index0 : STD_LOGIC;
  signal \write_index[7]_i_1_n_0\ : STD_LOGIC;
  signal \write_index[7]_i_4_n_0\ : STD_LOGIC;
  signal write_index_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal writes_done : STD_LOGIC;
  signal writes_done_i_1_n_0 : STD_LOGIC;
  signal \NLW_axi_awaddr_reg[31]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_axi_awaddr_reg[31]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt_cp_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_write_burst_counter_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of FSM_sequential_mst_exec_state_reg : label is "IDLE:0,INIT_WRITE:1";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_awvalid_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of axi_bready_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of axi_wvalid_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of burst_write_active_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of rd_wdfifo_en_INST_0 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of start_single_burst_write_i_2 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \write_index[1]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \write_index[2]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \write_index[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \write_index[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \write_index[6]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \write_index[7]_i_3\ : label is "soft_lutpair4";
begin
  M_AXI_AWADDR(22 downto 0) <= \^m_axi_awaddr\(22 downto 0);
  M_AXI_BREADY <= \^m_axi_bready\;
  M_AXI_WLAST <= \^m_axi_wlast\;
  axi_awvalid_reg_0 <= \^axi_awvalid_reg_0\;
  axi_wvalid_reg_0 <= \^axi_wvalid_reg_0\;
FSM_sequential_mst_exec_state_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000004F4"
    )
        port map (
      I0 => ov5640_vsync_reg,
      I1 => ov5640_vsync_reg1,
      I2 => mst_exec_state,
      I3 => writes_done,
      I4 => ui_rst,
      O => FSM_sequential_mst_exec_state_i_1_n_0
    );
FSM_sequential_mst_exec_state_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => FSM_sequential_mst_exec_state_i_1_n_0,
      Q => mst_exec_state,
      R => '0'
    );
\axi_awaddr[12]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^m_axi_awaddr\(0),
      O => \axi_awaddr[12]_i_2_n_0\
    );
\axi_awaddr[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => ui_rst,
      I1 => ov5640_vsync_reg1,
      I2 => ov5640_vsync_reg,
      O => burst_vaild1
    );
\axi_awaddr[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^axi_awvalid_reg_0\,
      I1 => M_AXI_AWREADY,
      O => axi_awvalid0
    );
\axi_awaddr_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[12]_i_1_n_6\,
      Q => \^m_axi_awaddr\(1),
      R => burst_vaild1
    );
\axi_awaddr_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[12]_i_1_n_5\,
      Q => \^m_axi_awaddr\(2),
      R => burst_vaild1
    );
\axi_awaddr_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[12]_i_1_n_4\,
      Q => \^m_axi_awaddr\(3),
      R => burst_vaild1
    );
\axi_awaddr_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \axi_awaddr_reg[12]_i_1_n_0\,
      CO(2) => \axi_awaddr_reg[12]_i_1_n_1\,
      CO(1) => \axi_awaddr_reg[12]_i_1_n_2\,
      CO(0) => \axi_awaddr_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \axi_awaddr_reg[12]_i_1_n_4\,
      O(2) => \axi_awaddr_reg[12]_i_1_n_5\,
      O(1) => \axi_awaddr_reg[12]_i_1_n_6\,
      O(0) => \axi_awaddr_reg[12]_i_1_n_7\,
      S(3 downto 1) => \^m_axi_awaddr\(3 downto 1),
      S(0) => \axi_awaddr[12]_i_2_n_0\
    );
\axi_awaddr_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[16]_i_1_n_7\,
      Q => \^m_axi_awaddr\(4),
      R => burst_vaild1
    );
\axi_awaddr_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[16]_i_1_n_6\,
      Q => \^m_axi_awaddr\(5),
      R => burst_vaild1
    );
\axi_awaddr_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[16]_i_1_n_5\,
      Q => \^m_axi_awaddr\(6),
      R => burst_vaild1
    );
\axi_awaddr_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[16]_i_1_n_4\,
      Q => \^m_axi_awaddr\(7),
      R => burst_vaild1
    );
\axi_awaddr_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_awaddr_reg[12]_i_1_n_0\,
      CO(3) => \axi_awaddr_reg[16]_i_1_n_0\,
      CO(2) => \axi_awaddr_reg[16]_i_1_n_1\,
      CO(1) => \axi_awaddr_reg[16]_i_1_n_2\,
      CO(0) => \axi_awaddr_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \axi_awaddr_reg[16]_i_1_n_4\,
      O(2) => \axi_awaddr_reg[16]_i_1_n_5\,
      O(1) => \axi_awaddr_reg[16]_i_1_n_6\,
      O(0) => \axi_awaddr_reg[16]_i_1_n_7\,
      S(3 downto 0) => \^m_axi_awaddr\(7 downto 4)
    );
\axi_awaddr_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[20]_i_1_n_7\,
      Q => \^m_axi_awaddr\(8),
      R => burst_vaild1
    );
\axi_awaddr_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[20]_i_1_n_6\,
      Q => \^m_axi_awaddr\(9),
      R => burst_vaild1
    );
\axi_awaddr_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[20]_i_1_n_5\,
      Q => \^m_axi_awaddr\(10),
      R => burst_vaild1
    );
\axi_awaddr_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[20]_i_1_n_4\,
      Q => \^m_axi_awaddr\(11),
      R => burst_vaild1
    );
\axi_awaddr_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_awaddr_reg[16]_i_1_n_0\,
      CO(3) => \axi_awaddr_reg[20]_i_1_n_0\,
      CO(2) => \axi_awaddr_reg[20]_i_1_n_1\,
      CO(1) => \axi_awaddr_reg[20]_i_1_n_2\,
      CO(0) => \axi_awaddr_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \axi_awaddr_reg[20]_i_1_n_4\,
      O(2) => \axi_awaddr_reg[20]_i_1_n_5\,
      O(1) => \axi_awaddr_reg[20]_i_1_n_6\,
      O(0) => \axi_awaddr_reg[20]_i_1_n_7\,
      S(3 downto 0) => \^m_axi_awaddr\(11 downto 8)
    );
\axi_awaddr_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[24]_i_1_n_7\,
      Q => \^m_axi_awaddr\(12),
      R => burst_vaild1
    );
\axi_awaddr_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[24]_i_1_n_6\,
      Q => \^m_axi_awaddr\(13),
      R => burst_vaild1
    );
\axi_awaddr_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[24]_i_1_n_5\,
      Q => \^m_axi_awaddr\(14),
      R => burst_vaild1
    );
\axi_awaddr_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[24]_i_1_n_4\,
      Q => \^m_axi_awaddr\(15),
      R => burst_vaild1
    );
\axi_awaddr_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_awaddr_reg[20]_i_1_n_0\,
      CO(3) => \axi_awaddr_reg[24]_i_1_n_0\,
      CO(2) => \axi_awaddr_reg[24]_i_1_n_1\,
      CO(1) => \axi_awaddr_reg[24]_i_1_n_2\,
      CO(0) => \axi_awaddr_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \axi_awaddr_reg[24]_i_1_n_4\,
      O(2) => \axi_awaddr_reg[24]_i_1_n_5\,
      O(1) => \axi_awaddr_reg[24]_i_1_n_6\,
      O(0) => \axi_awaddr_reg[24]_i_1_n_7\,
      S(3 downto 0) => \^m_axi_awaddr\(15 downto 12)
    );
\axi_awaddr_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[28]_i_1_n_7\,
      Q => \^m_axi_awaddr\(16),
      R => burst_vaild1
    );
\axi_awaddr_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[28]_i_1_n_6\,
      Q => \^m_axi_awaddr\(17),
      R => burst_vaild1
    );
\axi_awaddr_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[28]_i_1_n_5\,
      Q => \^m_axi_awaddr\(18),
      R => burst_vaild1
    );
\axi_awaddr_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[28]_i_1_n_4\,
      Q => \^m_axi_awaddr\(19),
      R => burst_vaild1
    );
\axi_awaddr_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_awaddr_reg[24]_i_1_n_0\,
      CO(3) => \axi_awaddr_reg[28]_i_1_n_0\,
      CO(2) => \axi_awaddr_reg[28]_i_1_n_1\,
      CO(1) => \axi_awaddr_reg[28]_i_1_n_2\,
      CO(0) => \axi_awaddr_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \axi_awaddr_reg[28]_i_1_n_4\,
      O(2) => \axi_awaddr_reg[28]_i_1_n_5\,
      O(1) => \axi_awaddr_reg[28]_i_1_n_6\,
      O(0) => \axi_awaddr_reg[28]_i_1_n_7\,
      S(3 downto 0) => \^m_axi_awaddr\(19 downto 16)
    );
\axi_awaddr_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[31]_i_3_n_7\,
      Q => \^m_axi_awaddr\(20),
      R => burst_vaild1
    );
\axi_awaddr_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[31]_i_3_n_6\,
      Q => \^m_axi_awaddr\(21),
      R => burst_vaild1
    );
\axi_awaddr_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[31]_i_3_n_5\,
      Q => \^m_axi_awaddr\(22),
      R => burst_vaild1
    );
\axi_awaddr_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \axi_awaddr_reg[28]_i_1_n_0\,
      CO(3 downto 2) => \NLW_axi_awaddr_reg[31]_i_3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \axi_awaddr_reg[31]_i_3_n_2\,
      CO(0) => \axi_awaddr_reg[31]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_axi_awaddr_reg[31]_i_3_O_UNCONNECTED\(3),
      O(2) => \axi_awaddr_reg[31]_i_3_n_5\,
      O(1) => \axi_awaddr_reg[31]_i_3_n_6\,
      O(0) => \axi_awaddr_reg[31]_i_3_n_7\,
      S(3) => '0',
      S(2 downto 0) => \^m_axi_awaddr\(22 downto 20)
    );
\axi_awaddr_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => axi_awvalid0,
      D => \axi_awaddr_reg[12]_i_1_n_7\,
      Q => \^m_axi_awaddr\(0),
      R => burst_vaild1
    );
axi_awvalid_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => start_single_burst_write_reg_n_0,
      I1 => M_AXI_AWREADY,
      I2 => \^axi_awvalid_reg_0\,
      O => axi_awvalid_i_1_n_0
    );
axi_awvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_awvalid_i_1_n_0,
      Q => \^axi_awvalid_reg_0\,
      R => burst_vaild1
    );
axi_bready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => M_AXI_BVALID,
      I1 => \^m_axi_bready\,
      O => axi_bready0
    );
axi_bready_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_bready0,
      Q => \^m_axi_bready\,
      R => burst_vaild1
    );
axi_wlast_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20FFFFFF20000000"
    )
        port map (
      I0 => write_index_reg(1),
      I1 => write_index_reg(0),
      I2 => axi_wlast_i_2_n_0,
      I3 => M_AXI_WREADY,
      I4 => \^axi_wvalid_reg_0\,
      I5 => \^m_axi_wlast\,
      O => axi_wlast_i_1_n_0
    );
axi_wlast_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => write_index_reg(4),
      I1 => write_index_reg(5),
      I2 => write_index_reg(2),
      I3 => write_index_reg(3),
      I4 => write_index_reg(7),
      I5 => write_index_reg(6),
      O => axi_wlast_i_2_n_0
    );
axi_wlast_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_wlast_i_1_n_0,
      Q => \^m_axi_wlast\,
      R => burst_vaild1
    );
axi_wvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F70"
    )
        port map (
      I0 => \^m_axi_wlast\,
      I1 => M_AXI_WREADY,
      I2 => \^axi_wvalid_reg_0\,
      I3 => start_single_burst_write_reg_n_0,
      O => axi_wvalid_i_1_n_0
    );
axi_wvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => axi_wvalid_i_1_n_0,
      Q => \^axi_wvalid_reg_0\,
      R => burst_vaild1
    );
burst_vaild_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"000B"
    )
        port map (
      I0 => ov5640_vsync_reg,
      I1 => ov5640_vsync_reg1,
      I2 => ui_rst,
      I3 => prog_empty,
      O => burst_vaild_i_1_n_0
    );
burst_vaild_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => burst_vaild_i_1_n_0,
      Q => burst_vaild,
      R => '0'
    );
burst_write_active_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F7F0"
    )
        port map (
      I0 => \^m_axi_bready\,
      I1 => M_AXI_BVALID,
      I2 => start_single_burst_write_reg_n_0,
      I3 => burst_write_active,
      O => burst_write_active_i_1_n_0
    );
burst_write_active_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => burst_write_active_i_1_n_0,
      Q => burst_write_active,
      R => burst_vaild1
    );
\cnt_cp[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => \write_burst_counter[0]_i_3_n_0\,
      I1 => \cnt_cp[0]_i_3_n_0\,
      I2 => mst_exec_state,
      O => cnt_cp
    );
\cnt_cp[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => write_burst_counter_reg(0),
      I1 => write_burst_counter_reg(1),
      I2 => burst_write_active,
      I3 => writes_done,
      I4 => start_single_burst_write_reg_n_0,
      I5 => \^axi_awvalid_reg_0\,
      O => \cnt_cp[0]_i_3_n_0\
    );
\cnt_cp[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[3]\,
      I1 => mst_exec_state,
      O => \cnt_cp[0]_i_4_n_0\
    );
\cnt_cp[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[2]\,
      I1 => mst_exec_state,
      O => \cnt_cp[0]_i_5_n_0\
    );
\cnt_cp[0]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[1]\,
      I1 => mst_exec_state,
      O => \cnt_cp[0]_i_6_n_0\
    );
\cnt_cp[0]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[0]\,
      I1 => mst_exec_state,
      O => \cnt_cp[0]_i_7_n_0\
    );
\cnt_cp[12]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[15]\,
      I1 => mst_exec_state,
      O => \cnt_cp[12]_i_2_n_0\
    );
\cnt_cp[12]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[14]\,
      I1 => mst_exec_state,
      O => \cnt_cp[12]_i_3_n_0\
    );
\cnt_cp[12]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[13]\,
      I1 => mst_exec_state,
      O => \cnt_cp[12]_i_4_n_0\
    );
\cnt_cp[12]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[12]\,
      I1 => mst_exec_state,
      O => \cnt_cp[12]_i_5_n_0\
    );
\cnt_cp[16]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[19]\,
      I1 => mst_exec_state,
      O => \cnt_cp[16]_i_2_n_0\
    );
\cnt_cp[16]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[18]\,
      I1 => mst_exec_state,
      O => \cnt_cp[16]_i_3_n_0\
    );
\cnt_cp[16]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[17]\,
      I1 => mst_exec_state,
      O => \cnt_cp[16]_i_4_n_0\
    );
\cnt_cp[16]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[16]\,
      I1 => mst_exec_state,
      O => \cnt_cp[16]_i_5_n_0\
    );
\cnt_cp[20]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[23]\,
      I1 => mst_exec_state,
      O => \cnt_cp[20]_i_2_n_0\
    );
\cnt_cp[20]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[22]\,
      I1 => mst_exec_state,
      O => \cnt_cp[20]_i_3_n_0\
    );
\cnt_cp[20]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[21]\,
      I1 => mst_exec_state,
      O => \cnt_cp[20]_i_4_n_0\
    );
\cnt_cp[20]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[20]\,
      I1 => mst_exec_state,
      O => \cnt_cp[20]_i_5_n_0\
    );
\cnt_cp[24]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[27]\,
      I1 => mst_exec_state,
      O => \cnt_cp[24]_i_2_n_0\
    );
\cnt_cp[24]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[26]\,
      I1 => mst_exec_state,
      O => \cnt_cp[24]_i_3_n_0\
    );
\cnt_cp[24]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[25]\,
      I1 => mst_exec_state,
      O => \cnt_cp[24]_i_4_n_0\
    );
\cnt_cp[24]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[24]\,
      I1 => mst_exec_state,
      O => \cnt_cp[24]_i_5_n_0\
    );
\cnt_cp[28]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[31]\,
      I1 => mst_exec_state,
      O => \cnt_cp[28]_i_2_n_0\
    );
\cnt_cp[28]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[30]\,
      I1 => mst_exec_state,
      O => \cnt_cp[28]_i_3_n_0\
    );
\cnt_cp[28]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[29]\,
      I1 => mst_exec_state,
      O => \cnt_cp[28]_i_4_n_0\
    );
\cnt_cp[28]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[28]\,
      I1 => mst_exec_state,
      O => \cnt_cp[28]_i_5_n_0\
    );
\cnt_cp[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[7]\,
      I1 => mst_exec_state,
      O => \cnt_cp[4]_i_2_n_0\
    );
\cnt_cp[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[6]\,
      I1 => mst_exec_state,
      O => \cnt_cp[4]_i_3_n_0\
    );
\cnt_cp[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[5]\,
      I1 => mst_exec_state,
      O => \cnt_cp[4]_i_4_n_0\
    );
\cnt_cp[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[4]\,
      I1 => mst_exec_state,
      O => \cnt_cp[4]_i_5_n_0\
    );
\cnt_cp[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[11]\,
      I1 => mst_exec_state,
      O => \cnt_cp[8]_i_2_n_0\
    );
\cnt_cp[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[10]\,
      I1 => mst_exec_state,
      O => \cnt_cp[8]_i_3_n_0\
    );
\cnt_cp[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[9]\,
      I1 => mst_exec_state,
      O => \cnt_cp[8]_i_4_n_0\
    );
\cnt_cp[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[8]\,
      I1 => mst_exec_state,
      O => \cnt_cp[8]_i_5_n_0\
    );
\cnt_cp_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[0]_i_2_n_7\,
      Q => \cnt_cp_reg_n_0_[0]\,
      S => ui_rst
    );
\cnt_cp_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_cp_reg[0]_i_2_n_0\,
      CO(2) => \cnt_cp_reg[0]_i_2_n_1\,
      CO(1) => \cnt_cp_reg[0]_i_2_n_2\,
      CO(0) => \cnt_cp_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3) => mst_exec_state,
      DI(2) => mst_exec_state,
      DI(1) => mst_exec_state,
      DI(0) => mst_exec_state,
      O(3) => \cnt_cp_reg[0]_i_2_n_4\,
      O(2) => \cnt_cp_reg[0]_i_2_n_5\,
      O(1) => \cnt_cp_reg[0]_i_2_n_6\,
      O(0) => \cnt_cp_reg[0]_i_2_n_7\,
      S(3) => \cnt_cp[0]_i_4_n_0\,
      S(2) => \cnt_cp[0]_i_5_n_0\,
      S(1) => \cnt_cp[0]_i_6_n_0\,
      S(0) => \cnt_cp[0]_i_7_n_0\
    );
\cnt_cp_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[8]_i_1_n_5\,
      Q => \cnt_cp_reg_n_0_[10]\,
      R => ui_rst
    );
\cnt_cp_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[8]_i_1_n_4\,
      Q => \cnt_cp_reg_n_0_[11]\,
      R => ui_rst
    );
\cnt_cp_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[12]_i_1_n_7\,
      Q => \cnt_cp_reg_n_0_[12]\,
      R => ui_rst
    );
\cnt_cp_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_cp_reg[8]_i_1_n_0\,
      CO(3) => \cnt_cp_reg[12]_i_1_n_0\,
      CO(2) => \cnt_cp_reg[12]_i_1_n_1\,
      CO(1) => \cnt_cp_reg[12]_i_1_n_2\,
      CO(0) => \cnt_cp_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => mst_exec_state,
      DI(2) => mst_exec_state,
      DI(1) => mst_exec_state,
      DI(0) => mst_exec_state,
      O(3) => \cnt_cp_reg[12]_i_1_n_4\,
      O(2) => \cnt_cp_reg[12]_i_1_n_5\,
      O(1) => \cnt_cp_reg[12]_i_1_n_6\,
      O(0) => \cnt_cp_reg[12]_i_1_n_7\,
      S(3) => \cnt_cp[12]_i_2_n_0\,
      S(2) => \cnt_cp[12]_i_3_n_0\,
      S(1) => \cnt_cp[12]_i_4_n_0\,
      S(0) => \cnt_cp[12]_i_5_n_0\
    );
\cnt_cp_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[12]_i_1_n_6\,
      Q => \cnt_cp_reg_n_0_[13]\,
      R => ui_rst
    );
\cnt_cp_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[12]_i_1_n_5\,
      Q => \cnt_cp_reg_n_0_[14]\,
      R => ui_rst
    );
\cnt_cp_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[12]_i_1_n_4\,
      Q => \cnt_cp_reg_n_0_[15]\,
      R => ui_rst
    );
\cnt_cp_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[16]_i_1_n_7\,
      Q => \cnt_cp_reg_n_0_[16]\,
      R => ui_rst
    );
\cnt_cp_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_cp_reg[12]_i_1_n_0\,
      CO(3) => \cnt_cp_reg[16]_i_1_n_0\,
      CO(2) => \cnt_cp_reg[16]_i_1_n_1\,
      CO(1) => \cnt_cp_reg[16]_i_1_n_2\,
      CO(0) => \cnt_cp_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => mst_exec_state,
      DI(2) => mst_exec_state,
      DI(1) => mst_exec_state,
      DI(0) => mst_exec_state,
      O(3) => \cnt_cp_reg[16]_i_1_n_4\,
      O(2) => \cnt_cp_reg[16]_i_1_n_5\,
      O(1) => \cnt_cp_reg[16]_i_1_n_6\,
      O(0) => \cnt_cp_reg[16]_i_1_n_7\,
      S(3) => \cnt_cp[16]_i_2_n_0\,
      S(2) => \cnt_cp[16]_i_3_n_0\,
      S(1) => \cnt_cp[16]_i_4_n_0\,
      S(0) => \cnt_cp[16]_i_5_n_0\
    );
\cnt_cp_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[16]_i_1_n_6\,
      Q => \cnt_cp_reg_n_0_[17]\,
      R => ui_rst
    );
\cnt_cp_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[16]_i_1_n_5\,
      Q => \cnt_cp_reg_n_0_[18]\,
      R => ui_rst
    );
\cnt_cp_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[16]_i_1_n_4\,
      Q => \cnt_cp_reg_n_0_[19]\,
      R => ui_rst
    );
\cnt_cp_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[0]_i_2_n_6\,
      Q => \cnt_cp_reg_n_0_[1]\,
      S => ui_rst
    );
\cnt_cp_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[20]_i_1_n_7\,
      Q => \cnt_cp_reg_n_0_[20]\,
      R => ui_rst
    );
\cnt_cp_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_cp_reg[16]_i_1_n_0\,
      CO(3) => \cnt_cp_reg[20]_i_1_n_0\,
      CO(2) => \cnt_cp_reg[20]_i_1_n_1\,
      CO(1) => \cnt_cp_reg[20]_i_1_n_2\,
      CO(0) => \cnt_cp_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => mst_exec_state,
      DI(2) => mst_exec_state,
      DI(1) => mst_exec_state,
      DI(0) => mst_exec_state,
      O(3) => \cnt_cp_reg[20]_i_1_n_4\,
      O(2) => \cnt_cp_reg[20]_i_1_n_5\,
      O(1) => \cnt_cp_reg[20]_i_1_n_6\,
      O(0) => \cnt_cp_reg[20]_i_1_n_7\,
      S(3) => \cnt_cp[20]_i_2_n_0\,
      S(2) => \cnt_cp[20]_i_3_n_0\,
      S(1) => \cnt_cp[20]_i_4_n_0\,
      S(0) => \cnt_cp[20]_i_5_n_0\
    );
\cnt_cp_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[20]_i_1_n_6\,
      Q => \cnt_cp_reg_n_0_[21]\,
      R => ui_rst
    );
\cnt_cp_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[20]_i_1_n_5\,
      Q => \cnt_cp_reg_n_0_[22]\,
      R => ui_rst
    );
\cnt_cp_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[20]_i_1_n_4\,
      Q => \cnt_cp_reg_n_0_[23]\,
      R => ui_rst
    );
\cnt_cp_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[24]_i_1_n_7\,
      Q => \cnt_cp_reg_n_0_[24]\,
      R => ui_rst
    );
\cnt_cp_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_cp_reg[20]_i_1_n_0\,
      CO(3) => \cnt_cp_reg[24]_i_1_n_0\,
      CO(2) => \cnt_cp_reg[24]_i_1_n_1\,
      CO(1) => \cnt_cp_reg[24]_i_1_n_2\,
      CO(0) => \cnt_cp_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => mst_exec_state,
      DI(2) => mst_exec_state,
      DI(1) => mst_exec_state,
      DI(0) => mst_exec_state,
      O(3) => \cnt_cp_reg[24]_i_1_n_4\,
      O(2) => \cnt_cp_reg[24]_i_1_n_5\,
      O(1) => \cnt_cp_reg[24]_i_1_n_6\,
      O(0) => \cnt_cp_reg[24]_i_1_n_7\,
      S(3) => \cnt_cp[24]_i_2_n_0\,
      S(2) => \cnt_cp[24]_i_3_n_0\,
      S(1) => \cnt_cp[24]_i_4_n_0\,
      S(0) => \cnt_cp[24]_i_5_n_0\
    );
\cnt_cp_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[24]_i_1_n_6\,
      Q => \cnt_cp_reg_n_0_[25]\,
      R => ui_rst
    );
\cnt_cp_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[24]_i_1_n_5\,
      Q => \cnt_cp_reg_n_0_[26]\,
      R => ui_rst
    );
\cnt_cp_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[24]_i_1_n_4\,
      Q => \cnt_cp_reg_n_0_[27]\,
      R => ui_rst
    );
\cnt_cp_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[28]_i_1_n_7\,
      Q => \cnt_cp_reg_n_0_[28]\,
      R => ui_rst
    );
\cnt_cp_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_cp_reg[24]_i_1_n_0\,
      CO(3) => \NLW_cnt_cp_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_cp_reg[28]_i_1_n_1\,
      CO(1) => \cnt_cp_reg[28]_i_1_n_2\,
      CO(0) => \cnt_cp_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => mst_exec_state,
      DI(1) => mst_exec_state,
      DI(0) => mst_exec_state,
      O(3) => \cnt_cp_reg[28]_i_1_n_4\,
      O(2) => \cnt_cp_reg[28]_i_1_n_5\,
      O(1) => \cnt_cp_reg[28]_i_1_n_6\,
      O(0) => \cnt_cp_reg[28]_i_1_n_7\,
      S(3) => \cnt_cp[28]_i_2_n_0\,
      S(2) => \cnt_cp[28]_i_3_n_0\,
      S(1) => \cnt_cp[28]_i_4_n_0\,
      S(0) => \cnt_cp[28]_i_5_n_0\
    );
\cnt_cp_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[28]_i_1_n_6\,
      Q => \cnt_cp_reg_n_0_[29]\,
      R => ui_rst
    );
\cnt_cp_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[0]_i_2_n_5\,
      Q => \cnt_cp_reg_n_0_[2]\,
      R => ui_rst
    );
\cnt_cp_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[28]_i_1_n_5\,
      Q => \cnt_cp_reg_n_0_[30]\,
      R => ui_rst
    );
\cnt_cp_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[28]_i_1_n_4\,
      Q => \cnt_cp_reg_n_0_[31]\,
      R => ui_rst
    );
\cnt_cp_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[0]_i_2_n_4\,
      Q => \cnt_cp_reg_n_0_[3]\,
      S => ui_rst
    );
\cnt_cp_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[4]_i_1_n_7\,
      Q => \cnt_cp_reg_n_0_[4]\,
      S => ui_rst
    );
\cnt_cp_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_cp_reg[0]_i_2_n_0\,
      CO(3) => \cnt_cp_reg[4]_i_1_n_0\,
      CO(2) => \cnt_cp_reg[4]_i_1_n_1\,
      CO(1) => \cnt_cp_reg[4]_i_1_n_2\,
      CO(0) => \cnt_cp_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => mst_exec_state,
      DI(2) => mst_exec_state,
      DI(1) => mst_exec_state,
      DI(0) => mst_exec_state,
      O(3) => \cnt_cp_reg[4]_i_1_n_4\,
      O(2) => \cnt_cp_reg[4]_i_1_n_5\,
      O(1) => \cnt_cp_reg[4]_i_1_n_6\,
      O(0) => \cnt_cp_reg[4]_i_1_n_7\,
      S(3) => \cnt_cp[4]_i_2_n_0\,
      S(2) => \cnt_cp[4]_i_3_n_0\,
      S(1) => \cnt_cp[4]_i_4_n_0\,
      S(0) => \cnt_cp[4]_i_5_n_0\
    );
\cnt_cp_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[4]_i_1_n_6\,
      Q => \cnt_cp_reg_n_0_[5]\,
      R => ui_rst
    );
\cnt_cp_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[4]_i_1_n_5\,
      Q => \cnt_cp_reg_n_0_[6]\,
      S => ui_rst
    );
\cnt_cp_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[4]_i_1_n_4\,
      Q => \cnt_cp_reg_n_0_[7]\,
      R => ui_rst
    );
\cnt_cp_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[8]_i_1_n_7\,
      Q => \cnt_cp_reg_n_0_[8]\,
      R => ui_rst
    );
\cnt_cp_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_cp_reg[4]_i_1_n_0\,
      CO(3) => \cnt_cp_reg[8]_i_1_n_0\,
      CO(2) => \cnt_cp_reg[8]_i_1_n_1\,
      CO(1) => \cnt_cp_reg[8]_i_1_n_2\,
      CO(0) => \cnt_cp_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3) => mst_exec_state,
      DI(2) => mst_exec_state,
      DI(1) => mst_exec_state,
      DI(0) => mst_exec_state,
      O(3) => \cnt_cp_reg[8]_i_1_n_4\,
      O(2) => \cnt_cp_reg[8]_i_1_n_5\,
      O(1) => \cnt_cp_reg[8]_i_1_n_6\,
      O(0) => \cnt_cp_reg[8]_i_1_n_7\,
      S(3) => \cnt_cp[8]_i_2_n_0\,
      S(2) => \cnt_cp[8]_i_3_n_0\,
      S(1) => \cnt_cp[8]_i_4_n_0\,
      S(0) => \cnt_cp[8]_i_5_n_0\
    );
\cnt_cp_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => M_AXI_ACLK,
      CE => cnt_cp,
      D => \cnt_cp_reg[8]_i_1_n_6\,
      Q => \cnt_cp_reg_n_0_[9]\,
      S => ui_rst
    );
rd_wdfifo_en_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => M_AXI_WREADY,
      I1 => \^axi_wvalid_reg_0\,
      O => rd_wdfifo_en
    );
start_single_burst_write_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000BABABA8A"
    )
        port map (
      I0 => start_single_burst_write_reg_n_0,
      I1 => writes_done,
      I2 => mst_exec_state,
      I3 => start_single_burst_write_i_2_n_0,
      I4 => start_single_burst_write_i_3_n_0,
      I5 => ui_rst,
      O => start_single_burst_write_i_1_n_0
    );
start_single_burst_write_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[18]\,
      I1 => \cnt_cp_reg_n_0_[19]\,
      I2 => \cnt_cp_reg_n_0_[16]\,
      I3 => \cnt_cp_reg_n_0_[17]\,
      I4 => \cnt_cp_reg_n_0_[21]\,
      I5 => \cnt_cp_reg_n_0_[20]\,
      O => start_single_burst_write_i_10_n_0
    );
start_single_burst_write_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[6]\,
      I1 => \cnt_cp_reg_n_0_[7]\,
      I2 => \cnt_cp_reg_n_0_[4]\,
      I3 => \cnt_cp_reg_n_0_[5]\,
      I4 => \cnt_cp_reg_n_0_[9]\,
      I5 => \cnt_cp_reg_n_0_[8]\,
      O => start_single_burst_write_i_11_n_0
    );
start_single_burst_write_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => burst_write_active,
      I1 => burst_vaild,
      I2 => start_single_burst_write_reg_n_0,
      I3 => \^axi_awvalid_reg_0\,
      O => start_single_burst_write_i_2_n_0
    );
start_single_burst_write_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => start_single_burst_write_i_4_n_0,
      I1 => start_single_burst_write_i_5_n_0,
      I2 => \write_burst_counter[0]_i_7_n_0\,
      I3 => \write_burst_counter[0]_i_6_n_0\,
      I4 => \write_burst_counter[0]_i_5_n_0\,
      I5 => start_single_burst_write_i_6_n_0,
      O => start_single_burst_write_i_3_n_0
    );
start_single_burst_write_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => start_single_burst_write_i_7_n_0,
      I1 => \cnt_cp_reg_n_0_[29]\,
      I2 => \cnt_cp_reg_n_0_[28]\,
      I3 => \cnt_cp_reg_n_0_[31]\,
      I4 => \cnt_cp_reg_n_0_[30]\,
      I5 => start_single_burst_write_i_8_n_0,
      O => start_single_burst_write_i_4_n_0
    );
start_single_burst_write_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[0]\,
      I1 => \cnt_cp_reg_n_0_[1]\,
      I2 => \cnt_cp_reg_n_0_[2]\,
      I3 => start_single_burst_write_reg_n_0,
      I4 => \cnt_cp_reg_n_0_[3]\,
      O => start_single_burst_write_i_5_n_0
    );
start_single_burst_write_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000080"
    )
        port map (
      I0 => start_single_burst_write_i_9_n_0,
      I1 => start_single_burst_write_i_10_n_0,
      I2 => start_single_burst_write_i_11_n_0,
      I3 => burst_write_active,
      I4 => \^axi_awvalid_reg_0\,
      O => start_single_burst_write_i_6_n_0
    );
start_single_burst_write_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => write_burst_counter_reg(0),
      I1 => write_burst_counter_reg(1),
      O => start_single_burst_write_i_7_n_0
    );
start_single_burst_write_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[24]\,
      I1 => \cnt_cp_reg_n_0_[25]\,
      I2 => \cnt_cp_reg_n_0_[22]\,
      I3 => \cnt_cp_reg_n_0_[23]\,
      I4 => \cnt_cp_reg_n_0_[27]\,
      I5 => \cnt_cp_reg_n_0_[26]\,
      O => start_single_burst_write_i_8_n_0
    );
start_single_burst_write_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \cnt_cp_reg_n_0_[12]\,
      I1 => \cnt_cp_reg_n_0_[13]\,
      I2 => \cnt_cp_reg_n_0_[10]\,
      I3 => \cnt_cp_reg_n_0_[11]\,
      I4 => \cnt_cp_reg_n_0_[15]\,
      I5 => \cnt_cp_reg_n_0_[14]\,
      O => start_single_burst_write_i_9_n_0
    );
start_single_burst_write_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => start_single_burst_write_i_1_n_0,
      Q => start_single_burst_write_reg_n_0,
      R => '0'
    );
\write_burst_counter[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EF000000"
    )
        port map (
      I0 => \write_burst_counter[0]_i_3_n_0\,
      I1 => write_burst_counter_reg(0),
      I2 => write_burst_counter_reg(1),
      I3 => M_AXI_AWREADY,
      I4 => \^axi_awvalid_reg_0\,
      O => write_burst_counter
    );
\write_burst_counter[0]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => write_burst_counter_reg(25),
      I1 => write_burst_counter_reg(24),
      I2 => write_burst_counter_reg(27),
      I3 => write_burst_counter_reg(26),
      O => \write_burst_counter[0]_i_10_n_0\
    );
\write_burst_counter[0]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => write_burst_counter_reg(30),
      I1 => write_burst_counter_reg(31),
      I2 => write_burst_counter_reg(28),
      I3 => write_burst_counter_reg(29),
      I4 => write_burst_counter_reg(2),
      I5 => write_burst_counter_reg(3),
      O => \write_burst_counter[0]_i_11_n_0\
    );
\write_burst_counter[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \write_burst_counter[0]_i_5_n_0\,
      I1 => \write_burst_counter[0]_i_6_n_0\,
      I2 => \write_burst_counter[0]_i_7_n_0\,
      O => \write_burst_counter[0]_i_3_n_0\
    );
\write_burst_counter[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_burst_counter_reg(0),
      O => \write_burst_counter[0]_i_4_n_0\
    );
\write_burst_counter[0]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => write_burst_counter_reg(14),
      I1 => write_burst_counter_reg(15),
      I2 => write_burst_counter_reg(12),
      I3 => write_burst_counter_reg(13),
      I4 => \write_burst_counter[0]_i_8_n_0\,
      O => \write_burst_counter[0]_i_5_n_0\
    );
\write_burst_counter[0]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFBFF"
    )
        port map (
      I0 => write_burst_counter_reg(6),
      I1 => write_burst_counter_reg(7),
      I2 => write_burst_counter_reg(4),
      I3 => write_burst_counter_reg(5),
      I4 => \write_burst_counter[0]_i_9_n_0\,
      O => \write_burst_counter[0]_i_6_n_0\
    );
\write_burst_counter[0]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \write_burst_counter[0]_i_10_n_0\,
      I1 => write_burst_counter_reg(21),
      I2 => write_burst_counter_reg(20),
      I3 => write_burst_counter_reg(23),
      I4 => write_burst_counter_reg(22),
      I5 => \write_burst_counter[0]_i_11_n_0\,
      O => \write_burst_counter[0]_i_7_n_0\
    );
\write_burst_counter[0]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => write_burst_counter_reg(17),
      I1 => write_burst_counter_reg(16),
      I2 => write_burst_counter_reg(19),
      I3 => write_burst_counter_reg(18),
      O => \write_burst_counter[0]_i_8_n_0\
    );
\write_burst_counter[0]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => write_burst_counter_reg(9),
      I1 => write_burst_counter_reg(8),
      I2 => write_burst_counter_reg(11),
      I3 => write_burst_counter_reg(10),
      O => \write_burst_counter[0]_i_9_n_0\
    );
\write_burst_counter_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[0]_i_2_n_7\,
      Q => write_burst_counter_reg(0),
      R => burst_vaild1
    );
\write_burst_counter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \write_burst_counter_reg[0]_i_2_n_0\,
      CO(2) => \write_burst_counter_reg[0]_i_2_n_1\,
      CO(1) => \write_burst_counter_reg[0]_i_2_n_2\,
      CO(0) => \write_burst_counter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \write_burst_counter_reg[0]_i_2_n_4\,
      O(2) => \write_burst_counter_reg[0]_i_2_n_5\,
      O(1) => \write_burst_counter_reg[0]_i_2_n_6\,
      O(0) => \write_burst_counter_reg[0]_i_2_n_7\,
      S(3 downto 1) => write_burst_counter_reg(3 downto 1),
      S(0) => \write_burst_counter[0]_i_4_n_0\
    );
\write_burst_counter_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[8]_i_1_n_5\,
      Q => write_burst_counter_reg(10),
      R => burst_vaild1
    );
\write_burst_counter_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[8]_i_1_n_4\,
      Q => write_burst_counter_reg(11),
      R => burst_vaild1
    );
\write_burst_counter_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[12]_i_1_n_7\,
      Q => write_burst_counter_reg(12),
      R => burst_vaild1
    );
\write_burst_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \write_burst_counter_reg[8]_i_1_n_0\,
      CO(3) => \write_burst_counter_reg[12]_i_1_n_0\,
      CO(2) => \write_burst_counter_reg[12]_i_1_n_1\,
      CO(1) => \write_burst_counter_reg[12]_i_1_n_2\,
      CO(0) => \write_burst_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \write_burst_counter_reg[12]_i_1_n_4\,
      O(2) => \write_burst_counter_reg[12]_i_1_n_5\,
      O(1) => \write_burst_counter_reg[12]_i_1_n_6\,
      O(0) => \write_burst_counter_reg[12]_i_1_n_7\,
      S(3 downto 0) => write_burst_counter_reg(15 downto 12)
    );
\write_burst_counter_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[12]_i_1_n_6\,
      Q => write_burst_counter_reg(13),
      R => burst_vaild1
    );
\write_burst_counter_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[12]_i_1_n_5\,
      Q => write_burst_counter_reg(14),
      R => burst_vaild1
    );
\write_burst_counter_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[12]_i_1_n_4\,
      Q => write_burst_counter_reg(15),
      R => burst_vaild1
    );
\write_burst_counter_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[16]_i_1_n_7\,
      Q => write_burst_counter_reg(16),
      R => burst_vaild1
    );
\write_burst_counter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \write_burst_counter_reg[12]_i_1_n_0\,
      CO(3) => \write_burst_counter_reg[16]_i_1_n_0\,
      CO(2) => \write_burst_counter_reg[16]_i_1_n_1\,
      CO(1) => \write_burst_counter_reg[16]_i_1_n_2\,
      CO(0) => \write_burst_counter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \write_burst_counter_reg[16]_i_1_n_4\,
      O(2) => \write_burst_counter_reg[16]_i_1_n_5\,
      O(1) => \write_burst_counter_reg[16]_i_1_n_6\,
      O(0) => \write_burst_counter_reg[16]_i_1_n_7\,
      S(3 downto 0) => write_burst_counter_reg(19 downto 16)
    );
\write_burst_counter_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[16]_i_1_n_6\,
      Q => write_burst_counter_reg(17),
      R => burst_vaild1
    );
\write_burst_counter_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[16]_i_1_n_5\,
      Q => write_burst_counter_reg(18),
      R => burst_vaild1
    );
\write_burst_counter_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[16]_i_1_n_4\,
      Q => write_burst_counter_reg(19),
      R => burst_vaild1
    );
\write_burst_counter_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[0]_i_2_n_6\,
      Q => write_burst_counter_reg(1),
      R => burst_vaild1
    );
\write_burst_counter_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[20]_i_1_n_7\,
      Q => write_burst_counter_reg(20),
      R => burst_vaild1
    );
\write_burst_counter_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \write_burst_counter_reg[16]_i_1_n_0\,
      CO(3) => \write_burst_counter_reg[20]_i_1_n_0\,
      CO(2) => \write_burst_counter_reg[20]_i_1_n_1\,
      CO(1) => \write_burst_counter_reg[20]_i_1_n_2\,
      CO(0) => \write_burst_counter_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \write_burst_counter_reg[20]_i_1_n_4\,
      O(2) => \write_burst_counter_reg[20]_i_1_n_5\,
      O(1) => \write_burst_counter_reg[20]_i_1_n_6\,
      O(0) => \write_burst_counter_reg[20]_i_1_n_7\,
      S(3 downto 0) => write_burst_counter_reg(23 downto 20)
    );
\write_burst_counter_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[20]_i_1_n_6\,
      Q => write_burst_counter_reg(21),
      R => burst_vaild1
    );
\write_burst_counter_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[20]_i_1_n_5\,
      Q => write_burst_counter_reg(22),
      R => burst_vaild1
    );
\write_burst_counter_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[20]_i_1_n_4\,
      Q => write_burst_counter_reg(23),
      R => burst_vaild1
    );
\write_burst_counter_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[24]_i_1_n_7\,
      Q => write_burst_counter_reg(24),
      R => burst_vaild1
    );
\write_burst_counter_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \write_burst_counter_reg[20]_i_1_n_0\,
      CO(3) => \write_burst_counter_reg[24]_i_1_n_0\,
      CO(2) => \write_burst_counter_reg[24]_i_1_n_1\,
      CO(1) => \write_burst_counter_reg[24]_i_1_n_2\,
      CO(0) => \write_burst_counter_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \write_burst_counter_reg[24]_i_1_n_4\,
      O(2) => \write_burst_counter_reg[24]_i_1_n_5\,
      O(1) => \write_burst_counter_reg[24]_i_1_n_6\,
      O(0) => \write_burst_counter_reg[24]_i_1_n_7\,
      S(3 downto 0) => write_burst_counter_reg(27 downto 24)
    );
\write_burst_counter_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[24]_i_1_n_6\,
      Q => write_burst_counter_reg(25),
      R => burst_vaild1
    );
\write_burst_counter_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[24]_i_1_n_5\,
      Q => write_burst_counter_reg(26),
      R => burst_vaild1
    );
\write_burst_counter_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[24]_i_1_n_4\,
      Q => write_burst_counter_reg(27),
      R => burst_vaild1
    );
\write_burst_counter_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[28]_i_1_n_7\,
      Q => write_burst_counter_reg(28),
      R => burst_vaild1
    );
\write_burst_counter_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \write_burst_counter_reg[24]_i_1_n_0\,
      CO(3) => \NLW_write_burst_counter_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \write_burst_counter_reg[28]_i_1_n_1\,
      CO(1) => \write_burst_counter_reg[28]_i_1_n_2\,
      CO(0) => \write_burst_counter_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \write_burst_counter_reg[28]_i_1_n_4\,
      O(2) => \write_burst_counter_reg[28]_i_1_n_5\,
      O(1) => \write_burst_counter_reg[28]_i_1_n_6\,
      O(0) => \write_burst_counter_reg[28]_i_1_n_7\,
      S(3 downto 0) => write_burst_counter_reg(31 downto 28)
    );
\write_burst_counter_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[28]_i_1_n_6\,
      Q => write_burst_counter_reg(29),
      R => burst_vaild1
    );
\write_burst_counter_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[0]_i_2_n_5\,
      Q => write_burst_counter_reg(2),
      R => burst_vaild1
    );
\write_burst_counter_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[28]_i_1_n_5\,
      Q => write_burst_counter_reg(30),
      R => burst_vaild1
    );
\write_burst_counter_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[28]_i_1_n_4\,
      Q => write_burst_counter_reg(31),
      R => burst_vaild1
    );
\write_burst_counter_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[0]_i_2_n_4\,
      Q => write_burst_counter_reg(3),
      R => burst_vaild1
    );
\write_burst_counter_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[4]_i_1_n_7\,
      Q => write_burst_counter_reg(4),
      R => burst_vaild1
    );
\write_burst_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \write_burst_counter_reg[0]_i_2_n_0\,
      CO(3) => \write_burst_counter_reg[4]_i_1_n_0\,
      CO(2) => \write_burst_counter_reg[4]_i_1_n_1\,
      CO(1) => \write_burst_counter_reg[4]_i_1_n_2\,
      CO(0) => \write_burst_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \write_burst_counter_reg[4]_i_1_n_4\,
      O(2) => \write_burst_counter_reg[4]_i_1_n_5\,
      O(1) => \write_burst_counter_reg[4]_i_1_n_6\,
      O(0) => \write_burst_counter_reg[4]_i_1_n_7\,
      S(3 downto 0) => write_burst_counter_reg(7 downto 4)
    );
\write_burst_counter_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[4]_i_1_n_6\,
      Q => write_burst_counter_reg(5),
      R => burst_vaild1
    );
\write_burst_counter_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[4]_i_1_n_5\,
      Q => write_burst_counter_reg(6),
      R => burst_vaild1
    );
\write_burst_counter_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[4]_i_1_n_4\,
      Q => write_burst_counter_reg(7),
      R => burst_vaild1
    );
\write_burst_counter_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[8]_i_1_n_7\,
      Q => write_burst_counter_reg(8),
      R => burst_vaild1
    );
\write_burst_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \write_burst_counter_reg[4]_i_1_n_0\,
      CO(3) => \write_burst_counter_reg[8]_i_1_n_0\,
      CO(2) => \write_burst_counter_reg[8]_i_1_n_1\,
      CO(1) => \write_burst_counter_reg[8]_i_1_n_2\,
      CO(0) => \write_burst_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \write_burst_counter_reg[8]_i_1_n_4\,
      O(2) => \write_burst_counter_reg[8]_i_1_n_5\,
      O(1) => \write_burst_counter_reg[8]_i_1_n_6\,
      O(0) => \write_burst_counter_reg[8]_i_1_n_7\,
      S(3 downto 0) => write_burst_counter_reg(11 downto 8)
    );
\write_burst_counter_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_burst_counter,
      D => \write_burst_counter_reg[8]_i_1_n_6\,
      Q => write_burst_counter_reg(9),
      R => burst_vaild1
    );
\write_index[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_index_reg(0),
      O => p_0_in(0)
    );
\write_index[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => write_index_reg(0),
      I1 => write_index_reg(1),
      O => p_0_in(1)
    );
\write_index[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => write_index_reg(1),
      I1 => write_index_reg(0),
      I2 => write_index_reg(2),
      O => p_0_in(2)
    );
\write_index[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => write_index_reg(2),
      I1 => write_index_reg(0),
      I2 => write_index_reg(1),
      I3 => write_index_reg(3),
      O => p_0_in(3)
    );
\write_index[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => write_index_reg(3),
      I1 => write_index_reg(1),
      I2 => write_index_reg(0),
      I3 => write_index_reg(2),
      I4 => write_index_reg(4),
      O => p_0_in(4)
    );
\write_index[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => write_index_reg(4),
      I1 => write_index_reg(2),
      I2 => write_index_reg(0),
      I3 => write_index_reg(1),
      I4 => write_index_reg(3),
      I5 => write_index_reg(5),
      O => p_0_in(5)
    );
\write_index[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \write_index[7]_i_4_n_0\,
      I1 => write_index_reg(6),
      O => p_0_in(6)
    );
\write_index[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF4"
    )
        port map (
      I0 => ov5640_vsync_reg,
      I1 => ov5640_vsync_reg1,
      I2 => ui_rst,
      I3 => start_single_burst_write_reg_n_0,
      O => \write_index[7]_i_1_n_0\
    );
\write_index[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EF000000"
    )
        port map (
      I0 => write_index_reg(7),
      I1 => \write_index[7]_i_4_n_0\,
      I2 => write_index_reg(6),
      I3 => \^axi_wvalid_reg_0\,
      I4 => M_AXI_WREADY,
      O => write_index0
    );
\write_index[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"9A"
    )
        port map (
      I0 => write_index_reg(7),
      I1 => \write_index[7]_i_4_n_0\,
      I2 => write_index_reg(6),
      O => p_0_in(7)
    );
\write_index[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => write_index_reg(4),
      I1 => write_index_reg(2),
      I2 => write_index_reg(0),
      I3 => write_index_reg(1),
      I4 => write_index_reg(3),
      I5 => write_index_reg(5),
      O => \write_index[7]_i_4_n_0\
    );
\write_index_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_index0,
      D => p_0_in(0),
      Q => write_index_reg(0),
      R => \write_index[7]_i_1_n_0\
    );
\write_index_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_index0,
      D => p_0_in(1),
      Q => write_index_reg(1),
      R => \write_index[7]_i_1_n_0\
    );
\write_index_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_index0,
      D => p_0_in(2),
      Q => write_index_reg(2),
      R => \write_index[7]_i_1_n_0\
    );
\write_index_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_index0,
      D => p_0_in(3),
      Q => write_index_reg(3),
      R => \write_index[7]_i_1_n_0\
    );
\write_index_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_index0,
      D => p_0_in(4),
      Q => write_index_reg(4),
      R => \write_index[7]_i_1_n_0\
    );
\write_index_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_index0,
      D => p_0_in(5),
      Q => write_index_reg(5),
      R => \write_index[7]_i_1_n_0\
    );
\write_index_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_index0,
      D => p_0_in(6),
      Q => write_index_reg(6),
      R => \write_index[7]_i_1_n_0\
    );
\write_index_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => write_index0,
      D => p_0_in(7),
      Q => write_index_reg(7),
      R => \write_index[7]_i_1_n_0\
    );
writes_done_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF02000000"
    )
        port map (
      I0 => write_burst_counter_reg(1),
      I1 => write_burst_counter_reg(0),
      I2 => \write_burst_counter[0]_i_3_n_0\,
      I3 => M_AXI_BVALID,
      I4 => \^m_axi_bready\,
      I5 => writes_done,
      O => writes_done_i_1_n_0
    );
writes_done_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => writes_done_i_1_n_0,
      Q => writes_done,
      R => burst_vaild1
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_video_to_aximm_1_0_video_to_aximm is
  port (
    M_AXI_BREADY : out STD_LOGIC;
    M_AXI_AWADDR : out STD_LOGIC_VECTOR ( 22 downto 0 );
    axi_awvalid_reg : out STD_LOGIC;
    axi_wvalid_reg : out STD_LOGIC;
    rd_wdfifo_en : out STD_LOGIC;
    M_AXI_RREADY : out STD_LOGIC;
    M_AXI_WLAST : out STD_LOGIC;
    M_AXI_ACLK : in STD_LOGIC;
    ov5640_vsync : in STD_LOGIC;
    ui_rst : in STD_LOGIC;
    prog_empty : in STD_LOGIC;
    M_AXI_BVALID : in STD_LOGIC;
    M_AXI_AWREADY : in STD_LOGIC;
    M_AXI_WREADY : in STD_LOGIC;
    M_AXI_RLAST : in STD_LOGIC;
    M_AXI_RVALID : in STD_LOGIC
  );
end system_video_to_aximm_1_0_video_to_aximm;

architecture STRUCTURE of system_video_to_aximm_1_0_video_to_aximm is
  signal ov5640_vsync_reg : STD_LOGIC;
  signal ov5640_vsync_reg1 : STD_LOGIC;
begin
AXI_DDR_RW_v1_0_M_AXI: entity work.system_video_to_aximm_1_0_AXI_DDR_RW_v1_0_M_AXI
     port map (
      M_AXI_ACLK => M_AXI_ACLK,
      M_AXI_RLAST => M_AXI_RLAST,
      M_AXI_RREADY => M_AXI_RREADY,
      M_AXI_RVALID => M_AXI_RVALID
    );
AXI_DDR_WD_A_v1_0_SA_AXI: entity work.system_video_to_aximm_1_0_AXI_DDR_WD_A_v1_0_SA_AXI
     port map (
      M_AXI_ACLK => M_AXI_ACLK,
      M_AXI_AWADDR(22 downto 0) => M_AXI_AWADDR(22 downto 0),
      M_AXI_AWREADY => M_AXI_AWREADY,
      M_AXI_BREADY => M_AXI_BREADY,
      M_AXI_BVALID => M_AXI_BVALID,
      M_AXI_WLAST => M_AXI_WLAST,
      M_AXI_WREADY => M_AXI_WREADY,
      axi_awvalid_reg_0 => axi_awvalid_reg,
      axi_wvalid_reg_0 => axi_wvalid_reg,
      ov5640_vsync_reg => ov5640_vsync_reg,
      ov5640_vsync_reg1 => ov5640_vsync_reg1,
      prog_empty => prog_empty,
      rd_wdfifo_en => rd_wdfifo_en,
      ui_rst => ui_rst
    );
ov5640_vsync_reg1_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => ov5640_vsync_reg,
      Q => ov5640_vsync_reg1,
      R => '0'
    );
ov5640_vsync_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => M_AXI_ACLK,
      CE => '1',
      D => ov5640_vsync,
      Q => ov5640_vsync_reg,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_video_to_aximm_1_0 is
  port (
    M_AXI_ACLK : in STD_LOGIC;
    ui_rst : in STD_LOGIC;
    ov5640_vsync : in STD_LOGIC;
    rd_wdfifo_en : out STD_LOGIC;
    fifo_data_rd : in STD_LOGIC_VECTOR ( 31 downto 0 );
    almost_empty : in STD_LOGIC;
    prog_empty : in STD_LOGIC;
    M_AXI_AWID : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_AWADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_AWLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M_AXI_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_AWVALID : out STD_LOGIC;
    M_AXI_AWLOCK : out STD_LOGIC;
    M_AXI_AWCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_AWPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_AWQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_AWUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_AWREADY : in STD_LOGIC;
    M_AXI_WDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_WLAST : out STD_LOGIC;
    M_AXI_WVALID : out STD_LOGIC;
    M_AXI_WREADY : in STD_LOGIC;
    M_AXI_WSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_BREADY : out STD_LOGIC;
    M_AXI_BID : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_BVALID : in STD_LOGIC;
    M_AXI_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_BUSER : in STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_ARADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_ARLEN : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M_AXI_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_ARVALID : out STD_LOGIC;
    M_AXI_ARREADY : in STD_LOGIC;
    M_AXI_ARID : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_ARLOCK : out STD_LOGIC;
    M_AXI_ARCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_ARPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_ARQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_ARUSER : out STD_LOGIC_VECTOR ( 0 to 0 );
    M_AXI_RREADY : out STD_LOGIC;
    M_AXI_RDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_RLAST : in STD_LOGIC;
    M_AXI_RVALID : in STD_LOGIC;
    M_AXI_RID : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_video_to_aximm_1_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_video_to_aximm_1_0 : entity is "system_video_to_aximm_1_0,video_to_aximm,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of system_video_to_aximm_1_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of system_video_to_aximm_1_0 : entity is "package_project";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of system_video_to_aximm_1_0 : entity is "video_to_aximm,Vivado 2019.2";
end system_video_to_aximm_1_0;

architecture STRUCTURE of system_video_to_aximm_1_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^m_axi_awaddr\ : STD_LOGIC_VECTOR ( 31 downto 9 );
  signal \^fifo_data_rd\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of M_AXI_ACLK : signal is "xilinx.com:signal:clock:1.0 ui_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of M_AXI_ACLK : signal is "XIL_INTERFACENAME ui_clk, ASSOCIATED_RESET ui_rst, ASSOCIATED_BUSIF M_AXI, FREQ_HZ 160000000, PHASE 0, CLK_DOMAIN system_mig_7series_0_0_ui_clk, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of M_AXI_ARLOCK : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARLOCK";
  attribute X_INTERFACE_INFO of M_AXI_ARREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARREADY";
  attribute X_INTERFACE_INFO of M_AXI_ARVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARVALID";
  attribute X_INTERFACE_INFO of M_AXI_AWLOCK : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWLOCK";
  attribute X_INTERFACE_INFO of M_AXI_AWREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWREADY";
  attribute X_INTERFACE_INFO of M_AXI_AWVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWVALID";
  attribute X_INTERFACE_INFO of M_AXI_BREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI BREADY";
  attribute X_INTERFACE_INFO of M_AXI_BVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI BVALID";
  attribute X_INTERFACE_INFO of M_AXI_RLAST : signal is "xilinx.com:interface:aximm:1.0 M_AXI RLAST";
  attribute X_INTERFACE_INFO of M_AXI_RREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI RREADY";
  attribute X_INTERFACE_INFO of M_AXI_RVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI RVALID";
  attribute X_INTERFACE_INFO of M_AXI_WLAST : signal is "xilinx.com:interface:aximm:1.0 M_AXI WLAST";
  attribute X_INTERFACE_INFO of M_AXI_WREADY : signal is "xilinx.com:interface:aximm:1.0 M_AXI WREADY";
  attribute X_INTERFACE_INFO of M_AXI_WVALID : signal is "xilinx.com:interface:aximm:1.0 M_AXI WVALID";
  attribute X_INTERFACE_INFO of ui_rst : signal is "xilinx.com:signal:reset:1.0 ui_rst RST";
  attribute X_INTERFACE_PARAMETER of ui_rst : signal is "XIL_INTERFACENAME ui_rst, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of M_AXI_ARADDR : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARADDR";
  attribute X_INTERFACE_INFO of M_AXI_ARBURST : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARBURST";
  attribute X_INTERFACE_INFO of M_AXI_ARCACHE : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARCACHE";
  attribute X_INTERFACE_INFO of M_AXI_ARID : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARID";
  attribute X_INTERFACE_INFO of M_AXI_ARLEN : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARLEN";
  attribute X_INTERFACE_INFO of M_AXI_ARPROT : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARPROT";
  attribute X_INTERFACE_INFO of M_AXI_ARQOS : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARQOS";
  attribute X_INTERFACE_INFO of M_AXI_ARSIZE : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARSIZE";
  attribute X_INTERFACE_INFO of M_AXI_ARUSER : signal is "xilinx.com:interface:aximm:1.0 M_AXI ARUSER";
  attribute X_INTERFACE_INFO of M_AXI_AWADDR : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWADDR";
  attribute X_INTERFACE_INFO of M_AXI_AWBURST : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWBURST";
  attribute X_INTERFACE_INFO of M_AXI_AWCACHE : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWCACHE";
  attribute X_INTERFACE_INFO of M_AXI_AWID : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWID";
  attribute X_INTERFACE_INFO of M_AXI_AWLEN : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWLEN";
  attribute X_INTERFACE_INFO of M_AXI_AWPROT : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWPROT";
  attribute X_INTERFACE_INFO of M_AXI_AWQOS : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWQOS";
  attribute X_INTERFACE_INFO of M_AXI_AWSIZE : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWSIZE";
  attribute X_INTERFACE_INFO of M_AXI_AWUSER : signal is "xilinx.com:interface:aximm:1.0 M_AXI AWUSER";
  attribute X_INTERFACE_INFO of M_AXI_BID : signal is "xilinx.com:interface:aximm:1.0 M_AXI BID";
  attribute X_INTERFACE_INFO of M_AXI_BRESP : signal is "xilinx.com:interface:aximm:1.0 M_AXI BRESP";
  attribute X_INTERFACE_INFO of M_AXI_BUSER : signal is "xilinx.com:interface:aximm:1.0 M_AXI BUSER";
  attribute X_INTERFACE_INFO of M_AXI_RDATA : signal is "xilinx.com:interface:aximm:1.0 M_AXI RDATA";
  attribute X_INTERFACE_INFO of M_AXI_RID : signal is "xilinx.com:interface:aximm:1.0 M_AXI RID";
  attribute X_INTERFACE_PARAMETER of M_AXI_RID : signal is "XIL_INTERFACENAME M_AXI, DATA_WIDTH 32, PROTOCOL AXI4, FREQ_HZ 160000000, ID_WIDTH 1, ADDR_WIDTH 32, AWUSER_WIDTH 1, ARUSER_WIDTH 1, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 1, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 256, PHASE 0, CLK_DOMAIN system_mig_7series_0_0_ui_clk, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of M_AXI_RRESP : signal is "xilinx.com:interface:aximm:1.0 M_AXI RRESP";
  attribute X_INTERFACE_INFO of M_AXI_WDATA : signal is "xilinx.com:interface:aximm:1.0 M_AXI WDATA";
  attribute X_INTERFACE_INFO of M_AXI_WSTRB : signal is "xilinx.com:interface:aximm:1.0 M_AXI WSTRB";
begin
  M_AXI_ARADDR(31) <= \<const0>\;
  M_AXI_ARADDR(30) <= \<const0>\;
  M_AXI_ARADDR(29) <= \<const0>\;
  M_AXI_ARADDR(28) <= \<const0>\;
  M_AXI_ARADDR(27) <= \<const0>\;
  M_AXI_ARADDR(26) <= \<const0>\;
  M_AXI_ARADDR(25) <= \<const0>\;
  M_AXI_ARADDR(24) <= \<const0>\;
  M_AXI_ARADDR(23) <= \<const0>\;
  M_AXI_ARADDR(22) <= \<const0>\;
  M_AXI_ARADDR(21) <= \<const0>\;
  M_AXI_ARADDR(20) <= \<const0>\;
  M_AXI_ARADDR(19) <= \<const0>\;
  M_AXI_ARADDR(18) <= \<const0>\;
  M_AXI_ARADDR(17) <= \<const0>\;
  M_AXI_ARADDR(16) <= \<const0>\;
  M_AXI_ARADDR(15) <= \<const0>\;
  M_AXI_ARADDR(14) <= \<const0>\;
  M_AXI_ARADDR(13) <= \<const0>\;
  M_AXI_ARADDR(12) <= \<const0>\;
  M_AXI_ARADDR(11) <= \<const0>\;
  M_AXI_ARADDR(10) <= \<const0>\;
  M_AXI_ARADDR(9) <= \<const0>\;
  M_AXI_ARADDR(8) <= \<const0>\;
  M_AXI_ARADDR(7) <= \<const0>\;
  M_AXI_ARADDR(6) <= \<const0>\;
  M_AXI_ARADDR(5) <= \<const0>\;
  M_AXI_ARADDR(4) <= \<const0>\;
  M_AXI_ARADDR(3) <= \<const0>\;
  M_AXI_ARADDR(2) <= \<const0>\;
  M_AXI_ARADDR(1) <= \<const0>\;
  M_AXI_ARADDR(0) <= \<const0>\;
  M_AXI_ARBURST(1) <= \<const0>\;
  M_AXI_ARBURST(0) <= \<const1>\;
  M_AXI_ARCACHE(3) <= \<const0>\;
  M_AXI_ARCACHE(2) <= \<const0>\;
  M_AXI_ARCACHE(1) <= \<const1>\;
  M_AXI_ARCACHE(0) <= \<const0>\;
  M_AXI_ARID(0) <= \<const0>\;
  M_AXI_ARLEN(7) <= \<const0>\;
  M_AXI_ARLEN(6) <= \<const1>\;
  M_AXI_ARLEN(5) <= \<const1>\;
  M_AXI_ARLEN(4) <= \<const1>\;
  M_AXI_ARLEN(3) <= \<const1>\;
  M_AXI_ARLEN(2) <= \<const1>\;
  M_AXI_ARLEN(1) <= \<const1>\;
  M_AXI_ARLEN(0) <= \<const1>\;
  M_AXI_ARLOCK <= \<const0>\;
  M_AXI_ARPROT(2) <= \<const0>\;
  M_AXI_ARPROT(1) <= \<const0>\;
  M_AXI_ARPROT(0) <= \<const0>\;
  M_AXI_ARQOS(3) <= \<const0>\;
  M_AXI_ARQOS(2) <= \<const0>\;
  M_AXI_ARQOS(1) <= \<const0>\;
  M_AXI_ARQOS(0) <= \<const0>\;
  M_AXI_ARSIZE(2) <= \<const0>\;
  M_AXI_ARSIZE(1) <= \<const1>\;
  M_AXI_ARSIZE(0) <= \<const0>\;
  M_AXI_ARUSER(0) <= \<const1>\;
  M_AXI_ARVALID <= \<const0>\;
  M_AXI_AWADDR(31 downto 9) <= \^m_axi_awaddr\(31 downto 9);
  M_AXI_AWADDR(8) <= \<const0>\;
  M_AXI_AWADDR(7) <= \<const0>\;
  M_AXI_AWADDR(6) <= \<const0>\;
  M_AXI_AWADDR(5) <= \<const0>\;
  M_AXI_AWADDR(4) <= \<const0>\;
  M_AXI_AWADDR(3) <= \<const0>\;
  M_AXI_AWADDR(2) <= \<const0>\;
  M_AXI_AWADDR(1) <= \<const0>\;
  M_AXI_AWADDR(0) <= \<const0>\;
  M_AXI_AWBURST(1) <= \<const0>\;
  M_AXI_AWBURST(0) <= \<const1>\;
  M_AXI_AWCACHE(3) <= \<const0>\;
  M_AXI_AWCACHE(2) <= \<const0>\;
  M_AXI_AWCACHE(1) <= \<const1>\;
  M_AXI_AWCACHE(0) <= \<const0>\;
  M_AXI_AWID(0) <= \<const0>\;
  M_AXI_AWLEN(7) <= \<const0>\;
  M_AXI_AWLEN(6) <= \<const1>\;
  M_AXI_AWLEN(5) <= \<const1>\;
  M_AXI_AWLEN(4) <= \<const1>\;
  M_AXI_AWLEN(3) <= \<const1>\;
  M_AXI_AWLEN(2) <= \<const1>\;
  M_AXI_AWLEN(1) <= \<const1>\;
  M_AXI_AWLEN(0) <= \<const1>\;
  M_AXI_AWLOCK <= \<const0>\;
  M_AXI_AWPROT(2) <= \<const0>\;
  M_AXI_AWPROT(1) <= \<const0>\;
  M_AXI_AWPROT(0) <= \<const0>\;
  M_AXI_AWQOS(3) <= \<const0>\;
  M_AXI_AWQOS(2) <= \<const0>\;
  M_AXI_AWQOS(1) <= \<const0>\;
  M_AXI_AWQOS(0) <= \<const0>\;
  M_AXI_AWSIZE(2) <= \<const0>\;
  M_AXI_AWSIZE(1) <= \<const1>\;
  M_AXI_AWSIZE(0) <= \<const0>\;
  M_AXI_AWUSER(0) <= \<const1>\;
  M_AXI_WDATA(31 downto 0) <= \^fifo_data_rd\(31 downto 0);
  M_AXI_WSTRB(3) <= \<const1>\;
  M_AXI_WSTRB(2) <= \<const1>\;
  M_AXI_WSTRB(1) <= \<const1>\;
  M_AXI_WSTRB(0) <= \<const1>\;
  \^fifo_data_rd\(31 downto 0) <= fifo_data_rd(31 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.system_video_to_aximm_1_0_video_to_aximm
     port map (
      M_AXI_ACLK => M_AXI_ACLK,
      M_AXI_AWADDR(22 downto 0) => \^m_axi_awaddr\(31 downto 9),
      M_AXI_AWREADY => M_AXI_AWREADY,
      M_AXI_BREADY => M_AXI_BREADY,
      M_AXI_BVALID => M_AXI_BVALID,
      M_AXI_RLAST => M_AXI_RLAST,
      M_AXI_RREADY => M_AXI_RREADY,
      M_AXI_RVALID => M_AXI_RVALID,
      M_AXI_WLAST => M_AXI_WLAST,
      M_AXI_WREADY => M_AXI_WREADY,
      axi_awvalid_reg => M_AXI_AWVALID,
      axi_wvalid_reg => M_AXI_WVALID,
      ov5640_vsync => ov5640_vsync,
      prog_empty => prog_empty,
      rd_wdfifo_en => rd_wdfifo_en,
      ui_rst => ui_rst
    );
end STRUCTURE;
