// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Tue Dec 26 10:18:08 2023
// Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               E:/Workspace/Vivado/2019.2/pcie_xdma_v1/vivado_prj/pcie_xdma.srcs/sources_1/bd/system/ip/system_hdmi_to_aximm_0_0/system_hdmi_to_aximm_0_0_sim_netlist.v
// Design      : system_hdmi_to_aximm_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "system_hdmi_to_aximm_0_0,hdmi_to_aximm,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "hdmi_to_aximm,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module system_hdmi_to_aximm_0_0
   (M_AXI_ACLK,
    ui_rst,
    hdmi_clk,
    hdmi_vsync,
    fifo_rst,
    rd_wdfifo_en,
    fifo_data_rd,
    almost_empty,
    prog_empty,
    M_AXI_AWID,
    M_AXI_AWADDR,
    M_AXI_AWLEN,
    M_AXI_AWSIZE,
    M_AXI_AWBURST,
    M_AXI_AWVALID,
    M_AXI_AWLOCK,
    M_AXI_AWCACHE,
    M_AXI_AWPROT,
    M_AXI_AWQOS,
    M_AXI_AWUSER,
    M_AXI_AWREADY,
    M_AXI_WDATA,
    M_AXI_WLAST,
    M_AXI_WVALID,
    M_AXI_WREADY,
    M_AXI_WSTRB,
    M_AXI_BREADY,
    M_AXI_BID,
    M_AXI_BVALID,
    M_AXI_BRESP,
    M_AXI_BUSER,
    M_AXI_ARADDR,
    M_AXI_ARLEN,
    M_AXI_ARSIZE,
    M_AXI_ARBURST,
    M_AXI_ARVALID,
    M_AXI_ARREADY,
    M_AXI_ARID,
    M_AXI_ARLOCK,
    M_AXI_ARCACHE,
    M_AXI_ARPROT,
    M_AXI_ARQOS,
    M_AXI_ARUSER,
    M_AXI_RREADY,
    M_AXI_RDATA,
    M_AXI_RRESP,
    M_AXI_RLAST,
    M_AXI_RVALID,
    M_AXI_RID);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 M_AXI_ACLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M_AXI_ACLK, ASSOCIATED_BUSIF M_AXI, FREQ_HZ 160000000, PHASE 0, CLK_DOMAIN system_mig_7series_0_0_ui_clk, INSERT_VIP 0" *) input M_AXI_ACLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 ui_rst RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ui_rst, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input ui_rst;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 hdmi_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME hdmi_clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_hdmi_ctrl_0_0_hdmi_out_clk, INSERT_VIP 0" *) input hdmi_clk;
  input hdmi_vsync;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 fifo_rst RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME fifo_rst, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) output fifo_rst;
  output rd_wdfifo_en;
  input [31:0]fifo_data_rd;
  input almost_empty;
  input prog_empty;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWID" *) output [0:0]M_AXI_AWID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWADDR" *) output [31:0]M_AXI_AWADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWLEN" *) output [7:0]M_AXI_AWLEN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWSIZE" *) output [2:0]M_AXI_AWSIZE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWBURST" *) output [1:0]M_AXI_AWBURST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWVALID" *) output M_AXI_AWVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWLOCK" *) output M_AXI_AWLOCK;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWCACHE" *) output [3:0]M_AXI_AWCACHE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWPROT" *) output [2:0]M_AXI_AWPROT;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWQOS" *) output [3:0]M_AXI_AWQOS;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWUSER" *) output [0:0]M_AXI_AWUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI AWREADY" *) input M_AXI_AWREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WDATA" *) output [31:0]M_AXI_WDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WLAST" *) output M_AXI_WLAST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WVALID" *) output M_AXI_WVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WREADY" *) input M_AXI_WREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI WSTRB" *) output [3:0]M_AXI_WSTRB;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BREADY" *) output M_AXI_BREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BID" *) input [0:0]M_AXI_BID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BVALID" *) input M_AXI_BVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BRESP" *) input [1:0]M_AXI_BRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI BUSER" *) input [0:0]M_AXI_BUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARADDR" *) output [31:0]M_AXI_ARADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARLEN" *) output [7:0]M_AXI_ARLEN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARSIZE" *) output [2:0]M_AXI_ARSIZE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARBURST" *) output [1:0]M_AXI_ARBURST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARVALID" *) output M_AXI_ARVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARREADY" *) input M_AXI_ARREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARID" *) output [0:0]M_AXI_ARID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARLOCK" *) output M_AXI_ARLOCK;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARCACHE" *) output [3:0]M_AXI_ARCACHE;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARPROT" *) output [2:0]M_AXI_ARPROT;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARQOS" *) output [3:0]M_AXI_ARQOS;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI ARUSER" *) output [0:0]M_AXI_ARUSER;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RREADY" *) output M_AXI_RREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RDATA" *) input [31:0]M_AXI_RDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RRESP" *) input [1:0]M_AXI_RRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RLAST" *) input M_AXI_RLAST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RVALID" *) input M_AXI_RVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 M_AXI RID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M_AXI, DATA_WIDTH 32, PROTOCOL AXI4, FREQ_HZ 160000000, ID_WIDTH 1, ADDR_WIDTH 32, AWUSER_WIDTH 1, ARUSER_WIDTH 1, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 1, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 1, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 1, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 256, PHASE 0, CLK_DOMAIN system_mig_7series_0_0_ui_clk, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [0:0]M_AXI_RID;

  wire \<const0> ;
  wire \<const1> ;
  wire M_AXI_ACLK;
  wire [31:9]\^M_AXI_AWADDR ;
  wire M_AXI_AWREADY;
  wire M_AXI_AWVALID;
  wire M_AXI_BREADY;
  wire M_AXI_BVALID;
  wire M_AXI_RLAST;
  wire M_AXI_RREADY;
  wire M_AXI_RVALID;
  wire M_AXI_WLAST;
  wire M_AXI_WREADY;
  wire M_AXI_WVALID;
  wire almost_empty;
  wire [31:0]fifo_data_rd;
  wire fifo_rst;
  wire hdmi_clk;
  wire hdmi_vsync;
  wire prog_empty;
  wire rd_wdfifo_en;
  wire ui_rst;

  assign M_AXI_ARADDR[31] = \<const0> ;
  assign M_AXI_ARADDR[30] = \<const0> ;
  assign M_AXI_ARADDR[29] = \<const0> ;
  assign M_AXI_ARADDR[28] = \<const0> ;
  assign M_AXI_ARADDR[27] = \<const0> ;
  assign M_AXI_ARADDR[26] = \<const0> ;
  assign M_AXI_ARADDR[25] = \<const0> ;
  assign M_AXI_ARADDR[24] = \<const0> ;
  assign M_AXI_ARADDR[23] = \<const0> ;
  assign M_AXI_ARADDR[22] = \<const0> ;
  assign M_AXI_ARADDR[21] = \<const1> ;
  assign M_AXI_ARADDR[20] = \<const0> ;
  assign M_AXI_ARADDR[19] = \<const0> ;
  assign M_AXI_ARADDR[18] = \<const0> ;
  assign M_AXI_ARADDR[17] = \<const0> ;
  assign M_AXI_ARADDR[16] = \<const0> ;
  assign M_AXI_ARADDR[15] = \<const0> ;
  assign M_AXI_ARADDR[14] = \<const0> ;
  assign M_AXI_ARADDR[13] = \<const0> ;
  assign M_AXI_ARADDR[12] = \<const0> ;
  assign M_AXI_ARADDR[11] = \<const0> ;
  assign M_AXI_ARADDR[10] = \<const0> ;
  assign M_AXI_ARADDR[9] = \<const0> ;
  assign M_AXI_ARADDR[8] = \<const0> ;
  assign M_AXI_ARADDR[7] = \<const0> ;
  assign M_AXI_ARADDR[6] = \<const0> ;
  assign M_AXI_ARADDR[5] = \<const0> ;
  assign M_AXI_ARADDR[4] = \<const0> ;
  assign M_AXI_ARADDR[3] = \<const0> ;
  assign M_AXI_ARADDR[2] = \<const0> ;
  assign M_AXI_ARADDR[1] = \<const0> ;
  assign M_AXI_ARADDR[0] = \<const0> ;
  assign M_AXI_ARBURST[1] = \<const0> ;
  assign M_AXI_ARBURST[0] = \<const1> ;
  assign M_AXI_ARCACHE[3] = \<const0> ;
  assign M_AXI_ARCACHE[2] = \<const0> ;
  assign M_AXI_ARCACHE[1] = \<const1> ;
  assign M_AXI_ARCACHE[0] = \<const0> ;
  assign M_AXI_ARID[0] = \<const0> ;
  assign M_AXI_ARLEN[7] = \<const0> ;
  assign M_AXI_ARLEN[6] = \<const1> ;
  assign M_AXI_ARLEN[5] = \<const1> ;
  assign M_AXI_ARLEN[4] = \<const1> ;
  assign M_AXI_ARLEN[3] = \<const1> ;
  assign M_AXI_ARLEN[2] = \<const1> ;
  assign M_AXI_ARLEN[1] = \<const1> ;
  assign M_AXI_ARLEN[0] = \<const1> ;
  assign M_AXI_ARLOCK = \<const0> ;
  assign M_AXI_ARPROT[2] = \<const0> ;
  assign M_AXI_ARPROT[1] = \<const0> ;
  assign M_AXI_ARPROT[0] = \<const0> ;
  assign M_AXI_ARQOS[3] = \<const0> ;
  assign M_AXI_ARQOS[2] = \<const0> ;
  assign M_AXI_ARQOS[1] = \<const0> ;
  assign M_AXI_ARQOS[0] = \<const0> ;
  assign M_AXI_ARSIZE[2] = \<const0> ;
  assign M_AXI_ARSIZE[1] = \<const1> ;
  assign M_AXI_ARSIZE[0] = \<const0> ;
  assign M_AXI_ARUSER[0] = \<const1> ;
  assign M_AXI_ARVALID = \<const0> ;
  assign M_AXI_AWADDR[31:9] = \^M_AXI_AWADDR [31:9];
  assign M_AXI_AWADDR[8] = \<const0> ;
  assign M_AXI_AWADDR[7] = \<const0> ;
  assign M_AXI_AWADDR[6] = \<const0> ;
  assign M_AXI_AWADDR[5] = \<const0> ;
  assign M_AXI_AWADDR[4] = \<const0> ;
  assign M_AXI_AWADDR[3] = \<const0> ;
  assign M_AXI_AWADDR[2] = \<const0> ;
  assign M_AXI_AWADDR[1] = \<const0> ;
  assign M_AXI_AWADDR[0] = \<const0> ;
  assign M_AXI_AWBURST[1] = \<const0> ;
  assign M_AXI_AWBURST[0] = \<const1> ;
  assign M_AXI_AWCACHE[3] = \<const0> ;
  assign M_AXI_AWCACHE[2] = \<const0> ;
  assign M_AXI_AWCACHE[1] = \<const1> ;
  assign M_AXI_AWCACHE[0] = \<const0> ;
  assign M_AXI_AWID[0] = \<const0> ;
  assign M_AXI_AWLEN[7] = \<const0> ;
  assign M_AXI_AWLEN[6] = \<const1> ;
  assign M_AXI_AWLEN[5] = \<const1> ;
  assign M_AXI_AWLEN[4] = \<const1> ;
  assign M_AXI_AWLEN[3] = \<const1> ;
  assign M_AXI_AWLEN[2] = \<const1> ;
  assign M_AXI_AWLEN[1] = \<const1> ;
  assign M_AXI_AWLEN[0] = \<const1> ;
  assign M_AXI_AWLOCK = \<const0> ;
  assign M_AXI_AWPROT[2] = \<const0> ;
  assign M_AXI_AWPROT[1] = \<const0> ;
  assign M_AXI_AWPROT[0] = \<const0> ;
  assign M_AXI_AWQOS[3] = \<const0> ;
  assign M_AXI_AWQOS[2] = \<const0> ;
  assign M_AXI_AWQOS[1] = \<const0> ;
  assign M_AXI_AWQOS[0] = \<const0> ;
  assign M_AXI_AWSIZE[2] = \<const0> ;
  assign M_AXI_AWSIZE[1] = \<const1> ;
  assign M_AXI_AWSIZE[0] = \<const0> ;
  assign M_AXI_AWUSER[0] = \<const1> ;
  assign M_AXI_WDATA[31:0] = fifo_data_rd;
  assign M_AXI_WSTRB[3] = \<const1> ;
  assign M_AXI_WSTRB[2] = \<const1> ;
  assign M_AXI_WSTRB[1] = \<const1> ;
  assign M_AXI_WSTRB[0] = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  system_hdmi_to_aximm_0_0_hdmi_to_aximm inst
       (.M_AXI_ACLK(M_AXI_ACLK),
        .M_AXI_AWADDR(\^M_AXI_AWADDR ),
        .M_AXI_AWREADY(M_AXI_AWREADY),
        .M_AXI_BREADY(M_AXI_BREADY),
        .M_AXI_BVALID(M_AXI_BVALID),
        .M_AXI_RLAST(M_AXI_RLAST),
        .M_AXI_RREADY(M_AXI_RREADY),
        .M_AXI_RVALID(M_AXI_RVALID),
        .M_AXI_WLAST(M_AXI_WLAST),
        .M_AXI_WREADY(M_AXI_WREADY),
        .almost_empty(almost_empty),
        .axi_awvalid_reg(M_AXI_AWVALID),
        .axi_wvalid_reg(M_AXI_WVALID),
        .fifo_rst_reg_reg_0(fifo_rst),
        .hdmi_clk(hdmi_clk),
        .hdmi_vsync(hdmi_vsync),
        .prog_empty(prog_empty),
        .rd_wdfifo_en(rd_wdfifo_en),
        .ui_rst(ui_rst));
endmodule

(* ORIG_REF_NAME = "AXI_DDR_RW_HDMI" *) 
module system_hdmi_to_aximm_0_0_AXI_DDR_RW_HDMI
   (M_AXI_RREADY,
    M_AXI_ACLK,
    M_AXI_RLAST,
    M_AXI_RVALID);
  output M_AXI_RREADY;
  input M_AXI_ACLK;
  input M_AXI_RLAST;
  input M_AXI_RVALID;

  wire M_AXI_ACLK;
  wire M_AXI_RLAST;
  wire M_AXI_RREADY;
  wire M_AXI_RVALID;
  wire axi_rready_i_1_n_0;

  LUT3 #(
    .INIT(8'h7C)) 
    axi_rready_i_1
       (.I0(M_AXI_RLAST),
        .I1(M_AXI_RREADY),
        .I2(M_AXI_RVALID),
        .O(axi_rready_i_1_n_0));
  FDRE axi_rready_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_rready_i_1_n_0),
        .Q(M_AXI_RREADY),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "AXI_DDR_WD_HDMI" *) 
module system_hdmi_to_aximm_0_0_AXI_DDR_WD_HDMI
   (M_AXI_BREADY,
    M_AXI_AWADDR,
    axi_awvalid_reg_0,
    axi_wvalid_reg_0,
    M_AXI_WLAST,
    rd_wdfifo_en,
    M_AXI_ACLK,
    ui_rst,
    fifo_rst_reg1,
    fifo_rst_reg2,
    almost_empty,
    prog_empty,
    M_AXI_BVALID,
    M_AXI_AWREADY,
    M_AXI_WREADY);
  output M_AXI_BREADY;
  output [22:0]M_AXI_AWADDR;
  output axi_awvalid_reg_0;
  output axi_wvalid_reg_0;
  output M_AXI_WLAST;
  output rd_wdfifo_en;
  input M_AXI_ACLK;
  input ui_rst;
  input fifo_rst_reg1;
  input fifo_rst_reg2;
  input almost_empty;
  input prog_empty;
  input M_AXI_BVALID;
  input M_AXI_AWREADY;
  input M_AXI_WREADY;

  wire FSM_sequential_mst_exec_state_i_1_n_0;
  wire M_AXI_ACLK;
  wire [22:0]M_AXI_AWADDR;
  wire M_AXI_AWADDR_carry__0_n_0;
  wire M_AXI_AWADDR_carry__0_n_1;
  wire M_AXI_AWADDR_carry__0_n_2;
  wire M_AXI_AWADDR_carry__0_n_3;
  wire M_AXI_AWADDR_carry__1_n_1;
  wire M_AXI_AWADDR_carry__1_n_2;
  wire M_AXI_AWADDR_carry__1_n_3;
  wire M_AXI_AWADDR_carry_i_1_n_0;
  wire M_AXI_AWADDR_carry_n_0;
  wire M_AXI_AWADDR_carry_n_1;
  wire M_AXI_AWADDR_carry_n_2;
  wire M_AXI_AWADDR_carry_n_3;
  wire M_AXI_AWREADY;
  wire M_AXI_BREADY;
  wire M_AXI_BVALID;
  wire M_AXI_WLAST;
  wire M_AXI_WREADY;
  wire almost_empty;
  wire \axi_awaddr[12]_i_2_n_0 ;
  wire [31:20]axi_awaddr_reg;
  wire \axi_awaddr_reg[12]_i_1_n_0 ;
  wire \axi_awaddr_reg[12]_i_1_n_1 ;
  wire \axi_awaddr_reg[12]_i_1_n_2 ;
  wire \axi_awaddr_reg[12]_i_1_n_3 ;
  wire \axi_awaddr_reg[12]_i_1_n_4 ;
  wire \axi_awaddr_reg[12]_i_1_n_5 ;
  wire \axi_awaddr_reg[12]_i_1_n_6 ;
  wire \axi_awaddr_reg[12]_i_1_n_7 ;
  wire \axi_awaddr_reg[16]_i_1_n_0 ;
  wire \axi_awaddr_reg[16]_i_1_n_1 ;
  wire \axi_awaddr_reg[16]_i_1_n_2 ;
  wire \axi_awaddr_reg[16]_i_1_n_3 ;
  wire \axi_awaddr_reg[16]_i_1_n_4 ;
  wire \axi_awaddr_reg[16]_i_1_n_5 ;
  wire \axi_awaddr_reg[16]_i_1_n_6 ;
  wire \axi_awaddr_reg[16]_i_1_n_7 ;
  wire \axi_awaddr_reg[19]_i_3_n_0 ;
  wire \axi_awaddr_reg[19]_i_3_n_1 ;
  wire \axi_awaddr_reg[19]_i_3_n_2 ;
  wire \axi_awaddr_reg[19]_i_3_n_3 ;
  wire \axi_awaddr_reg[19]_i_3_n_4 ;
  wire \axi_awaddr_reg[19]_i_3_n_5 ;
  wire \axi_awaddr_reg[19]_i_3_n_6 ;
  wire \axi_awaddr_reg[19]_i_3_n_7 ;
  wire \axi_awaddr_reg[21]_i_1_n_0 ;
  wire \axi_awaddr_reg[21]_i_1_n_1 ;
  wire \axi_awaddr_reg[21]_i_1_n_2 ;
  wire \axi_awaddr_reg[21]_i_1_n_3 ;
  wire \axi_awaddr_reg[21]_i_1_n_4 ;
  wire \axi_awaddr_reg[21]_i_1_n_5 ;
  wire \axi_awaddr_reg[21]_i_1_n_6 ;
  wire \axi_awaddr_reg[21]_i_1_n_7 ;
  wire \axi_awaddr_reg[25]_i_1_n_0 ;
  wire \axi_awaddr_reg[25]_i_1_n_1 ;
  wire \axi_awaddr_reg[25]_i_1_n_2 ;
  wire \axi_awaddr_reg[25]_i_1_n_3 ;
  wire \axi_awaddr_reg[25]_i_1_n_4 ;
  wire \axi_awaddr_reg[25]_i_1_n_5 ;
  wire \axi_awaddr_reg[25]_i_1_n_6 ;
  wire \axi_awaddr_reg[25]_i_1_n_7 ;
  wire \axi_awaddr_reg[29]_i_1_n_2 ;
  wire \axi_awaddr_reg[29]_i_1_n_3 ;
  wire \axi_awaddr_reg[29]_i_1_n_5 ;
  wire \axi_awaddr_reg[29]_i_1_n_6 ;
  wire \axi_awaddr_reg[29]_i_1_n_7 ;
  wire axi_awvalid0;
  wire axi_awvalid_i_1_n_0;
  wire axi_awvalid_reg_0;
  wire axi_bready0;
  wire axi_wlast_i_1_n_0;
  wire axi_wlast_i_2_n_0;
  wire axi_wvalid_i_1_n_0;
  wire axi_wvalid_reg_0;
  wire burst_vaild;
  wire burst_vaild1;
  wire burst_vaild_i_1_n_0;
  wire burst_write_active;
  wire burst_write_active_i_1_n_0;
  wire cnt_cp;
  wire \cnt_cp[0]_i_3_n_0 ;
  wire \cnt_cp[0]_i_4_n_0 ;
  wire \cnt_cp[0]_i_5_n_0 ;
  wire \cnt_cp[0]_i_6_n_0 ;
  wire \cnt_cp[0]_i_7_n_0 ;
  wire \cnt_cp[12]_i_2_n_0 ;
  wire \cnt_cp[12]_i_3_n_0 ;
  wire \cnt_cp[12]_i_4_n_0 ;
  wire \cnt_cp[12]_i_5_n_0 ;
  wire \cnt_cp[16]_i_2_n_0 ;
  wire \cnt_cp[16]_i_3_n_0 ;
  wire \cnt_cp[16]_i_4_n_0 ;
  wire \cnt_cp[16]_i_5_n_0 ;
  wire \cnt_cp[20]_i_2_n_0 ;
  wire \cnt_cp[20]_i_3_n_0 ;
  wire \cnt_cp[20]_i_4_n_0 ;
  wire \cnt_cp[20]_i_5_n_0 ;
  wire \cnt_cp[24]_i_2_n_0 ;
  wire \cnt_cp[24]_i_3_n_0 ;
  wire \cnt_cp[24]_i_4_n_0 ;
  wire \cnt_cp[24]_i_5_n_0 ;
  wire \cnt_cp[28]_i_2_n_0 ;
  wire \cnt_cp[28]_i_3_n_0 ;
  wire \cnt_cp[28]_i_4_n_0 ;
  wire \cnt_cp[28]_i_5_n_0 ;
  wire \cnt_cp[4]_i_2_n_0 ;
  wire \cnt_cp[4]_i_3_n_0 ;
  wire \cnt_cp[4]_i_4_n_0 ;
  wire \cnt_cp[4]_i_5_n_0 ;
  wire \cnt_cp[8]_i_2_n_0 ;
  wire \cnt_cp[8]_i_3_n_0 ;
  wire \cnt_cp[8]_i_4_n_0 ;
  wire \cnt_cp[8]_i_5_n_0 ;
  wire \cnt_cp_reg[0]_i_2_n_0 ;
  wire \cnt_cp_reg[0]_i_2_n_1 ;
  wire \cnt_cp_reg[0]_i_2_n_2 ;
  wire \cnt_cp_reg[0]_i_2_n_3 ;
  wire \cnt_cp_reg[0]_i_2_n_4 ;
  wire \cnt_cp_reg[0]_i_2_n_5 ;
  wire \cnt_cp_reg[0]_i_2_n_6 ;
  wire \cnt_cp_reg[0]_i_2_n_7 ;
  wire \cnt_cp_reg[12]_i_1_n_0 ;
  wire \cnt_cp_reg[12]_i_1_n_1 ;
  wire \cnt_cp_reg[12]_i_1_n_2 ;
  wire \cnt_cp_reg[12]_i_1_n_3 ;
  wire \cnt_cp_reg[12]_i_1_n_4 ;
  wire \cnt_cp_reg[12]_i_1_n_5 ;
  wire \cnt_cp_reg[12]_i_1_n_6 ;
  wire \cnt_cp_reg[12]_i_1_n_7 ;
  wire \cnt_cp_reg[16]_i_1_n_0 ;
  wire \cnt_cp_reg[16]_i_1_n_1 ;
  wire \cnt_cp_reg[16]_i_1_n_2 ;
  wire \cnt_cp_reg[16]_i_1_n_3 ;
  wire \cnt_cp_reg[16]_i_1_n_4 ;
  wire \cnt_cp_reg[16]_i_1_n_5 ;
  wire \cnt_cp_reg[16]_i_1_n_6 ;
  wire \cnt_cp_reg[16]_i_1_n_7 ;
  wire \cnt_cp_reg[20]_i_1_n_0 ;
  wire \cnt_cp_reg[20]_i_1_n_1 ;
  wire \cnt_cp_reg[20]_i_1_n_2 ;
  wire \cnt_cp_reg[20]_i_1_n_3 ;
  wire \cnt_cp_reg[20]_i_1_n_4 ;
  wire \cnt_cp_reg[20]_i_1_n_5 ;
  wire \cnt_cp_reg[20]_i_1_n_6 ;
  wire \cnt_cp_reg[20]_i_1_n_7 ;
  wire \cnt_cp_reg[24]_i_1_n_0 ;
  wire \cnt_cp_reg[24]_i_1_n_1 ;
  wire \cnt_cp_reg[24]_i_1_n_2 ;
  wire \cnt_cp_reg[24]_i_1_n_3 ;
  wire \cnt_cp_reg[24]_i_1_n_4 ;
  wire \cnt_cp_reg[24]_i_1_n_5 ;
  wire \cnt_cp_reg[24]_i_1_n_6 ;
  wire \cnt_cp_reg[24]_i_1_n_7 ;
  wire \cnt_cp_reg[28]_i_1_n_1 ;
  wire \cnt_cp_reg[28]_i_1_n_2 ;
  wire \cnt_cp_reg[28]_i_1_n_3 ;
  wire \cnt_cp_reg[28]_i_1_n_4 ;
  wire \cnt_cp_reg[28]_i_1_n_5 ;
  wire \cnt_cp_reg[28]_i_1_n_6 ;
  wire \cnt_cp_reg[28]_i_1_n_7 ;
  wire \cnt_cp_reg[4]_i_1_n_0 ;
  wire \cnt_cp_reg[4]_i_1_n_1 ;
  wire \cnt_cp_reg[4]_i_1_n_2 ;
  wire \cnt_cp_reg[4]_i_1_n_3 ;
  wire \cnt_cp_reg[4]_i_1_n_4 ;
  wire \cnt_cp_reg[4]_i_1_n_5 ;
  wire \cnt_cp_reg[4]_i_1_n_6 ;
  wire \cnt_cp_reg[4]_i_1_n_7 ;
  wire \cnt_cp_reg[8]_i_1_n_0 ;
  wire \cnt_cp_reg[8]_i_1_n_1 ;
  wire \cnt_cp_reg[8]_i_1_n_2 ;
  wire \cnt_cp_reg[8]_i_1_n_3 ;
  wire \cnt_cp_reg[8]_i_1_n_4 ;
  wire \cnt_cp_reg[8]_i_1_n_5 ;
  wire \cnt_cp_reg[8]_i_1_n_6 ;
  wire \cnt_cp_reg[8]_i_1_n_7 ;
  wire \cnt_cp_reg_n_0_[0] ;
  wire \cnt_cp_reg_n_0_[10] ;
  wire \cnt_cp_reg_n_0_[11] ;
  wire \cnt_cp_reg_n_0_[12] ;
  wire \cnt_cp_reg_n_0_[13] ;
  wire \cnt_cp_reg_n_0_[14] ;
  wire \cnt_cp_reg_n_0_[15] ;
  wire \cnt_cp_reg_n_0_[16] ;
  wire \cnt_cp_reg_n_0_[17] ;
  wire \cnt_cp_reg_n_0_[18] ;
  wire \cnt_cp_reg_n_0_[19] ;
  wire \cnt_cp_reg_n_0_[1] ;
  wire \cnt_cp_reg_n_0_[20] ;
  wire \cnt_cp_reg_n_0_[21] ;
  wire \cnt_cp_reg_n_0_[22] ;
  wire \cnt_cp_reg_n_0_[23] ;
  wire \cnt_cp_reg_n_0_[24] ;
  wire \cnt_cp_reg_n_0_[25] ;
  wire \cnt_cp_reg_n_0_[26] ;
  wire \cnt_cp_reg_n_0_[27] ;
  wire \cnt_cp_reg_n_0_[28] ;
  wire \cnt_cp_reg_n_0_[29] ;
  wire \cnt_cp_reg_n_0_[2] ;
  wire \cnt_cp_reg_n_0_[30] ;
  wire \cnt_cp_reg_n_0_[31] ;
  wire \cnt_cp_reg_n_0_[3] ;
  wire \cnt_cp_reg_n_0_[4] ;
  wire \cnt_cp_reg_n_0_[5] ;
  wire \cnt_cp_reg_n_0_[6] ;
  wire \cnt_cp_reg_n_0_[7] ;
  wire \cnt_cp_reg_n_0_[8] ;
  wire \cnt_cp_reg_n_0_[9] ;
  wire fifo_rst_reg1;
  wire fifo_rst_reg2;
  wire mst_exec_state;
  wire [7:0]p_0_in;
  wire prog_empty;
  wire rd_wdfifo_en;
  wire start_single_burst_write_i_10_n_0;
  wire start_single_burst_write_i_11_n_0;
  wire start_single_burst_write_i_1_n_0;
  wire start_single_burst_write_i_2_n_0;
  wire start_single_burst_write_i_3_n_0;
  wire start_single_burst_write_i_4_n_0;
  wire start_single_burst_write_i_5_n_0;
  wire start_single_burst_write_i_6_n_0;
  wire start_single_burst_write_i_7_n_0;
  wire start_single_burst_write_i_8_n_0;
  wire start_single_burst_write_i_9_n_0;
  wire start_single_burst_write_reg_n_0;
  wire ui_rst;
  wire write_burst_counter;
  wire \write_burst_counter[0]_i_10_n_0 ;
  wire \write_burst_counter[0]_i_11_n_0 ;
  wire \write_burst_counter[0]_i_3_n_0 ;
  wire \write_burst_counter[0]_i_4_n_0 ;
  wire \write_burst_counter[0]_i_5_n_0 ;
  wire \write_burst_counter[0]_i_6_n_0 ;
  wire \write_burst_counter[0]_i_7_n_0 ;
  wire \write_burst_counter[0]_i_8_n_0 ;
  wire \write_burst_counter[0]_i_9_n_0 ;
  wire [31:0]write_burst_counter_reg;
  wire \write_burst_counter_reg[0]_i_2_n_0 ;
  wire \write_burst_counter_reg[0]_i_2_n_1 ;
  wire \write_burst_counter_reg[0]_i_2_n_2 ;
  wire \write_burst_counter_reg[0]_i_2_n_3 ;
  wire \write_burst_counter_reg[0]_i_2_n_4 ;
  wire \write_burst_counter_reg[0]_i_2_n_5 ;
  wire \write_burst_counter_reg[0]_i_2_n_6 ;
  wire \write_burst_counter_reg[0]_i_2_n_7 ;
  wire \write_burst_counter_reg[12]_i_1_n_0 ;
  wire \write_burst_counter_reg[12]_i_1_n_1 ;
  wire \write_burst_counter_reg[12]_i_1_n_2 ;
  wire \write_burst_counter_reg[12]_i_1_n_3 ;
  wire \write_burst_counter_reg[12]_i_1_n_4 ;
  wire \write_burst_counter_reg[12]_i_1_n_5 ;
  wire \write_burst_counter_reg[12]_i_1_n_6 ;
  wire \write_burst_counter_reg[12]_i_1_n_7 ;
  wire \write_burst_counter_reg[16]_i_1_n_0 ;
  wire \write_burst_counter_reg[16]_i_1_n_1 ;
  wire \write_burst_counter_reg[16]_i_1_n_2 ;
  wire \write_burst_counter_reg[16]_i_1_n_3 ;
  wire \write_burst_counter_reg[16]_i_1_n_4 ;
  wire \write_burst_counter_reg[16]_i_1_n_5 ;
  wire \write_burst_counter_reg[16]_i_1_n_6 ;
  wire \write_burst_counter_reg[16]_i_1_n_7 ;
  wire \write_burst_counter_reg[20]_i_1_n_0 ;
  wire \write_burst_counter_reg[20]_i_1_n_1 ;
  wire \write_burst_counter_reg[20]_i_1_n_2 ;
  wire \write_burst_counter_reg[20]_i_1_n_3 ;
  wire \write_burst_counter_reg[20]_i_1_n_4 ;
  wire \write_burst_counter_reg[20]_i_1_n_5 ;
  wire \write_burst_counter_reg[20]_i_1_n_6 ;
  wire \write_burst_counter_reg[20]_i_1_n_7 ;
  wire \write_burst_counter_reg[24]_i_1_n_0 ;
  wire \write_burst_counter_reg[24]_i_1_n_1 ;
  wire \write_burst_counter_reg[24]_i_1_n_2 ;
  wire \write_burst_counter_reg[24]_i_1_n_3 ;
  wire \write_burst_counter_reg[24]_i_1_n_4 ;
  wire \write_burst_counter_reg[24]_i_1_n_5 ;
  wire \write_burst_counter_reg[24]_i_1_n_6 ;
  wire \write_burst_counter_reg[24]_i_1_n_7 ;
  wire \write_burst_counter_reg[28]_i_1_n_1 ;
  wire \write_burst_counter_reg[28]_i_1_n_2 ;
  wire \write_burst_counter_reg[28]_i_1_n_3 ;
  wire \write_burst_counter_reg[28]_i_1_n_4 ;
  wire \write_burst_counter_reg[28]_i_1_n_5 ;
  wire \write_burst_counter_reg[28]_i_1_n_6 ;
  wire \write_burst_counter_reg[28]_i_1_n_7 ;
  wire \write_burst_counter_reg[4]_i_1_n_0 ;
  wire \write_burst_counter_reg[4]_i_1_n_1 ;
  wire \write_burst_counter_reg[4]_i_1_n_2 ;
  wire \write_burst_counter_reg[4]_i_1_n_3 ;
  wire \write_burst_counter_reg[4]_i_1_n_4 ;
  wire \write_burst_counter_reg[4]_i_1_n_5 ;
  wire \write_burst_counter_reg[4]_i_1_n_6 ;
  wire \write_burst_counter_reg[4]_i_1_n_7 ;
  wire \write_burst_counter_reg[8]_i_1_n_0 ;
  wire \write_burst_counter_reg[8]_i_1_n_1 ;
  wire \write_burst_counter_reg[8]_i_1_n_2 ;
  wire \write_burst_counter_reg[8]_i_1_n_3 ;
  wire \write_burst_counter_reg[8]_i_1_n_4 ;
  wire \write_burst_counter_reg[8]_i_1_n_5 ;
  wire \write_burst_counter_reg[8]_i_1_n_6 ;
  wire \write_burst_counter_reg[8]_i_1_n_7 ;
  wire write_index0;
  wire \write_index[7]_i_1_n_0 ;
  wire \write_index[7]_i_4_n_0 ;
  wire [7:0]write_index_reg;
  wire writes_done;
  wire writes_done_i_1_n_0;
  wire writes_done_i_2_n_0;
  wire [3:3]NLW_M_AXI_AWADDR_carry__1_CO_UNCONNECTED;
  wire [3:2]\NLW_axi_awaddr_reg[29]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_axi_awaddr_reg[29]_i_1_O_UNCONNECTED ;
  wire [3:3]\NLW_cnt_cp_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_write_burst_counter_reg[28]_i_1_CO_UNCONNECTED ;

  LUT5 #(
    .INIT(32'h00000DFD)) 
    FSM_sequential_mst_exec_state_i_1
       (.I0(fifo_rst_reg2),
        .I1(fifo_rst_reg1),
        .I2(mst_exec_state),
        .I3(writes_done),
        .I4(ui_rst),
        .O(FSM_sequential_mst_exec_state_i_1_n_0));
  (* FSM_ENCODED_STATES = "IDLE:0,INIT_WRITE:1" *) 
  FDRE FSM_sequential_mst_exec_state_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(FSM_sequential_mst_exec_state_i_1_n_0),
        .Q(mst_exec_state),
        .R(1'b0));
  CARRY4 M_AXI_AWADDR_carry
       (.CI(1'b0),
        .CO({M_AXI_AWADDR_carry_n_0,M_AXI_AWADDR_carry_n_1,M_AXI_AWADDR_carry_n_2,M_AXI_AWADDR_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,axi_awaddr_reg[21],1'b0}),
        .O(M_AXI_AWADDR[14:11]),
        .S({axi_awaddr_reg[23:22],M_AXI_AWADDR_carry_i_1_n_0,axi_awaddr_reg[20]}));
  CARRY4 M_AXI_AWADDR_carry__0
       (.CI(M_AXI_AWADDR_carry_n_0),
        .CO({M_AXI_AWADDR_carry__0_n_0,M_AXI_AWADDR_carry__0_n_1,M_AXI_AWADDR_carry__0_n_2,M_AXI_AWADDR_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(M_AXI_AWADDR[18:15]),
        .S(axi_awaddr_reg[27:24]));
  CARRY4 M_AXI_AWADDR_carry__1
       (.CI(M_AXI_AWADDR_carry__0_n_0),
        .CO({NLW_M_AXI_AWADDR_carry__1_CO_UNCONNECTED[3],M_AXI_AWADDR_carry__1_n_1,M_AXI_AWADDR_carry__1_n_2,M_AXI_AWADDR_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(M_AXI_AWADDR[22:19]),
        .S(axi_awaddr_reg[31:28]));
  LUT1 #(
    .INIT(2'h1)) 
    M_AXI_AWADDR_carry_i_1
       (.I0(axi_awaddr_reg[21]),
        .O(M_AXI_AWADDR_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \axi_awaddr[12]_i_2 
       (.I0(M_AXI_AWADDR[0]),
        .O(\axi_awaddr[12]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hAE)) 
    \axi_awaddr[19]_i_1 
       (.I0(ui_rst),
        .I1(fifo_rst_reg2),
        .I2(fifo_rst_reg1),
        .O(burst_vaild1));
  LUT2 #(
    .INIT(4'h8)) 
    \axi_awaddr[19]_i_2 
       (.I0(axi_awvalid_reg_0),
        .I1(M_AXI_AWREADY),
        .O(axi_awvalid0));
  FDRE \axi_awaddr_reg[10] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[12]_i_1_n_6 ),
        .Q(M_AXI_AWADDR[1]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[11] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[12]_i_1_n_5 ),
        .Q(M_AXI_AWADDR[2]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[12] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[12]_i_1_n_4 ),
        .Q(M_AXI_AWADDR[3]),
        .R(burst_vaild1));
  CARRY4 \axi_awaddr_reg[12]_i_1 
       (.CI(1'b0),
        .CO({\axi_awaddr_reg[12]_i_1_n_0 ,\axi_awaddr_reg[12]_i_1_n_1 ,\axi_awaddr_reg[12]_i_1_n_2 ,\axi_awaddr_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\axi_awaddr_reg[12]_i_1_n_4 ,\axi_awaddr_reg[12]_i_1_n_5 ,\axi_awaddr_reg[12]_i_1_n_6 ,\axi_awaddr_reg[12]_i_1_n_7 }),
        .S({M_AXI_AWADDR[3:1],\axi_awaddr[12]_i_2_n_0 }));
  FDRE \axi_awaddr_reg[13] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[16]_i_1_n_7 ),
        .Q(M_AXI_AWADDR[4]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[14] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[16]_i_1_n_6 ),
        .Q(M_AXI_AWADDR[5]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[15] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[16]_i_1_n_5 ),
        .Q(M_AXI_AWADDR[6]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[16] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[16]_i_1_n_4 ),
        .Q(M_AXI_AWADDR[7]),
        .R(burst_vaild1));
  CARRY4 \axi_awaddr_reg[16]_i_1 
       (.CI(\axi_awaddr_reg[12]_i_1_n_0 ),
        .CO({\axi_awaddr_reg[16]_i_1_n_0 ,\axi_awaddr_reg[16]_i_1_n_1 ,\axi_awaddr_reg[16]_i_1_n_2 ,\axi_awaddr_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[16]_i_1_n_4 ,\axi_awaddr_reg[16]_i_1_n_5 ,\axi_awaddr_reg[16]_i_1_n_6 ,\axi_awaddr_reg[16]_i_1_n_7 }),
        .S(M_AXI_AWADDR[7:4]));
  FDRE \axi_awaddr_reg[17] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[19]_i_3_n_7 ),
        .Q(M_AXI_AWADDR[8]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[18] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[19]_i_3_n_6 ),
        .Q(M_AXI_AWADDR[9]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[19] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[19]_i_3_n_5 ),
        .Q(M_AXI_AWADDR[10]),
        .R(burst_vaild1));
  CARRY4 \axi_awaddr_reg[19]_i_3 
       (.CI(\axi_awaddr_reg[16]_i_1_n_0 ),
        .CO({\axi_awaddr_reg[19]_i_3_n_0 ,\axi_awaddr_reg[19]_i_3_n_1 ,\axi_awaddr_reg[19]_i_3_n_2 ,\axi_awaddr_reg[19]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[19]_i_3_n_4 ,\axi_awaddr_reg[19]_i_3_n_5 ,\axi_awaddr_reg[19]_i_3_n_6 ,\axi_awaddr_reg[19]_i_3_n_7 }),
        .S({axi_awaddr_reg[20],M_AXI_AWADDR[10:8]}));
  FDRE \axi_awaddr_reg[20] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[19]_i_3_n_4 ),
        .Q(axi_awaddr_reg[20]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[21] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[21]_i_1_n_7 ),
        .Q(axi_awaddr_reg[21]),
        .R(burst_vaild1));
  CARRY4 \axi_awaddr_reg[21]_i_1 
       (.CI(\axi_awaddr_reg[19]_i_3_n_0 ),
        .CO({\axi_awaddr_reg[21]_i_1_n_0 ,\axi_awaddr_reg[21]_i_1_n_1 ,\axi_awaddr_reg[21]_i_1_n_2 ,\axi_awaddr_reg[21]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[21]_i_1_n_4 ,\axi_awaddr_reg[21]_i_1_n_5 ,\axi_awaddr_reg[21]_i_1_n_6 ,\axi_awaddr_reg[21]_i_1_n_7 }),
        .S(axi_awaddr_reg[24:21]));
  FDRE \axi_awaddr_reg[22] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[21]_i_1_n_6 ),
        .Q(axi_awaddr_reg[22]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[23] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[21]_i_1_n_5 ),
        .Q(axi_awaddr_reg[23]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[24] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[21]_i_1_n_4 ),
        .Q(axi_awaddr_reg[24]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[25] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[25]_i_1_n_7 ),
        .Q(axi_awaddr_reg[25]),
        .R(burst_vaild1));
  CARRY4 \axi_awaddr_reg[25]_i_1 
       (.CI(\axi_awaddr_reg[21]_i_1_n_0 ),
        .CO({\axi_awaddr_reg[25]_i_1_n_0 ,\axi_awaddr_reg[25]_i_1_n_1 ,\axi_awaddr_reg[25]_i_1_n_2 ,\axi_awaddr_reg[25]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\axi_awaddr_reg[25]_i_1_n_4 ,\axi_awaddr_reg[25]_i_1_n_5 ,\axi_awaddr_reg[25]_i_1_n_6 ,\axi_awaddr_reg[25]_i_1_n_7 }),
        .S(axi_awaddr_reg[28:25]));
  FDRE \axi_awaddr_reg[26] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[25]_i_1_n_6 ),
        .Q(axi_awaddr_reg[26]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[27] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[25]_i_1_n_5 ),
        .Q(axi_awaddr_reg[27]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[28] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[25]_i_1_n_4 ),
        .Q(axi_awaddr_reg[28]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[29] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[29]_i_1_n_7 ),
        .Q(axi_awaddr_reg[29]),
        .R(burst_vaild1));
  CARRY4 \axi_awaddr_reg[29]_i_1 
       (.CI(\axi_awaddr_reg[25]_i_1_n_0 ),
        .CO({\NLW_axi_awaddr_reg[29]_i_1_CO_UNCONNECTED [3:2],\axi_awaddr_reg[29]_i_1_n_2 ,\axi_awaddr_reg[29]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_axi_awaddr_reg[29]_i_1_O_UNCONNECTED [3],\axi_awaddr_reg[29]_i_1_n_5 ,\axi_awaddr_reg[29]_i_1_n_6 ,\axi_awaddr_reg[29]_i_1_n_7 }),
        .S({1'b0,axi_awaddr_reg[31:29]}));
  FDRE \axi_awaddr_reg[30] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[29]_i_1_n_6 ),
        .Q(axi_awaddr_reg[30]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[31] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[29]_i_1_n_5 ),
        .Q(axi_awaddr_reg[31]),
        .R(burst_vaild1));
  FDRE \axi_awaddr_reg[9] 
       (.C(M_AXI_ACLK),
        .CE(axi_awvalid0),
        .D(\axi_awaddr_reg[12]_i_1_n_7 ),
        .Q(M_AXI_AWADDR[0]),
        .R(burst_vaild1));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h3A)) 
    axi_awvalid_i_1
       (.I0(start_single_burst_write_reg_n_0),
        .I1(M_AXI_AWREADY),
        .I2(axi_awvalid_reg_0),
        .O(axi_awvalid_i_1_n_0));
  FDRE axi_awvalid_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_awvalid_i_1_n_0),
        .Q(axi_awvalid_reg_0),
        .R(burst_vaild1));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_bready_i_1
       (.I0(M_AXI_BVALID),
        .I1(M_AXI_BREADY),
        .O(axi_bready0));
  FDRE axi_bready_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_bready0),
        .Q(M_AXI_BREADY),
        .R(burst_vaild1));
  LUT6 #(
    .INIT(64'h20FFFFFF20000000)) 
    axi_wlast_i_1
       (.I0(write_index_reg[1]),
        .I1(write_index_reg[0]),
        .I2(axi_wlast_i_2_n_0),
        .I3(M_AXI_WREADY),
        .I4(axi_wvalid_reg_0),
        .I5(M_AXI_WLAST),
        .O(axi_wlast_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    axi_wlast_i_2
       (.I0(write_index_reg[4]),
        .I1(write_index_reg[5]),
        .I2(write_index_reg[2]),
        .I3(write_index_reg[3]),
        .I4(write_index_reg[7]),
        .I5(write_index_reg[6]),
        .O(axi_wlast_i_2_n_0));
  FDRE axi_wlast_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_wlast_i_1_n_0),
        .Q(M_AXI_WLAST),
        .R(burst_vaild1));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h7F70)) 
    axi_wvalid_i_1
       (.I0(M_AXI_WLAST),
        .I1(M_AXI_WREADY),
        .I2(axi_wvalid_reg_0),
        .I3(start_single_burst_write_reg_n_0),
        .O(axi_wvalid_i_1_n_0));
  FDRE axi_wvalid_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(axi_wvalid_i_1_n_0),
        .Q(axi_wvalid_reg_0),
        .R(burst_vaild1));
  LUT5 #(
    .INIT(32'h00001011)) 
    burst_vaild_i_1
       (.I0(almost_empty),
        .I1(prog_empty),
        .I2(fifo_rst_reg1),
        .I3(fifo_rst_reg2),
        .I4(ui_rst),
        .O(burst_vaild_i_1_n_0));
  FDRE burst_vaild_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(burst_vaild_i_1_n_0),
        .Q(burst_vaild),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hF7F0)) 
    burst_write_active_i_1
       (.I0(M_AXI_BREADY),
        .I1(M_AXI_BVALID),
        .I2(start_single_burst_write_reg_n_0),
        .I3(burst_write_active),
        .O(burst_write_active_i_1_n_0));
  FDRE burst_write_active_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(burst_write_active_i_1_n_0),
        .Q(burst_write_active),
        .R(burst_vaild1));
  LUT4 #(
    .INIT(16'h04FF)) 
    \cnt_cp[0]_i_1 
       (.I0(\write_burst_counter[0]_i_3_n_0 ),
        .I1(\cnt_cp[0]_i_3_n_0 ),
        .I2(burst_write_active),
        .I3(mst_exec_state),
        .O(cnt_cp));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \cnt_cp[0]_i_3 
       (.I0(write_burst_counter_reg[1]),
        .I1(write_burst_counter_reg[2]),
        .I2(write_burst_counter_reg[0]),
        .I3(writes_done),
        .I4(start_single_burst_write_reg_n_0),
        .I5(axi_awvalid_reg_0),
        .O(\cnt_cp[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_cp[0]_i_4 
       (.I0(\cnt_cp_reg_n_0_[3] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[0]_i_5 
       (.I0(\cnt_cp_reg_n_0_[2] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_cp[0]_i_6 
       (.I0(\cnt_cp_reg_n_0_[1] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_cp[0]_i_7 
       (.I0(\cnt_cp_reg_n_0_[0] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[0]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[12]_i_2 
       (.I0(\cnt_cp_reg_n_0_[15] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[12]_i_3 
       (.I0(\cnt_cp_reg_n_0_[14] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[12]_i_4 
       (.I0(\cnt_cp_reg_n_0_[13] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[12]_i_5 
       (.I0(\cnt_cp_reg_n_0_[12] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[16]_i_2 
       (.I0(\cnt_cp_reg_n_0_[19] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[16]_i_3 
       (.I0(\cnt_cp_reg_n_0_[18] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[16]_i_4 
       (.I0(\cnt_cp_reg_n_0_[17] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[16]_i_5 
       (.I0(\cnt_cp_reg_n_0_[16] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[20]_i_2 
       (.I0(\cnt_cp_reg_n_0_[23] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[20]_i_3 
       (.I0(\cnt_cp_reg_n_0_[22] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[20]_i_4 
       (.I0(\cnt_cp_reg_n_0_[21] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[20]_i_5 
       (.I0(\cnt_cp_reg_n_0_[20] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[24]_i_2 
       (.I0(\cnt_cp_reg_n_0_[27] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[24]_i_3 
       (.I0(\cnt_cp_reg_n_0_[26] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[24]_i_4 
       (.I0(\cnt_cp_reg_n_0_[25] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[24]_i_5 
       (.I0(\cnt_cp_reg_n_0_[24] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[28]_i_2 
       (.I0(\cnt_cp_reg_n_0_[31] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[28]_i_3 
       (.I0(\cnt_cp_reg_n_0_[30] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[28]_i_4 
       (.I0(\cnt_cp_reg_n_0_[29] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[28]_i_5 
       (.I0(\cnt_cp_reg_n_0_[28] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[28]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[4]_i_2 
       (.I0(\cnt_cp_reg_n_0_[7] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_cp[4]_i_3 
       (.I0(\cnt_cp_reg_n_0_[6] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[4]_i_4 
       (.I0(\cnt_cp_reg_n_0_[5] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_cp[4]_i_5 
       (.I0(\cnt_cp_reg_n_0_[4] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[8]_i_2 
       (.I0(\cnt_cp_reg_n_0_[11] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[8]_i_3 
       (.I0(\cnt_cp_reg_n_0_[10] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_cp[8]_i_4 
       (.I0(\cnt_cp_reg_n_0_[9] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt_cp[8]_i_5 
       (.I0(\cnt_cp_reg_n_0_[8] ),
        .I1(mst_exec_state),
        .O(\cnt_cp[8]_i_5_n_0 ));
  FDSE \cnt_cp_reg[0] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[0]_i_2_n_7 ),
        .Q(\cnt_cp_reg_n_0_[0] ),
        .S(ui_rst));
  CARRY4 \cnt_cp_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_cp_reg[0]_i_2_n_0 ,\cnt_cp_reg[0]_i_2_n_1 ,\cnt_cp_reg[0]_i_2_n_2 ,\cnt_cp_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({mst_exec_state,mst_exec_state,mst_exec_state,mst_exec_state}),
        .O({\cnt_cp_reg[0]_i_2_n_4 ,\cnt_cp_reg[0]_i_2_n_5 ,\cnt_cp_reg[0]_i_2_n_6 ,\cnt_cp_reg[0]_i_2_n_7 }),
        .S({\cnt_cp[0]_i_4_n_0 ,\cnt_cp[0]_i_5_n_0 ,\cnt_cp[0]_i_6_n_0 ,\cnt_cp[0]_i_7_n_0 }));
  FDRE \cnt_cp_reg[10] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[8]_i_1_n_5 ),
        .Q(\cnt_cp_reg_n_0_[10] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[11] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[8]_i_1_n_4 ),
        .Q(\cnt_cp_reg_n_0_[11] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[12] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[12]_i_1_n_7 ),
        .Q(\cnt_cp_reg_n_0_[12] ),
        .R(ui_rst));
  CARRY4 \cnt_cp_reg[12]_i_1 
       (.CI(\cnt_cp_reg[8]_i_1_n_0 ),
        .CO({\cnt_cp_reg[12]_i_1_n_0 ,\cnt_cp_reg[12]_i_1_n_1 ,\cnt_cp_reg[12]_i_1_n_2 ,\cnt_cp_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({mst_exec_state,mst_exec_state,mst_exec_state,mst_exec_state}),
        .O({\cnt_cp_reg[12]_i_1_n_4 ,\cnt_cp_reg[12]_i_1_n_5 ,\cnt_cp_reg[12]_i_1_n_6 ,\cnt_cp_reg[12]_i_1_n_7 }),
        .S({\cnt_cp[12]_i_2_n_0 ,\cnt_cp[12]_i_3_n_0 ,\cnt_cp[12]_i_4_n_0 ,\cnt_cp[12]_i_5_n_0 }));
  FDRE \cnt_cp_reg[13] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[12]_i_1_n_6 ),
        .Q(\cnt_cp_reg_n_0_[13] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[14] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[12]_i_1_n_5 ),
        .Q(\cnt_cp_reg_n_0_[14] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[15] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[12]_i_1_n_4 ),
        .Q(\cnt_cp_reg_n_0_[15] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[16] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[16]_i_1_n_7 ),
        .Q(\cnt_cp_reg_n_0_[16] ),
        .R(ui_rst));
  CARRY4 \cnt_cp_reg[16]_i_1 
       (.CI(\cnt_cp_reg[12]_i_1_n_0 ),
        .CO({\cnt_cp_reg[16]_i_1_n_0 ,\cnt_cp_reg[16]_i_1_n_1 ,\cnt_cp_reg[16]_i_1_n_2 ,\cnt_cp_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({mst_exec_state,mst_exec_state,mst_exec_state,mst_exec_state}),
        .O({\cnt_cp_reg[16]_i_1_n_4 ,\cnt_cp_reg[16]_i_1_n_5 ,\cnt_cp_reg[16]_i_1_n_6 ,\cnt_cp_reg[16]_i_1_n_7 }),
        .S({\cnt_cp[16]_i_2_n_0 ,\cnt_cp[16]_i_3_n_0 ,\cnt_cp[16]_i_4_n_0 ,\cnt_cp[16]_i_5_n_0 }));
  FDRE \cnt_cp_reg[17] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[16]_i_1_n_6 ),
        .Q(\cnt_cp_reg_n_0_[17] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[18] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[16]_i_1_n_5 ),
        .Q(\cnt_cp_reg_n_0_[18] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[19] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[16]_i_1_n_4 ),
        .Q(\cnt_cp_reg_n_0_[19] ),
        .R(ui_rst));
  FDSE \cnt_cp_reg[1] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[0]_i_2_n_6 ),
        .Q(\cnt_cp_reg_n_0_[1] ),
        .S(ui_rst));
  FDRE \cnt_cp_reg[20] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[20]_i_1_n_7 ),
        .Q(\cnt_cp_reg_n_0_[20] ),
        .R(ui_rst));
  CARRY4 \cnt_cp_reg[20]_i_1 
       (.CI(\cnt_cp_reg[16]_i_1_n_0 ),
        .CO({\cnt_cp_reg[20]_i_1_n_0 ,\cnt_cp_reg[20]_i_1_n_1 ,\cnt_cp_reg[20]_i_1_n_2 ,\cnt_cp_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({mst_exec_state,mst_exec_state,mst_exec_state,mst_exec_state}),
        .O({\cnt_cp_reg[20]_i_1_n_4 ,\cnt_cp_reg[20]_i_1_n_5 ,\cnt_cp_reg[20]_i_1_n_6 ,\cnt_cp_reg[20]_i_1_n_7 }),
        .S({\cnt_cp[20]_i_2_n_0 ,\cnt_cp[20]_i_3_n_0 ,\cnt_cp[20]_i_4_n_0 ,\cnt_cp[20]_i_5_n_0 }));
  FDRE \cnt_cp_reg[21] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[20]_i_1_n_6 ),
        .Q(\cnt_cp_reg_n_0_[21] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[22] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[20]_i_1_n_5 ),
        .Q(\cnt_cp_reg_n_0_[22] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[23] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[20]_i_1_n_4 ),
        .Q(\cnt_cp_reg_n_0_[23] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[24] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[24]_i_1_n_7 ),
        .Q(\cnt_cp_reg_n_0_[24] ),
        .R(ui_rst));
  CARRY4 \cnt_cp_reg[24]_i_1 
       (.CI(\cnt_cp_reg[20]_i_1_n_0 ),
        .CO({\cnt_cp_reg[24]_i_1_n_0 ,\cnt_cp_reg[24]_i_1_n_1 ,\cnt_cp_reg[24]_i_1_n_2 ,\cnt_cp_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({mst_exec_state,mst_exec_state,mst_exec_state,mst_exec_state}),
        .O({\cnt_cp_reg[24]_i_1_n_4 ,\cnt_cp_reg[24]_i_1_n_5 ,\cnt_cp_reg[24]_i_1_n_6 ,\cnt_cp_reg[24]_i_1_n_7 }),
        .S({\cnt_cp[24]_i_2_n_0 ,\cnt_cp[24]_i_3_n_0 ,\cnt_cp[24]_i_4_n_0 ,\cnt_cp[24]_i_5_n_0 }));
  FDRE \cnt_cp_reg[25] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[24]_i_1_n_6 ),
        .Q(\cnt_cp_reg_n_0_[25] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[26] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[24]_i_1_n_5 ),
        .Q(\cnt_cp_reg_n_0_[26] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[27] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[24]_i_1_n_4 ),
        .Q(\cnt_cp_reg_n_0_[27] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[28] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[28]_i_1_n_7 ),
        .Q(\cnt_cp_reg_n_0_[28] ),
        .R(ui_rst));
  CARRY4 \cnt_cp_reg[28]_i_1 
       (.CI(\cnt_cp_reg[24]_i_1_n_0 ),
        .CO({\NLW_cnt_cp_reg[28]_i_1_CO_UNCONNECTED [3],\cnt_cp_reg[28]_i_1_n_1 ,\cnt_cp_reg[28]_i_1_n_2 ,\cnt_cp_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mst_exec_state,mst_exec_state,mst_exec_state}),
        .O({\cnt_cp_reg[28]_i_1_n_4 ,\cnt_cp_reg[28]_i_1_n_5 ,\cnt_cp_reg[28]_i_1_n_6 ,\cnt_cp_reg[28]_i_1_n_7 }),
        .S({\cnt_cp[28]_i_2_n_0 ,\cnt_cp[28]_i_3_n_0 ,\cnt_cp[28]_i_4_n_0 ,\cnt_cp[28]_i_5_n_0 }));
  FDRE \cnt_cp_reg[29] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[28]_i_1_n_6 ),
        .Q(\cnt_cp_reg_n_0_[29] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[0]_i_2_n_5 ),
        .Q(\cnt_cp_reg_n_0_[2] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[30] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[28]_i_1_n_5 ),
        .Q(\cnt_cp_reg_n_0_[30] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[31] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[28]_i_1_n_4 ),
        .Q(\cnt_cp_reg_n_0_[31] ),
        .R(ui_rst));
  FDSE \cnt_cp_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[0]_i_2_n_4 ),
        .Q(\cnt_cp_reg_n_0_[3] ),
        .S(ui_rst));
  FDSE \cnt_cp_reg[4] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[4]_i_1_n_7 ),
        .Q(\cnt_cp_reg_n_0_[4] ),
        .S(ui_rst));
  CARRY4 \cnt_cp_reg[4]_i_1 
       (.CI(\cnt_cp_reg[0]_i_2_n_0 ),
        .CO({\cnt_cp_reg[4]_i_1_n_0 ,\cnt_cp_reg[4]_i_1_n_1 ,\cnt_cp_reg[4]_i_1_n_2 ,\cnt_cp_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({mst_exec_state,mst_exec_state,mst_exec_state,mst_exec_state}),
        .O({\cnt_cp_reg[4]_i_1_n_4 ,\cnt_cp_reg[4]_i_1_n_5 ,\cnt_cp_reg[4]_i_1_n_6 ,\cnt_cp_reg[4]_i_1_n_7 }),
        .S({\cnt_cp[4]_i_2_n_0 ,\cnt_cp[4]_i_3_n_0 ,\cnt_cp[4]_i_4_n_0 ,\cnt_cp[4]_i_5_n_0 }));
  FDRE \cnt_cp_reg[5] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[4]_i_1_n_6 ),
        .Q(\cnt_cp_reg_n_0_[5] ),
        .R(ui_rst));
  FDSE \cnt_cp_reg[6] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[4]_i_1_n_5 ),
        .Q(\cnt_cp_reg_n_0_[6] ),
        .S(ui_rst));
  FDRE \cnt_cp_reg[7] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[4]_i_1_n_4 ),
        .Q(\cnt_cp_reg_n_0_[7] ),
        .R(ui_rst));
  FDRE \cnt_cp_reg[8] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[8]_i_1_n_7 ),
        .Q(\cnt_cp_reg_n_0_[8] ),
        .R(ui_rst));
  CARRY4 \cnt_cp_reg[8]_i_1 
       (.CI(\cnt_cp_reg[4]_i_1_n_0 ),
        .CO({\cnt_cp_reg[8]_i_1_n_0 ,\cnt_cp_reg[8]_i_1_n_1 ,\cnt_cp_reg[8]_i_1_n_2 ,\cnt_cp_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({mst_exec_state,mst_exec_state,mst_exec_state,mst_exec_state}),
        .O({\cnt_cp_reg[8]_i_1_n_4 ,\cnt_cp_reg[8]_i_1_n_5 ,\cnt_cp_reg[8]_i_1_n_6 ,\cnt_cp_reg[8]_i_1_n_7 }),
        .S({\cnt_cp[8]_i_2_n_0 ,\cnt_cp[8]_i_3_n_0 ,\cnt_cp[8]_i_4_n_0 ,\cnt_cp[8]_i_5_n_0 }));
  FDSE \cnt_cp_reg[9] 
       (.C(M_AXI_ACLK),
        .CE(cnt_cp),
        .D(\cnt_cp_reg[8]_i_1_n_6 ),
        .Q(\cnt_cp_reg_n_0_[9] ),
        .S(ui_rst));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h8)) 
    rd_wdfifo_en_INST_0
       (.I0(M_AXI_WREADY),
        .I1(axi_wvalid_reg_0),
        .O(rd_wdfifo_en));
  LUT6 #(
    .INIT(64'h00000000BABABA8A)) 
    start_single_burst_write_i_1
       (.I0(start_single_burst_write_reg_n_0),
        .I1(writes_done),
        .I2(mst_exec_state),
        .I3(start_single_burst_write_i_2_n_0),
        .I4(start_single_burst_write_i_3_n_0),
        .I5(ui_rst),
        .O(start_single_burst_write_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    start_single_burst_write_i_10
       (.I0(\cnt_cp_reg_n_0_[7] ),
        .I1(\cnt_cp_reg_n_0_[8] ),
        .I2(\cnt_cp_reg_n_0_[5] ),
        .I3(\cnt_cp_reg_n_0_[6] ),
        .I4(\cnt_cp_reg_n_0_[10] ),
        .I5(\cnt_cp_reg_n_0_[9] ),
        .O(start_single_burst_write_i_10_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    start_single_burst_write_i_11
       (.I0(\cnt_cp_reg_n_0_[13] ),
        .I1(\cnt_cp_reg_n_0_[14] ),
        .I2(\cnt_cp_reg_n_0_[11] ),
        .I3(\cnt_cp_reg_n_0_[12] ),
        .I4(\cnt_cp_reg_n_0_[16] ),
        .I5(\cnt_cp_reg_n_0_[15] ),
        .O(start_single_burst_write_i_11_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    start_single_burst_write_i_2
       (.I0(start_single_burst_write_i_4_n_0),
        .I1(start_single_burst_write_i_5_n_0),
        .I2(\write_burst_counter[0]_i_7_n_0 ),
        .I3(\write_burst_counter[0]_i_6_n_0 ),
        .I4(\write_burst_counter[0]_i_5_n_0 ),
        .I5(start_single_burst_write_i_6_n_0),
        .O(start_single_burst_write_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    start_single_burst_write_i_3
       (.I0(burst_write_active),
        .I1(burst_vaild),
        .I2(start_single_burst_write_reg_n_0),
        .I3(axi_awvalid_reg_0),
        .O(start_single_burst_write_i_3_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    start_single_burst_write_i_4
       (.I0(start_single_burst_write_i_7_n_0),
        .I1(start_single_burst_write_i_8_n_0),
        .I2(start_single_burst_write_i_9_n_0),
        .O(start_single_burst_write_i_4_n_0));
  LUT5 #(
    .INIT(32'h00000001)) 
    start_single_burst_write_i_5
       (.I0(\cnt_cp_reg_n_0_[0] ),
        .I1(\cnt_cp_reg_n_0_[1] ),
        .I2(\cnt_cp_reg_n_0_[2] ),
        .I3(\cnt_cp_reg_n_0_[4] ),
        .I4(\cnt_cp_reg_n_0_[3] ),
        .O(start_single_burst_write_i_5_n_0));
  LUT5 #(
    .INIT(32'h00020000)) 
    start_single_burst_write_i_6
       (.I0(start_single_burst_write_i_10_n_0),
        .I1(start_single_burst_write_reg_n_0),
        .I2(axi_awvalid_reg_0),
        .I3(burst_write_active),
        .I4(start_single_burst_write_i_11_n_0),
        .O(start_single_burst_write_i_6_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    start_single_burst_write_i_7
       (.I0(\cnt_cp_reg_n_0_[19] ),
        .I1(\cnt_cp_reg_n_0_[20] ),
        .I2(\cnt_cp_reg_n_0_[17] ),
        .I3(\cnt_cp_reg_n_0_[18] ),
        .I4(\cnt_cp_reg_n_0_[22] ),
        .I5(\cnt_cp_reg_n_0_[21] ),
        .O(start_single_burst_write_i_7_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    start_single_burst_write_i_8
       (.I0(\cnt_cp_reg_n_0_[25] ),
        .I1(\cnt_cp_reg_n_0_[26] ),
        .I2(\cnt_cp_reg_n_0_[23] ),
        .I3(\cnt_cp_reg_n_0_[24] ),
        .I4(\cnt_cp_reg_n_0_[28] ),
        .I5(\cnt_cp_reg_n_0_[27] ),
        .O(start_single_burst_write_i_8_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    start_single_burst_write_i_9
       (.I0(write_burst_counter_reg[0]),
        .I1(\cnt_cp_reg_n_0_[31] ),
        .I2(\cnt_cp_reg_n_0_[29] ),
        .I3(\cnt_cp_reg_n_0_[30] ),
        .I4(write_burst_counter_reg[2]),
        .I5(write_burst_counter_reg[1]),
        .O(start_single_burst_write_i_9_n_0));
  FDRE start_single_burst_write_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(start_single_burst_write_i_1_n_0),
        .Q(start_single_burst_write_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFB000000000000)) 
    \write_burst_counter[0]_i_1 
       (.I0(\write_burst_counter[0]_i_3_n_0 ),
        .I1(write_burst_counter_reg[2]),
        .I2(write_burst_counter_reg[1]),
        .I3(write_burst_counter_reg[0]),
        .I4(M_AXI_AWREADY),
        .I5(axi_awvalid_reg_0),
        .O(write_burst_counter));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \write_burst_counter[0]_i_10 
       (.I0(write_burst_counter_reg[27]),
        .I1(write_burst_counter_reg[26]),
        .I2(write_burst_counter_reg[29]),
        .I3(write_burst_counter_reg[28]),
        .O(\write_burst_counter[0]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \write_burst_counter[0]_i_11 
       (.I0(write_burst_counter_reg[3]),
        .I1(write_burst_counter_reg[30]),
        .I2(write_burst_counter_reg[31]),
        .I3(write_burst_counter_reg[5]),
        .I4(write_burst_counter_reg[4]),
        .O(\write_burst_counter[0]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \write_burst_counter[0]_i_3 
       (.I0(\write_burst_counter[0]_i_5_n_0 ),
        .I1(\write_burst_counter[0]_i_6_n_0 ),
        .I2(\write_burst_counter[0]_i_7_n_0 ),
        .O(\write_burst_counter[0]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \write_burst_counter[0]_i_4 
       (.I0(write_burst_counter_reg[0]),
        .O(\write_burst_counter[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \write_burst_counter[0]_i_5 
       (.I0(write_burst_counter_reg[16]),
        .I1(write_burst_counter_reg[17]),
        .I2(write_burst_counter_reg[14]),
        .I3(write_burst_counter_reg[15]),
        .I4(\write_burst_counter[0]_i_8_n_0 ),
        .O(\write_burst_counter[0]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFF7FF)) 
    \write_burst_counter[0]_i_6 
       (.I0(write_burst_counter_reg[8]),
        .I1(write_burst_counter_reg[9]),
        .I2(write_burst_counter_reg[6]),
        .I3(write_burst_counter_reg[7]),
        .I4(\write_burst_counter[0]_i_9_n_0 ),
        .O(\write_burst_counter[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \write_burst_counter[0]_i_7 
       (.I0(\write_burst_counter[0]_i_10_n_0 ),
        .I1(write_burst_counter_reg[23]),
        .I2(write_burst_counter_reg[22]),
        .I3(write_burst_counter_reg[25]),
        .I4(write_burst_counter_reg[24]),
        .I5(\write_burst_counter[0]_i_11_n_0 ),
        .O(\write_burst_counter[0]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \write_burst_counter[0]_i_8 
       (.I0(write_burst_counter_reg[19]),
        .I1(write_burst_counter_reg[18]),
        .I2(write_burst_counter_reg[21]),
        .I3(write_burst_counter_reg[20]),
        .O(\write_burst_counter[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \write_burst_counter[0]_i_9 
       (.I0(write_burst_counter_reg[11]),
        .I1(write_burst_counter_reg[10]),
        .I2(write_burst_counter_reg[12]),
        .I3(write_burst_counter_reg[13]),
        .O(\write_burst_counter[0]_i_9_n_0 ));
  FDRE \write_burst_counter_reg[0] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[0]_i_2_n_7 ),
        .Q(write_burst_counter_reg[0]),
        .R(burst_vaild1));
  CARRY4 \write_burst_counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\write_burst_counter_reg[0]_i_2_n_0 ,\write_burst_counter_reg[0]_i_2_n_1 ,\write_burst_counter_reg[0]_i_2_n_2 ,\write_burst_counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\write_burst_counter_reg[0]_i_2_n_4 ,\write_burst_counter_reg[0]_i_2_n_5 ,\write_burst_counter_reg[0]_i_2_n_6 ,\write_burst_counter_reg[0]_i_2_n_7 }),
        .S({write_burst_counter_reg[3:1],\write_burst_counter[0]_i_4_n_0 }));
  FDRE \write_burst_counter_reg[10] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[8]_i_1_n_5 ),
        .Q(write_burst_counter_reg[10]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[11] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[8]_i_1_n_4 ),
        .Q(write_burst_counter_reg[11]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[12] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[12]_i_1_n_7 ),
        .Q(write_burst_counter_reg[12]),
        .R(burst_vaild1));
  CARRY4 \write_burst_counter_reg[12]_i_1 
       (.CI(\write_burst_counter_reg[8]_i_1_n_0 ),
        .CO({\write_burst_counter_reg[12]_i_1_n_0 ,\write_burst_counter_reg[12]_i_1_n_1 ,\write_burst_counter_reg[12]_i_1_n_2 ,\write_burst_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\write_burst_counter_reg[12]_i_1_n_4 ,\write_burst_counter_reg[12]_i_1_n_5 ,\write_burst_counter_reg[12]_i_1_n_6 ,\write_burst_counter_reg[12]_i_1_n_7 }),
        .S(write_burst_counter_reg[15:12]));
  FDRE \write_burst_counter_reg[13] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[12]_i_1_n_6 ),
        .Q(write_burst_counter_reg[13]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[14] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[12]_i_1_n_5 ),
        .Q(write_burst_counter_reg[14]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[15] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[12]_i_1_n_4 ),
        .Q(write_burst_counter_reg[15]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[16] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[16]_i_1_n_7 ),
        .Q(write_burst_counter_reg[16]),
        .R(burst_vaild1));
  CARRY4 \write_burst_counter_reg[16]_i_1 
       (.CI(\write_burst_counter_reg[12]_i_1_n_0 ),
        .CO({\write_burst_counter_reg[16]_i_1_n_0 ,\write_burst_counter_reg[16]_i_1_n_1 ,\write_burst_counter_reg[16]_i_1_n_2 ,\write_burst_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\write_burst_counter_reg[16]_i_1_n_4 ,\write_burst_counter_reg[16]_i_1_n_5 ,\write_burst_counter_reg[16]_i_1_n_6 ,\write_burst_counter_reg[16]_i_1_n_7 }),
        .S(write_burst_counter_reg[19:16]));
  FDRE \write_burst_counter_reg[17] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[16]_i_1_n_6 ),
        .Q(write_burst_counter_reg[17]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[18] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[16]_i_1_n_5 ),
        .Q(write_burst_counter_reg[18]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[19] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[16]_i_1_n_4 ),
        .Q(write_burst_counter_reg[19]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[1] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[0]_i_2_n_6 ),
        .Q(write_burst_counter_reg[1]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[20] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[20]_i_1_n_7 ),
        .Q(write_burst_counter_reg[20]),
        .R(burst_vaild1));
  CARRY4 \write_burst_counter_reg[20]_i_1 
       (.CI(\write_burst_counter_reg[16]_i_1_n_0 ),
        .CO({\write_burst_counter_reg[20]_i_1_n_0 ,\write_burst_counter_reg[20]_i_1_n_1 ,\write_burst_counter_reg[20]_i_1_n_2 ,\write_burst_counter_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\write_burst_counter_reg[20]_i_1_n_4 ,\write_burst_counter_reg[20]_i_1_n_5 ,\write_burst_counter_reg[20]_i_1_n_6 ,\write_burst_counter_reg[20]_i_1_n_7 }),
        .S(write_burst_counter_reg[23:20]));
  FDRE \write_burst_counter_reg[21] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[20]_i_1_n_6 ),
        .Q(write_burst_counter_reg[21]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[22] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[20]_i_1_n_5 ),
        .Q(write_burst_counter_reg[22]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[23] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[20]_i_1_n_4 ),
        .Q(write_burst_counter_reg[23]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[24] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[24]_i_1_n_7 ),
        .Q(write_burst_counter_reg[24]),
        .R(burst_vaild1));
  CARRY4 \write_burst_counter_reg[24]_i_1 
       (.CI(\write_burst_counter_reg[20]_i_1_n_0 ),
        .CO({\write_burst_counter_reg[24]_i_1_n_0 ,\write_burst_counter_reg[24]_i_1_n_1 ,\write_burst_counter_reg[24]_i_1_n_2 ,\write_burst_counter_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\write_burst_counter_reg[24]_i_1_n_4 ,\write_burst_counter_reg[24]_i_1_n_5 ,\write_burst_counter_reg[24]_i_1_n_6 ,\write_burst_counter_reg[24]_i_1_n_7 }),
        .S(write_burst_counter_reg[27:24]));
  FDRE \write_burst_counter_reg[25] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[24]_i_1_n_6 ),
        .Q(write_burst_counter_reg[25]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[26] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[24]_i_1_n_5 ),
        .Q(write_burst_counter_reg[26]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[27] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[24]_i_1_n_4 ),
        .Q(write_burst_counter_reg[27]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[28] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[28]_i_1_n_7 ),
        .Q(write_burst_counter_reg[28]),
        .R(burst_vaild1));
  CARRY4 \write_burst_counter_reg[28]_i_1 
       (.CI(\write_burst_counter_reg[24]_i_1_n_0 ),
        .CO({\NLW_write_burst_counter_reg[28]_i_1_CO_UNCONNECTED [3],\write_burst_counter_reg[28]_i_1_n_1 ,\write_burst_counter_reg[28]_i_1_n_2 ,\write_burst_counter_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\write_burst_counter_reg[28]_i_1_n_4 ,\write_burst_counter_reg[28]_i_1_n_5 ,\write_burst_counter_reg[28]_i_1_n_6 ,\write_burst_counter_reg[28]_i_1_n_7 }),
        .S(write_burst_counter_reg[31:28]));
  FDRE \write_burst_counter_reg[29] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[28]_i_1_n_6 ),
        .Q(write_burst_counter_reg[29]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[0]_i_2_n_5 ),
        .Q(write_burst_counter_reg[2]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[30] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[28]_i_1_n_5 ),
        .Q(write_burst_counter_reg[30]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[31] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[28]_i_1_n_4 ),
        .Q(write_burst_counter_reg[31]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[0]_i_2_n_4 ),
        .Q(write_burst_counter_reg[3]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[4] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[4]_i_1_n_7 ),
        .Q(write_burst_counter_reg[4]),
        .R(burst_vaild1));
  CARRY4 \write_burst_counter_reg[4]_i_1 
       (.CI(\write_burst_counter_reg[0]_i_2_n_0 ),
        .CO({\write_burst_counter_reg[4]_i_1_n_0 ,\write_burst_counter_reg[4]_i_1_n_1 ,\write_burst_counter_reg[4]_i_1_n_2 ,\write_burst_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\write_burst_counter_reg[4]_i_1_n_4 ,\write_burst_counter_reg[4]_i_1_n_5 ,\write_burst_counter_reg[4]_i_1_n_6 ,\write_burst_counter_reg[4]_i_1_n_7 }),
        .S(write_burst_counter_reg[7:4]));
  FDRE \write_burst_counter_reg[5] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[4]_i_1_n_6 ),
        .Q(write_burst_counter_reg[5]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[6] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[4]_i_1_n_5 ),
        .Q(write_burst_counter_reg[6]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[7] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[4]_i_1_n_4 ),
        .Q(write_burst_counter_reg[7]),
        .R(burst_vaild1));
  FDRE \write_burst_counter_reg[8] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[8]_i_1_n_7 ),
        .Q(write_burst_counter_reg[8]),
        .R(burst_vaild1));
  CARRY4 \write_burst_counter_reg[8]_i_1 
       (.CI(\write_burst_counter_reg[4]_i_1_n_0 ),
        .CO({\write_burst_counter_reg[8]_i_1_n_0 ,\write_burst_counter_reg[8]_i_1_n_1 ,\write_burst_counter_reg[8]_i_1_n_2 ,\write_burst_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\write_burst_counter_reg[8]_i_1_n_4 ,\write_burst_counter_reg[8]_i_1_n_5 ,\write_burst_counter_reg[8]_i_1_n_6 ,\write_burst_counter_reg[8]_i_1_n_7 }),
        .S(write_burst_counter_reg[11:8]));
  FDRE \write_burst_counter_reg[9] 
       (.C(M_AXI_ACLK),
        .CE(write_burst_counter),
        .D(\write_burst_counter_reg[8]_i_1_n_6 ),
        .Q(write_burst_counter_reg[9]),
        .R(burst_vaild1));
  LUT1 #(
    .INIT(2'h1)) 
    \write_index[0]_i_1 
       (.I0(write_index_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \write_index[1]_i_1 
       (.I0(write_index_reg[0]),
        .I1(write_index_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \write_index[2]_i_1 
       (.I0(write_index_reg[1]),
        .I1(write_index_reg[0]),
        .I2(write_index_reg[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \write_index[3]_i_1 
       (.I0(write_index_reg[2]),
        .I1(write_index_reg[0]),
        .I2(write_index_reg[1]),
        .I3(write_index_reg[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \write_index[4]_i_1 
       (.I0(write_index_reg[3]),
        .I1(write_index_reg[1]),
        .I2(write_index_reg[0]),
        .I3(write_index_reg[2]),
        .I4(write_index_reg[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \write_index[5]_i_1 
       (.I0(write_index_reg[4]),
        .I1(write_index_reg[2]),
        .I2(write_index_reg[0]),
        .I3(write_index_reg[1]),
        .I4(write_index_reg[3]),
        .I5(write_index_reg[5]),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \write_index[6]_i_1 
       (.I0(\write_index[7]_i_4_n_0 ),
        .I1(write_index_reg[6]),
        .O(p_0_in[6]));
  LUT4 #(
    .INIT(16'hFFF4)) 
    \write_index[7]_i_1 
       (.I0(fifo_rst_reg1),
        .I1(fifo_rst_reg2),
        .I2(ui_rst),
        .I3(start_single_burst_write_reg_n_0),
        .O(\write_index[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hEF000000)) 
    \write_index[7]_i_2 
       (.I0(write_index_reg[7]),
        .I1(\write_index[7]_i_4_n_0 ),
        .I2(write_index_reg[6]),
        .I3(axi_wvalid_reg_0),
        .I4(M_AXI_WREADY),
        .O(write_index0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h9A)) 
    \write_index[7]_i_3 
       (.I0(write_index_reg[7]),
        .I1(\write_index[7]_i_4_n_0 ),
        .I2(write_index_reg[6]),
        .O(p_0_in[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \write_index[7]_i_4 
       (.I0(write_index_reg[4]),
        .I1(write_index_reg[2]),
        .I2(write_index_reg[0]),
        .I3(write_index_reg[1]),
        .I4(write_index_reg[3]),
        .I5(write_index_reg[5]),
        .O(\write_index[7]_i_4_n_0 ));
  FDRE \write_index_reg[0] 
       (.C(M_AXI_ACLK),
        .CE(write_index0),
        .D(p_0_in[0]),
        .Q(write_index_reg[0]),
        .R(\write_index[7]_i_1_n_0 ));
  FDRE \write_index_reg[1] 
       (.C(M_AXI_ACLK),
        .CE(write_index0),
        .D(p_0_in[1]),
        .Q(write_index_reg[1]),
        .R(\write_index[7]_i_1_n_0 ));
  FDRE \write_index_reg[2] 
       (.C(M_AXI_ACLK),
        .CE(write_index0),
        .D(p_0_in[2]),
        .Q(write_index_reg[2]),
        .R(\write_index[7]_i_1_n_0 ));
  FDRE \write_index_reg[3] 
       (.C(M_AXI_ACLK),
        .CE(write_index0),
        .D(p_0_in[3]),
        .Q(write_index_reg[3]),
        .R(\write_index[7]_i_1_n_0 ));
  FDRE \write_index_reg[4] 
       (.C(M_AXI_ACLK),
        .CE(write_index0),
        .D(p_0_in[4]),
        .Q(write_index_reg[4]),
        .R(\write_index[7]_i_1_n_0 ));
  FDRE \write_index_reg[5] 
       (.C(M_AXI_ACLK),
        .CE(write_index0),
        .D(p_0_in[5]),
        .Q(write_index_reg[5]),
        .R(\write_index[7]_i_1_n_0 ));
  FDRE \write_index_reg[6] 
       (.C(M_AXI_ACLK),
        .CE(write_index0),
        .D(p_0_in[6]),
        .Q(write_index_reg[6]),
        .R(\write_index[7]_i_1_n_0 ));
  FDRE \write_index_reg[7] 
       (.C(M_AXI_ACLK),
        .CE(write_index0),
        .D(p_0_in[7]),
        .Q(write_index_reg[7]),
        .R(\write_index[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00100000)) 
    writes_done_i_1
       (.I0(write_burst_counter_reg[0]),
        .I1(write_burst_counter_reg[1]),
        .I2(write_burst_counter_reg[2]),
        .I3(\write_burst_counter[0]_i_3_n_0 ),
        .I4(writes_done_i_2_n_0),
        .I5(writes_done),
        .O(writes_done_i_1_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    writes_done_i_2
       (.I0(M_AXI_BVALID),
        .I1(M_AXI_BREADY),
        .O(writes_done_i_2_n_0));
  FDRE writes_done_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(writes_done_i_1_n_0),
        .Q(writes_done),
        .R(burst_vaild1));
endmodule

(* ORIG_REF_NAME = "hdmi_to_aximm" *) 
module system_hdmi_to_aximm_0_0_hdmi_to_aximm
   (M_AXI_BREADY,
    fifo_rst_reg_reg_0,
    M_AXI_AWADDR,
    axi_awvalid_reg,
    axi_wvalid_reg,
    rd_wdfifo_en,
    M_AXI_RREADY,
    M_AXI_WLAST,
    M_AXI_ACLK,
    hdmi_vsync,
    hdmi_clk,
    ui_rst,
    almost_empty,
    prog_empty,
    M_AXI_BVALID,
    M_AXI_AWREADY,
    M_AXI_WREADY,
    M_AXI_RLAST,
    M_AXI_RVALID);
  output M_AXI_BREADY;
  output fifo_rst_reg_reg_0;
  output [22:0]M_AXI_AWADDR;
  output axi_awvalid_reg;
  output axi_wvalid_reg;
  output rd_wdfifo_en;
  output M_AXI_RREADY;
  output M_AXI_WLAST;
  input M_AXI_ACLK;
  input hdmi_vsync;
  input hdmi_clk;
  input ui_rst;
  input almost_empty;
  input prog_empty;
  input M_AXI_BVALID;
  input M_AXI_AWREADY;
  input M_AXI_WREADY;
  input M_AXI_RLAST;
  input M_AXI_RVALID;

  wire M_AXI_ACLK;
  wire [22:0]M_AXI_AWADDR;
  wire M_AXI_AWREADY;
  wire M_AXI_BREADY;
  wire M_AXI_BVALID;
  wire M_AXI_RLAST;
  wire M_AXI_RREADY;
  wire M_AXI_RVALID;
  wire M_AXI_WLAST;
  wire M_AXI_WREADY;
  wire almost_empty;
  wire axi_awvalid_reg;
  wire axi_wvalid_reg;
  wire \cnt[0]_i_2_n_0 ;
  wire \cnt[0]_i_3_n_0 ;
  wire \cnt[0]_i_4_n_0 ;
  wire \cnt[0]_i_5_n_0 ;
  wire \cnt[12]_i_2_n_0 ;
  wire \cnt[12]_i_3_n_0 ;
  wire \cnt[12]_i_4_n_0 ;
  wire \cnt[12]_i_5_n_0 ;
  wire \cnt[16]_i_2_n_0 ;
  wire \cnt[16]_i_3_n_0 ;
  wire \cnt[16]_i_4_n_0 ;
  wire \cnt[16]_i_5_n_0 ;
  wire \cnt[20]_i_2_n_0 ;
  wire \cnt[20]_i_3_n_0 ;
  wire \cnt[20]_i_4_n_0 ;
  wire \cnt[20]_i_5_n_0 ;
  wire \cnt[24]_i_2_n_0 ;
  wire \cnt[24]_i_3_n_0 ;
  wire \cnt[24]_i_4_n_0 ;
  wire \cnt[24]_i_5_n_0 ;
  wire \cnt[28]_i_2_n_0 ;
  wire \cnt[28]_i_3_n_0 ;
  wire \cnt[28]_i_4_n_0 ;
  wire \cnt[28]_i_5_n_0 ;
  wire \cnt[4]_i_2_n_0 ;
  wire \cnt[4]_i_3_n_0 ;
  wire \cnt[4]_i_4_n_0 ;
  wire \cnt[4]_i_5_n_0 ;
  wire \cnt[8]_i_2_n_0 ;
  wire \cnt[8]_i_3_n_0 ;
  wire \cnt[8]_i_4_n_0 ;
  wire \cnt[8]_i_5_n_0 ;
  wire [31:0]cnt_reg;
  wire \cnt_reg[0]_i_1_n_0 ;
  wire \cnt_reg[0]_i_1_n_1 ;
  wire \cnt_reg[0]_i_1_n_2 ;
  wire \cnt_reg[0]_i_1_n_3 ;
  wire \cnt_reg[0]_i_1_n_4 ;
  wire \cnt_reg[0]_i_1_n_5 ;
  wire \cnt_reg[0]_i_1_n_6 ;
  wire \cnt_reg[0]_i_1_n_7 ;
  wire \cnt_reg[12]_i_1_n_0 ;
  wire \cnt_reg[12]_i_1_n_1 ;
  wire \cnt_reg[12]_i_1_n_2 ;
  wire \cnt_reg[12]_i_1_n_3 ;
  wire \cnt_reg[12]_i_1_n_4 ;
  wire \cnt_reg[12]_i_1_n_5 ;
  wire \cnt_reg[12]_i_1_n_6 ;
  wire \cnt_reg[12]_i_1_n_7 ;
  wire \cnt_reg[16]_i_1_n_0 ;
  wire \cnt_reg[16]_i_1_n_1 ;
  wire \cnt_reg[16]_i_1_n_2 ;
  wire \cnt_reg[16]_i_1_n_3 ;
  wire \cnt_reg[16]_i_1_n_4 ;
  wire \cnt_reg[16]_i_1_n_5 ;
  wire \cnt_reg[16]_i_1_n_6 ;
  wire \cnt_reg[16]_i_1_n_7 ;
  wire \cnt_reg[20]_i_1_n_0 ;
  wire \cnt_reg[20]_i_1_n_1 ;
  wire \cnt_reg[20]_i_1_n_2 ;
  wire \cnt_reg[20]_i_1_n_3 ;
  wire \cnt_reg[20]_i_1_n_4 ;
  wire \cnt_reg[20]_i_1_n_5 ;
  wire \cnt_reg[20]_i_1_n_6 ;
  wire \cnt_reg[20]_i_1_n_7 ;
  wire \cnt_reg[24]_i_1_n_0 ;
  wire \cnt_reg[24]_i_1_n_1 ;
  wire \cnt_reg[24]_i_1_n_2 ;
  wire \cnt_reg[24]_i_1_n_3 ;
  wire \cnt_reg[24]_i_1_n_4 ;
  wire \cnt_reg[24]_i_1_n_5 ;
  wire \cnt_reg[24]_i_1_n_6 ;
  wire \cnt_reg[24]_i_1_n_7 ;
  wire \cnt_reg[28]_i_1_n_1 ;
  wire \cnt_reg[28]_i_1_n_2 ;
  wire \cnt_reg[28]_i_1_n_3 ;
  wire \cnt_reg[28]_i_1_n_4 ;
  wire \cnt_reg[28]_i_1_n_5 ;
  wire \cnt_reg[28]_i_1_n_6 ;
  wire \cnt_reg[28]_i_1_n_7 ;
  wire \cnt_reg[4]_i_1_n_0 ;
  wire \cnt_reg[4]_i_1_n_1 ;
  wire \cnt_reg[4]_i_1_n_2 ;
  wire \cnt_reg[4]_i_1_n_3 ;
  wire \cnt_reg[4]_i_1_n_4 ;
  wire \cnt_reg[4]_i_1_n_5 ;
  wire \cnt_reg[4]_i_1_n_6 ;
  wire \cnt_reg[4]_i_1_n_7 ;
  wire \cnt_reg[8]_i_1_n_0 ;
  wire \cnt_reg[8]_i_1_n_1 ;
  wire \cnt_reg[8]_i_1_n_2 ;
  wire \cnt_reg[8]_i_1_n_3 ;
  wire \cnt_reg[8]_i_1_n_4 ;
  wire \cnt_reg[8]_i_1_n_5 ;
  wire \cnt_reg[8]_i_1_n_6 ;
  wire \cnt_reg[8]_i_1_n_7 ;
  wire fifo_rst_pulse;
  wire fifo_rst_reg1;
  wire fifo_rst_reg2;
  wire fifo_rst_reg_i_1_n_0;
  wire fifo_rst_reg_i_3_n_0;
  wire fifo_rst_reg_i_4_n_0;
  wire fifo_rst_reg_i_5_n_0;
  wire fifo_rst_reg_i_6_n_0;
  wire fifo_rst_reg_i_7_n_0;
  wire fifo_rst_reg_i_8_n_0;
  wire fifo_rst_reg_i_9_n_0;
  wire fifo_rst_reg_reg_0;
  wire hdmi_clk;
  wire hdmi_vsync;
  wire hdmi_vsync_reg;
  wire hdmi_vsync_reg1;
  wire prog_empty;
  wire rd_wdfifo_en;
  wire ui_rst;
  wire [3:3]\NLW_cnt_reg[28]_i_1_CO_UNCONNECTED ;

  system_hdmi_to_aximm_0_0_AXI_DDR_RW_HDMI AXI_DDR_RW_HDMI
       (.M_AXI_ACLK(M_AXI_ACLK),
        .M_AXI_RLAST(M_AXI_RLAST),
        .M_AXI_RREADY(M_AXI_RREADY),
        .M_AXI_RVALID(M_AXI_RVALID));
  system_hdmi_to_aximm_0_0_AXI_DDR_WD_HDMI AXI_DDR_WD_HDMI
       (.M_AXI_ACLK(M_AXI_ACLK),
        .M_AXI_AWADDR(M_AXI_AWADDR),
        .M_AXI_AWREADY(M_AXI_AWREADY),
        .M_AXI_BREADY(M_AXI_BREADY),
        .M_AXI_BVALID(M_AXI_BVALID),
        .M_AXI_WLAST(M_AXI_WLAST),
        .M_AXI_WREADY(M_AXI_WREADY),
        .almost_empty(almost_empty),
        .axi_awvalid_reg_0(axi_awvalid_reg),
        .axi_wvalid_reg_0(axi_wvalid_reg),
        .fifo_rst_reg1(fifo_rst_reg1),
        .fifo_rst_reg2(fifo_rst_reg2),
        .prog_empty(prog_empty),
        .rd_wdfifo_en(rd_wdfifo_en),
        .ui_rst(ui_rst));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[0]_i_2 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[3]),
        .O(\cnt[0]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[0]_i_3 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[2]),
        .O(\cnt[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[0]_i_4 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[1]),
        .O(\cnt[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \cnt[0]_i_5 
       (.I0(cnt_reg[0]),
        .I1(fifo_rst_reg_reg_0),
        .O(\cnt[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[12]_i_2 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[15]),
        .O(\cnt[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[12]_i_3 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[14]),
        .O(\cnt[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[12]_i_4 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[13]),
        .O(\cnt[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[12]_i_5 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[12]),
        .O(\cnt[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[16]_i_2 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[19]),
        .O(\cnt[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[16]_i_3 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[18]),
        .O(\cnt[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[16]_i_4 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[17]),
        .O(\cnt[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[16]_i_5 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[16]),
        .O(\cnt[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[20]_i_2 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[23]),
        .O(\cnt[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[20]_i_3 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[22]),
        .O(\cnt[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[20]_i_4 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[21]),
        .O(\cnt[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[20]_i_5 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[20]),
        .O(\cnt[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[24]_i_2 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[27]),
        .O(\cnt[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[24]_i_3 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[26]),
        .O(\cnt[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[24]_i_4 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[25]),
        .O(\cnt[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[24]_i_5 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[24]),
        .O(\cnt[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[28]_i_2 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[31]),
        .O(\cnt[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[28]_i_3 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[30]),
        .O(\cnt[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[28]_i_4 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[29]),
        .O(\cnt[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[28]_i_5 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[28]),
        .O(\cnt[28]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[4]_i_2 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[7]),
        .O(\cnt[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[4]_i_3 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[6]),
        .O(\cnt[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[4]_i_4 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[5]),
        .O(\cnt[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[4]_i_5 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[4]),
        .O(\cnt[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[8]_i_2 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[11]),
        .O(\cnt[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[8]_i_3 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[10]),
        .O(\cnt[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[8]_i_4 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[9]),
        .O(\cnt[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt[8]_i_5 
       (.I0(fifo_rst_reg_reg_0),
        .I1(cnt_reg[8]),
        .O(\cnt[8]_i_5_n_0 ));
  FDCE \cnt_reg[0] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[0]_i_1_n_7 ),
        .Q(cnt_reg[0]));
  CARRY4 \cnt_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\cnt_reg[0]_i_1_n_0 ,\cnt_reg[0]_i_1_n_1 ,\cnt_reg[0]_i_1_n_2 ,\cnt_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,fifo_rst_reg_reg_0}),
        .O({\cnt_reg[0]_i_1_n_4 ,\cnt_reg[0]_i_1_n_5 ,\cnt_reg[0]_i_1_n_6 ,\cnt_reg[0]_i_1_n_7 }),
        .S({\cnt[0]_i_2_n_0 ,\cnt[0]_i_3_n_0 ,\cnt[0]_i_4_n_0 ,\cnt[0]_i_5_n_0 }));
  FDCE \cnt_reg[10] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[8]_i_1_n_5 ),
        .Q(cnt_reg[10]));
  FDCE \cnt_reg[11] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[8]_i_1_n_4 ),
        .Q(cnt_reg[11]));
  FDCE \cnt_reg[12] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[12]_i_1_n_7 ),
        .Q(cnt_reg[12]));
  CARRY4 \cnt_reg[12]_i_1 
       (.CI(\cnt_reg[8]_i_1_n_0 ),
        .CO({\cnt_reg[12]_i_1_n_0 ,\cnt_reg[12]_i_1_n_1 ,\cnt_reg[12]_i_1_n_2 ,\cnt_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_1_n_4 ,\cnt_reg[12]_i_1_n_5 ,\cnt_reg[12]_i_1_n_6 ,\cnt_reg[12]_i_1_n_7 }),
        .S({\cnt[12]_i_2_n_0 ,\cnt[12]_i_3_n_0 ,\cnt[12]_i_4_n_0 ,\cnt[12]_i_5_n_0 }));
  FDCE \cnt_reg[13] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[12]_i_1_n_6 ),
        .Q(cnt_reg[13]));
  FDCE \cnt_reg[14] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[12]_i_1_n_5 ),
        .Q(cnt_reg[14]));
  FDCE \cnt_reg[15] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[12]_i_1_n_4 ),
        .Q(cnt_reg[15]));
  FDCE \cnt_reg[16] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[16]_i_1_n_7 ),
        .Q(cnt_reg[16]));
  CARRY4 \cnt_reg[16]_i_1 
       (.CI(\cnt_reg[12]_i_1_n_0 ),
        .CO({\cnt_reg[16]_i_1_n_0 ,\cnt_reg[16]_i_1_n_1 ,\cnt_reg[16]_i_1_n_2 ,\cnt_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[16]_i_1_n_4 ,\cnt_reg[16]_i_1_n_5 ,\cnt_reg[16]_i_1_n_6 ,\cnt_reg[16]_i_1_n_7 }),
        .S({\cnt[16]_i_2_n_0 ,\cnt[16]_i_3_n_0 ,\cnt[16]_i_4_n_0 ,\cnt[16]_i_5_n_0 }));
  FDCE \cnt_reg[17] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[16]_i_1_n_6 ),
        .Q(cnt_reg[17]));
  FDCE \cnt_reg[18] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[16]_i_1_n_5 ),
        .Q(cnt_reg[18]));
  FDCE \cnt_reg[19] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[16]_i_1_n_4 ),
        .Q(cnt_reg[19]));
  FDCE \cnt_reg[1] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[0]_i_1_n_6 ),
        .Q(cnt_reg[1]));
  FDCE \cnt_reg[20] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[20]_i_1_n_7 ),
        .Q(cnt_reg[20]));
  CARRY4 \cnt_reg[20]_i_1 
       (.CI(\cnt_reg[16]_i_1_n_0 ),
        .CO({\cnt_reg[20]_i_1_n_0 ,\cnt_reg[20]_i_1_n_1 ,\cnt_reg[20]_i_1_n_2 ,\cnt_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[20]_i_1_n_4 ,\cnt_reg[20]_i_1_n_5 ,\cnt_reg[20]_i_1_n_6 ,\cnt_reg[20]_i_1_n_7 }),
        .S({\cnt[20]_i_2_n_0 ,\cnt[20]_i_3_n_0 ,\cnt[20]_i_4_n_0 ,\cnt[20]_i_5_n_0 }));
  FDCE \cnt_reg[21] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[20]_i_1_n_6 ),
        .Q(cnt_reg[21]));
  FDCE \cnt_reg[22] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[20]_i_1_n_5 ),
        .Q(cnt_reg[22]));
  FDCE \cnt_reg[23] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[20]_i_1_n_4 ),
        .Q(cnt_reg[23]));
  FDCE \cnt_reg[24] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[24]_i_1_n_7 ),
        .Q(cnt_reg[24]));
  CARRY4 \cnt_reg[24]_i_1 
       (.CI(\cnt_reg[20]_i_1_n_0 ),
        .CO({\cnt_reg[24]_i_1_n_0 ,\cnt_reg[24]_i_1_n_1 ,\cnt_reg[24]_i_1_n_2 ,\cnt_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[24]_i_1_n_4 ,\cnt_reg[24]_i_1_n_5 ,\cnt_reg[24]_i_1_n_6 ,\cnt_reg[24]_i_1_n_7 }),
        .S({\cnt[24]_i_2_n_0 ,\cnt[24]_i_3_n_0 ,\cnt[24]_i_4_n_0 ,\cnt[24]_i_5_n_0 }));
  FDCE \cnt_reg[25] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[24]_i_1_n_6 ),
        .Q(cnt_reg[25]));
  FDCE \cnt_reg[26] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[24]_i_1_n_5 ),
        .Q(cnt_reg[26]));
  FDCE \cnt_reg[27] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[24]_i_1_n_4 ),
        .Q(cnt_reg[27]));
  FDCE \cnt_reg[28] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[28]_i_1_n_7 ),
        .Q(cnt_reg[28]));
  CARRY4 \cnt_reg[28]_i_1 
       (.CI(\cnt_reg[24]_i_1_n_0 ),
        .CO({\NLW_cnt_reg[28]_i_1_CO_UNCONNECTED [3],\cnt_reg[28]_i_1_n_1 ,\cnt_reg[28]_i_1_n_2 ,\cnt_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[28]_i_1_n_4 ,\cnt_reg[28]_i_1_n_5 ,\cnt_reg[28]_i_1_n_6 ,\cnt_reg[28]_i_1_n_7 }),
        .S({\cnt[28]_i_2_n_0 ,\cnt[28]_i_3_n_0 ,\cnt[28]_i_4_n_0 ,\cnt[28]_i_5_n_0 }));
  FDCE \cnt_reg[29] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[28]_i_1_n_6 ),
        .Q(cnt_reg[29]));
  FDCE \cnt_reg[2] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[0]_i_1_n_5 ),
        .Q(cnt_reg[2]));
  FDCE \cnt_reg[30] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[28]_i_1_n_5 ),
        .Q(cnt_reg[30]));
  FDCE \cnt_reg[31] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[28]_i_1_n_4 ),
        .Q(cnt_reg[31]));
  FDCE \cnt_reg[3] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[0]_i_1_n_4 ),
        .Q(cnt_reg[3]));
  FDCE \cnt_reg[4] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[4]_i_1_n_7 ),
        .Q(cnt_reg[4]));
  CARRY4 \cnt_reg[4]_i_1 
       (.CI(\cnt_reg[0]_i_1_n_0 ),
        .CO({\cnt_reg[4]_i_1_n_0 ,\cnt_reg[4]_i_1_n_1 ,\cnt_reg[4]_i_1_n_2 ,\cnt_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_1_n_4 ,\cnt_reg[4]_i_1_n_5 ,\cnt_reg[4]_i_1_n_6 ,\cnt_reg[4]_i_1_n_7 }),
        .S({\cnt[4]_i_2_n_0 ,\cnt[4]_i_3_n_0 ,\cnt[4]_i_4_n_0 ,\cnt[4]_i_5_n_0 }));
  FDCE \cnt_reg[5] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[4]_i_1_n_6 ),
        .Q(cnt_reg[5]));
  FDCE \cnt_reg[6] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[4]_i_1_n_5 ),
        .Q(cnt_reg[6]));
  FDCE \cnt_reg[7] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[4]_i_1_n_4 ),
        .Q(cnt_reg[7]));
  FDCE \cnt_reg[8] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[8]_i_1_n_7 ),
        .Q(cnt_reg[8]));
  CARRY4 \cnt_reg[8]_i_1 
       (.CI(\cnt_reg[4]_i_1_n_0 ),
        .CO({\cnt_reg[8]_i_1_n_0 ,\cnt_reg[8]_i_1_n_1 ,\cnt_reg[8]_i_1_n_2 ,\cnt_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_1_n_4 ,\cnt_reg[8]_i_1_n_5 ,\cnt_reg[8]_i_1_n_6 ,\cnt_reg[8]_i_1_n_7 }),
        .S({\cnt[8]_i_2_n_0 ,\cnt[8]_i_3_n_0 ,\cnt[8]_i_4_n_0 ,\cnt[8]_i_5_n_0 }));
  FDCE \cnt_reg[9] 
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(\cnt_reg[8]_i_1_n_6 ),
        .Q(cnt_reg[9]));
  FDRE fifo_rst_reg1_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(fifo_rst_reg_reg_0),
        .Q(fifo_rst_reg1),
        .R(1'b0));
  FDRE fifo_rst_reg2_reg
       (.C(M_AXI_ACLK),
        .CE(1'b1),
        .D(fifo_rst_reg1),
        .Q(fifo_rst_reg2),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAABAAAAAAA8)) 
    fifo_rst_reg_i_1
       (.I0(fifo_rst_pulse),
        .I1(fifo_rst_reg_i_3_n_0),
        .I2(fifo_rst_reg_i_4_n_0),
        .I3(fifo_rst_reg_i_5_n_0),
        .I4(fifo_rst_reg_i_6_n_0),
        .I5(fifo_rst_reg_reg_0),
        .O(fifo_rst_reg_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    fifo_rst_reg_i_2
       (.I0(hdmi_vsync_reg1),
        .I1(hdmi_vsync_reg),
        .O(fifo_rst_pulse));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    fifo_rst_reg_i_3
       (.I0(cnt_reg[15]),
        .I1(cnt_reg[16]),
        .I2(cnt_reg[13]),
        .I3(cnt_reg[14]),
        .I4(cnt_reg[12]),
        .I5(cnt_reg[11]),
        .O(fifo_rst_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    fifo_rst_reg_i_4
       (.I0(cnt_reg[21]),
        .I1(cnt_reg[22]),
        .I2(cnt_reg[19]),
        .I3(cnt_reg[20]),
        .I4(cnt_reg[18]),
        .I5(cnt_reg[17]),
        .O(fifo_rst_reg_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    fifo_rst_reg_i_5
       (.I0(cnt_reg[27]),
        .I1(cnt_reg[28]),
        .I2(cnt_reg[25]),
        .I3(cnt_reg[26]),
        .I4(cnt_reg[24]),
        .I5(cnt_reg[23]),
        .O(fifo_rst_reg_i_5_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    fifo_rst_reg_i_6
       (.I0(cnt_reg[9]),
        .I1(cnt_reg[10]),
        .I2(cnt_reg[7]),
        .I3(cnt_reg[8]),
        .I4(fifo_rst_reg_i_7_n_0),
        .I5(fifo_rst_reg_i_8_n_0),
        .O(fifo_rst_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'hFFFF00F088F800F0)) 
    fifo_rst_reg_i_7
       (.I0(cnt_reg[1]),
        .I1(cnt_reg[0]),
        .I2(hdmi_vsync_reg1),
        .I3(hdmi_vsync_reg),
        .I4(fifo_rst_reg_i_9_n_0),
        .I5(cnt_reg[4]),
        .O(fifo_rst_reg_i_7_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFA8)) 
    fifo_rst_reg_i_8
       (.I0(fifo_rst_reg_i_9_n_0),
        .I1(cnt_reg[3]),
        .I2(cnt_reg[2]),
        .I3(cnt_reg[29]),
        .I4(cnt_reg[30]),
        .I5(cnt_reg[31]),
        .O(fifo_rst_reg_i_8_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    fifo_rst_reg_i_9
       (.I0(cnt_reg[5]),
        .I1(cnt_reg[6]),
        .O(fifo_rst_reg_i_9_n_0));
  FDCE fifo_rst_reg_reg
       (.C(hdmi_clk),
        .CE(1'b1),
        .CLR(ui_rst),
        .D(fifo_rst_reg_i_1_n_0),
        .Q(fifo_rst_reg_reg_0));
  FDRE hdmi_vsync_reg1_reg
       (.C(hdmi_clk),
        .CE(1'b1),
        .D(hdmi_vsync_reg),
        .Q(hdmi_vsync_reg1),
        .R(1'b0));
  FDRE hdmi_vsync_reg_reg
       (.C(hdmi_clk),
        .CE(1'b1),
        .D(hdmi_vsync),
        .Q(hdmi_vsync_reg),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
