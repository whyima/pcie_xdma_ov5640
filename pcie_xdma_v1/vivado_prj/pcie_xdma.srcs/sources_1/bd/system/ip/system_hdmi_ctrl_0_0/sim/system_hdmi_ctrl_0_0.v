// (c) Copyright 1995-2023 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:module_ref:hdmi_ctrl:1.0
// IP Revision: 1

`timescale 1ns/1ps

(* IP_DEFINITION_SOURCE = "module_ref" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module system_hdmi_ctrl_0_0 (
  sys_clk,
  sys_rst_n,
  sys_init_done,
  ddc_scl,
  ddc_sda,
  hdmi_in_clk,
  hdmi_in_rst_n,
  hdmi_in_hsync,
  hdmi_in_vsync,
  hdmi_in_rgb,
  hdmi_in_de,
  fifo_wr_en,
  cfg_done,
  hdmi_out_clk,
  hdmi_out_rst_n,
  hdmi_out_hsync,
  hdmi_out_vsync,
  hdmi_out_rgb,
  hdmi_out_de,
  h_cnt,
  v_cnt
);

(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sys_clk, ASSOCIATED_RESET sys_rst_n, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_sys_clk, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 sys_clk CLK" *)
input wire sys_clk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sys_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 sys_rst_n RST" *)
input wire sys_rst_n;
input wire sys_init_done;
output wire ddc_scl;
inout wire ddc_sda;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME hdmi_in_clk, ASSOCIATED_RESET hdmi_in_rst_n, FREQ_HZ 148500000, PHASE 0.000, CLK_DOMAIN system_hdmi_in_clk_0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 hdmi_in_clk CLK" *)
input wire hdmi_in_clk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME hdmi_in_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 hdmi_in_rst_n RST" *)
output wire hdmi_in_rst_n;
input wire hdmi_in_hsync;
input wire hdmi_in_vsync;
input wire [23 : 0] hdmi_in_rgb;
input wire hdmi_in_de;
output wire fifo_wr_en;
output wire cfg_done;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME hdmi_out_clk, ASSOCIATED_RESET hdmi_out_rst_n, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_hdmi_ctrl_0_0_hdmi_out_clk, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 hdmi_out_clk CLK" *)
output wire hdmi_out_clk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME hdmi_out_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 hdmi_out_rst_n RST" *)
output wire hdmi_out_rst_n;
output wire hdmi_out_hsync;
output wire hdmi_out_vsync;
output wire [31 : 0] hdmi_out_rgb;
output wire hdmi_out_de;
output wire [31 : 0] h_cnt;
output wire [31 : 0] v_cnt;

  hdmi_ctrl inst (
    .sys_clk(sys_clk),
    .sys_rst_n(sys_rst_n),
    .sys_init_done(sys_init_done),
    .ddc_scl(ddc_scl),
    .ddc_sda(ddc_sda),
    .hdmi_in_clk(hdmi_in_clk),
    .hdmi_in_rst_n(hdmi_in_rst_n),
    .hdmi_in_hsync(hdmi_in_hsync),
    .hdmi_in_vsync(hdmi_in_vsync),
    .hdmi_in_rgb(hdmi_in_rgb),
    .hdmi_in_de(hdmi_in_de),
    .fifo_wr_en(fifo_wr_en),
    .cfg_done(cfg_done),
    .hdmi_out_clk(hdmi_out_clk),
    .hdmi_out_rst_n(hdmi_out_rst_n),
    .hdmi_out_hsync(hdmi_out_hsync),
    .hdmi_out_vsync(hdmi_out_vsync),
    .hdmi_out_rgb(hdmi_out_rgb),
    .hdmi_out_de(hdmi_out_de),
    .h_cnt(h_cnt),
    .v_cnt(v_cnt)
  );
endmodule
