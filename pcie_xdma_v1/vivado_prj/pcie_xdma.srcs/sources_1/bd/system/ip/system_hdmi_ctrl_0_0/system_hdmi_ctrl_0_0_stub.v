// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Mon Dec 25 15:59:50 2023
// Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               E:/Workspace/Vivado/2019.2/pcie_xdma_v1/vivado_prj/pcie_xdma.srcs/sources_1/bd/system/ip/system_hdmi_ctrl_0_0/system_hdmi_ctrl_0_0_stub.v
// Design      : system_hdmi_ctrl_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "hdmi_ctrl,Vivado 2019.2" *)
module system_hdmi_ctrl_0_0(sys_clk, sys_rst_n, sys_init_done, ddc_scl, 
  ddc_sda, hdmi_in_clk, hdmi_in_rst_n, hdmi_in_hsync, hdmi_in_vsync, hdmi_in_rgb, hdmi_in_de, 
  fifo_wr_en, cfg_done, hdmi_out_clk, hdmi_out_rst_n, hdmi_out_hsync, hdmi_out_vsync, 
  hdmi_out_rgb, hdmi_out_de, h_cnt, v_cnt)
/* synthesis syn_black_box black_box_pad_pin="sys_clk,sys_rst_n,sys_init_done,ddc_scl,ddc_sda,hdmi_in_clk,hdmi_in_rst_n,hdmi_in_hsync,hdmi_in_vsync,hdmi_in_rgb[23:0],hdmi_in_de,fifo_wr_en,cfg_done,hdmi_out_clk,hdmi_out_rst_n,hdmi_out_hsync,hdmi_out_vsync,hdmi_out_rgb[31:0],hdmi_out_de,h_cnt[31:0],v_cnt[31:0]" */;
  input sys_clk;
  input sys_rst_n;
  input sys_init_done;
  output ddc_scl;
  inout ddc_sda;
  input hdmi_in_clk;
  output hdmi_in_rst_n;
  input hdmi_in_hsync;
  input hdmi_in_vsync;
  input [23:0]hdmi_in_rgb;
  input hdmi_in_de;
  output fifo_wr_en;
  output cfg_done;
  output hdmi_out_clk;
  output hdmi_out_rst_n;
  output hdmi_out_hsync;
  output hdmi_out_vsync;
  output [31:0]hdmi_out_rgb;
  output hdmi_out_de;
  output [31:0]h_cnt;
  output [31:0]v_cnt;
endmodule
