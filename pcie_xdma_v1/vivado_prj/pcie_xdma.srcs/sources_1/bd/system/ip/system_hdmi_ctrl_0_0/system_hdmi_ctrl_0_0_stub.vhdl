-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
-- Date        : Mon Dec 25 15:59:50 2023
-- Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               E:/Workspace/Vivado/2019.2/pcie_xdma_v1/vivado_prj/pcie_xdma.srcs/sources_1/bd/system/ip/system_hdmi_ctrl_0_0/system_hdmi_ctrl_0_0_stub.vhdl
-- Design      : system_hdmi_ctrl_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity system_hdmi_ctrl_0_0 is
  Port ( 
    sys_clk : in STD_LOGIC;
    sys_rst_n : in STD_LOGIC;
    sys_init_done : in STD_LOGIC;
    ddc_scl : out STD_LOGIC;
    ddc_sda : inout STD_LOGIC;
    hdmi_in_clk : in STD_LOGIC;
    hdmi_in_rst_n : out STD_LOGIC;
    hdmi_in_hsync : in STD_LOGIC;
    hdmi_in_vsync : in STD_LOGIC;
    hdmi_in_rgb : in STD_LOGIC_VECTOR ( 23 downto 0 );
    hdmi_in_de : in STD_LOGIC;
    fifo_wr_en : out STD_LOGIC;
    cfg_done : out STD_LOGIC;
    hdmi_out_clk : out STD_LOGIC;
    hdmi_out_rst_n : out STD_LOGIC;
    hdmi_out_hsync : out STD_LOGIC;
    hdmi_out_vsync : out STD_LOGIC;
    hdmi_out_rgb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    hdmi_out_de : out STD_LOGIC;
    h_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 );
    v_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );

end system_hdmi_ctrl_0_0;

architecture stub of system_hdmi_ctrl_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "sys_clk,sys_rst_n,sys_init_done,ddc_scl,ddc_sda,hdmi_in_clk,hdmi_in_rst_n,hdmi_in_hsync,hdmi_in_vsync,hdmi_in_rgb[23:0],hdmi_in_de,fifo_wr_en,cfg_done,hdmi_out_clk,hdmi_out_rst_n,hdmi_out_hsync,hdmi_out_vsync,hdmi_out_rgb[31:0],hdmi_out_de,h_cnt[31:0],v_cnt[31:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "hdmi_ctrl,Vivado 2019.2";
begin
end;
