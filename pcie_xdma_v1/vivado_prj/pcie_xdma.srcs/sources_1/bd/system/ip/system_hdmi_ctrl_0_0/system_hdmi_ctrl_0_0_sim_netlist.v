// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
// Date        : Mon Dec 25 15:59:50 2023
// Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               E:/Workspace/Vivado/2019.2/pcie_xdma_v1/vivado_prj/pcie_xdma.srcs/sources_1/bd/system/ip/system_hdmi_ctrl_0_0/system_hdmi_ctrl_0_0_sim_netlist.v
// Design      : system_hdmi_ctrl_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "system_hdmi_ctrl_0_0,hdmi_ctrl,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "module_ref" *) 
(* X_CORE_INFO = "hdmi_ctrl,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module system_hdmi_ctrl_0_0
   (sys_clk,
    sys_rst_n,
    sys_init_done,
    ddc_scl,
    ddc_sda,
    hdmi_in_clk,
    hdmi_in_rst_n,
    hdmi_in_hsync,
    hdmi_in_vsync,
    hdmi_in_rgb,
    hdmi_in_de,
    fifo_wr_en,
    cfg_done,
    hdmi_out_clk,
    hdmi_out_rst_n,
    hdmi_out_hsync,
    hdmi_out_vsync,
    hdmi_out_rgb,
    hdmi_out_de,
    h_cnt,
    v_cnt);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 sys_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sys_clk, ASSOCIATED_RESET sys_rst_n, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_sys_clk, INSERT_VIP 0" *) input sys_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 sys_rst_n RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME sys_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input sys_rst_n;
  input sys_init_done;
  output ddc_scl;
  inout ddc_sda;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 hdmi_in_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME hdmi_in_clk, ASSOCIATED_RESET hdmi_in_rst_n, FREQ_HZ 148500000, PHASE 0.000, CLK_DOMAIN system_hdmi_in_clk_0, INSERT_VIP 0" *) input hdmi_in_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 hdmi_in_rst_n RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME hdmi_in_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) output hdmi_in_rst_n;
  input hdmi_in_hsync;
  input hdmi_in_vsync;
  input [23:0]hdmi_in_rgb;
  input hdmi_in_de;
  output fifo_wr_en;
  output cfg_done;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 hdmi_out_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME hdmi_out_clk, ASSOCIATED_RESET hdmi_out_rst_n, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_hdmi_ctrl_0_0_hdmi_out_clk, INSERT_VIP 0" *) output hdmi_out_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 hdmi_out_rst_n RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME hdmi_out_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) output hdmi_out_rst_n;
  output hdmi_out_hsync;
  output hdmi_out_vsync;
  output [31:0]hdmi_out_rgb;
  output hdmi_out_de;
  output [31:0]h_cnt;
  output [31:0]v_cnt;

  wire \<const0> ;
  wire cfg_done;
  wire ddc_scl;
  wire ddc_sda;
  wire fifo_wr_en;
  wire [31:0]h_cnt;
  wire hdmi_in_clk;
  wire hdmi_in_de;
  wire hdmi_in_hsync;
  wire [23:0]hdmi_in_rgb;
  wire hdmi_in_vsync;
  wire sys_clk;
  wire sys_init_done;
  wire sys_rst_n;
  wire [31:0]v_cnt;

  assign hdmi_in_rst_n = sys_rst_n;
  assign hdmi_out_clk = hdmi_in_clk;
  assign hdmi_out_de = hdmi_in_de;
  assign hdmi_out_hsync = hdmi_in_hsync;
  assign hdmi_out_rgb[31] = \<const0> ;
  assign hdmi_out_rgb[30] = \<const0> ;
  assign hdmi_out_rgb[29] = \<const0> ;
  assign hdmi_out_rgb[28] = \<const0> ;
  assign hdmi_out_rgb[27] = \<const0> ;
  assign hdmi_out_rgb[26] = \<const0> ;
  assign hdmi_out_rgb[25] = \<const0> ;
  assign hdmi_out_rgb[24] = \<const0> ;
  assign hdmi_out_rgb[23:0] = hdmi_in_rgb;
  assign hdmi_out_rst_n = sys_rst_n;
  assign hdmi_out_vsync = hdmi_in_vsync;
  GND GND
       (.G(\<const0> ));
  system_hdmi_ctrl_0_0_hdmi_ctrl inst
       (.cfg_done_reg(cfg_done),
        .ddc_scl(ddc_scl),
        .ddc_sda(ddc_sda),
        .fifo_wr_en(fifo_wr_en),
        .h_cnt(h_cnt),
        .hdmi_in_clk(hdmi_in_clk),
        .hdmi_in_de(hdmi_in_de),
        .hdmi_in_vsync(hdmi_in_vsync),
        .sys_clk(sys_clk),
        .sys_init_done(sys_init_done),
        .sys_rst_n(sys_rst_n),
        .v_cnt(v_cnt));
endmodule

(* ORIG_REF_NAME = "hdmi_cfg" *) 
module system_hdmi_ctrl_0_0_hdmi_cfg
   (cfg_start,
    cfg_done_reg_0,
    \reg_num_reg[1]_0 ,
    Q,
    \reg_num_reg[1]_1 ,
    \reg_num_reg[1]_2 ,
    CLK,
    cfg_start_reg_0,
    i2c_sda_reg_reg_i_3,
    i2c_sda_reg_reg_i_3_0,
    i2c_sda_reg_reg_i_3_1,
    E);
  output cfg_start;
  output cfg_done_reg_0;
  output \reg_num_reg[1]_0 ;
  output [2:0]Q;
  output \reg_num_reg[1]_1 ;
  output \reg_num_reg[1]_2 ;
  input CLK;
  input cfg_start_reg_0;
  input i2c_sda_reg_reg_i_3;
  input i2c_sda_reg_reg_i_3_0;
  input i2c_sda_reg_reg_i_3_1;
  input [0:0]E;

  wire CLK;
  wire [0:0]E;
  wire [2:0]Q;
  wire cfg_done_i_1_n_0;
  wire cfg_done_i_3_n_0;
  wire cfg_done_reg_0;
  wire cfg_start;
  wire cfg_start_0;
  wire cfg_start_i_2_n_0;
  wire cfg_start_i_3_n_0;
  wire cfg_start_i_4_n_0;
  wire cfg_start_reg_0;
  wire \cnt_wait[10]_i_3_n_0 ;
  wire \cnt_wait[10]_i_4_n_0 ;
  wire [10:0]cnt_wait_reg;
  wire i2c_sda_reg_reg_i_3;
  wire i2c_sda_reg_reg_i_3_0;
  wire i2c_sda_reg_reg_i_3_1;
  wire [6:0]p_0_in;
  wire [10:0]p_0_in__0;
  wire [6:3]reg_num_reg;
  wire \reg_num_reg[1]_0 ;
  wire \reg_num_reg[1]_1 ;
  wire \reg_num_reg[1]_2 ;
  wire sel;

  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFF80)) 
    cfg_done_i_1
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(cfg_done_i_3_n_0),
        .I3(cfg_done_reg_0),
        .O(cfg_done_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    cfg_done_i_3
       (.I0(reg_num_reg[3]),
        .I1(reg_num_reg[4]),
        .I2(E),
        .I3(Q[0]),
        .I4(reg_num_reg[6]),
        .I5(reg_num_reg[5]),
        .O(cfg_done_i_3_n_0));
  FDCE cfg_done_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(cfg_start_reg_0),
        .D(cfg_done_i_1_n_0),
        .Q(cfg_done_reg_0));
  LUT4 #(
    .INIT(16'hFF02)) 
    cfg_start_i_1
       (.I0(cfg_start_i_2_n_0),
        .I1(cfg_start_i_3_n_0),
        .I2(cnt_wait_reg[4]),
        .I3(cfg_start_i_4_n_0),
        .O(cfg_start_0));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    cfg_start_i_2
       (.I0(cnt_wait_reg[7]),
        .I1(cnt_wait_reg[8]),
        .I2(cnt_wait_reg[6]),
        .I3(cnt_wait_reg[5]),
        .I4(cnt_wait_reg[10]),
        .I5(cnt_wait_reg[9]),
        .O(cfg_start_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    cfg_start_i_3
       (.I0(cnt_wait_reg[2]),
        .I1(cnt_wait_reg[0]),
        .I2(cnt_wait_reg[1]),
        .I3(cnt_wait_reg[3]),
        .O(cfg_start_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    cfg_start_i_4
       (.I0(reg_num_reg[5]),
        .I1(reg_num_reg[6]),
        .I2(\reg_num_reg[1]_1 ),
        .I3(E),
        .I4(reg_num_reg[3]),
        .I5(reg_num_reg[4]),
        .O(cfg_start_i_4_n_0));
  FDCE cfg_start_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(cfg_start_reg_0),
        .D(cfg_start_0),
        .Q(cfg_start));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_wait[0]_i_1 
       (.I0(cnt_wait_reg[0]),
        .O(p_0_in__0[0]));
  LUT6 #(
    .INIT(64'h57FFFFFFFFFFFFFF)) 
    \cnt_wait[10]_i_1 
       (.I0(\cnt_wait[10]_i_3_n_0 ),
        .I1(cnt_wait_reg[5]),
        .I2(cnt_wait_reg[4]),
        .I3(cnt_wait_reg[10]),
        .I4(cnt_wait_reg[9]),
        .I5(cnt_wait_reg[6]),
        .O(sel));
  LUT6 #(
    .INIT(64'hF7FFFFFF08000000)) 
    \cnt_wait[10]_i_2 
       (.I0(cnt_wait_reg[9]),
        .I1(cnt_wait_reg[7]),
        .I2(\cnt_wait[10]_i_4_n_0 ),
        .I3(cnt_wait_reg[6]),
        .I4(cnt_wait_reg[8]),
        .I5(cnt_wait_reg[10]),
        .O(p_0_in__0[10]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_wait[10]_i_3 
       (.I0(cnt_wait_reg[7]),
        .I1(cnt_wait_reg[8]),
        .O(\cnt_wait[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \cnt_wait[10]_i_4 
       (.I0(cnt_wait_reg[4]),
        .I1(cnt_wait_reg[2]),
        .I2(cnt_wait_reg[0]),
        .I3(cnt_wait_reg[1]),
        .I4(cnt_wait_reg[3]),
        .I5(cnt_wait_reg[5]),
        .O(\cnt_wait[10]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt_wait[1]_i_1 
       (.I0(cnt_wait_reg[0]),
        .I1(cnt_wait_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt_wait[2]_i_1 
       (.I0(cnt_wait_reg[1]),
        .I1(cnt_wait_reg[0]),
        .I2(cnt_wait_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \cnt_wait[3]_i_1 
       (.I0(cnt_wait_reg[2]),
        .I1(cnt_wait_reg[0]),
        .I2(cnt_wait_reg[1]),
        .I3(cnt_wait_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \cnt_wait[4]_i_1 
       (.I0(cnt_wait_reg[3]),
        .I1(cnt_wait_reg[1]),
        .I2(cnt_wait_reg[0]),
        .I3(cnt_wait_reg[2]),
        .I4(cnt_wait_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \cnt_wait[5]_i_1 
       (.I0(cnt_wait_reg[4]),
        .I1(cnt_wait_reg[2]),
        .I2(cnt_wait_reg[0]),
        .I3(cnt_wait_reg[1]),
        .I4(cnt_wait_reg[3]),
        .I5(cnt_wait_reg[5]),
        .O(p_0_in__0[5]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hDF20)) 
    \cnt_wait[6]_i_1 
       (.I0(cnt_wait_reg[5]),
        .I1(cfg_start_i_3_n_0),
        .I2(cnt_wait_reg[4]),
        .I3(cnt_wait_reg[6]),
        .O(p_0_in__0[6]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \cnt_wait[7]_i_1 
       (.I0(cnt_wait_reg[6]),
        .I1(cnt_wait_reg[4]),
        .I2(cfg_start_i_3_n_0),
        .I3(cnt_wait_reg[5]),
        .I4(cnt_wait_reg[7]),
        .O(p_0_in__0[7]));
  LUT6 #(
    .INIT(64'hF7FFFFFF08000000)) 
    \cnt_wait[8]_i_1 
       (.I0(cnt_wait_reg[7]),
        .I1(cnt_wait_reg[5]),
        .I2(cfg_start_i_3_n_0),
        .I3(cnt_wait_reg[4]),
        .I4(cnt_wait_reg[6]),
        .I5(cnt_wait_reg[8]),
        .O(p_0_in__0[8]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \cnt_wait[9]_i_1 
       (.I0(cnt_wait_reg[8]),
        .I1(cnt_wait_reg[6]),
        .I2(\cnt_wait[10]_i_4_n_0 ),
        .I3(cnt_wait_reg[7]),
        .I4(cnt_wait_reg[9]),
        .O(p_0_in__0[9]));
  FDCE \cnt_wait_reg[0] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[0]),
        .Q(cnt_wait_reg[0]));
  FDCE \cnt_wait_reg[10] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[10]),
        .Q(cnt_wait_reg[10]));
  FDCE \cnt_wait_reg[1] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[1]),
        .Q(cnt_wait_reg[1]));
  FDCE \cnt_wait_reg[2] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[2]),
        .Q(cnt_wait_reg[2]));
  FDCE \cnt_wait_reg[3] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[3]),
        .Q(cnt_wait_reg[3]));
  FDCE \cnt_wait_reg[4] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[4]),
        .Q(cnt_wait_reg[4]));
  FDCE \cnt_wait_reg[5] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[5]),
        .Q(cnt_wait_reg[5]));
  FDCE \cnt_wait_reg[6] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[6]),
        .Q(cnt_wait_reg[6]));
  FDCE \cnt_wait_reg[7] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[7]),
        .Q(cnt_wait_reg[7]));
  FDCE \cnt_wait_reg[8] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[8]),
        .Q(cnt_wait_reg[8]));
  FDCE \cnt_wait_reg[9] 
       (.C(CLK),
        .CE(sel),
        .CLR(cfg_start_reg_0),
        .D(p_0_in__0[9]),
        .Q(cnt_wait_reg[9]));
  LUT6 #(
    .INIT(64'h1013001000010000)) 
    i2c_sda_reg_reg_i_10
       (.I0(Q[1]),
        .I1(cfg_done_reg_0),
        .I2(i2c_sda_reg_reg_i_3),
        .I3(Q[2]),
        .I4(i2c_sda_reg_reg_i_3_0),
        .I5(i2c_sda_reg_reg_i_3_1),
        .O(\reg_num_reg[1]_0 ));
  LUT3 #(
    .INIT(8'h10)) 
    i2c_sda_reg_reg_i_12
       (.I0(Q[1]),
        .I1(cfg_done_reg_0),
        .I2(i2c_sda_reg_reg_i_3),
        .O(\reg_num_reg[1]_2 ));
  LUT1 #(
    .INIT(2'h1)) 
    \reg_num[0]_i_1 
       (.I0(Q[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \reg_num[1]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \reg_num[2]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \reg_num[3]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(reg_num_reg[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \reg_num[4]_i_1 
       (.I0(reg_num_reg[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(reg_num_reg[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \reg_num[5]_i_1 
       (.I0(reg_num_reg[4]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(reg_num_reg[3]),
        .I5(reg_num_reg[5]),
        .O(p_0_in[5]));
  LUT6 #(
    .INIT(64'hF7FFFFFF08000000)) 
    \reg_num[6]_i_1 
       (.I0(reg_num_reg[5]),
        .I1(reg_num_reg[3]),
        .I2(\reg_num_reg[1]_1 ),
        .I3(Q[0]),
        .I4(reg_num_reg[4]),
        .I5(reg_num_reg[6]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \reg_num[6]_i_2 
       (.I0(Q[1]),
        .I1(Q[2]),
        .O(\reg_num_reg[1]_1 ));
  FDCE \reg_num_reg[0] 
       (.C(CLK),
        .CE(E),
        .CLR(cfg_start_reg_0),
        .D(p_0_in[0]),
        .Q(Q[0]));
  FDCE \reg_num_reg[1] 
       (.C(CLK),
        .CE(E),
        .CLR(cfg_start_reg_0),
        .D(p_0_in[1]),
        .Q(Q[1]));
  FDCE \reg_num_reg[2] 
       (.C(CLK),
        .CE(E),
        .CLR(cfg_start_reg_0),
        .D(p_0_in[2]),
        .Q(Q[2]));
  FDCE \reg_num_reg[3] 
       (.C(CLK),
        .CE(E),
        .CLR(cfg_start_reg_0),
        .D(p_0_in[3]),
        .Q(reg_num_reg[3]));
  FDCE \reg_num_reg[4] 
       (.C(CLK),
        .CE(E),
        .CLR(cfg_start_reg_0),
        .D(p_0_in[4]),
        .Q(reg_num_reg[4]));
  FDCE \reg_num_reg[5] 
       (.C(CLK),
        .CE(E),
        .CLR(cfg_start_reg_0),
        .D(p_0_in[5]),
        .Q(reg_num_reg[5]));
  FDCE \reg_num_reg[6] 
       (.C(CLK),
        .CE(E),
        .CLR(cfg_start_reg_0),
        .D(p_0_in[6]),
        .Q(reg_num_reg[6]));
endmodule

(* ORIG_REF_NAME = "hdmi_ctrl" *) 
module system_hdmi_ctrl_0_0_hdmi_ctrl
   (v_cnt,
    h_cnt,
    ddc_scl,
    cfg_done_reg,
    fifo_wr_en,
    ddc_sda,
    hdmi_in_vsync,
    sys_clk,
    hdmi_in_clk,
    hdmi_in_de,
    sys_rst_n,
    sys_init_done);
  output [31:0]v_cnt;
  output [31:0]h_cnt;
  output ddc_scl;
  output cfg_done_reg;
  output fifo_wr_en;
  inout ddc_sda;
  input hdmi_in_vsync;
  input sys_clk;
  input hdmi_in_clk;
  input hdmi_in_de;
  input sys_rst_n;
  input sys_init_done;

  wire cfg_done_reg;
  wire ddc_scl;
  wire ddc_sda;
  wire fifo_wr_en;
  wire [31:0]h_cnt;
  wire \h_cnt[11]_i_2_n_0 ;
  wire \h_cnt[11]_i_3_n_0 ;
  wire \h_cnt[11]_i_4_n_0 ;
  wire \h_cnt[11]_i_5_n_0 ;
  wire \h_cnt[15]_i_2_n_0 ;
  wire \h_cnt[15]_i_3_n_0 ;
  wire \h_cnt[15]_i_4_n_0 ;
  wire \h_cnt[15]_i_5_n_0 ;
  wire \h_cnt[19]_i_2_n_0 ;
  wire \h_cnt[19]_i_3_n_0 ;
  wire \h_cnt[19]_i_4_n_0 ;
  wire \h_cnt[19]_i_5_n_0 ;
  wire \h_cnt[23]_i_2_n_0 ;
  wire \h_cnt[23]_i_3_n_0 ;
  wire \h_cnt[23]_i_4_n_0 ;
  wire \h_cnt[23]_i_5_n_0 ;
  wire \h_cnt[27]_i_2_n_0 ;
  wire \h_cnt[27]_i_3_n_0 ;
  wire \h_cnt[27]_i_4_n_0 ;
  wire \h_cnt[27]_i_5_n_0 ;
  wire \h_cnt[31]_i_2_n_0 ;
  wire \h_cnt[31]_i_3_n_0 ;
  wire \h_cnt[31]_i_4_n_0 ;
  wire \h_cnt[31]_i_5_n_0 ;
  wire \h_cnt[31]_i_6_n_0 ;
  wire \h_cnt[3]_i_2_n_0 ;
  wire \h_cnt[3]_i_3_n_0 ;
  wire \h_cnt[3]_i_4_n_0 ;
  wire \h_cnt[3]_i_5_n_0 ;
  wire \h_cnt[7]_i_2_n_0 ;
  wire \h_cnt[7]_i_3_n_0 ;
  wire \h_cnt[7]_i_4_n_0 ;
  wire \h_cnt[7]_i_5_n_0 ;
  wire \h_cnt_reg[11]_i_1_n_0 ;
  wire \h_cnt_reg[11]_i_1_n_1 ;
  wire \h_cnt_reg[11]_i_1_n_2 ;
  wire \h_cnt_reg[11]_i_1_n_3 ;
  wire \h_cnt_reg[11]_i_1_n_4 ;
  wire \h_cnt_reg[11]_i_1_n_5 ;
  wire \h_cnt_reg[11]_i_1_n_6 ;
  wire \h_cnt_reg[11]_i_1_n_7 ;
  wire \h_cnt_reg[15]_i_1_n_0 ;
  wire \h_cnt_reg[15]_i_1_n_1 ;
  wire \h_cnt_reg[15]_i_1_n_2 ;
  wire \h_cnt_reg[15]_i_1_n_3 ;
  wire \h_cnt_reg[15]_i_1_n_4 ;
  wire \h_cnt_reg[15]_i_1_n_5 ;
  wire \h_cnt_reg[15]_i_1_n_6 ;
  wire \h_cnt_reg[15]_i_1_n_7 ;
  wire \h_cnt_reg[19]_i_1_n_0 ;
  wire \h_cnt_reg[19]_i_1_n_1 ;
  wire \h_cnt_reg[19]_i_1_n_2 ;
  wire \h_cnt_reg[19]_i_1_n_3 ;
  wire \h_cnt_reg[19]_i_1_n_4 ;
  wire \h_cnt_reg[19]_i_1_n_5 ;
  wire \h_cnt_reg[19]_i_1_n_6 ;
  wire \h_cnt_reg[19]_i_1_n_7 ;
  wire \h_cnt_reg[23]_i_1_n_0 ;
  wire \h_cnt_reg[23]_i_1_n_1 ;
  wire \h_cnt_reg[23]_i_1_n_2 ;
  wire \h_cnt_reg[23]_i_1_n_3 ;
  wire \h_cnt_reg[23]_i_1_n_4 ;
  wire \h_cnt_reg[23]_i_1_n_5 ;
  wire \h_cnt_reg[23]_i_1_n_6 ;
  wire \h_cnt_reg[23]_i_1_n_7 ;
  wire \h_cnt_reg[27]_i_1_n_0 ;
  wire \h_cnt_reg[27]_i_1_n_1 ;
  wire \h_cnt_reg[27]_i_1_n_2 ;
  wire \h_cnt_reg[27]_i_1_n_3 ;
  wire \h_cnt_reg[27]_i_1_n_4 ;
  wire \h_cnt_reg[27]_i_1_n_5 ;
  wire \h_cnt_reg[27]_i_1_n_6 ;
  wire \h_cnt_reg[27]_i_1_n_7 ;
  wire \h_cnt_reg[31]_i_1_n_1 ;
  wire \h_cnt_reg[31]_i_1_n_2 ;
  wire \h_cnt_reg[31]_i_1_n_3 ;
  wire \h_cnt_reg[31]_i_1_n_4 ;
  wire \h_cnt_reg[31]_i_1_n_5 ;
  wire \h_cnt_reg[31]_i_1_n_6 ;
  wire \h_cnt_reg[31]_i_1_n_7 ;
  wire \h_cnt_reg[3]_i_1_n_0 ;
  wire \h_cnt_reg[3]_i_1_n_1 ;
  wire \h_cnt_reg[3]_i_1_n_2 ;
  wire \h_cnt_reg[3]_i_1_n_3 ;
  wire \h_cnt_reg[3]_i_1_n_4 ;
  wire \h_cnt_reg[3]_i_1_n_5 ;
  wire \h_cnt_reg[3]_i_1_n_6 ;
  wire \h_cnt_reg[3]_i_1_n_7 ;
  wire \h_cnt_reg[7]_i_1_n_0 ;
  wire \h_cnt_reg[7]_i_1_n_1 ;
  wire \h_cnt_reg[7]_i_1_n_2 ;
  wire \h_cnt_reg[7]_i_1_n_3 ;
  wire \h_cnt_reg[7]_i_1_n_4 ;
  wire \h_cnt_reg[7]_i_1_n_5 ;
  wire \h_cnt_reg[7]_i_1_n_6 ;
  wire \h_cnt_reg[7]_i_1_n_7 ;
  wire hdmi_in_clk;
  wire hdmi_in_de;
  wire hdmi_in_vsync;
  wire sys_clk;
  wire sys_init_done;
  wire sys_rst_n;
  wire [31:0]v_cnt;
  wire v_cnt0;
  wire \v_cnt[11]_i_2_n_0 ;
  wire \v_cnt[11]_i_3_n_0 ;
  wire \v_cnt[11]_i_4_n_0 ;
  wire \v_cnt[11]_i_5_n_0 ;
  wire \v_cnt[15]_i_2_n_0 ;
  wire \v_cnt[15]_i_3_n_0 ;
  wire \v_cnt[15]_i_4_n_0 ;
  wire \v_cnt[15]_i_5_n_0 ;
  wire \v_cnt[19]_i_2_n_0 ;
  wire \v_cnt[19]_i_3_n_0 ;
  wire \v_cnt[19]_i_4_n_0 ;
  wire \v_cnt[19]_i_5_n_0 ;
  wire \v_cnt[23]_i_2_n_0 ;
  wire \v_cnt[23]_i_3_n_0 ;
  wire \v_cnt[23]_i_4_n_0 ;
  wire \v_cnt[23]_i_5_n_0 ;
  wire \v_cnt[27]_i_2_n_0 ;
  wire \v_cnt[27]_i_3_n_0 ;
  wire \v_cnt[27]_i_4_n_0 ;
  wire \v_cnt[27]_i_5_n_0 ;
  wire \v_cnt[31]_i_10_n_0 ;
  wire \v_cnt[31]_i_11_n_0 ;
  wire \v_cnt[31]_i_12_n_0 ;
  wire \v_cnt[31]_i_3_n_0 ;
  wire \v_cnt[31]_i_4_n_0 ;
  wire \v_cnt[31]_i_5_n_0 ;
  wire \v_cnt[31]_i_6_n_0 ;
  wire \v_cnt[31]_i_7_n_0 ;
  wire \v_cnt[31]_i_8_n_0 ;
  wire \v_cnt[31]_i_9_n_0 ;
  wire \v_cnt[3]_i_2_n_0 ;
  wire \v_cnt[3]_i_3_n_0 ;
  wire \v_cnt[3]_i_4_n_0 ;
  wire \v_cnt[3]_i_5_n_0 ;
  wire \v_cnt[3]_i_6_n_0 ;
  wire \v_cnt[7]_i_2_n_0 ;
  wire \v_cnt[7]_i_3_n_0 ;
  wire \v_cnt[7]_i_4_n_0 ;
  wire \v_cnt[7]_i_5_n_0 ;
  wire \v_cnt_reg[11]_i_1_n_0 ;
  wire \v_cnt_reg[11]_i_1_n_1 ;
  wire \v_cnt_reg[11]_i_1_n_2 ;
  wire \v_cnt_reg[11]_i_1_n_3 ;
  wire \v_cnt_reg[11]_i_1_n_4 ;
  wire \v_cnt_reg[11]_i_1_n_5 ;
  wire \v_cnt_reg[11]_i_1_n_6 ;
  wire \v_cnt_reg[11]_i_1_n_7 ;
  wire \v_cnt_reg[15]_i_1_n_0 ;
  wire \v_cnt_reg[15]_i_1_n_1 ;
  wire \v_cnt_reg[15]_i_1_n_2 ;
  wire \v_cnt_reg[15]_i_1_n_3 ;
  wire \v_cnt_reg[15]_i_1_n_4 ;
  wire \v_cnt_reg[15]_i_1_n_5 ;
  wire \v_cnt_reg[15]_i_1_n_6 ;
  wire \v_cnt_reg[15]_i_1_n_7 ;
  wire \v_cnt_reg[19]_i_1_n_0 ;
  wire \v_cnt_reg[19]_i_1_n_1 ;
  wire \v_cnt_reg[19]_i_1_n_2 ;
  wire \v_cnt_reg[19]_i_1_n_3 ;
  wire \v_cnt_reg[19]_i_1_n_4 ;
  wire \v_cnt_reg[19]_i_1_n_5 ;
  wire \v_cnt_reg[19]_i_1_n_6 ;
  wire \v_cnt_reg[19]_i_1_n_7 ;
  wire \v_cnt_reg[23]_i_1_n_0 ;
  wire \v_cnt_reg[23]_i_1_n_1 ;
  wire \v_cnt_reg[23]_i_1_n_2 ;
  wire \v_cnt_reg[23]_i_1_n_3 ;
  wire \v_cnt_reg[23]_i_1_n_4 ;
  wire \v_cnt_reg[23]_i_1_n_5 ;
  wire \v_cnt_reg[23]_i_1_n_6 ;
  wire \v_cnt_reg[23]_i_1_n_7 ;
  wire \v_cnt_reg[27]_i_1_n_0 ;
  wire \v_cnt_reg[27]_i_1_n_1 ;
  wire \v_cnt_reg[27]_i_1_n_2 ;
  wire \v_cnt_reg[27]_i_1_n_3 ;
  wire \v_cnt_reg[27]_i_1_n_4 ;
  wire \v_cnt_reg[27]_i_1_n_5 ;
  wire \v_cnt_reg[27]_i_1_n_6 ;
  wire \v_cnt_reg[27]_i_1_n_7 ;
  wire \v_cnt_reg[31]_i_2_n_1 ;
  wire \v_cnt_reg[31]_i_2_n_2 ;
  wire \v_cnt_reg[31]_i_2_n_3 ;
  wire \v_cnt_reg[31]_i_2_n_4 ;
  wire \v_cnt_reg[31]_i_2_n_5 ;
  wire \v_cnt_reg[31]_i_2_n_6 ;
  wire \v_cnt_reg[31]_i_2_n_7 ;
  wire \v_cnt_reg[3]_i_1_n_0 ;
  wire \v_cnt_reg[3]_i_1_n_1 ;
  wire \v_cnt_reg[3]_i_1_n_2 ;
  wire \v_cnt_reg[3]_i_1_n_3 ;
  wire \v_cnt_reg[3]_i_1_n_4 ;
  wire \v_cnt_reg[3]_i_1_n_5 ;
  wire \v_cnt_reg[3]_i_1_n_6 ;
  wire \v_cnt_reg[3]_i_1_n_7 ;
  wire \v_cnt_reg[7]_i_1_n_0 ;
  wire \v_cnt_reg[7]_i_1_n_1 ;
  wire \v_cnt_reg[7]_i_1_n_2 ;
  wire \v_cnt_reg[7]_i_1_n_3 ;
  wire \v_cnt_reg[7]_i_1_n_4 ;
  wire \v_cnt_reg[7]_i_1_n_5 ;
  wire \v_cnt_reg[7]_i_1_n_6 ;
  wire \v_cnt_reg[7]_i_1_n_7 ;
  wire [3:3]\NLW_h_cnt_reg[31]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_v_cnt_reg[31]_i_2_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h8)) 
    fifo_wr_en_INST_0
       (.I0(v_cnt[0]),
        .I1(hdmi_in_de),
        .O(fifo_wr_en));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[11]_i_2 
       (.I0(hdmi_in_de),
        .I1(h_cnt[11]),
        .O(\h_cnt[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[11]_i_3 
       (.I0(hdmi_in_de),
        .I1(h_cnt[10]),
        .O(\h_cnt[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[11]_i_4 
       (.I0(hdmi_in_de),
        .I1(h_cnt[9]),
        .O(\h_cnt[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[11]_i_5 
       (.I0(hdmi_in_de),
        .I1(h_cnt[8]),
        .O(\h_cnt[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[15]_i_2 
       (.I0(hdmi_in_de),
        .I1(h_cnt[15]),
        .O(\h_cnt[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[15]_i_3 
       (.I0(hdmi_in_de),
        .I1(h_cnt[14]),
        .O(\h_cnt[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[15]_i_4 
       (.I0(hdmi_in_de),
        .I1(h_cnt[13]),
        .O(\h_cnt[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[15]_i_5 
       (.I0(hdmi_in_de),
        .I1(h_cnt[12]),
        .O(\h_cnt[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[19]_i_2 
       (.I0(hdmi_in_de),
        .I1(h_cnt[19]),
        .O(\h_cnt[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[19]_i_3 
       (.I0(hdmi_in_de),
        .I1(h_cnt[18]),
        .O(\h_cnt[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[19]_i_4 
       (.I0(hdmi_in_de),
        .I1(h_cnt[17]),
        .O(\h_cnt[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[19]_i_5 
       (.I0(hdmi_in_de),
        .I1(h_cnt[16]),
        .O(\h_cnt[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[23]_i_2 
       (.I0(hdmi_in_de),
        .I1(h_cnt[23]),
        .O(\h_cnt[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[23]_i_3 
       (.I0(hdmi_in_de),
        .I1(h_cnt[22]),
        .O(\h_cnt[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[23]_i_4 
       (.I0(hdmi_in_de),
        .I1(h_cnt[21]),
        .O(\h_cnt[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[23]_i_5 
       (.I0(hdmi_in_de),
        .I1(h_cnt[20]),
        .O(\h_cnt[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[27]_i_2 
       (.I0(hdmi_in_de),
        .I1(h_cnt[27]),
        .O(\h_cnt[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[27]_i_3 
       (.I0(hdmi_in_de),
        .I1(h_cnt[26]),
        .O(\h_cnt[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[27]_i_4 
       (.I0(hdmi_in_de),
        .I1(h_cnt[25]),
        .O(\h_cnt[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[27]_i_5 
       (.I0(hdmi_in_de),
        .I1(h_cnt[24]),
        .O(\h_cnt[27]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \h_cnt[31]_i_2 
       (.I0(sys_rst_n),
        .O(\h_cnt[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[31]_i_3 
       (.I0(hdmi_in_de),
        .I1(h_cnt[31]),
        .O(\h_cnt[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[31]_i_4 
       (.I0(hdmi_in_de),
        .I1(h_cnt[30]),
        .O(\h_cnt[31]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[31]_i_5 
       (.I0(hdmi_in_de),
        .I1(h_cnt[29]),
        .O(\h_cnt[31]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[31]_i_6 
       (.I0(hdmi_in_de),
        .I1(h_cnt[28]),
        .O(\h_cnt[31]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[3]_i_2 
       (.I0(hdmi_in_de),
        .I1(h_cnt[3]),
        .O(\h_cnt[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[3]_i_3 
       (.I0(hdmi_in_de),
        .I1(h_cnt[2]),
        .O(\h_cnt[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[3]_i_4 
       (.I0(hdmi_in_de),
        .I1(h_cnt[1]),
        .O(\h_cnt[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h4)) 
    \h_cnt[3]_i_5 
       (.I0(h_cnt[0]),
        .I1(hdmi_in_de),
        .O(\h_cnt[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[7]_i_2 
       (.I0(hdmi_in_de),
        .I1(h_cnt[7]),
        .O(\h_cnt[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[7]_i_3 
       (.I0(hdmi_in_de),
        .I1(h_cnt[6]),
        .O(\h_cnt[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[7]_i_4 
       (.I0(hdmi_in_de),
        .I1(h_cnt[5]),
        .O(\h_cnt[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \h_cnt[7]_i_5 
       (.I0(hdmi_in_de),
        .I1(h_cnt[4]),
        .O(\h_cnt[7]_i_5_n_0 ));
  FDCE \h_cnt_reg[0] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[3]_i_1_n_7 ),
        .Q(h_cnt[0]));
  FDCE \h_cnt_reg[10] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[11]_i_1_n_5 ),
        .Q(h_cnt[10]));
  FDCE \h_cnt_reg[11] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[11]_i_1_n_4 ),
        .Q(h_cnt[11]));
  CARRY4 \h_cnt_reg[11]_i_1 
       (.CI(\h_cnt_reg[7]_i_1_n_0 ),
        .CO({\h_cnt_reg[11]_i_1_n_0 ,\h_cnt_reg[11]_i_1_n_1 ,\h_cnt_reg[11]_i_1_n_2 ,\h_cnt_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\h_cnt_reg[11]_i_1_n_4 ,\h_cnt_reg[11]_i_1_n_5 ,\h_cnt_reg[11]_i_1_n_6 ,\h_cnt_reg[11]_i_1_n_7 }),
        .S({\h_cnt[11]_i_2_n_0 ,\h_cnt[11]_i_3_n_0 ,\h_cnt[11]_i_4_n_0 ,\h_cnt[11]_i_5_n_0 }));
  FDCE \h_cnt_reg[12] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[15]_i_1_n_7 ),
        .Q(h_cnt[12]));
  FDCE \h_cnt_reg[13] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[15]_i_1_n_6 ),
        .Q(h_cnt[13]));
  FDCE \h_cnt_reg[14] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[15]_i_1_n_5 ),
        .Q(h_cnt[14]));
  FDCE \h_cnt_reg[15] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[15]_i_1_n_4 ),
        .Q(h_cnt[15]));
  CARRY4 \h_cnt_reg[15]_i_1 
       (.CI(\h_cnt_reg[11]_i_1_n_0 ),
        .CO({\h_cnt_reg[15]_i_1_n_0 ,\h_cnt_reg[15]_i_1_n_1 ,\h_cnt_reg[15]_i_1_n_2 ,\h_cnt_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\h_cnt_reg[15]_i_1_n_4 ,\h_cnt_reg[15]_i_1_n_5 ,\h_cnt_reg[15]_i_1_n_6 ,\h_cnt_reg[15]_i_1_n_7 }),
        .S({\h_cnt[15]_i_2_n_0 ,\h_cnt[15]_i_3_n_0 ,\h_cnt[15]_i_4_n_0 ,\h_cnt[15]_i_5_n_0 }));
  FDCE \h_cnt_reg[16] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[19]_i_1_n_7 ),
        .Q(h_cnt[16]));
  FDCE \h_cnt_reg[17] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[19]_i_1_n_6 ),
        .Q(h_cnt[17]));
  FDCE \h_cnt_reg[18] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[19]_i_1_n_5 ),
        .Q(h_cnt[18]));
  FDCE \h_cnt_reg[19] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[19]_i_1_n_4 ),
        .Q(h_cnt[19]));
  CARRY4 \h_cnt_reg[19]_i_1 
       (.CI(\h_cnt_reg[15]_i_1_n_0 ),
        .CO({\h_cnt_reg[19]_i_1_n_0 ,\h_cnt_reg[19]_i_1_n_1 ,\h_cnt_reg[19]_i_1_n_2 ,\h_cnt_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\h_cnt_reg[19]_i_1_n_4 ,\h_cnt_reg[19]_i_1_n_5 ,\h_cnt_reg[19]_i_1_n_6 ,\h_cnt_reg[19]_i_1_n_7 }),
        .S({\h_cnt[19]_i_2_n_0 ,\h_cnt[19]_i_3_n_0 ,\h_cnt[19]_i_4_n_0 ,\h_cnt[19]_i_5_n_0 }));
  FDCE \h_cnt_reg[1] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[3]_i_1_n_6 ),
        .Q(h_cnt[1]));
  FDCE \h_cnt_reg[20] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[23]_i_1_n_7 ),
        .Q(h_cnt[20]));
  FDCE \h_cnt_reg[21] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[23]_i_1_n_6 ),
        .Q(h_cnt[21]));
  FDCE \h_cnt_reg[22] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[23]_i_1_n_5 ),
        .Q(h_cnt[22]));
  FDCE \h_cnt_reg[23] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[23]_i_1_n_4 ),
        .Q(h_cnt[23]));
  CARRY4 \h_cnt_reg[23]_i_1 
       (.CI(\h_cnt_reg[19]_i_1_n_0 ),
        .CO({\h_cnt_reg[23]_i_1_n_0 ,\h_cnt_reg[23]_i_1_n_1 ,\h_cnt_reg[23]_i_1_n_2 ,\h_cnt_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\h_cnt_reg[23]_i_1_n_4 ,\h_cnt_reg[23]_i_1_n_5 ,\h_cnt_reg[23]_i_1_n_6 ,\h_cnt_reg[23]_i_1_n_7 }),
        .S({\h_cnt[23]_i_2_n_0 ,\h_cnt[23]_i_3_n_0 ,\h_cnt[23]_i_4_n_0 ,\h_cnt[23]_i_5_n_0 }));
  FDCE \h_cnt_reg[24] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[27]_i_1_n_7 ),
        .Q(h_cnt[24]));
  FDCE \h_cnt_reg[25] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[27]_i_1_n_6 ),
        .Q(h_cnt[25]));
  FDCE \h_cnt_reg[26] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[27]_i_1_n_5 ),
        .Q(h_cnt[26]));
  FDCE \h_cnt_reg[27] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[27]_i_1_n_4 ),
        .Q(h_cnt[27]));
  CARRY4 \h_cnt_reg[27]_i_1 
       (.CI(\h_cnt_reg[23]_i_1_n_0 ),
        .CO({\h_cnt_reg[27]_i_1_n_0 ,\h_cnt_reg[27]_i_1_n_1 ,\h_cnt_reg[27]_i_1_n_2 ,\h_cnt_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\h_cnt_reg[27]_i_1_n_4 ,\h_cnt_reg[27]_i_1_n_5 ,\h_cnt_reg[27]_i_1_n_6 ,\h_cnt_reg[27]_i_1_n_7 }),
        .S({\h_cnt[27]_i_2_n_0 ,\h_cnt[27]_i_3_n_0 ,\h_cnt[27]_i_4_n_0 ,\h_cnt[27]_i_5_n_0 }));
  FDCE \h_cnt_reg[28] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[31]_i_1_n_7 ),
        .Q(h_cnt[28]));
  FDCE \h_cnt_reg[29] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[31]_i_1_n_6 ),
        .Q(h_cnt[29]));
  FDCE \h_cnt_reg[2] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[3]_i_1_n_5 ),
        .Q(h_cnt[2]));
  FDCE \h_cnt_reg[30] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[31]_i_1_n_5 ),
        .Q(h_cnt[30]));
  FDCE \h_cnt_reg[31] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[31]_i_1_n_4 ),
        .Q(h_cnt[31]));
  CARRY4 \h_cnt_reg[31]_i_1 
       (.CI(\h_cnt_reg[27]_i_1_n_0 ),
        .CO({\NLW_h_cnt_reg[31]_i_1_CO_UNCONNECTED [3],\h_cnt_reg[31]_i_1_n_1 ,\h_cnt_reg[31]_i_1_n_2 ,\h_cnt_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\h_cnt_reg[31]_i_1_n_4 ,\h_cnt_reg[31]_i_1_n_5 ,\h_cnt_reg[31]_i_1_n_6 ,\h_cnt_reg[31]_i_1_n_7 }),
        .S({\h_cnt[31]_i_3_n_0 ,\h_cnt[31]_i_4_n_0 ,\h_cnt[31]_i_5_n_0 ,\h_cnt[31]_i_6_n_0 }));
  FDCE \h_cnt_reg[3] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[3]_i_1_n_4 ),
        .Q(h_cnt[3]));
  CARRY4 \h_cnt_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\h_cnt_reg[3]_i_1_n_0 ,\h_cnt_reg[3]_i_1_n_1 ,\h_cnt_reg[3]_i_1_n_2 ,\h_cnt_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,hdmi_in_de}),
        .O({\h_cnt_reg[3]_i_1_n_4 ,\h_cnt_reg[3]_i_1_n_5 ,\h_cnt_reg[3]_i_1_n_6 ,\h_cnt_reg[3]_i_1_n_7 }),
        .S({\h_cnt[3]_i_2_n_0 ,\h_cnt[3]_i_3_n_0 ,\h_cnt[3]_i_4_n_0 ,\h_cnt[3]_i_5_n_0 }));
  FDCE \h_cnt_reg[4] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[7]_i_1_n_7 ),
        .Q(h_cnt[4]));
  FDCE \h_cnt_reg[5] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[7]_i_1_n_6 ),
        .Q(h_cnt[5]));
  FDCE \h_cnt_reg[6] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[7]_i_1_n_5 ),
        .Q(h_cnt[6]));
  FDCE \h_cnt_reg[7] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[7]_i_1_n_4 ),
        .Q(h_cnt[7]));
  CARRY4 \h_cnt_reg[7]_i_1 
       (.CI(\h_cnt_reg[3]_i_1_n_0 ),
        .CO({\h_cnt_reg[7]_i_1_n_0 ,\h_cnt_reg[7]_i_1_n_1 ,\h_cnt_reg[7]_i_1_n_2 ,\h_cnt_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\h_cnt_reg[7]_i_1_n_4 ,\h_cnt_reg[7]_i_1_n_5 ,\h_cnt_reg[7]_i_1_n_6 ,\h_cnt_reg[7]_i_1_n_7 }),
        .S({\h_cnt[7]_i_2_n_0 ,\h_cnt[7]_i_3_n_0 ,\h_cnt[7]_i_4_n_0 ,\h_cnt[7]_i_5_n_0 }));
  FDCE \h_cnt_reg[8] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[11]_i_1_n_7 ),
        .Q(h_cnt[8]));
  FDCE \h_cnt_reg[9] 
       (.C(hdmi_in_clk),
        .CE(1'b1),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\h_cnt_reg[11]_i_1_n_6 ),
        .Q(h_cnt[9]));
  system_hdmi_ctrl_0_0_hdmi_i2c hdmi_i2c_inst
       (.cfg_done_reg(cfg_done_reg),
        .ddc_scl(ddc_scl),
        .ddc_sda(ddc_sda),
        .sys_clk(sys_clk),
        .sys_init_done(sys_init_done),
        .sys_rst_n(sys_rst_n));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[11]_i_2 
       (.I0(v_cnt[11]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[11]_i_3 
       (.I0(v_cnt[10]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[11]_i_4 
       (.I0(v_cnt[9]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[11]_i_5 
       (.I0(v_cnt[8]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[11]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[15]_i_2 
       (.I0(v_cnt[15]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[15]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[15]_i_3 
       (.I0(v_cnt[14]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[15]_i_4 
       (.I0(v_cnt[13]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[15]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[15]_i_5 
       (.I0(v_cnt[12]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[19]_i_2 
       (.I0(v_cnt[19]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[19]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[19]_i_3 
       (.I0(v_cnt[18]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[19]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[19]_i_4 
       (.I0(v_cnt[17]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[19]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[19]_i_5 
       (.I0(v_cnt[16]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[19]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[23]_i_2 
       (.I0(v_cnt[23]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[23]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[23]_i_3 
       (.I0(v_cnt[22]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[23]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[23]_i_4 
       (.I0(v_cnt[21]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[23]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[23]_i_5 
       (.I0(v_cnt[20]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[23]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[27]_i_2 
       (.I0(v_cnt[27]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[27]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[27]_i_3 
       (.I0(v_cnt[26]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[27]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[27]_i_4 
       (.I0(v_cnt[25]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[27]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[27]_i_5 
       (.I0(v_cnt[24]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[27]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hF8)) 
    \v_cnt[31]_i_1 
       (.I0(\v_cnt[31]_i_3_n_0 ),
        .I1(\v_cnt[31]_i_4_n_0 ),
        .I2(hdmi_in_vsync),
        .O(v_cnt0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \v_cnt[31]_i_10 
       (.I0(h_cnt[16]),
        .I1(h_cnt[17]),
        .I2(h_cnt[14]),
        .I3(h_cnt[15]),
        .I4(h_cnt[19]),
        .I5(h_cnt[18]),
        .O(\v_cnt[31]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    \v_cnt[31]_i_11 
       (.I0(h_cnt[10]),
        .I1(h_cnt[11]),
        .I2(h_cnt[8]),
        .I3(h_cnt[9]),
        .I4(h_cnt[13]),
        .I5(h_cnt[12]),
        .O(\v_cnt[31]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h0000800000000000)) 
    \v_cnt[31]_i_12 
       (.I0(h_cnt[4]),
        .I1(h_cnt[5]),
        .I2(h_cnt[2]),
        .I3(h_cnt[3]),
        .I4(h_cnt[7]),
        .I5(h_cnt[6]),
        .O(\v_cnt[31]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \v_cnt[31]_i_3 
       (.I0(h_cnt[28]),
        .I1(h_cnt[29]),
        .I2(h_cnt[26]),
        .I3(h_cnt[27]),
        .I4(h_cnt[31]),
        .I5(h_cnt[30]),
        .O(\v_cnt[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \v_cnt[31]_i_4 
       (.I0(\v_cnt[31]_i_9_n_0 ),
        .I1(\v_cnt[31]_i_10_n_0 ),
        .I2(\v_cnt[31]_i_11_n_0 ),
        .I3(\v_cnt[31]_i_12_n_0 ),
        .I4(h_cnt[0]),
        .I5(h_cnt[1]),
        .O(\v_cnt[31]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[31]_i_5 
       (.I0(v_cnt[31]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[31]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[31]_i_6 
       (.I0(v_cnt[30]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[31]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[31]_i_7 
       (.I0(v_cnt[29]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[31]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[31]_i_8 
       (.I0(v_cnt[28]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[31]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \v_cnt[31]_i_9 
       (.I0(h_cnt[22]),
        .I1(h_cnt[23]),
        .I2(h_cnt[20]),
        .I3(h_cnt[21]),
        .I4(h_cnt[25]),
        .I5(h_cnt[24]),
        .O(\v_cnt[31]_i_9_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \v_cnt[3]_i_2 
       (.I0(hdmi_in_vsync),
        .O(\v_cnt[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[3]_i_3 
       (.I0(v_cnt[3]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[3]_i_4 
       (.I0(v_cnt[2]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[3]_i_5 
       (.I0(v_cnt[1]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \v_cnt[3]_i_6 
       (.I0(v_cnt[0]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[3]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[7]_i_2 
       (.I0(v_cnt[7]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[7]_i_3 
       (.I0(v_cnt[6]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[7]_i_4 
       (.I0(v_cnt[5]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \v_cnt[7]_i_5 
       (.I0(v_cnt[4]),
        .I1(hdmi_in_vsync),
        .O(\v_cnt[7]_i_5_n_0 ));
  FDCE \v_cnt_reg[0] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[3]_i_1_n_7 ),
        .Q(v_cnt[0]));
  FDCE \v_cnt_reg[10] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[11]_i_1_n_5 ),
        .Q(v_cnt[10]));
  FDCE \v_cnt_reg[11] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[11]_i_1_n_4 ),
        .Q(v_cnt[11]));
  CARRY4 \v_cnt_reg[11]_i_1 
       (.CI(\v_cnt_reg[7]_i_1_n_0 ),
        .CO({\v_cnt_reg[11]_i_1_n_0 ,\v_cnt_reg[11]_i_1_n_1 ,\v_cnt_reg[11]_i_1_n_2 ,\v_cnt_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\v_cnt_reg[11]_i_1_n_4 ,\v_cnt_reg[11]_i_1_n_5 ,\v_cnt_reg[11]_i_1_n_6 ,\v_cnt_reg[11]_i_1_n_7 }),
        .S({\v_cnt[11]_i_2_n_0 ,\v_cnt[11]_i_3_n_0 ,\v_cnt[11]_i_4_n_0 ,\v_cnt[11]_i_5_n_0 }));
  FDCE \v_cnt_reg[12] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[15]_i_1_n_7 ),
        .Q(v_cnt[12]));
  FDCE \v_cnt_reg[13] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[15]_i_1_n_6 ),
        .Q(v_cnt[13]));
  FDCE \v_cnt_reg[14] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[15]_i_1_n_5 ),
        .Q(v_cnt[14]));
  FDCE \v_cnt_reg[15] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[15]_i_1_n_4 ),
        .Q(v_cnt[15]));
  CARRY4 \v_cnt_reg[15]_i_1 
       (.CI(\v_cnt_reg[11]_i_1_n_0 ),
        .CO({\v_cnt_reg[15]_i_1_n_0 ,\v_cnt_reg[15]_i_1_n_1 ,\v_cnt_reg[15]_i_1_n_2 ,\v_cnt_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\v_cnt_reg[15]_i_1_n_4 ,\v_cnt_reg[15]_i_1_n_5 ,\v_cnt_reg[15]_i_1_n_6 ,\v_cnt_reg[15]_i_1_n_7 }),
        .S({\v_cnt[15]_i_2_n_0 ,\v_cnt[15]_i_3_n_0 ,\v_cnt[15]_i_4_n_0 ,\v_cnt[15]_i_5_n_0 }));
  FDCE \v_cnt_reg[16] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[19]_i_1_n_7 ),
        .Q(v_cnt[16]));
  FDCE \v_cnt_reg[17] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[19]_i_1_n_6 ),
        .Q(v_cnt[17]));
  FDCE \v_cnt_reg[18] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[19]_i_1_n_5 ),
        .Q(v_cnt[18]));
  FDCE \v_cnt_reg[19] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[19]_i_1_n_4 ),
        .Q(v_cnt[19]));
  CARRY4 \v_cnt_reg[19]_i_1 
       (.CI(\v_cnt_reg[15]_i_1_n_0 ),
        .CO({\v_cnt_reg[19]_i_1_n_0 ,\v_cnt_reg[19]_i_1_n_1 ,\v_cnt_reg[19]_i_1_n_2 ,\v_cnt_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\v_cnt_reg[19]_i_1_n_4 ,\v_cnt_reg[19]_i_1_n_5 ,\v_cnt_reg[19]_i_1_n_6 ,\v_cnt_reg[19]_i_1_n_7 }),
        .S({\v_cnt[19]_i_2_n_0 ,\v_cnt[19]_i_3_n_0 ,\v_cnt[19]_i_4_n_0 ,\v_cnt[19]_i_5_n_0 }));
  FDCE \v_cnt_reg[1] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[3]_i_1_n_6 ),
        .Q(v_cnt[1]));
  FDCE \v_cnt_reg[20] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[23]_i_1_n_7 ),
        .Q(v_cnt[20]));
  FDCE \v_cnt_reg[21] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[23]_i_1_n_6 ),
        .Q(v_cnt[21]));
  FDCE \v_cnt_reg[22] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[23]_i_1_n_5 ),
        .Q(v_cnt[22]));
  FDCE \v_cnt_reg[23] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[23]_i_1_n_4 ),
        .Q(v_cnt[23]));
  CARRY4 \v_cnt_reg[23]_i_1 
       (.CI(\v_cnt_reg[19]_i_1_n_0 ),
        .CO({\v_cnt_reg[23]_i_1_n_0 ,\v_cnt_reg[23]_i_1_n_1 ,\v_cnt_reg[23]_i_1_n_2 ,\v_cnt_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\v_cnt_reg[23]_i_1_n_4 ,\v_cnt_reg[23]_i_1_n_5 ,\v_cnt_reg[23]_i_1_n_6 ,\v_cnt_reg[23]_i_1_n_7 }),
        .S({\v_cnt[23]_i_2_n_0 ,\v_cnt[23]_i_3_n_0 ,\v_cnt[23]_i_4_n_0 ,\v_cnt[23]_i_5_n_0 }));
  FDCE \v_cnt_reg[24] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[27]_i_1_n_7 ),
        .Q(v_cnt[24]));
  FDCE \v_cnt_reg[25] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[27]_i_1_n_6 ),
        .Q(v_cnt[25]));
  FDCE \v_cnt_reg[26] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[27]_i_1_n_5 ),
        .Q(v_cnt[26]));
  FDCE \v_cnt_reg[27] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[27]_i_1_n_4 ),
        .Q(v_cnt[27]));
  CARRY4 \v_cnt_reg[27]_i_1 
       (.CI(\v_cnt_reg[23]_i_1_n_0 ),
        .CO({\v_cnt_reg[27]_i_1_n_0 ,\v_cnt_reg[27]_i_1_n_1 ,\v_cnt_reg[27]_i_1_n_2 ,\v_cnt_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\v_cnt_reg[27]_i_1_n_4 ,\v_cnt_reg[27]_i_1_n_5 ,\v_cnt_reg[27]_i_1_n_6 ,\v_cnt_reg[27]_i_1_n_7 }),
        .S({\v_cnt[27]_i_2_n_0 ,\v_cnt[27]_i_3_n_0 ,\v_cnt[27]_i_4_n_0 ,\v_cnt[27]_i_5_n_0 }));
  FDCE \v_cnt_reg[28] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[31]_i_2_n_7 ),
        .Q(v_cnt[28]));
  FDCE \v_cnt_reg[29] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[31]_i_2_n_6 ),
        .Q(v_cnt[29]));
  FDCE \v_cnt_reg[2] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[3]_i_1_n_5 ),
        .Q(v_cnt[2]));
  FDCE \v_cnt_reg[30] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[31]_i_2_n_5 ),
        .Q(v_cnt[30]));
  FDCE \v_cnt_reg[31] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[31]_i_2_n_4 ),
        .Q(v_cnt[31]));
  CARRY4 \v_cnt_reg[31]_i_2 
       (.CI(\v_cnt_reg[27]_i_1_n_0 ),
        .CO({\NLW_v_cnt_reg[31]_i_2_CO_UNCONNECTED [3],\v_cnt_reg[31]_i_2_n_1 ,\v_cnt_reg[31]_i_2_n_2 ,\v_cnt_reg[31]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\v_cnt_reg[31]_i_2_n_4 ,\v_cnt_reg[31]_i_2_n_5 ,\v_cnt_reg[31]_i_2_n_6 ,\v_cnt_reg[31]_i_2_n_7 }),
        .S({\v_cnt[31]_i_5_n_0 ,\v_cnt[31]_i_6_n_0 ,\v_cnt[31]_i_7_n_0 ,\v_cnt[31]_i_8_n_0 }));
  FDCE \v_cnt_reg[3] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[3]_i_1_n_4 ),
        .Q(v_cnt[3]));
  CARRY4 \v_cnt_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\v_cnt_reg[3]_i_1_n_0 ,\v_cnt_reg[3]_i_1_n_1 ,\v_cnt_reg[3]_i_1_n_2 ,\v_cnt_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\v_cnt[3]_i_2_n_0 }),
        .O({\v_cnt_reg[3]_i_1_n_4 ,\v_cnt_reg[3]_i_1_n_5 ,\v_cnt_reg[3]_i_1_n_6 ,\v_cnt_reg[3]_i_1_n_7 }),
        .S({\v_cnt[3]_i_3_n_0 ,\v_cnt[3]_i_4_n_0 ,\v_cnt[3]_i_5_n_0 ,\v_cnt[3]_i_6_n_0 }));
  FDCE \v_cnt_reg[4] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[7]_i_1_n_7 ),
        .Q(v_cnt[4]));
  FDCE \v_cnt_reg[5] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[7]_i_1_n_6 ),
        .Q(v_cnt[5]));
  FDCE \v_cnt_reg[6] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[7]_i_1_n_5 ),
        .Q(v_cnt[6]));
  FDCE \v_cnt_reg[7] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[7]_i_1_n_4 ),
        .Q(v_cnt[7]));
  CARRY4 \v_cnt_reg[7]_i_1 
       (.CI(\v_cnt_reg[3]_i_1_n_0 ),
        .CO({\v_cnt_reg[7]_i_1_n_0 ,\v_cnt_reg[7]_i_1_n_1 ,\v_cnt_reg[7]_i_1_n_2 ,\v_cnt_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\v_cnt_reg[7]_i_1_n_4 ,\v_cnt_reg[7]_i_1_n_5 ,\v_cnt_reg[7]_i_1_n_6 ,\v_cnt_reg[7]_i_1_n_7 }),
        .S({\v_cnt[7]_i_2_n_0 ,\v_cnt[7]_i_3_n_0 ,\v_cnt[7]_i_4_n_0 ,\v_cnt[7]_i_5_n_0 }));
  FDCE \v_cnt_reg[8] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[11]_i_1_n_7 ),
        .Q(v_cnt[8]));
  FDCE \v_cnt_reg[9] 
       (.C(hdmi_in_clk),
        .CE(v_cnt0),
        .CLR(\h_cnt[31]_i_2_n_0 ),
        .D(\v_cnt_reg[11]_i_1_n_6 ),
        .Q(v_cnt[9]));
endmodule

(* ORIG_REF_NAME = "hdmi_i2c" *) 
module system_hdmi_ctrl_0_0_hdmi_i2c
   (cfg_done_reg,
    ddc_scl,
    ddc_sda,
    sys_clk,
    sys_init_done,
    sys_rst_n);
  output cfg_done_reg;
  output ddc_scl;
  inout ddc_sda;
  input sys_clk;
  input sys_init_done;
  input sys_rst_n;

  wire cfg_clk;
  wire cfg_done_reg;
  wire cfg_end;
  wire cfg_start;
  wire ddc_scl;
  wire ddc_sda;
  wire hdmi_cfg_inst_n_2;
  wire hdmi_cfg_inst_n_6;
  wire hdmi_cfg_inst_n_7;
  wire i2c_ctrl_inst_n_2;
  wire i2c_ctrl_inst_n_3;
  wire i2c_ctrl_inst_n_5;
  wire i2c_ctrl_inst_n_6;
  wire [2:0]reg_num_reg;
  wire sys_clk;
  wire sys_init_done;
  wire sys_rst_n;

  system_hdmi_ctrl_0_0_hdmi_cfg hdmi_cfg_inst
       (.CLK(cfg_clk),
        .E(cfg_end),
        .Q(reg_num_reg),
        .cfg_done_reg_0(cfg_done_reg),
        .cfg_start(cfg_start),
        .cfg_start_reg_0(i2c_ctrl_inst_n_2),
        .i2c_sda_reg_reg_i_3(i2c_ctrl_inst_n_3),
        .i2c_sda_reg_reg_i_3_0(i2c_ctrl_inst_n_6),
        .i2c_sda_reg_reg_i_3_1(i2c_ctrl_inst_n_5),
        .\reg_num_reg[1]_0 (hdmi_cfg_inst_n_2),
        .\reg_num_reg[1]_1 (hdmi_cfg_inst_n_6),
        .\reg_num_reg[1]_2 (hdmi_cfg_inst_n_7));
  system_hdmi_ctrl_0_0_i2c_ctrl i2c_ctrl_inst
       (.CLK(cfg_clk),
        .E(cfg_end),
        .Q(reg_num_reg),
        .cfg_start(cfg_start),
        .\cnt_bit_reg[0]_0 (i2c_ctrl_inst_n_5),
        .\cnt_bit_reg[1]_0 (i2c_ctrl_inst_n_6),
        .\cnt_bit_reg[2]_0 (i2c_ctrl_inst_n_3),
        .ddc_scl(ddc_scl),
        .ddc_sda(ddc_sda),
        .i2c_sda_reg_reg_i_1_0(hdmi_cfg_inst_n_7),
        .i2c_sda_reg_reg_i_1_1(hdmi_cfg_inst_n_2),
        .i2c_sda_reg_reg_i_1_2(cfg_done_reg),
        .i2c_sda_reg_reg_i_1_3(hdmi_cfg_inst_n_6),
        .sys_clk(sys_clk),
        .sys_init_done(sys_init_done),
        .sys_init_done_0(i2c_ctrl_inst_n_2),
        .sys_rst_n(sys_rst_n));
endmodule

(* ORIG_REF_NAME = "i2c_ctrl" *) 
module system_hdmi_ctrl_0_0_i2c_ctrl
   (E,
    CLK,
    sys_init_done_0,
    \cnt_bit_reg[2]_0 ,
    ddc_scl,
    \cnt_bit_reg[0]_0 ,
    \cnt_bit_reg[1]_0 ,
    ddc_sda,
    sys_clk,
    cfg_start,
    i2c_sda_reg_reg_i_1_0,
    Q,
    i2c_sda_reg_reg_i_1_1,
    i2c_sda_reg_reg_i_1_2,
    i2c_sda_reg_reg_i_1_3,
    sys_init_done,
    sys_rst_n);
  output [0:0]E;
  output CLK;
  output sys_init_done_0;
  output \cnt_bit_reg[2]_0 ;
  output ddc_scl;
  output \cnt_bit_reg[0]_0 ;
  output \cnt_bit_reg[1]_0 ;
  inout ddc_sda;
  input sys_clk;
  input cfg_start;
  input i2c_sda_reg_reg_i_1_0;
  input [2:0]Q;
  input i2c_sda_reg_reg_i_1_1;
  input i2c_sda_reg_reg_i_1_2;
  input i2c_sda_reg_reg_i_1_3;
  input sys_init_done;
  input sys_rst_n;

  wire CLK;
  wire [0:0]E;
  wire \FSM_onehot_state[0]_i_1_n_0 ;
  wire \FSM_onehot_state[10]_i_1_n_0 ;
  wire \FSM_onehot_state[11]_i_1_n_0 ;
  wire \FSM_onehot_state[11]_i_2_n_0 ;
  wire \FSM_onehot_state[12]_i_1_n_0 ;
  wire \FSM_onehot_state[13]_i_1_n_0 ;
  wire \FSM_onehot_state[14]_i_1_n_0 ;
  wire \FSM_onehot_state[14]_i_2_n_0 ;
  wire \FSM_onehot_state[15]_i_1_n_0 ;
  wire \FSM_onehot_state[15]_i_2_n_0 ;
  wire \FSM_onehot_state[15]_i_3_n_0 ;
  wire \FSM_onehot_state[2]_i_1_n_0 ;
  wire \FSM_onehot_state[6]_i_1_n_0 ;
  wire \FSM_onehot_state_reg_n_0_[11] ;
  wire \FSM_onehot_state_reg_n_0_[13] ;
  wire \FSM_onehot_state_reg_n_0_[14] ;
  wire \FSM_onehot_state_reg_n_0_[15] ;
  wire \FSM_onehot_state_reg_n_0_[2] ;
  wire \FSM_onehot_state_reg_n_0_[6] ;
  wire [2:0]Q;
  wire ack;
  wire ack__0;
  wire ack_reg_i_1_n_0;
  wire ack_reg_i_3_n_0;
  wire ack_reg_i_4_n_0;
  wire cfg_start;
  wire \cnt_bit[0]_i_1_n_0 ;
  wire \cnt_bit[1]_i_1_n_0 ;
  wire \cnt_bit[2]_i_1_n_0 ;
  wire \cnt_bit_reg[0]_0 ;
  wire \cnt_bit_reg[1]_0 ;
  wire \cnt_bit_reg[2]_0 ;
  wire [7:0]cnt_clk;
  wire \cnt_clk[0]_i_2_n_0 ;
  wire \cnt_clk[4]_i_2_n_0 ;
  wire \cnt_clk[7]_i_2_n_0 ;
  wire [7:0]cnt_clk_0;
  wire [1:0]cnt_i2c_clk;
  wire \cnt_i2c_clk[0]_i_1_n_0 ;
  wire \cnt_i2c_clk[1]_i_1_n_0 ;
  wire cnt_i2c_clk_en;
  wire cnt_i2c_clk_en1;
  wire cnt_i2c_clk_en_i_1_n_0;
  wire ddc_scl;
  wire ddc_scl_INST_0_i_1_n_0;
  wire ddc_scl_INST_0_i_2_n_0;
  wire ddc_scl_INST_0_i_3_n_0;
  wire ddc_sda;
  wire ddc_sda_INST_0_i_1_n_0;
  wire i2c_clk_i_1_n_0;
  wire i2c_clk_i_2_n_0;
  wire i2c_sda_reg;
  wire i2c_sda_reg__0;
  wire i2c_sda_reg_reg_i_11_n_0;
  wire i2c_sda_reg_reg_i_13_n_0;
  wire i2c_sda_reg_reg_i_14_n_0;
  wire i2c_sda_reg_reg_i_15_n_0;
  wire i2c_sda_reg_reg_i_1_0;
  wire i2c_sda_reg_reg_i_1_1;
  wire i2c_sda_reg_reg_i_1_2;
  wire i2c_sda_reg_reg_i_1_3;
  wire i2c_sda_reg_reg_i_1_n_0;
  wire i2c_sda_reg_reg_i_3_n_0;
  wire i2c_sda_reg_reg_i_4_n_0;
  wire i2c_sda_reg_reg_i_5_n_0;
  wire i2c_sda_reg_reg_i_6_n_0;
  wire i2c_sda_reg_reg_i_7_n_0;
  wire i2c_sda_reg_reg_i_8_n_0;
  wire i2c_sda_reg_reg_i_9_n_0;
  wire p_1_in;
  wire p_3_in;
  wire p_5_in;
  wire sys_clk;
  wire sys_init_done;
  wire sys_init_done_0;
  wire sys_rst_n;

  LUT6 #(
    .INIT(64'hAA2AFFFFAA2AAA2A)) 
    \FSM_onehot_state[0]_i_1 
       (.I0(p_3_in),
        .I1(cnt_i2c_clk[1]),
        .I2(cnt_i2c_clk[0]),
        .I3(ack),
        .I4(\FSM_onehot_state[14]_i_2_n_0 ),
        .I5(\FSM_onehot_state_reg_n_0_[6] ),
        .O(\FSM_onehot_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hFF2A2A2A)) 
    \FSM_onehot_state[10]_i_1 
       (.I0(p_1_in),
        .I1(cnt_i2c_clk[0]),
        .I2(cnt_i2c_clk[1]),
        .I3(cfg_start),
        .I4(\FSM_onehot_state_reg_n_0_[11] ),
        .O(\FSM_onehot_state[10]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h4F)) 
    \FSM_onehot_state[11]_i_1 
       (.I0(cfg_start),
        .I1(\FSM_onehot_state_reg_n_0_[11] ),
        .I2(\FSM_onehot_state[11]_i_2_n_0 ),
        .O(\FSM_onehot_state[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFFFFFFFFFFFFF)) 
    \FSM_onehot_state[11]_i_2 
       (.I0(\cnt_bit_reg[2]_0 ),
        .I1(\cnt_bit_reg[1]_0 ),
        .I2(\cnt_bit_reg[0]_0 ),
        .I3(cnt_i2c_clk[1]),
        .I4(cnt_i2c_clk[0]),
        .I5(\FSM_onehot_state_reg_n_0_[15] ),
        .O(\FSM_onehot_state[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF4F4F4F444F4F4F4)) 
    \FSM_onehot_state[12]_i_1 
       (.I0(\FSM_onehot_state[14]_i_2_n_0 ),
        .I1(\FSM_onehot_state_reg_n_0_[2] ),
        .I2(p_5_in),
        .I3(cnt_i2c_clk[1]),
        .I4(cnt_i2c_clk[0]),
        .I5(ack),
        .O(\FSM_onehot_state[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF080008000800)) 
    \FSM_onehot_state[13]_i_1 
       (.I0(cnt_i2c_clk[1]),
        .I1(cnt_i2c_clk[0]),
        .I2(ack),
        .I3(p_5_in),
        .I4(\FSM_onehot_state_reg_n_0_[13] ),
        .I5(\FSM_onehot_state[14]_i_2_n_0 ),
        .O(\FSM_onehot_state[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF4F4F4F444F4F4F4)) 
    \FSM_onehot_state[14]_i_1 
       (.I0(\FSM_onehot_state[14]_i_2_n_0 ),
        .I1(\FSM_onehot_state_reg_n_0_[13] ),
        .I2(\FSM_onehot_state_reg_n_0_[14] ),
        .I3(cnt_i2c_clk[1]),
        .I4(cnt_i2c_clk[0]),
        .I5(ack),
        .O(\FSM_onehot_state[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \FSM_onehot_state[14]_i_2 
       (.I0(cnt_i2c_clk[0]),
        .I1(cnt_i2c_clk[1]),
        .I2(\cnt_bit_reg[0]_0 ),
        .I3(\cnt_bit_reg[1]_0 ),
        .I4(\cnt_bit_reg[2]_0 ),
        .O(\FSM_onehot_state[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hF0F4F0F4F0F4F044)) 
    \FSM_onehot_state[15]_i_1 
       (.I0(ack),
        .I1(\FSM_onehot_state_reg_n_0_[14] ),
        .I2(\FSM_onehot_state_reg_n_0_[15] ),
        .I3(\FSM_onehot_state[15]_i_2_n_0 ),
        .I4(\FSM_onehot_state[15]_i_3_n_0 ),
        .I5(\cnt_bit_reg[2]_0 ),
        .O(\FSM_onehot_state[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \FSM_onehot_state[15]_i_2 
       (.I0(cnt_i2c_clk[0]),
        .I1(cnt_i2c_clk[1]),
        .O(\FSM_onehot_state[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \FSM_onehot_state[15]_i_3 
       (.I0(\cnt_bit_reg[0]_0 ),
        .I1(\cnt_bit_reg[1]_0 ),
        .O(\FSM_onehot_state[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF080008000800)) 
    \FSM_onehot_state[2]_i_1 
       (.I0(cnt_i2c_clk[1]),
        .I1(cnt_i2c_clk[0]),
        .I2(ack),
        .I3(p_3_in),
        .I4(\FSM_onehot_state_reg_n_0_[2] ),
        .I5(\FSM_onehot_state[14]_i_2_n_0 ),
        .O(\FSM_onehot_state[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF8888888)) 
    \FSM_onehot_state[6]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(\FSM_onehot_state[14]_i_2_n_0 ),
        .I2(cnt_i2c_clk[0]),
        .I3(cnt_i2c_clk[1]),
        .I4(p_1_in),
        .O(\FSM_onehot_state[6]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\FSM_onehot_state[0]_i_1_n_0 ),
        .Q(p_3_in));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[10] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\FSM_onehot_state[10]_i_1_n_0 ),
        .Q(p_1_in));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDPE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg[11] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_onehot_state[11]_i_1_n_0 ),
        .PRE(sys_init_done_0),
        .Q(\FSM_onehot_state_reg_n_0_[11] ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[12] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\FSM_onehot_state[12]_i_1_n_0 ),
        .Q(p_5_in));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[13] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\FSM_onehot_state[13]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[13] ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[14] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\FSM_onehot_state[14]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[14] ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[15] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\FSM_onehot_state[15]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[15] ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\FSM_onehot_state[2]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[2] ));
  (* FSM_ENCODED_STATES = "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[6] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\FSM_onehot_state[6]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[6] ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    ack_reg
       (.CLR(1'b0),
        .D(ack_reg_i_1_n_0),
        .G(ack__0),
        .GE(1'b1),
        .Q(ack));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFAAAB)) 
    ack_reg_i_1
       (.I0(ack_reg_i_3_n_0),
        .I1(\FSM_onehot_state_reg_n_0_[14] ),
        .I2(p_5_in),
        .I3(p_3_in),
        .I4(p_1_in),
        .I5(\FSM_onehot_state_reg_n_0_[11] ),
        .O(ack_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF1FF)) 
    ack_reg_i_2
       (.I0(cnt_i2c_clk[0]),
        .I1(cnt_i2c_clk[1]),
        .I2(ddc_scl_INST_0_i_2_n_0),
        .I3(ack_reg_i_4_n_0),
        .I4(p_1_in),
        .I5(\FSM_onehot_state_reg_n_0_[11] ),
        .O(ack__0));
  LUT2 #(
    .INIT(4'hE)) 
    ack_reg_i_3
       (.I0(ddc_sda),
        .I1(ddc_scl_INST_0_i_2_n_0),
        .O(ack_reg_i_3_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    ack_reg_i_4
       (.I0(\FSM_onehot_state_reg_n_0_[14] ),
        .I1(p_5_in),
        .I2(p_3_in),
        .O(ack_reg_i_4_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    cfg_done_i_2
       (.I0(sys_init_done),
        .I1(sys_rst_n),
        .O(sys_init_done_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h1540)) 
    \cnt_bit[0]_i_1 
       (.I0(i2c_sda_reg_reg_i_9_n_0),
        .I1(cnt_i2c_clk[1]),
        .I2(cnt_i2c_clk[0]),
        .I3(\cnt_bit_reg[0]_0 ),
        .O(\cnt_bit[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h13332000)) 
    \cnt_bit[1]_i_1 
       (.I0(\cnt_bit_reg[0]_0 ),
        .I1(i2c_sda_reg_reg_i_9_n_0),
        .I2(cnt_i2c_clk[1]),
        .I3(cnt_i2c_clk[0]),
        .I4(\cnt_bit_reg[1]_0 ),
        .O(\cnt_bit[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h070F0F0F08000000)) 
    \cnt_bit[2]_i_1 
       (.I0(\cnt_bit_reg[1]_0 ),
        .I1(\cnt_bit_reg[0]_0 ),
        .I2(i2c_sda_reg_reg_i_9_n_0),
        .I3(cnt_i2c_clk[1]),
        .I4(cnt_i2c_clk[0]),
        .I5(\cnt_bit_reg[2]_0 ),
        .O(\cnt_bit[2]_i_1_n_0 ));
  FDCE \cnt_bit_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\cnt_bit[0]_i_1_n_0 ),
        .Q(\cnt_bit_reg[0]_0 ));
  FDCE \cnt_bit_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\cnt_bit[1]_i_1_n_0 ),
        .Q(\cnt_bit_reg[1]_0 ));
  FDCE \cnt_bit_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\cnt_bit[2]_i_1_n_0 ),
        .Q(\cnt_bit_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h00FF00F7)) 
    \cnt_clk[0]_i_1 
       (.I0(cnt_clk[4]),
        .I1(cnt_clk[3]),
        .I2(cnt_clk[1]),
        .I3(cnt_clk[0]),
        .I4(\cnt_clk[0]_i_2_n_0 ),
        .O(cnt_clk_0[0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cnt_clk[0]_i_2 
       (.I0(cnt_clk[5]),
        .I1(cnt_clk[2]),
        .I2(cnt_clk[7]),
        .I3(cnt_clk[6]),
        .O(\cnt_clk[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt_clk[1]_i_1 
       (.I0(cnt_clk[0]),
        .I1(cnt_clk[1]),
        .O(cnt_clk_0[1]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt_clk[2]_i_1 
       (.I0(cnt_clk[1]),
        .I1(cnt_clk[0]),
        .I2(cnt_clk[2]),
        .O(cnt_clk_0[2]));
  LUT6 #(
    .INIT(64'hFFFFC0001111C000)) 
    \cnt_clk[3]_i_1 
       (.I0(cnt_clk[4]),
        .I1(cnt_clk[1]),
        .I2(cnt_clk[0]),
        .I3(cnt_clk[2]),
        .I4(cnt_clk[3]),
        .I5(\cnt_clk[4]_i_2_n_0 ),
        .O(cnt_clk_0[3]));
  LUT6 #(
    .INIT(64'hFFFF800055558000)) 
    \cnt_clk[4]_i_1 
       (.I0(cnt_clk[3]),
        .I1(cnt_clk[1]),
        .I2(cnt_clk[0]),
        .I3(cnt_clk[2]),
        .I4(cnt_clk[4]),
        .I5(\cnt_clk[4]_i_2_n_0 ),
        .O(cnt_clk_0[4]));
  LUT6 #(
    .INIT(64'h55FF55FFFFFFFFFE)) 
    \cnt_clk[4]_i_2 
       (.I0(cnt_clk[1]),
        .I1(cnt_clk[6]),
        .I2(cnt_clk[7]),
        .I3(cnt_clk[2]),
        .I4(cnt_clk[5]),
        .I5(cnt_clk[0]),
        .O(\cnt_clk[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \cnt_clk[5]_i_1 
       (.I0(cnt_clk[3]),
        .I1(cnt_clk[4]),
        .I2(cnt_clk[2]),
        .I3(cnt_clk[0]),
        .I4(cnt_clk[1]),
        .I5(cnt_clk[5]),
        .O(cnt_clk_0[5]));
  LUT5 #(
    .INIT(32'hDFFF2000)) 
    \cnt_clk[6]_i_1 
       (.I0(cnt_clk[5]),
        .I1(\cnt_clk[7]_i_2_n_0 ),
        .I2(cnt_clk[4]),
        .I3(cnt_clk[3]),
        .I4(cnt_clk[6]),
        .O(cnt_clk_0[6]));
  LUT6 #(
    .INIT(64'hFF7FFFFF00800000)) 
    \cnt_clk[7]_i_1 
       (.I0(cnt_clk[6]),
        .I1(cnt_clk[3]),
        .I2(cnt_clk[4]),
        .I3(\cnt_clk[7]_i_2_n_0 ),
        .I4(cnt_clk[5]),
        .I5(cnt_clk[7]),
        .O(cnt_clk_0[7]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \cnt_clk[7]_i_2 
       (.I0(cnt_clk[2]),
        .I1(cnt_clk[0]),
        .I2(cnt_clk[1]),
        .O(\cnt_clk[7]_i_2_n_0 ));
  FDCE \cnt_clk_reg[0] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_clk_0[0]),
        .Q(cnt_clk[0]));
  FDCE \cnt_clk_reg[1] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_clk_0[1]),
        .Q(cnt_clk[1]));
  FDCE \cnt_clk_reg[2] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_clk_0[2]),
        .Q(cnt_clk[2]));
  FDCE \cnt_clk_reg[3] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_clk_0[3]),
        .Q(cnt_clk[3]));
  FDCE \cnt_clk_reg[4] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_clk_0[4]),
        .Q(cnt_clk[4]));
  FDCE \cnt_clk_reg[5] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_clk_0[5]),
        .Q(cnt_clk[5]));
  FDCE \cnt_clk_reg[6] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_clk_0[6]),
        .Q(cnt_clk[6]));
  FDCE \cnt_clk_reg[7] 
       (.C(sys_clk),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_clk_0[7]),
        .Q(cnt_clk[7]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt_i2c_clk[0]_i_1 
       (.I0(cnt_i2c_clk_en),
        .I1(cnt_i2c_clk[0]),
        .O(\cnt_i2c_clk[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt_i2c_clk[1]_i_1 
       (.I0(cnt_i2c_clk[0]),
        .I1(cnt_i2c_clk_en),
        .I2(cnt_i2c_clk[1]),
        .O(\cnt_i2c_clk[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hC8)) 
    cnt_i2c_clk_en_i_1
       (.I0(cfg_start),
        .I1(\FSM_onehot_state[11]_i_2_n_0 ),
        .I2(cnt_i2c_clk_en),
        .O(cnt_i2c_clk_en_i_1_n_0));
  FDCE cnt_i2c_clk_en_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_i2c_clk_en_i_1_n_0),
        .Q(cnt_i2c_clk_en));
  FDCE \cnt_i2c_clk_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\cnt_i2c_clk[0]_i_1_n_0 ),
        .Q(cnt_i2c_clk[0]));
  FDCE \cnt_i2c_clk_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(\cnt_i2c_clk[1]_i_1_n_0 ),
        .Q(cnt_i2c_clk[1]));
  LUT6 #(
    .INIT(64'hBEBEBEBEBEBEBEAA)) 
    ddc_scl_INST_0
       (.I0(ddc_scl_INST_0_i_1_n_0),
        .I1(cnt_i2c_clk[0]),
        .I2(cnt_i2c_clk[1]),
        .I3(\FSM_onehot_state_reg_n_0_[14] ),
        .I4(p_5_in),
        .I5(p_3_in),
        .O(ddc_scl));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFC3F28)) 
    ddc_scl_INST_0_i_1
       (.I0(ddc_scl_INST_0_i_2_n_0),
        .I1(cnt_i2c_clk[1]),
        .I2(cnt_i2c_clk[0]),
        .I3(p_1_in),
        .I4(\FSM_onehot_state_reg_n_0_[15] ),
        .I5(ddc_scl_INST_0_i_3_n_0),
        .O(ddc_scl_INST_0_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    ddc_scl_INST_0_i_2
       (.I0(\FSM_onehot_state_reg_n_0_[13] ),
        .I1(\FSM_onehot_state_reg_n_0_[6] ),
        .I2(\FSM_onehot_state_reg_n_0_[2] ),
        .O(ddc_scl_INST_0_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hFFFFAAA8)) 
    ddc_scl_INST_0_i_3
       (.I0(\FSM_onehot_state_reg_n_0_[15] ),
        .I1(\cnt_bit_reg[1]_0 ),
        .I2(\cnt_bit_reg[0]_0 ),
        .I3(\cnt_bit_reg[2]_0 ),
        .I4(\FSM_onehot_state_reg_n_0_[11] ),
        .O(ddc_scl_INST_0_i_3_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    ddc_sda_INST_0
       (.I0(i2c_sda_reg),
        .I1(ddc_sda_INST_0_i_1_n_0),
        .O(ddc_sda));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h01)) 
    ddc_sda_INST_0_i_1
       (.I0(p_3_in),
        .I1(p_5_in),
        .I2(\FSM_onehot_state_reg_n_0_[14] ),
        .O(ddc_sda_INST_0_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFBF0040)) 
    i2c_clk_i_1
       (.I0(i2c_clk_i_2_n_0),
        .I1(cnt_clk[4]),
        .I2(cnt_clk[3]),
        .I3(cnt_clk[1]),
        .I4(CLK),
        .O(i2c_clk_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    i2c_clk_i_2
       (.I0(cnt_clk[6]),
        .I1(cnt_clk[7]),
        .I2(cnt_clk[2]),
        .I3(cnt_clk[5]),
        .I4(cnt_clk[0]),
        .O(i2c_clk_i_2_n_0));
  FDPE i2c_clk_reg
       (.C(sys_clk),
        .CE(1'b1),
        .D(i2c_clk_i_1_n_0),
        .PRE(sys_init_done_0),
        .Q(CLK));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    i2c_end_i_1
       (.I0(\FSM_onehot_state_reg_n_0_[15] ),
        .I1(cnt_i2c_clk[0]),
        .I2(cnt_i2c_clk[1]),
        .I3(\cnt_bit_reg[0]_0 ),
        .I4(\cnt_bit_reg[1]_0 ),
        .I5(\cnt_bit_reg[2]_0 ),
        .O(cnt_i2c_clk_en1));
  FDCE i2c_end_reg
       (.C(CLK),
        .CE(1'b1),
        .CLR(sys_init_done_0),
        .D(cnt_i2c_clk_en1),
        .Q(E));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    i2c_sda_reg_reg
       (.CLR(1'b0),
        .D(i2c_sda_reg_reg_i_1_n_0),
        .G(i2c_sda_reg__0),
        .GE(1'b1),
        .Q(i2c_sda_reg));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    i2c_sda_reg_reg_i_1
       (.I0(i2c_sda_reg_reg_i_3_n_0),
        .I1(i2c_sda_reg_reg_i_4_n_0),
        .I2(i2c_sda_reg_reg_i_5_n_0),
        .I3(i2c_sda_reg_reg_i_6_n_0),
        .I4(i2c_sda_reg_reg_i_7_n_0),
        .I5(i2c_sda_reg_reg_i_8_n_0),
        .O(i2c_sda_reg_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'hCCEECCEE0C220022)) 
    i2c_sda_reg_reg_i_11
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(\cnt_bit_reg[0]_0 ),
        .I2(\cnt_bit_reg[1]_0 ),
        .I3(Q[2]),
        .I4(\FSM_onehot_state_reg_n_0_[13] ),
        .I5(\FSM_onehot_state_reg_n_0_[2] ),
        .O(i2c_sda_reg_reg_i_11_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    i2c_sda_reg_reg_i_13
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(\cnt_bit_reg[2]_0 ),
        .O(i2c_sda_reg_reg_i_13_n_0));
  LUT6 #(
    .INIT(64'h00F00080FFF00080)) 
    i2c_sda_reg_reg_i_14
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(Q[0]),
        .I2(\cnt_bit_reg[1]_0 ),
        .I3(\cnt_bit_reg[0]_0 ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(\cnt_bit_reg[2]_0 ),
        .O(i2c_sda_reg_reg_i_14_n_0));
  LUT6 #(
    .INIT(64'h808FC0C080800000)) 
    i2c_sda_reg_reg_i_15
       (.I0(\FSM_onehot_state_reg_n_0_[13] ),
        .I1(Q[1]),
        .I2(\cnt_bit_reg[0]_0 ),
        .I3(\cnt_bit_reg[1]_0 ),
        .I4(Q[0]),
        .I5(\FSM_onehot_state_reg_n_0_[2] ),
        .O(i2c_sda_reg_reg_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    i2c_sda_reg_reg_i_2
       (.I0(\FSM_onehot_state_reg_n_0_[15] ),
        .I1(\FSM_onehot_state_reg_n_0_[13] ),
        .I2(\FSM_onehot_state_reg_n_0_[6] ),
        .I3(\FSM_onehot_state_reg_n_0_[2] ),
        .I4(i2c_sda_reg_reg_i_9_n_0),
        .O(i2c_sda_reg__0));
  LUT6 #(
    .INIT(64'hFFAAAAAAEAEAEAEA)) 
    i2c_sda_reg_reg_i_3
       (.I0(ack_reg_i_4_n_0),
        .I1(\FSM_onehot_state_reg_n_0_[13] ),
        .I2(i2c_sda_reg_reg_i_1_1),
        .I3(i2c_sda_reg_reg_i_11_n_0),
        .I4(i2c_sda_reg_reg_i_1_0),
        .I5(Q[0]),
        .O(i2c_sda_reg_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF02200000)) 
    i2c_sda_reg_reg_i_4
       (.I0(i2c_sda_reg_reg_i_13_n_0),
        .I1(i2c_sda_reg_reg_i_1_2),
        .I2(\cnt_bit_reg[1]_0 ),
        .I3(\cnt_bit_reg[0]_0 ),
        .I4(i2c_sda_reg_reg_i_1_3),
        .I5(ddc_scl_INST_0_i_3_n_0),
        .O(i2c_sda_reg_reg_i_4_n_0));
  LUT6 #(
    .INIT(64'h8000000080000088)) 
    i2c_sda_reg_reg_i_5
       (.I0(\FSM_onehot_state_reg_n_0_[2] ),
        .I1(i2c_sda_reg_reg_i_1_0),
        .I2(Q[2]),
        .I3(\cnt_bit_reg[1]_0 ),
        .I4(\cnt_bit_reg[0]_0 ),
        .I5(Q[0]),
        .O(i2c_sda_reg_reg_i_5_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    i2c_sda_reg_reg_i_6
       (.I0(\FSM_onehot_state_reg_n_0_[15] ),
        .I1(cnt_i2c_clk[1]),
        .I2(cnt_i2c_clk[0]),
        .O(i2c_sda_reg_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'h0000000000F20022)) 
    i2c_sda_reg_reg_i_7
       (.I0(i2c_sda_reg_reg_i_14_n_0),
        .I1(Q[1]),
        .I2(i2c_sda_reg_reg_i_15_n_0),
        .I3(i2c_sda_reg_reg_i_1_2),
        .I4(\cnt_bit_reg[2]_0 ),
        .I5(Q[2]),
        .O(i2c_sda_reg_reg_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h02)) 
    i2c_sda_reg_reg_i_8
       (.I0(p_1_in),
        .I1(cnt_i2c_clk[1]),
        .I2(cnt_i2c_clk[0]),
        .O(i2c_sda_reg_reg_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    i2c_sda_reg_reg_i_9
       (.I0(\FSM_onehot_state_reg_n_0_[11] ),
        .I1(p_1_in),
        .I2(p_3_in),
        .I3(p_5_in),
        .I4(\FSM_onehot_state_reg_n_0_[14] ),
        .O(i2c_sda_reg_reg_i_9_n_0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
