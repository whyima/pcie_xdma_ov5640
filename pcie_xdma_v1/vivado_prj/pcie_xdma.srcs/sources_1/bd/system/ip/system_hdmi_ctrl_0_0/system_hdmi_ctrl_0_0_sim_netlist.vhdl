-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (win64) Build 2708876 Wed Nov  6 21:40:23 MST 2019
-- Date        : Mon Dec 25 15:59:51 2023
-- Host        : DESKTOP-QGH9D13 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               E:/Workspace/Vivado/2019.2/pcie_xdma_v1/vivado_prj/pcie_xdma.srcs/sources_1/bd/system/ip/system_hdmi_ctrl_0_0/system_hdmi_ctrl_0_0_sim_netlist.vhdl
-- Design      : system_hdmi_ctrl_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_hdmi_ctrl_0_0_hdmi_cfg is
  port (
    cfg_start : out STD_LOGIC;
    cfg_done_reg_0 : out STD_LOGIC;
    \reg_num_reg[1]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \reg_num_reg[1]_1\ : out STD_LOGIC;
    \reg_num_reg[1]_2\ : out STD_LOGIC;
    CLK : in STD_LOGIC;
    cfg_start_reg_0 : in STD_LOGIC;
    i2c_sda_reg_reg_i_3 : in STD_LOGIC;
    i2c_sda_reg_reg_i_3_0 : in STD_LOGIC;
    i2c_sda_reg_reg_i_3_1 : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_hdmi_ctrl_0_0_hdmi_cfg : entity is "hdmi_cfg";
end system_hdmi_ctrl_0_0_hdmi_cfg;

architecture STRUCTURE of system_hdmi_ctrl_0_0_hdmi_cfg is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal cfg_done_i_1_n_0 : STD_LOGIC;
  signal cfg_done_i_3_n_0 : STD_LOGIC;
  signal \^cfg_done_reg_0\ : STD_LOGIC;
  signal cfg_start_0 : STD_LOGIC;
  signal cfg_start_i_2_n_0 : STD_LOGIC;
  signal cfg_start_i_3_n_0 : STD_LOGIC;
  signal cfg_start_i_4_n_0 : STD_LOGIC;
  signal \cnt_wait[10]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_wait[10]_i_4_n_0\ : STD_LOGIC;
  signal cnt_wait_reg : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal reg_num_reg : STD_LOGIC_VECTOR ( 6 downto 3 );
  signal \^reg_num_reg[1]_1\ : STD_LOGIC;
  signal sel : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of cfg_done_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of cfg_start_i_3 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \cnt_wait[0]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \cnt_wait[10]_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \cnt_wait[1]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \cnt_wait[2]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \cnt_wait[3]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt_wait[4]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt_wait[6]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt_wait[7]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt_wait[9]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \reg_num[1]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \reg_num[2]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \reg_num[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \reg_num[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \reg_num[6]_i_2\ : label is "soft_lutpair4";
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
  cfg_done_reg_0 <= \^cfg_done_reg_0\;
  \reg_num_reg[1]_1\ <= \^reg_num_reg[1]_1\;
cfg_done_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF80"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => cfg_done_i_3_n_0,
      I3 => \^cfg_done_reg_0\,
      O => cfg_done_i_1_n_0
    );
cfg_done_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000010"
    )
        port map (
      I0 => reg_num_reg(3),
      I1 => reg_num_reg(4),
      I2 => E(0),
      I3 => \^q\(0),
      I4 => reg_num_reg(6),
      I5 => reg_num_reg(5),
      O => cfg_done_i_3_n_0
    );
cfg_done_reg: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => '1',
      CLR => cfg_start_reg_0,
      D => cfg_done_i_1_n_0,
      Q => \^cfg_done_reg_0\
    );
cfg_start_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF02"
    )
        port map (
      I0 => cfg_start_i_2_n_0,
      I1 => cfg_start_i_3_n_0,
      I2 => cnt_wait_reg(4),
      I3 => cfg_start_i_4_n_0,
      O => cfg_start_0
    );
cfg_start_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => cnt_wait_reg(7),
      I1 => cnt_wait_reg(8),
      I2 => cnt_wait_reg(6),
      I3 => cnt_wait_reg(5),
      I4 => cnt_wait_reg(10),
      I5 => cnt_wait_reg(9),
      O => cfg_start_i_2_n_0
    );
cfg_start_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => cnt_wait_reg(2),
      I1 => cnt_wait_reg(0),
      I2 => cnt_wait_reg(1),
      I3 => cnt_wait_reg(3),
      O => cfg_start_i_3_n_0
    );
cfg_start_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => reg_num_reg(5),
      I1 => reg_num_reg(6),
      I2 => \^reg_num_reg[1]_1\,
      I3 => E(0),
      I4 => reg_num_reg(3),
      I5 => reg_num_reg(4),
      O => cfg_start_i_4_n_0
    );
cfg_start_reg: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => '1',
      CLR => cfg_start_reg_0,
      D => cfg_start_0,
      Q => cfg_start
    );
\cnt_wait[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_wait_reg(0),
      O => \p_0_in__0\(0)
    );
\cnt_wait[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57FFFFFFFFFFFFFF"
    )
        port map (
      I0 => \cnt_wait[10]_i_3_n_0\,
      I1 => cnt_wait_reg(5),
      I2 => cnt_wait_reg(4),
      I3 => cnt_wait_reg(10),
      I4 => cnt_wait_reg(9),
      I5 => cnt_wait_reg(6),
      O => sel
    );
\cnt_wait[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFFFFF08000000"
    )
        port map (
      I0 => cnt_wait_reg(9),
      I1 => cnt_wait_reg(7),
      I2 => \cnt_wait[10]_i_4_n_0\,
      I3 => cnt_wait_reg(6),
      I4 => cnt_wait_reg(8),
      I5 => cnt_wait_reg(10),
      O => \p_0_in__0\(10)
    );
\cnt_wait[10]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_wait_reg(7),
      I1 => cnt_wait_reg(8),
      O => \cnt_wait[10]_i_3_n_0\
    );
\cnt_wait[10]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => cnt_wait_reg(4),
      I1 => cnt_wait_reg(2),
      I2 => cnt_wait_reg(0),
      I3 => cnt_wait_reg(1),
      I4 => cnt_wait_reg(3),
      I5 => cnt_wait_reg(5),
      O => \cnt_wait[10]_i_4_n_0\
    );
\cnt_wait[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_wait_reg(0),
      I1 => cnt_wait_reg(1),
      O => \p_0_in__0\(1)
    );
\cnt_wait[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt_wait_reg(1),
      I1 => cnt_wait_reg(0),
      I2 => cnt_wait_reg(2),
      O => \p_0_in__0\(2)
    );
\cnt_wait[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => cnt_wait_reg(2),
      I1 => cnt_wait_reg(0),
      I2 => cnt_wait_reg(1),
      I3 => cnt_wait_reg(3),
      O => \p_0_in__0\(3)
    );
\cnt_wait[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => cnt_wait_reg(3),
      I1 => cnt_wait_reg(1),
      I2 => cnt_wait_reg(0),
      I3 => cnt_wait_reg(2),
      I4 => cnt_wait_reg(4),
      O => \p_0_in__0\(4)
    );
\cnt_wait[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => cnt_wait_reg(4),
      I1 => cnt_wait_reg(2),
      I2 => cnt_wait_reg(0),
      I3 => cnt_wait_reg(1),
      I4 => cnt_wait_reg(3),
      I5 => cnt_wait_reg(5),
      O => \p_0_in__0\(5)
    );
\cnt_wait[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DF20"
    )
        port map (
      I0 => cnt_wait_reg(5),
      I1 => cfg_start_i_3_n_0,
      I2 => cnt_wait_reg(4),
      I3 => cnt_wait_reg(6),
      O => \p_0_in__0\(6)
    );
\cnt_wait[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F7FF0800"
    )
        port map (
      I0 => cnt_wait_reg(6),
      I1 => cnt_wait_reg(4),
      I2 => cfg_start_i_3_n_0,
      I3 => cnt_wait_reg(5),
      I4 => cnt_wait_reg(7),
      O => \p_0_in__0\(7)
    );
\cnt_wait[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFFFFF08000000"
    )
        port map (
      I0 => cnt_wait_reg(7),
      I1 => cnt_wait_reg(5),
      I2 => cfg_start_i_3_n_0,
      I3 => cnt_wait_reg(4),
      I4 => cnt_wait_reg(6),
      I5 => cnt_wait_reg(8),
      O => \p_0_in__0\(8)
    );
\cnt_wait[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F7FF0800"
    )
        port map (
      I0 => cnt_wait_reg(8),
      I1 => cnt_wait_reg(6),
      I2 => \cnt_wait[10]_i_4_n_0\,
      I3 => cnt_wait_reg(7),
      I4 => cnt_wait_reg(9),
      O => \p_0_in__0\(9)
    );
\cnt_wait_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(0),
      Q => cnt_wait_reg(0)
    );
\cnt_wait_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(10),
      Q => cnt_wait_reg(10)
    );
\cnt_wait_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(1),
      Q => cnt_wait_reg(1)
    );
\cnt_wait_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(2),
      Q => cnt_wait_reg(2)
    );
\cnt_wait_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(3),
      Q => cnt_wait_reg(3)
    );
\cnt_wait_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(4),
      Q => cnt_wait_reg(4)
    );
\cnt_wait_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(5),
      Q => cnt_wait_reg(5)
    );
\cnt_wait_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(6),
      Q => cnt_wait_reg(6)
    );
\cnt_wait_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(7),
      Q => cnt_wait_reg(7)
    );
\cnt_wait_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(8),
      Q => cnt_wait_reg(8)
    );
\cnt_wait_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => sel,
      CLR => cfg_start_reg_0,
      D => \p_0_in__0\(9),
      Q => cnt_wait_reg(9)
    );
i2c_sda_reg_reg_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1013001000010000"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^cfg_done_reg_0\,
      I2 => i2c_sda_reg_reg_i_3,
      I3 => \^q\(2),
      I4 => i2c_sda_reg_reg_i_3_0,
      I5 => i2c_sda_reg_reg_i_3_1,
      O => \reg_num_reg[1]_0\
    );
i2c_sda_reg_reg_i_12: unisim.vcomponents.LUT3
    generic map(
      INIT => X"10"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^cfg_done_reg_0\,
      I2 => i2c_sda_reg_reg_i_3,
      O => \reg_num_reg[1]_2\
    );
\reg_num[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => p_0_in(0)
    );
\reg_num[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => p_0_in(1)
    );
\reg_num[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      O => p_0_in(2)
    );
\reg_num[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => reg_num_reg(3),
      O => p_0_in(3)
    );
\reg_num[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => reg_num_reg(3),
      I1 => \^q\(2),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => reg_num_reg(4),
      O => p_0_in(4)
    );
\reg_num[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => reg_num_reg(4),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(2),
      I4 => reg_num_reg(3),
      I5 => reg_num_reg(5),
      O => p_0_in(5)
    );
\reg_num[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFFFFF08000000"
    )
        port map (
      I0 => reg_num_reg(5),
      I1 => reg_num_reg(3),
      I2 => \^reg_num_reg[1]_1\,
      I3 => \^q\(0),
      I4 => reg_num_reg(4),
      I5 => reg_num_reg(6),
      O => p_0_in(6)
    );
\reg_num[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      O => \^reg_num_reg[1]_1\
    );
\reg_num_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => E(0),
      CLR => cfg_start_reg_0,
      D => p_0_in(0),
      Q => \^q\(0)
    );
\reg_num_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => E(0),
      CLR => cfg_start_reg_0,
      D => p_0_in(1),
      Q => \^q\(1)
    );
\reg_num_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => E(0),
      CLR => cfg_start_reg_0,
      D => p_0_in(2),
      Q => \^q\(2)
    );
\reg_num_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => E(0),
      CLR => cfg_start_reg_0,
      D => p_0_in(3),
      Q => reg_num_reg(3)
    );
\reg_num_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => E(0),
      CLR => cfg_start_reg_0,
      D => p_0_in(4),
      Q => reg_num_reg(4)
    );
\reg_num_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => E(0),
      CLR => cfg_start_reg_0,
      D => p_0_in(5),
      Q => reg_num_reg(5)
    );
\reg_num_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => CLK,
      CE => E(0),
      CLR => cfg_start_reg_0,
      D => p_0_in(6),
      Q => reg_num_reg(6)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_hdmi_ctrl_0_0_i2c_ctrl is
  port (
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : out STD_LOGIC;
    sys_init_done_0 : out STD_LOGIC;
    \cnt_bit_reg[2]_0\ : out STD_LOGIC;
    ddc_scl : out STD_LOGIC;
    \cnt_bit_reg[0]_0\ : out STD_LOGIC;
    \cnt_bit_reg[1]_0\ : out STD_LOGIC;
    ddc_sda : inout STD_LOGIC;
    sys_clk : in STD_LOGIC;
    cfg_start : in STD_LOGIC;
    i2c_sda_reg_reg_i_1_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 2 downto 0 );
    i2c_sda_reg_reg_i_1_1 : in STD_LOGIC;
    i2c_sda_reg_reg_i_1_2 : in STD_LOGIC;
    i2c_sda_reg_reg_i_1_3 : in STD_LOGIC;
    sys_init_done : in STD_LOGIC;
    sys_rst_n : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_hdmi_ctrl_0_0_i2c_ctrl : entity is "i2c_ctrl";
end system_hdmi_ctrl_0_0_i2c_ctrl;

architecture STRUCTURE of system_hdmi_ctrl_0_0_i2c_ctrl is
  signal \^clk\ : STD_LOGIC;
  signal \FSM_onehot_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[10]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[11]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[11]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[12]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[13]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[14]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[14]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[15]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[15]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[15]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[6]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[11]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[13]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[14]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[15]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[6]\ : STD_LOGIC;
  signal ack : STD_LOGIC;
  signal \ack__0\ : STD_LOGIC;
  signal ack_reg_i_1_n_0 : STD_LOGIC;
  signal ack_reg_i_3_n_0 : STD_LOGIC;
  signal ack_reg_i_4_n_0 : STD_LOGIC;
  signal \cnt_bit[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_bit[1]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_bit[2]_i_1_n_0\ : STD_LOGIC;
  signal \^cnt_bit_reg[0]_0\ : STD_LOGIC;
  signal \^cnt_bit_reg[1]_0\ : STD_LOGIC;
  signal \^cnt_bit_reg[2]_0\ : STD_LOGIC;
  signal cnt_clk : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \cnt_clk[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk[4]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk[7]_i_2_n_0\ : STD_LOGIC;
  signal cnt_clk_0 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal cnt_i2c_clk : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \cnt_i2c_clk[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_i2c_clk[1]_i_1_n_0\ : STD_LOGIC;
  signal cnt_i2c_clk_en : STD_LOGIC;
  signal cnt_i2c_clk_en1 : STD_LOGIC;
  signal cnt_i2c_clk_en_i_1_n_0 : STD_LOGIC;
  signal ddc_scl_INST_0_i_1_n_0 : STD_LOGIC;
  signal ddc_scl_INST_0_i_2_n_0 : STD_LOGIC;
  signal ddc_scl_INST_0_i_3_n_0 : STD_LOGIC;
  signal ddc_sda_INST_0_i_1_n_0 : STD_LOGIC;
  signal i2c_clk_i_1_n_0 : STD_LOGIC;
  signal i2c_clk_i_2_n_0 : STD_LOGIC;
  signal i2c_sda_reg : STD_LOGIC;
  signal \i2c_sda_reg__0\ : STD_LOGIC;
  signal i2c_sda_reg_reg_i_11_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_13_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_14_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_15_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_1_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_3_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_4_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_5_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_6_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_7_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_8_n_0 : STD_LOGIC;
  signal i2c_sda_reg_reg_i_9_n_0 : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal p_3_in : STD_LOGIC;
  signal p_5_in : STD_LOGIC;
  signal \^sys_init_done_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state[10]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \FSM_onehot_state[14]_i_2\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \FSM_onehot_state[15]_i_2\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \FSM_onehot_state[15]_i_3\ : label is "soft_lutpair11";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[10]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[11]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[12]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[13]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[14]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[15]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[6]\ : label is "SEND_B_ADDR_H:0000000000000010,ACK_1:0000000000000001,RD_DATA:0000000000010000,SEND_D_ADDR:0000000001000000,SEND_RD_ADDR:0000000100000000,ACK_5:0000000010000000,START_2:0000001000000000,START_1:0000010000000000,IDLE:0000100000000000,ACK_4:0100000000000000,ACK_3:0001000000000000,WR_DATA:0010000000000000,SEND_B_ADDR_L:0000000000000100,STOP:1000000000000000,N_ACK:0000000000100000,ACK_2:0000000000001000";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ack_reg : label is "LD";
  attribute SOFT_HLUTNM of \cnt_bit[0]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \cnt_bit[1]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \cnt_clk[0]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \cnt_clk[0]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \cnt_clk[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \cnt_clk[2]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \cnt_clk[7]_i_2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \cnt_i2c_clk[0]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \cnt_i2c_clk[1]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of ddc_scl_INST_0_i_2 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of ddc_scl_INST_0_i_3 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of ddc_sda_INST_0_i_1 : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of i2c_clk_i_2 : label is "soft_lutpair8";
  attribute XILINX_LEGACY_PRIM of i2c_sda_reg_reg : label is "LD";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_2 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_8 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of i2c_sda_reg_reg_i_9 : label is "soft_lutpair14";
begin
  CLK <= \^clk\;
  \cnt_bit_reg[0]_0\ <= \^cnt_bit_reg[0]_0\;
  \cnt_bit_reg[1]_0\ <= \^cnt_bit_reg[1]_0\;
  \cnt_bit_reg[2]_0\ <= \^cnt_bit_reg[2]_0\;
  sys_init_done_0 <= \^sys_init_done_0\;
\FSM_onehot_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA2AFFFFAA2AAA2A"
    )
        port map (
      I0 => p_3_in,
      I1 => cnt_i2c_clk(1),
      I2 => cnt_i2c_clk(0),
      I3 => ack,
      I4 => \FSM_onehot_state[14]_i_2_n_0\,
      I5 => \FSM_onehot_state_reg_n_0_[6]\,
      O => \FSM_onehot_state[0]_i_1_n_0\
    );
\FSM_onehot_state[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF2A2A2A"
    )
        port map (
      I0 => p_1_in,
      I1 => cnt_i2c_clk(0),
      I2 => cnt_i2c_clk(1),
      I3 => cfg_start,
      I4 => \FSM_onehot_state_reg_n_0_[11]\,
      O => \FSM_onehot_state[10]_i_1_n_0\
    );
\FSM_onehot_state[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => cfg_start,
      I1 => \FSM_onehot_state_reg_n_0_[11]\,
      I2 => \FSM_onehot_state[11]_i_2_n_0\,
      O => \FSM_onehot_state[11]_i_1_n_0\
    );
\FSM_onehot_state[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \^cnt_bit_reg[2]_0\,
      I1 => \^cnt_bit_reg[1]_0\,
      I2 => \^cnt_bit_reg[0]_0\,
      I3 => cnt_i2c_clk(1),
      I4 => cnt_i2c_clk(0),
      I5 => \FSM_onehot_state_reg_n_0_[15]\,
      O => \FSM_onehot_state[11]_i_2_n_0\
    );
\FSM_onehot_state[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4F4F4F444F4F4F4"
    )
        port map (
      I0 => \FSM_onehot_state[14]_i_2_n_0\,
      I1 => \FSM_onehot_state_reg_n_0_[2]\,
      I2 => p_5_in,
      I3 => cnt_i2c_clk(1),
      I4 => cnt_i2c_clk(0),
      I5 => ack,
      O => \FSM_onehot_state[12]_i_1_n_0\
    );
\FSM_onehot_state[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF080008000800"
    )
        port map (
      I0 => cnt_i2c_clk(1),
      I1 => cnt_i2c_clk(0),
      I2 => ack,
      I3 => p_5_in,
      I4 => \FSM_onehot_state_reg_n_0_[13]\,
      I5 => \FSM_onehot_state[14]_i_2_n_0\,
      O => \FSM_onehot_state[13]_i_1_n_0\
    );
\FSM_onehot_state[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4F4F4F444F4F4F4"
    )
        port map (
      I0 => \FSM_onehot_state[14]_i_2_n_0\,
      I1 => \FSM_onehot_state_reg_n_0_[13]\,
      I2 => \FSM_onehot_state_reg_n_0_[14]\,
      I3 => cnt_i2c_clk(1),
      I4 => cnt_i2c_clk(0),
      I5 => ack,
      O => \FSM_onehot_state[14]_i_1_n_0\
    );
\FSM_onehot_state[14]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => cnt_i2c_clk(0),
      I1 => cnt_i2c_clk(1),
      I2 => \^cnt_bit_reg[0]_0\,
      I3 => \^cnt_bit_reg[1]_0\,
      I4 => \^cnt_bit_reg[2]_0\,
      O => \FSM_onehot_state[14]_i_2_n_0\
    );
\FSM_onehot_state[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F4F0F4F0F4F044"
    )
        port map (
      I0 => ack,
      I1 => \FSM_onehot_state_reg_n_0_[14]\,
      I2 => \FSM_onehot_state_reg_n_0_[15]\,
      I3 => \FSM_onehot_state[15]_i_2_n_0\,
      I4 => \FSM_onehot_state[15]_i_3_n_0\,
      I5 => \^cnt_bit_reg[2]_0\,
      O => \FSM_onehot_state[15]_i_1_n_0\
    );
\FSM_onehot_state[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => cnt_i2c_clk(0),
      I1 => cnt_i2c_clk(1),
      O => \FSM_onehot_state[15]_i_2_n_0\
    );
\FSM_onehot_state[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \^cnt_bit_reg[0]_0\,
      I1 => \^cnt_bit_reg[1]_0\,
      O => \FSM_onehot_state[15]_i_3_n_0\
    );
\FSM_onehot_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF080008000800"
    )
        port map (
      I0 => cnt_i2c_clk(1),
      I1 => cnt_i2c_clk(0),
      I2 => ack,
      I3 => p_3_in,
      I4 => \FSM_onehot_state_reg_n_0_[2]\,
      I5 => \FSM_onehot_state[14]_i_2_n_0\,
      O => \FSM_onehot_state[2]_i_1_n_0\
    );
\FSM_onehot_state[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8888888"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => \FSM_onehot_state[14]_i_2_n_0\,
      I2 => cnt_i2c_clk(0),
      I3 => cnt_i2c_clk(1),
      I4 => p_1_in,
      O => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \FSM_onehot_state[0]_i_1_n_0\,
      Q => p_3_in
    );
\FSM_onehot_state_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \FSM_onehot_state[10]_i_1_n_0\,
      Q => p_1_in
    );
\FSM_onehot_state_reg[11]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => \^clk\,
      CE => '1',
      D => \FSM_onehot_state[11]_i_1_n_0\,
      PRE => \^sys_init_done_0\,
      Q => \FSM_onehot_state_reg_n_0_[11]\
    );
\FSM_onehot_state_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \FSM_onehot_state[12]_i_1_n_0\,
      Q => p_5_in
    );
\FSM_onehot_state_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \FSM_onehot_state[13]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[13]\
    );
\FSM_onehot_state_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \FSM_onehot_state[14]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[14]\
    );
\FSM_onehot_state_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \FSM_onehot_state[15]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[15]\
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \FSM_onehot_state[2]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[2]\
    );
\FSM_onehot_state_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \FSM_onehot_state[6]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[6]\
    );
ack_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => ack_reg_i_1_n_0,
      G => \ack__0\,
      GE => '1',
      Q => ack
    );
ack_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFAAAB"
    )
        port map (
      I0 => ack_reg_i_3_n_0,
      I1 => \FSM_onehot_state_reg_n_0_[14]\,
      I2 => p_5_in,
      I3 => p_3_in,
      I4 => p_1_in,
      I5 => \FSM_onehot_state_reg_n_0_[11]\,
      O => ack_reg_i_1_n_0
    );
ack_reg_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF1FF"
    )
        port map (
      I0 => cnt_i2c_clk(0),
      I1 => cnt_i2c_clk(1),
      I2 => ddc_scl_INST_0_i_2_n_0,
      I3 => ack_reg_i_4_n_0,
      I4 => p_1_in,
      I5 => \FSM_onehot_state_reg_n_0_[11]\,
      O => \ack__0\
    );
ack_reg_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => ddc_sda,
      I1 => ddc_scl_INST_0_i_2_n_0,
      O => ack_reg_i_3_n_0
    );
ack_reg_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[14]\,
      I1 => p_5_in,
      I2 => p_3_in,
      O => ack_reg_i_4_n_0
    );
cfg_done_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => sys_init_done,
      I1 => sys_rst_n,
      O => \^sys_init_done_0\
    );
\cnt_bit[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1540"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_9_n_0,
      I1 => cnt_i2c_clk(1),
      I2 => cnt_i2c_clk(0),
      I3 => \^cnt_bit_reg[0]_0\,
      O => \cnt_bit[0]_i_1_n_0\
    );
\cnt_bit[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"13332000"
    )
        port map (
      I0 => \^cnt_bit_reg[0]_0\,
      I1 => i2c_sda_reg_reg_i_9_n_0,
      I2 => cnt_i2c_clk(1),
      I3 => cnt_i2c_clk(0),
      I4 => \^cnt_bit_reg[1]_0\,
      O => \cnt_bit[1]_i_1_n_0\
    );
\cnt_bit[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"070F0F0F08000000"
    )
        port map (
      I0 => \^cnt_bit_reg[1]_0\,
      I1 => \^cnt_bit_reg[0]_0\,
      I2 => i2c_sda_reg_reg_i_9_n_0,
      I3 => cnt_i2c_clk(1),
      I4 => cnt_i2c_clk(0),
      I5 => \^cnt_bit_reg[2]_0\,
      O => \cnt_bit[2]_i_1_n_0\
    );
\cnt_bit_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \cnt_bit[0]_i_1_n_0\,
      Q => \^cnt_bit_reg[0]_0\
    );
\cnt_bit_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \cnt_bit[1]_i_1_n_0\,
      Q => \^cnt_bit_reg[1]_0\
    );
\cnt_bit_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \cnt_bit[2]_i_1_n_0\,
      Q => \^cnt_bit_reg[2]_0\
    );
\cnt_clk[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00FF00F7"
    )
        port map (
      I0 => cnt_clk(4),
      I1 => cnt_clk(3),
      I2 => cnt_clk(1),
      I3 => cnt_clk(0),
      I4 => \cnt_clk[0]_i_2_n_0\,
      O => cnt_clk_0(0)
    );
\cnt_clk[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_clk(5),
      I1 => cnt_clk(2),
      I2 => cnt_clk(7),
      I3 => cnt_clk(6),
      O => \cnt_clk[0]_i_2_n_0\
    );
\cnt_clk[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_clk(0),
      I1 => cnt_clk(1),
      O => cnt_clk_0(1)
    );
\cnt_clk[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt_clk(1),
      I1 => cnt_clk(0),
      I2 => cnt_clk(2),
      O => cnt_clk_0(2)
    );
\cnt_clk[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFC0001111C000"
    )
        port map (
      I0 => cnt_clk(4),
      I1 => cnt_clk(1),
      I2 => cnt_clk(0),
      I3 => cnt_clk(2),
      I4 => cnt_clk(3),
      I5 => \cnt_clk[4]_i_2_n_0\,
      O => cnt_clk_0(3)
    );
\cnt_clk[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF800055558000"
    )
        port map (
      I0 => cnt_clk(3),
      I1 => cnt_clk(1),
      I2 => cnt_clk(0),
      I3 => cnt_clk(2),
      I4 => cnt_clk(4),
      I5 => \cnt_clk[4]_i_2_n_0\,
      O => cnt_clk_0(4)
    );
\cnt_clk[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"55FF55FFFFFFFFFE"
    )
        port map (
      I0 => cnt_clk(1),
      I1 => cnt_clk(6),
      I2 => cnt_clk(7),
      I3 => cnt_clk(2),
      I4 => cnt_clk(5),
      I5 => cnt_clk(0),
      O => \cnt_clk[4]_i_2_n_0\
    );
\cnt_clk[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => cnt_clk(3),
      I1 => cnt_clk(4),
      I2 => cnt_clk(2),
      I3 => cnt_clk(0),
      I4 => cnt_clk(1),
      I5 => cnt_clk(5),
      O => cnt_clk_0(5)
    );
\cnt_clk[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"DFFF2000"
    )
        port map (
      I0 => cnt_clk(5),
      I1 => \cnt_clk[7]_i_2_n_0\,
      I2 => cnt_clk(4),
      I3 => cnt_clk(3),
      I4 => cnt_clk(6),
      O => cnt_clk_0(6)
    );
\cnt_clk[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF7FFFFF00800000"
    )
        port map (
      I0 => cnt_clk(6),
      I1 => cnt_clk(3),
      I2 => cnt_clk(4),
      I3 => \cnt_clk[7]_i_2_n_0\,
      I4 => cnt_clk(5),
      I5 => cnt_clk(7),
      O => cnt_clk_0(7)
    );
\cnt_clk[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => cnt_clk(2),
      I1 => cnt_clk(0),
      I2 => cnt_clk(1),
      O => \cnt_clk[7]_i_2_n_0\
    );
\cnt_clk_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_clk_0(0),
      Q => cnt_clk(0)
    );
\cnt_clk_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_clk_0(1),
      Q => cnt_clk(1)
    );
\cnt_clk_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_clk_0(2),
      Q => cnt_clk(2)
    );
\cnt_clk_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_clk_0(3),
      Q => cnt_clk(3)
    );
\cnt_clk_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_clk_0(4),
      Q => cnt_clk(4)
    );
\cnt_clk_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_clk_0(5),
      Q => cnt_clk(5)
    );
\cnt_clk_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_clk_0(6),
      Q => cnt_clk(6)
    );
\cnt_clk_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => sys_clk,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_clk_0(7),
      Q => cnt_clk(7)
    );
\cnt_i2c_clk[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_i2c_clk_en,
      I1 => cnt_i2c_clk(0),
      O => \cnt_i2c_clk[0]_i_1_n_0\
    );
\cnt_i2c_clk[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt_i2c_clk(0),
      I1 => cnt_i2c_clk_en,
      I2 => cnt_i2c_clk(1),
      O => \cnt_i2c_clk[1]_i_1_n_0\
    );
cnt_i2c_clk_en_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"C8"
    )
        port map (
      I0 => cfg_start,
      I1 => \FSM_onehot_state[11]_i_2_n_0\,
      I2 => cnt_i2c_clk_en,
      O => cnt_i2c_clk_en_i_1_n_0
    );
cnt_i2c_clk_en_reg: unisim.vcomponents.FDCE
     port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_i2c_clk_en_i_1_n_0,
      Q => cnt_i2c_clk_en
    );
\cnt_i2c_clk_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \cnt_i2c_clk[0]_i_1_n_0\,
      Q => cnt_i2c_clk(0)
    );
\cnt_i2c_clk_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => \cnt_i2c_clk[1]_i_1_n_0\,
      Q => cnt_i2c_clk(1)
    );
ddc_scl_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BEBEBEBEBEBEBEAA"
    )
        port map (
      I0 => ddc_scl_INST_0_i_1_n_0,
      I1 => cnt_i2c_clk(0),
      I2 => cnt_i2c_clk(1),
      I3 => \FSM_onehot_state_reg_n_0_[14]\,
      I4 => p_5_in,
      I5 => p_3_in,
      O => ddc_scl
    );
ddc_scl_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFC3F28"
    )
        port map (
      I0 => ddc_scl_INST_0_i_2_n_0,
      I1 => cnt_i2c_clk(1),
      I2 => cnt_i2c_clk(0),
      I3 => p_1_in,
      I4 => \FSM_onehot_state_reg_n_0_[15]\,
      I5 => ddc_scl_INST_0_i_3_n_0,
      O => ddc_scl_INST_0_i_1_n_0
    );
ddc_scl_INST_0_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[13]\,
      I1 => \FSM_onehot_state_reg_n_0_[6]\,
      I2 => \FSM_onehot_state_reg_n_0_[2]\,
      O => ddc_scl_INST_0_i_2_n_0
    );
ddc_scl_INST_0_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFAAA8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[15]\,
      I1 => \^cnt_bit_reg[1]_0\,
      I2 => \^cnt_bit_reg[0]_0\,
      I3 => \^cnt_bit_reg[2]_0\,
      I4 => \FSM_onehot_state_reg_n_0_[11]\,
      O => ddc_scl_INST_0_i_3_n_0
    );
ddc_sda_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => i2c_sda_reg,
      I1 => ddc_sda_INST_0_i_1_n_0,
      O => ddc_sda
    );
ddc_sda_INST_0_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => p_3_in,
      I1 => p_5_in,
      I2 => \FSM_onehot_state_reg_n_0_[14]\,
      O => ddc_sda_INST_0_i_1_n_0
    );
i2c_clk_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFBF0040"
    )
        port map (
      I0 => i2c_clk_i_2_n_0,
      I1 => cnt_clk(4),
      I2 => cnt_clk(3),
      I3 => cnt_clk(1),
      I4 => \^clk\,
      O => i2c_clk_i_1_n_0
    );
i2c_clk_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt_clk(6),
      I1 => cnt_clk(7),
      I2 => cnt_clk(2),
      I3 => cnt_clk(5),
      I4 => cnt_clk(0),
      O => i2c_clk_i_2_n_0
    );
i2c_clk_reg: unisim.vcomponents.FDPE
     port map (
      C => sys_clk,
      CE => '1',
      D => i2c_clk_i_1_n_0,
      PRE => \^sys_init_done_0\,
      Q => \^clk\
    );
i2c_end_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[15]\,
      I1 => cnt_i2c_clk(0),
      I2 => cnt_i2c_clk(1),
      I3 => \^cnt_bit_reg[0]_0\,
      I4 => \^cnt_bit_reg[1]_0\,
      I5 => \^cnt_bit_reg[2]_0\,
      O => cnt_i2c_clk_en1
    );
i2c_end_reg: unisim.vcomponents.FDCE
     port map (
      C => \^clk\,
      CE => '1',
      CLR => \^sys_init_done_0\,
      D => cnt_i2c_clk_en1,
      Q => E(0)
    );
i2c_sda_reg_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => i2c_sda_reg_reg_i_1_n_0,
      G => \i2c_sda_reg__0\,
      GE => '1',
      Q => i2c_sda_reg
    );
i2c_sda_reg_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_3_n_0,
      I1 => i2c_sda_reg_reg_i_4_n_0,
      I2 => i2c_sda_reg_reg_i_5_n_0,
      I3 => i2c_sda_reg_reg_i_6_n_0,
      I4 => i2c_sda_reg_reg_i_7_n_0,
      I5 => i2c_sda_reg_reg_i_8_n_0,
      O => i2c_sda_reg_reg_i_1_n_0
    );
i2c_sda_reg_reg_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCEECCEE0C220022"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => \^cnt_bit_reg[0]_0\,
      I2 => \^cnt_bit_reg[1]_0\,
      I3 => Q(2),
      I4 => \FSM_onehot_state_reg_n_0_[13]\,
      I5 => \FSM_onehot_state_reg_n_0_[2]\,
      O => i2c_sda_reg_reg_i_11_n_0
    );
i2c_sda_reg_reg_i_13: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => \^cnt_bit_reg[2]_0\,
      O => i2c_sda_reg_reg_i_13_n_0
    );
i2c_sda_reg_reg_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00F00080FFF00080"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => Q(0),
      I2 => \^cnt_bit_reg[1]_0\,
      I3 => \^cnt_bit_reg[0]_0\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => \^cnt_bit_reg[2]_0\,
      O => i2c_sda_reg_reg_i_14_n_0
    );
i2c_sda_reg_reg_i_15: unisim.vcomponents.LUT6
    generic map(
      INIT => X"808FC0C080800000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[13]\,
      I1 => Q(1),
      I2 => \^cnt_bit_reg[0]_0\,
      I3 => \^cnt_bit_reg[1]_0\,
      I4 => Q(0),
      I5 => \FSM_onehot_state_reg_n_0_[2]\,
      O => i2c_sda_reg_reg_i_15_n_0
    );
i2c_sda_reg_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[15]\,
      I1 => \FSM_onehot_state_reg_n_0_[13]\,
      I2 => \FSM_onehot_state_reg_n_0_[6]\,
      I3 => \FSM_onehot_state_reg_n_0_[2]\,
      I4 => i2c_sda_reg_reg_i_9_n_0,
      O => \i2c_sda_reg__0\
    );
i2c_sda_reg_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFAAAAAAEAEAEAEA"
    )
        port map (
      I0 => ack_reg_i_4_n_0,
      I1 => \FSM_onehot_state_reg_n_0_[13]\,
      I2 => i2c_sda_reg_reg_i_1_1,
      I3 => i2c_sda_reg_reg_i_11_n_0,
      I4 => i2c_sda_reg_reg_i_1_0,
      I5 => Q(0),
      O => i2c_sda_reg_reg_i_3_n_0
    );
i2c_sda_reg_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF02200000"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_13_n_0,
      I1 => i2c_sda_reg_reg_i_1_2,
      I2 => \^cnt_bit_reg[1]_0\,
      I3 => \^cnt_bit_reg[0]_0\,
      I4 => i2c_sda_reg_reg_i_1_3,
      I5 => ddc_scl_INST_0_i_3_n_0,
      O => i2c_sda_reg_reg_i_4_n_0
    );
i2c_sda_reg_reg_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000080000088"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[2]\,
      I1 => i2c_sda_reg_reg_i_1_0,
      I2 => Q(2),
      I3 => \^cnt_bit_reg[1]_0\,
      I4 => \^cnt_bit_reg[0]_0\,
      I5 => Q(0),
      O => i2c_sda_reg_reg_i_5_n_0
    );
i2c_sda_reg_reg_i_6: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[15]\,
      I1 => cnt_i2c_clk(1),
      I2 => cnt_i2c_clk(0),
      O => i2c_sda_reg_reg_i_6_n_0
    );
i2c_sda_reg_reg_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000F20022"
    )
        port map (
      I0 => i2c_sda_reg_reg_i_14_n_0,
      I1 => Q(1),
      I2 => i2c_sda_reg_reg_i_15_n_0,
      I3 => i2c_sda_reg_reg_i_1_2,
      I4 => \^cnt_bit_reg[2]_0\,
      I5 => Q(2),
      O => i2c_sda_reg_reg_i_7_n_0
    );
i2c_sda_reg_reg_i_8: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => p_1_in,
      I1 => cnt_i2c_clk(1),
      I2 => cnt_i2c_clk(0),
      O => i2c_sda_reg_reg_i_8_n_0
    );
i2c_sda_reg_reg_i_9: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[11]\,
      I1 => p_1_in,
      I2 => p_3_in,
      I3 => p_5_in,
      I4 => \FSM_onehot_state_reg_n_0_[14]\,
      O => i2c_sda_reg_reg_i_9_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_hdmi_ctrl_0_0_hdmi_i2c is
  port (
    cfg_done_reg : out STD_LOGIC;
    ddc_scl : out STD_LOGIC;
    ddc_sda : inout STD_LOGIC;
    sys_clk : in STD_LOGIC;
    sys_init_done : in STD_LOGIC;
    sys_rst_n : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_hdmi_ctrl_0_0_hdmi_i2c : entity is "hdmi_i2c";
end system_hdmi_ctrl_0_0_hdmi_i2c;

architecture STRUCTURE of system_hdmi_ctrl_0_0_hdmi_i2c is
  signal cfg_clk : STD_LOGIC;
  signal \^cfg_done_reg\ : STD_LOGIC;
  signal cfg_end : STD_LOGIC;
  signal cfg_start : STD_LOGIC;
  signal hdmi_cfg_inst_n_2 : STD_LOGIC;
  signal hdmi_cfg_inst_n_6 : STD_LOGIC;
  signal hdmi_cfg_inst_n_7 : STD_LOGIC;
  signal i2c_ctrl_inst_n_2 : STD_LOGIC;
  signal i2c_ctrl_inst_n_3 : STD_LOGIC;
  signal i2c_ctrl_inst_n_5 : STD_LOGIC;
  signal i2c_ctrl_inst_n_6 : STD_LOGIC;
  signal reg_num_reg : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  cfg_done_reg <= \^cfg_done_reg\;
hdmi_cfg_inst: entity work.system_hdmi_ctrl_0_0_hdmi_cfg
     port map (
      CLK => cfg_clk,
      E(0) => cfg_end,
      Q(2 downto 0) => reg_num_reg(2 downto 0),
      cfg_done_reg_0 => \^cfg_done_reg\,
      cfg_start => cfg_start,
      cfg_start_reg_0 => i2c_ctrl_inst_n_2,
      i2c_sda_reg_reg_i_3 => i2c_ctrl_inst_n_3,
      i2c_sda_reg_reg_i_3_0 => i2c_ctrl_inst_n_6,
      i2c_sda_reg_reg_i_3_1 => i2c_ctrl_inst_n_5,
      \reg_num_reg[1]_0\ => hdmi_cfg_inst_n_2,
      \reg_num_reg[1]_1\ => hdmi_cfg_inst_n_6,
      \reg_num_reg[1]_2\ => hdmi_cfg_inst_n_7
    );
i2c_ctrl_inst: entity work.system_hdmi_ctrl_0_0_i2c_ctrl
     port map (
      CLK => cfg_clk,
      E(0) => cfg_end,
      Q(2 downto 0) => reg_num_reg(2 downto 0),
      cfg_start => cfg_start,
      \cnt_bit_reg[0]_0\ => i2c_ctrl_inst_n_5,
      \cnt_bit_reg[1]_0\ => i2c_ctrl_inst_n_6,
      \cnt_bit_reg[2]_0\ => i2c_ctrl_inst_n_3,
      ddc_scl => ddc_scl,
      ddc_sda => ddc_sda,
      i2c_sda_reg_reg_i_1_0 => hdmi_cfg_inst_n_7,
      i2c_sda_reg_reg_i_1_1 => hdmi_cfg_inst_n_2,
      i2c_sda_reg_reg_i_1_2 => \^cfg_done_reg\,
      i2c_sda_reg_reg_i_1_3 => hdmi_cfg_inst_n_6,
      sys_clk => sys_clk,
      sys_init_done => sys_init_done,
      sys_init_done_0 => i2c_ctrl_inst_n_2,
      sys_rst_n => sys_rst_n
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_hdmi_ctrl_0_0_hdmi_ctrl is
  port (
    v_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 );
    h_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ddc_scl : out STD_LOGIC;
    cfg_done_reg : out STD_LOGIC;
    fifo_wr_en : out STD_LOGIC;
    ddc_sda : inout STD_LOGIC;
    hdmi_in_vsync : in STD_LOGIC;
    sys_clk : in STD_LOGIC;
    hdmi_in_clk : in STD_LOGIC;
    hdmi_in_de : in STD_LOGIC;
    sys_rst_n : in STD_LOGIC;
    sys_init_done : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of system_hdmi_ctrl_0_0_hdmi_ctrl : entity is "hdmi_ctrl";
end system_hdmi_ctrl_0_0_hdmi_ctrl;

architecture STRUCTURE of system_hdmi_ctrl_0_0_hdmi_ctrl is
  signal \^h_cnt\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \h_cnt[11]_i_2_n_0\ : STD_LOGIC;
  signal \h_cnt[11]_i_3_n_0\ : STD_LOGIC;
  signal \h_cnt[11]_i_4_n_0\ : STD_LOGIC;
  signal \h_cnt[11]_i_5_n_0\ : STD_LOGIC;
  signal \h_cnt[15]_i_2_n_0\ : STD_LOGIC;
  signal \h_cnt[15]_i_3_n_0\ : STD_LOGIC;
  signal \h_cnt[15]_i_4_n_0\ : STD_LOGIC;
  signal \h_cnt[15]_i_5_n_0\ : STD_LOGIC;
  signal \h_cnt[19]_i_2_n_0\ : STD_LOGIC;
  signal \h_cnt[19]_i_3_n_0\ : STD_LOGIC;
  signal \h_cnt[19]_i_4_n_0\ : STD_LOGIC;
  signal \h_cnt[19]_i_5_n_0\ : STD_LOGIC;
  signal \h_cnt[23]_i_2_n_0\ : STD_LOGIC;
  signal \h_cnt[23]_i_3_n_0\ : STD_LOGIC;
  signal \h_cnt[23]_i_4_n_0\ : STD_LOGIC;
  signal \h_cnt[23]_i_5_n_0\ : STD_LOGIC;
  signal \h_cnt[27]_i_2_n_0\ : STD_LOGIC;
  signal \h_cnt[27]_i_3_n_0\ : STD_LOGIC;
  signal \h_cnt[27]_i_4_n_0\ : STD_LOGIC;
  signal \h_cnt[27]_i_5_n_0\ : STD_LOGIC;
  signal \h_cnt[31]_i_2_n_0\ : STD_LOGIC;
  signal \h_cnt[31]_i_3_n_0\ : STD_LOGIC;
  signal \h_cnt[31]_i_4_n_0\ : STD_LOGIC;
  signal \h_cnt[31]_i_5_n_0\ : STD_LOGIC;
  signal \h_cnt[31]_i_6_n_0\ : STD_LOGIC;
  signal \h_cnt[3]_i_2_n_0\ : STD_LOGIC;
  signal \h_cnt[3]_i_3_n_0\ : STD_LOGIC;
  signal \h_cnt[3]_i_4_n_0\ : STD_LOGIC;
  signal \h_cnt[3]_i_5_n_0\ : STD_LOGIC;
  signal \h_cnt[7]_i_2_n_0\ : STD_LOGIC;
  signal \h_cnt[7]_i_3_n_0\ : STD_LOGIC;
  signal \h_cnt[7]_i_4_n_0\ : STD_LOGIC;
  signal \h_cnt[7]_i_5_n_0\ : STD_LOGIC;
  signal \h_cnt_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \h_cnt_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \h_cnt_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \h_cnt_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \h_cnt_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \h_cnt_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \h_cnt_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \h_cnt_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \h_cnt_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \h_cnt_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \h_cnt_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \h_cnt_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \h_cnt_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \h_cnt_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \h_cnt_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \h_cnt_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \h_cnt_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \h_cnt_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \h_cnt_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \h_cnt_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \h_cnt_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \h_cnt_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \h_cnt_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \h_cnt_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \h_cnt_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \h_cnt_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \h_cnt_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \h_cnt_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \h_cnt_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \h_cnt_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \h_cnt_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \h_cnt_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \h_cnt_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \h_cnt_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \h_cnt_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \h_cnt_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \h_cnt_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \h_cnt_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \h_cnt_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \h_cnt_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \h_cnt_reg[31]_i_1_n_1\ : STD_LOGIC;
  signal \h_cnt_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \h_cnt_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \h_cnt_reg[31]_i_1_n_4\ : STD_LOGIC;
  signal \h_cnt_reg[31]_i_1_n_5\ : STD_LOGIC;
  signal \h_cnt_reg[31]_i_1_n_6\ : STD_LOGIC;
  signal \h_cnt_reg[31]_i_1_n_7\ : STD_LOGIC;
  signal \h_cnt_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \h_cnt_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \h_cnt_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \h_cnt_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \h_cnt_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \h_cnt_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \h_cnt_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \h_cnt_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \h_cnt_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \h_cnt_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \h_cnt_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \h_cnt_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \h_cnt_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \h_cnt_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \h_cnt_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \h_cnt_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \^v_cnt\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal v_cnt0 : STD_LOGIC;
  signal \v_cnt[11]_i_2_n_0\ : STD_LOGIC;
  signal \v_cnt[11]_i_3_n_0\ : STD_LOGIC;
  signal \v_cnt[11]_i_4_n_0\ : STD_LOGIC;
  signal \v_cnt[11]_i_5_n_0\ : STD_LOGIC;
  signal \v_cnt[15]_i_2_n_0\ : STD_LOGIC;
  signal \v_cnt[15]_i_3_n_0\ : STD_LOGIC;
  signal \v_cnt[15]_i_4_n_0\ : STD_LOGIC;
  signal \v_cnt[15]_i_5_n_0\ : STD_LOGIC;
  signal \v_cnt[19]_i_2_n_0\ : STD_LOGIC;
  signal \v_cnt[19]_i_3_n_0\ : STD_LOGIC;
  signal \v_cnt[19]_i_4_n_0\ : STD_LOGIC;
  signal \v_cnt[19]_i_5_n_0\ : STD_LOGIC;
  signal \v_cnt[23]_i_2_n_0\ : STD_LOGIC;
  signal \v_cnt[23]_i_3_n_0\ : STD_LOGIC;
  signal \v_cnt[23]_i_4_n_0\ : STD_LOGIC;
  signal \v_cnt[23]_i_5_n_0\ : STD_LOGIC;
  signal \v_cnt[27]_i_2_n_0\ : STD_LOGIC;
  signal \v_cnt[27]_i_3_n_0\ : STD_LOGIC;
  signal \v_cnt[27]_i_4_n_0\ : STD_LOGIC;
  signal \v_cnt[27]_i_5_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_10_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_11_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_12_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_3_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_4_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_5_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_6_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_7_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_8_n_0\ : STD_LOGIC;
  signal \v_cnt[31]_i_9_n_0\ : STD_LOGIC;
  signal \v_cnt[3]_i_2_n_0\ : STD_LOGIC;
  signal \v_cnt[3]_i_3_n_0\ : STD_LOGIC;
  signal \v_cnt[3]_i_4_n_0\ : STD_LOGIC;
  signal \v_cnt[3]_i_5_n_0\ : STD_LOGIC;
  signal \v_cnt[3]_i_6_n_0\ : STD_LOGIC;
  signal \v_cnt[7]_i_2_n_0\ : STD_LOGIC;
  signal \v_cnt[7]_i_3_n_0\ : STD_LOGIC;
  signal \v_cnt[7]_i_4_n_0\ : STD_LOGIC;
  signal \v_cnt[7]_i_5_n_0\ : STD_LOGIC;
  signal \v_cnt_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \v_cnt_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \v_cnt_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \v_cnt_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \v_cnt_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \v_cnt_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \v_cnt_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \v_cnt_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \v_cnt_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \v_cnt_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \v_cnt_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \v_cnt_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \v_cnt_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \v_cnt_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \v_cnt_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \v_cnt_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \v_cnt_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \v_cnt_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \v_cnt_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \v_cnt_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \v_cnt_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \v_cnt_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \v_cnt_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \v_cnt_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \v_cnt_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \v_cnt_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \v_cnt_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \v_cnt_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \v_cnt_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \v_cnt_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \v_cnt_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \v_cnt_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \v_cnt_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \v_cnt_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \v_cnt_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \v_cnt_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \v_cnt_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \v_cnt_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \v_cnt_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \v_cnt_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \v_cnt_reg[31]_i_2_n_1\ : STD_LOGIC;
  signal \v_cnt_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \v_cnt_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \v_cnt_reg[31]_i_2_n_4\ : STD_LOGIC;
  signal \v_cnt_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \v_cnt_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \v_cnt_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \v_cnt_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \v_cnt_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \v_cnt_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \v_cnt_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \v_cnt_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \v_cnt_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \v_cnt_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \v_cnt_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \v_cnt_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \v_cnt_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \v_cnt_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \v_cnt_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \v_cnt_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \v_cnt_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \v_cnt_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \v_cnt_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_h_cnt_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_v_cnt_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  h_cnt(31 downto 0) <= \^h_cnt\(31 downto 0);
  v_cnt(31 downto 0) <= \^v_cnt\(31 downto 0);
fifo_wr_en_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^v_cnt\(0),
      I1 => hdmi_in_de,
      O => fifo_wr_en
    );
\h_cnt[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(11),
      O => \h_cnt[11]_i_2_n_0\
    );
\h_cnt[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(10),
      O => \h_cnt[11]_i_3_n_0\
    );
\h_cnt[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(9),
      O => \h_cnt[11]_i_4_n_0\
    );
\h_cnt[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(8),
      O => \h_cnt[11]_i_5_n_0\
    );
\h_cnt[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(15),
      O => \h_cnt[15]_i_2_n_0\
    );
\h_cnt[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(14),
      O => \h_cnt[15]_i_3_n_0\
    );
\h_cnt[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(13),
      O => \h_cnt[15]_i_4_n_0\
    );
\h_cnt[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(12),
      O => \h_cnt[15]_i_5_n_0\
    );
\h_cnt[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(19),
      O => \h_cnt[19]_i_2_n_0\
    );
\h_cnt[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(18),
      O => \h_cnt[19]_i_3_n_0\
    );
\h_cnt[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(17),
      O => \h_cnt[19]_i_4_n_0\
    );
\h_cnt[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(16),
      O => \h_cnt[19]_i_5_n_0\
    );
\h_cnt[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(23),
      O => \h_cnt[23]_i_2_n_0\
    );
\h_cnt[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(22),
      O => \h_cnt[23]_i_3_n_0\
    );
\h_cnt[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(21),
      O => \h_cnt[23]_i_4_n_0\
    );
\h_cnt[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(20),
      O => \h_cnt[23]_i_5_n_0\
    );
\h_cnt[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(27),
      O => \h_cnt[27]_i_2_n_0\
    );
\h_cnt[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(26),
      O => \h_cnt[27]_i_3_n_0\
    );
\h_cnt[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(25),
      O => \h_cnt[27]_i_4_n_0\
    );
\h_cnt[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(24),
      O => \h_cnt[27]_i_5_n_0\
    );
\h_cnt[31]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sys_rst_n,
      O => \h_cnt[31]_i_2_n_0\
    );
\h_cnt[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(31),
      O => \h_cnt[31]_i_3_n_0\
    );
\h_cnt[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(30),
      O => \h_cnt[31]_i_4_n_0\
    );
\h_cnt[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(29),
      O => \h_cnt[31]_i_5_n_0\
    );
\h_cnt[31]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(28),
      O => \h_cnt[31]_i_6_n_0\
    );
\h_cnt[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(3),
      O => \h_cnt[3]_i_2_n_0\
    );
\h_cnt[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(2),
      O => \h_cnt[3]_i_3_n_0\
    );
\h_cnt[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(1),
      O => \h_cnt[3]_i_4_n_0\
    );
\h_cnt[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"4"
    )
        port map (
      I0 => \^h_cnt\(0),
      I1 => hdmi_in_de,
      O => \h_cnt[3]_i_5_n_0\
    );
\h_cnt[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(7),
      O => \h_cnt[7]_i_2_n_0\
    );
\h_cnt[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(6),
      O => \h_cnt[7]_i_3_n_0\
    );
\h_cnt[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(5),
      O => \h_cnt[7]_i_4_n_0\
    );
\h_cnt[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => hdmi_in_de,
      I1 => \^h_cnt\(4),
      O => \h_cnt[7]_i_5_n_0\
    );
\h_cnt_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[3]_i_1_n_7\,
      Q => \^h_cnt\(0)
    );
\h_cnt_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[11]_i_1_n_5\,
      Q => \^h_cnt\(10)
    );
\h_cnt_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[11]_i_1_n_4\,
      Q => \^h_cnt\(11)
    );
\h_cnt_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \h_cnt_reg[7]_i_1_n_0\,
      CO(3) => \h_cnt_reg[11]_i_1_n_0\,
      CO(2) => \h_cnt_reg[11]_i_1_n_1\,
      CO(1) => \h_cnt_reg[11]_i_1_n_2\,
      CO(0) => \h_cnt_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \h_cnt_reg[11]_i_1_n_4\,
      O(2) => \h_cnt_reg[11]_i_1_n_5\,
      O(1) => \h_cnt_reg[11]_i_1_n_6\,
      O(0) => \h_cnt_reg[11]_i_1_n_7\,
      S(3) => \h_cnt[11]_i_2_n_0\,
      S(2) => \h_cnt[11]_i_3_n_0\,
      S(1) => \h_cnt[11]_i_4_n_0\,
      S(0) => \h_cnt[11]_i_5_n_0\
    );
\h_cnt_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[15]_i_1_n_7\,
      Q => \^h_cnt\(12)
    );
\h_cnt_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[15]_i_1_n_6\,
      Q => \^h_cnt\(13)
    );
\h_cnt_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[15]_i_1_n_5\,
      Q => \^h_cnt\(14)
    );
\h_cnt_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[15]_i_1_n_4\,
      Q => \^h_cnt\(15)
    );
\h_cnt_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \h_cnt_reg[11]_i_1_n_0\,
      CO(3) => \h_cnt_reg[15]_i_1_n_0\,
      CO(2) => \h_cnt_reg[15]_i_1_n_1\,
      CO(1) => \h_cnt_reg[15]_i_1_n_2\,
      CO(0) => \h_cnt_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \h_cnt_reg[15]_i_1_n_4\,
      O(2) => \h_cnt_reg[15]_i_1_n_5\,
      O(1) => \h_cnt_reg[15]_i_1_n_6\,
      O(0) => \h_cnt_reg[15]_i_1_n_7\,
      S(3) => \h_cnt[15]_i_2_n_0\,
      S(2) => \h_cnt[15]_i_3_n_0\,
      S(1) => \h_cnt[15]_i_4_n_0\,
      S(0) => \h_cnt[15]_i_5_n_0\
    );
\h_cnt_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[19]_i_1_n_7\,
      Q => \^h_cnt\(16)
    );
\h_cnt_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[19]_i_1_n_6\,
      Q => \^h_cnt\(17)
    );
\h_cnt_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[19]_i_1_n_5\,
      Q => \^h_cnt\(18)
    );
\h_cnt_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[19]_i_1_n_4\,
      Q => \^h_cnt\(19)
    );
\h_cnt_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \h_cnt_reg[15]_i_1_n_0\,
      CO(3) => \h_cnt_reg[19]_i_1_n_0\,
      CO(2) => \h_cnt_reg[19]_i_1_n_1\,
      CO(1) => \h_cnt_reg[19]_i_1_n_2\,
      CO(0) => \h_cnt_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \h_cnt_reg[19]_i_1_n_4\,
      O(2) => \h_cnt_reg[19]_i_1_n_5\,
      O(1) => \h_cnt_reg[19]_i_1_n_6\,
      O(0) => \h_cnt_reg[19]_i_1_n_7\,
      S(3) => \h_cnt[19]_i_2_n_0\,
      S(2) => \h_cnt[19]_i_3_n_0\,
      S(1) => \h_cnt[19]_i_4_n_0\,
      S(0) => \h_cnt[19]_i_5_n_0\
    );
\h_cnt_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[3]_i_1_n_6\,
      Q => \^h_cnt\(1)
    );
\h_cnt_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[23]_i_1_n_7\,
      Q => \^h_cnt\(20)
    );
\h_cnt_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[23]_i_1_n_6\,
      Q => \^h_cnt\(21)
    );
\h_cnt_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[23]_i_1_n_5\,
      Q => \^h_cnt\(22)
    );
\h_cnt_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[23]_i_1_n_4\,
      Q => \^h_cnt\(23)
    );
\h_cnt_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \h_cnt_reg[19]_i_1_n_0\,
      CO(3) => \h_cnt_reg[23]_i_1_n_0\,
      CO(2) => \h_cnt_reg[23]_i_1_n_1\,
      CO(1) => \h_cnt_reg[23]_i_1_n_2\,
      CO(0) => \h_cnt_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \h_cnt_reg[23]_i_1_n_4\,
      O(2) => \h_cnt_reg[23]_i_1_n_5\,
      O(1) => \h_cnt_reg[23]_i_1_n_6\,
      O(0) => \h_cnt_reg[23]_i_1_n_7\,
      S(3) => \h_cnt[23]_i_2_n_0\,
      S(2) => \h_cnt[23]_i_3_n_0\,
      S(1) => \h_cnt[23]_i_4_n_0\,
      S(0) => \h_cnt[23]_i_5_n_0\
    );
\h_cnt_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[27]_i_1_n_7\,
      Q => \^h_cnt\(24)
    );
\h_cnt_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[27]_i_1_n_6\,
      Q => \^h_cnt\(25)
    );
\h_cnt_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[27]_i_1_n_5\,
      Q => \^h_cnt\(26)
    );
\h_cnt_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[27]_i_1_n_4\,
      Q => \^h_cnt\(27)
    );
\h_cnt_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \h_cnt_reg[23]_i_1_n_0\,
      CO(3) => \h_cnt_reg[27]_i_1_n_0\,
      CO(2) => \h_cnt_reg[27]_i_1_n_1\,
      CO(1) => \h_cnt_reg[27]_i_1_n_2\,
      CO(0) => \h_cnt_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \h_cnt_reg[27]_i_1_n_4\,
      O(2) => \h_cnt_reg[27]_i_1_n_5\,
      O(1) => \h_cnt_reg[27]_i_1_n_6\,
      O(0) => \h_cnt_reg[27]_i_1_n_7\,
      S(3) => \h_cnt[27]_i_2_n_0\,
      S(2) => \h_cnt[27]_i_3_n_0\,
      S(1) => \h_cnt[27]_i_4_n_0\,
      S(0) => \h_cnt[27]_i_5_n_0\
    );
\h_cnt_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[31]_i_1_n_7\,
      Q => \^h_cnt\(28)
    );
\h_cnt_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[31]_i_1_n_6\,
      Q => \^h_cnt\(29)
    );
\h_cnt_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[3]_i_1_n_5\,
      Q => \^h_cnt\(2)
    );
\h_cnt_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[31]_i_1_n_5\,
      Q => \^h_cnt\(30)
    );
\h_cnt_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[31]_i_1_n_4\,
      Q => \^h_cnt\(31)
    );
\h_cnt_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \h_cnt_reg[27]_i_1_n_0\,
      CO(3) => \NLW_h_cnt_reg[31]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \h_cnt_reg[31]_i_1_n_1\,
      CO(1) => \h_cnt_reg[31]_i_1_n_2\,
      CO(0) => \h_cnt_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \h_cnt_reg[31]_i_1_n_4\,
      O(2) => \h_cnt_reg[31]_i_1_n_5\,
      O(1) => \h_cnt_reg[31]_i_1_n_6\,
      O(0) => \h_cnt_reg[31]_i_1_n_7\,
      S(3) => \h_cnt[31]_i_3_n_0\,
      S(2) => \h_cnt[31]_i_4_n_0\,
      S(1) => \h_cnt[31]_i_5_n_0\,
      S(0) => \h_cnt[31]_i_6_n_0\
    );
\h_cnt_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[3]_i_1_n_4\,
      Q => \^h_cnt\(3)
    );
\h_cnt_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \h_cnt_reg[3]_i_1_n_0\,
      CO(2) => \h_cnt_reg[3]_i_1_n_1\,
      CO(1) => \h_cnt_reg[3]_i_1_n_2\,
      CO(0) => \h_cnt_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => hdmi_in_de,
      O(3) => \h_cnt_reg[3]_i_1_n_4\,
      O(2) => \h_cnt_reg[3]_i_1_n_5\,
      O(1) => \h_cnt_reg[3]_i_1_n_6\,
      O(0) => \h_cnt_reg[3]_i_1_n_7\,
      S(3) => \h_cnt[3]_i_2_n_0\,
      S(2) => \h_cnt[3]_i_3_n_0\,
      S(1) => \h_cnt[3]_i_4_n_0\,
      S(0) => \h_cnt[3]_i_5_n_0\
    );
\h_cnt_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[7]_i_1_n_7\,
      Q => \^h_cnt\(4)
    );
\h_cnt_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[7]_i_1_n_6\,
      Q => \^h_cnt\(5)
    );
\h_cnt_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[7]_i_1_n_5\,
      Q => \^h_cnt\(6)
    );
\h_cnt_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[7]_i_1_n_4\,
      Q => \^h_cnt\(7)
    );
\h_cnt_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \h_cnt_reg[3]_i_1_n_0\,
      CO(3) => \h_cnt_reg[7]_i_1_n_0\,
      CO(2) => \h_cnt_reg[7]_i_1_n_1\,
      CO(1) => \h_cnt_reg[7]_i_1_n_2\,
      CO(0) => \h_cnt_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \h_cnt_reg[7]_i_1_n_4\,
      O(2) => \h_cnt_reg[7]_i_1_n_5\,
      O(1) => \h_cnt_reg[7]_i_1_n_6\,
      O(0) => \h_cnt_reg[7]_i_1_n_7\,
      S(3) => \h_cnt[7]_i_2_n_0\,
      S(2) => \h_cnt[7]_i_3_n_0\,
      S(1) => \h_cnt[7]_i_4_n_0\,
      S(0) => \h_cnt[7]_i_5_n_0\
    );
\h_cnt_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[11]_i_1_n_7\,
      Q => \^h_cnt\(8)
    );
\h_cnt_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => '1',
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \h_cnt_reg[11]_i_1_n_6\,
      Q => \^h_cnt\(9)
    );
hdmi_i2c_inst: entity work.system_hdmi_ctrl_0_0_hdmi_i2c
     port map (
      cfg_done_reg => cfg_done_reg,
      ddc_scl => ddc_scl,
      ddc_sda => ddc_sda,
      sys_clk => sys_clk,
      sys_init_done => sys_init_done,
      sys_rst_n => sys_rst_n
    );
\v_cnt[11]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(11),
      I1 => hdmi_in_vsync,
      O => \v_cnt[11]_i_2_n_0\
    );
\v_cnt[11]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(10),
      I1 => hdmi_in_vsync,
      O => \v_cnt[11]_i_3_n_0\
    );
\v_cnt[11]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(9),
      I1 => hdmi_in_vsync,
      O => \v_cnt[11]_i_4_n_0\
    );
\v_cnt[11]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(8),
      I1 => hdmi_in_vsync,
      O => \v_cnt[11]_i_5_n_0\
    );
\v_cnt[15]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(15),
      I1 => hdmi_in_vsync,
      O => \v_cnt[15]_i_2_n_0\
    );
\v_cnt[15]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(14),
      I1 => hdmi_in_vsync,
      O => \v_cnt[15]_i_3_n_0\
    );
\v_cnt[15]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(13),
      I1 => hdmi_in_vsync,
      O => \v_cnt[15]_i_4_n_0\
    );
\v_cnt[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(12),
      I1 => hdmi_in_vsync,
      O => \v_cnt[15]_i_5_n_0\
    );
\v_cnt[19]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(19),
      I1 => hdmi_in_vsync,
      O => \v_cnt[19]_i_2_n_0\
    );
\v_cnt[19]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(18),
      I1 => hdmi_in_vsync,
      O => \v_cnt[19]_i_3_n_0\
    );
\v_cnt[19]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(17),
      I1 => hdmi_in_vsync,
      O => \v_cnt[19]_i_4_n_0\
    );
\v_cnt[19]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(16),
      I1 => hdmi_in_vsync,
      O => \v_cnt[19]_i_5_n_0\
    );
\v_cnt[23]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(23),
      I1 => hdmi_in_vsync,
      O => \v_cnt[23]_i_2_n_0\
    );
\v_cnt[23]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(22),
      I1 => hdmi_in_vsync,
      O => \v_cnt[23]_i_3_n_0\
    );
\v_cnt[23]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(21),
      I1 => hdmi_in_vsync,
      O => \v_cnt[23]_i_4_n_0\
    );
\v_cnt[23]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(20),
      I1 => hdmi_in_vsync,
      O => \v_cnt[23]_i_5_n_0\
    );
\v_cnt[27]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(27),
      I1 => hdmi_in_vsync,
      O => \v_cnt[27]_i_2_n_0\
    );
\v_cnt[27]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(26),
      I1 => hdmi_in_vsync,
      O => \v_cnt[27]_i_3_n_0\
    );
\v_cnt[27]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(25),
      I1 => hdmi_in_vsync,
      O => \v_cnt[27]_i_4_n_0\
    );
\v_cnt[27]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(24),
      I1 => hdmi_in_vsync,
      O => \v_cnt[27]_i_5_n_0\
    );
\v_cnt[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F8"
    )
        port map (
      I0 => \v_cnt[31]_i_3_n_0\,
      I1 => \v_cnt[31]_i_4_n_0\,
      I2 => hdmi_in_vsync,
      O => v_cnt0
    );
\v_cnt[31]_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \^h_cnt\(16),
      I1 => \^h_cnt\(17),
      I2 => \^h_cnt\(14),
      I3 => \^h_cnt\(15),
      I4 => \^h_cnt\(19),
      I5 => \^h_cnt\(18),
      O => \v_cnt[31]_i_10_n_0\
    );
\v_cnt[31]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000002000"
    )
        port map (
      I0 => \^h_cnt\(10),
      I1 => \^h_cnt\(11),
      I2 => \^h_cnt\(8),
      I3 => \^h_cnt\(9),
      I4 => \^h_cnt\(13),
      I5 => \^h_cnt\(12),
      O => \v_cnt[31]_i_11_n_0\
    );
\v_cnt[31]_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000800000000000"
    )
        port map (
      I0 => \^h_cnt\(4),
      I1 => \^h_cnt\(5),
      I2 => \^h_cnt\(2),
      I3 => \^h_cnt\(3),
      I4 => \^h_cnt\(7),
      I5 => \^h_cnt\(6),
      O => \v_cnt[31]_i_12_n_0\
    );
\v_cnt[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \^h_cnt\(28),
      I1 => \^h_cnt\(29),
      I2 => \^h_cnt\(26),
      I3 => \^h_cnt\(27),
      I4 => \^h_cnt\(31),
      I5 => \^h_cnt\(30),
      O => \v_cnt[31]_i_3_n_0\
    );
\v_cnt[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \v_cnt[31]_i_9_n_0\,
      I1 => \v_cnt[31]_i_10_n_0\,
      I2 => \v_cnt[31]_i_11_n_0\,
      I3 => \v_cnt[31]_i_12_n_0\,
      I4 => \^h_cnt\(0),
      I5 => \^h_cnt\(1),
      O => \v_cnt[31]_i_4_n_0\
    );
\v_cnt[31]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(31),
      I1 => hdmi_in_vsync,
      O => \v_cnt[31]_i_5_n_0\
    );
\v_cnt[31]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(30),
      I1 => hdmi_in_vsync,
      O => \v_cnt[31]_i_6_n_0\
    );
\v_cnt[31]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(29),
      I1 => hdmi_in_vsync,
      O => \v_cnt[31]_i_7_n_0\
    );
\v_cnt[31]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(28),
      I1 => hdmi_in_vsync,
      O => \v_cnt[31]_i_8_n_0\
    );
\v_cnt[31]_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \^h_cnt\(22),
      I1 => \^h_cnt\(23),
      I2 => \^h_cnt\(20),
      I3 => \^h_cnt\(21),
      I4 => \^h_cnt\(25),
      I5 => \^h_cnt\(24),
      O => \v_cnt[31]_i_9_n_0\
    );
\v_cnt[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => hdmi_in_vsync,
      O => \v_cnt[3]_i_2_n_0\
    );
\v_cnt[3]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(3),
      I1 => hdmi_in_vsync,
      O => \v_cnt[3]_i_3_n_0\
    );
\v_cnt[3]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(2),
      I1 => hdmi_in_vsync,
      O => \v_cnt[3]_i_4_n_0\
    );
\v_cnt[3]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(1),
      I1 => hdmi_in_vsync,
      O => \v_cnt[3]_i_5_n_0\
    );
\v_cnt[3]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^v_cnt\(0),
      I1 => hdmi_in_vsync,
      O => \v_cnt[3]_i_6_n_0\
    );
\v_cnt[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(7),
      I1 => hdmi_in_vsync,
      O => \v_cnt[7]_i_2_n_0\
    );
\v_cnt[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(6),
      I1 => hdmi_in_vsync,
      O => \v_cnt[7]_i_3_n_0\
    );
\v_cnt[7]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(5),
      I1 => hdmi_in_vsync,
      O => \v_cnt[7]_i_4_n_0\
    );
\v_cnt[7]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^v_cnt\(4),
      I1 => hdmi_in_vsync,
      O => \v_cnt[7]_i_5_n_0\
    );
\v_cnt_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[3]_i_1_n_7\,
      Q => \^v_cnt\(0)
    );
\v_cnt_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[11]_i_1_n_5\,
      Q => \^v_cnt\(10)
    );
\v_cnt_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[11]_i_1_n_4\,
      Q => \^v_cnt\(11)
    );
\v_cnt_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \v_cnt_reg[7]_i_1_n_0\,
      CO(3) => \v_cnt_reg[11]_i_1_n_0\,
      CO(2) => \v_cnt_reg[11]_i_1_n_1\,
      CO(1) => \v_cnt_reg[11]_i_1_n_2\,
      CO(0) => \v_cnt_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \v_cnt_reg[11]_i_1_n_4\,
      O(2) => \v_cnt_reg[11]_i_1_n_5\,
      O(1) => \v_cnt_reg[11]_i_1_n_6\,
      O(0) => \v_cnt_reg[11]_i_1_n_7\,
      S(3) => \v_cnt[11]_i_2_n_0\,
      S(2) => \v_cnt[11]_i_3_n_0\,
      S(1) => \v_cnt[11]_i_4_n_0\,
      S(0) => \v_cnt[11]_i_5_n_0\
    );
\v_cnt_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[15]_i_1_n_7\,
      Q => \^v_cnt\(12)
    );
\v_cnt_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[15]_i_1_n_6\,
      Q => \^v_cnt\(13)
    );
\v_cnt_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[15]_i_1_n_5\,
      Q => \^v_cnt\(14)
    );
\v_cnt_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[15]_i_1_n_4\,
      Q => \^v_cnt\(15)
    );
\v_cnt_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \v_cnt_reg[11]_i_1_n_0\,
      CO(3) => \v_cnt_reg[15]_i_1_n_0\,
      CO(2) => \v_cnt_reg[15]_i_1_n_1\,
      CO(1) => \v_cnt_reg[15]_i_1_n_2\,
      CO(0) => \v_cnt_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \v_cnt_reg[15]_i_1_n_4\,
      O(2) => \v_cnt_reg[15]_i_1_n_5\,
      O(1) => \v_cnt_reg[15]_i_1_n_6\,
      O(0) => \v_cnt_reg[15]_i_1_n_7\,
      S(3) => \v_cnt[15]_i_2_n_0\,
      S(2) => \v_cnt[15]_i_3_n_0\,
      S(1) => \v_cnt[15]_i_4_n_0\,
      S(0) => \v_cnt[15]_i_5_n_0\
    );
\v_cnt_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[19]_i_1_n_7\,
      Q => \^v_cnt\(16)
    );
\v_cnt_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[19]_i_1_n_6\,
      Q => \^v_cnt\(17)
    );
\v_cnt_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[19]_i_1_n_5\,
      Q => \^v_cnt\(18)
    );
\v_cnt_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[19]_i_1_n_4\,
      Q => \^v_cnt\(19)
    );
\v_cnt_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \v_cnt_reg[15]_i_1_n_0\,
      CO(3) => \v_cnt_reg[19]_i_1_n_0\,
      CO(2) => \v_cnt_reg[19]_i_1_n_1\,
      CO(1) => \v_cnt_reg[19]_i_1_n_2\,
      CO(0) => \v_cnt_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \v_cnt_reg[19]_i_1_n_4\,
      O(2) => \v_cnt_reg[19]_i_1_n_5\,
      O(1) => \v_cnt_reg[19]_i_1_n_6\,
      O(0) => \v_cnt_reg[19]_i_1_n_7\,
      S(3) => \v_cnt[19]_i_2_n_0\,
      S(2) => \v_cnt[19]_i_3_n_0\,
      S(1) => \v_cnt[19]_i_4_n_0\,
      S(0) => \v_cnt[19]_i_5_n_0\
    );
\v_cnt_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[3]_i_1_n_6\,
      Q => \^v_cnt\(1)
    );
\v_cnt_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[23]_i_1_n_7\,
      Q => \^v_cnt\(20)
    );
\v_cnt_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[23]_i_1_n_6\,
      Q => \^v_cnt\(21)
    );
\v_cnt_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[23]_i_1_n_5\,
      Q => \^v_cnt\(22)
    );
\v_cnt_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[23]_i_1_n_4\,
      Q => \^v_cnt\(23)
    );
\v_cnt_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \v_cnt_reg[19]_i_1_n_0\,
      CO(3) => \v_cnt_reg[23]_i_1_n_0\,
      CO(2) => \v_cnt_reg[23]_i_1_n_1\,
      CO(1) => \v_cnt_reg[23]_i_1_n_2\,
      CO(0) => \v_cnt_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \v_cnt_reg[23]_i_1_n_4\,
      O(2) => \v_cnt_reg[23]_i_1_n_5\,
      O(1) => \v_cnt_reg[23]_i_1_n_6\,
      O(0) => \v_cnt_reg[23]_i_1_n_7\,
      S(3) => \v_cnt[23]_i_2_n_0\,
      S(2) => \v_cnt[23]_i_3_n_0\,
      S(1) => \v_cnt[23]_i_4_n_0\,
      S(0) => \v_cnt[23]_i_5_n_0\
    );
\v_cnt_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[27]_i_1_n_7\,
      Q => \^v_cnt\(24)
    );
\v_cnt_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[27]_i_1_n_6\,
      Q => \^v_cnt\(25)
    );
\v_cnt_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[27]_i_1_n_5\,
      Q => \^v_cnt\(26)
    );
\v_cnt_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[27]_i_1_n_4\,
      Q => \^v_cnt\(27)
    );
\v_cnt_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \v_cnt_reg[23]_i_1_n_0\,
      CO(3) => \v_cnt_reg[27]_i_1_n_0\,
      CO(2) => \v_cnt_reg[27]_i_1_n_1\,
      CO(1) => \v_cnt_reg[27]_i_1_n_2\,
      CO(0) => \v_cnt_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \v_cnt_reg[27]_i_1_n_4\,
      O(2) => \v_cnt_reg[27]_i_1_n_5\,
      O(1) => \v_cnt_reg[27]_i_1_n_6\,
      O(0) => \v_cnt_reg[27]_i_1_n_7\,
      S(3) => \v_cnt[27]_i_2_n_0\,
      S(2) => \v_cnt[27]_i_3_n_0\,
      S(1) => \v_cnt[27]_i_4_n_0\,
      S(0) => \v_cnt[27]_i_5_n_0\
    );
\v_cnt_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[31]_i_2_n_7\,
      Q => \^v_cnt\(28)
    );
\v_cnt_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[31]_i_2_n_6\,
      Q => \^v_cnt\(29)
    );
\v_cnt_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[3]_i_1_n_5\,
      Q => \^v_cnt\(2)
    );
\v_cnt_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[31]_i_2_n_5\,
      Q => \^v_cnt\(30)
    );
\v_cnt_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[31]_i_2_n_4\,
      Q => \^v_cnt\(31)
    );
\v_cnt_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \v_cnt_reg[27]_i_1_n_0\,
      CO(3) => \NLW_v_cnt_reg[31]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \v_cnt_reg[31]_i_2_n_1\,
      CO(1) => \v_cnt_reg[31]_i_2_n_2\,
      CO(0) => \v_cnt_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \v_cnt_reg[31]_i_2_n_4\,
      O(2) => \v_cnt_reg[31]_i_2_n_5\,
      O(1) => \v_cnt_reg[31]_i_2_n_6\,
      O(0) => \v_cnt_reg[31]_i_2_n_7\,
      S(3) => \v_cnt[31]_i_5_n_0\,
      S(2) => \v_cnt[31]_i_6_n_0\,
      S(1) => \v_cnt[31]_i_7_n_0\,
      S(0) => \v_cnt[31]_i_8_n_0\
    );
\v_cnt_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[3]_i_1_n_4\,
      Q => \^v_cnt\(3)
    );
\v_cnt_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \v_cnt_reg[3]_i_1_n_0\,
      CO(2) => \v_cnt_reg[3]_i_1_n_1\,
      CO(1) => \v_cnt_reg[3]_i_1_n_2\,
      CO(0) => \v_cnt_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \v_cnt[3]_i_2_n_0\,
      O(3) => \v_cnt_reg[3]_i_1_n_4\,
      O(2) => \v_cnt_reg[3]_i_1_n_5\,
      O(1) => \v_cnt_reg[3]_i_1_n_6\,
      O(0) => \v_cnt_reg[3]_i_1_n_7\,
      S(3) => \v_cnt[3]_i_3_n_0\,
      S(2) => \v_cnt[3]_i_4_n_0\,
      S(1) => \v_cnt[3]_i_5_n_0\,
      S(0) => \v_cnt[3]_i_6_n_0\
    );
\v_cnt_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[7]_i_1_n_7\,
      Q => \^v_cnt\(4)
    );
\v_cnt_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[7]_i_1_n_6\,
      Q => \^v_cnt\(5)
    );
\v_cnt_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[7]_i_1_n_5\,
      Q => \^v_cnt\(6)
    );
\v_cnt_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[7]_i_1_n_4\,
      Q => \^v_cnt\(7)
    );
\v_cnt_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \v_cnt_reg[3]_i_1_n_0\,
      CO(3) => \v_cnt_reg[7]_i_1_n_0\,
      CO(2) => \v_cnt_reg[7]_i_1_n_1\,
      CO(1) => \v_cnt_reg[7]_i_1_n_2\,
      CO(0) => \v_cnt_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \v_cnt_reg[7]_i_1_n_4\,
      O(2) => \v_cnt_reg[7]_i_1_n_5\,
      O(1) => \v_cnt_reg[7]_i_1_n_6\,
      O(0) => \v_cnt_reg[7]_i_1_n_7\,
      S(3) => \v_cnt[7]_i_2_n_0\,
      S(2) => \v_cnt[7]_i_3_n_0\,
      S(1) => \v_cnt[7]_i_4_n_0\,
      S(0) => \v_cnt[7]_i_5_n_0\
    );
\v_cnt_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[11]_i_1_n_7\,
      Q => \^v_cnt\(8)
    );
\v_cnt_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => hdmi_in_clk,
      CE => v_cnt0,
      CLR => \h_cnt[31]_i_2_n_0\,
      D => \v_cnt_reg[11]_i_1_n_6\,
      Q => \^v_cnt\(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_hdmi_ctrl_0_0 is
  port (
    sys_clk : in STD_LOGIC;
    sys_rst_n : in STD_LOGIC;
    sys_init_done : in STD_LOGIC;
    ddc_scl : out STD_LOGIC;
    ddc_sda : inout STD_LOGIC;
    hdmi_in_clk : in STD_LOGIC;
    hdmi_in_rst_n : out STD_LOGIC;
    hdmi_in_hsync : in STD_LOGIC;
    hdmi_in_vsync : in STD_LOGIC;
    hdmi_in_rgb : in STD_LOGIC_VECTOR ( 23 downto 0 );
    hdmi_in_de : in STD_LOGIC;
    fifo_wr_en : out STD_LOGIC;
    cfg_done : out STD_LOGIC;
    hdmi_out_clk : out STD_LOGIC;
    hdmi_out_rst_n : out STD_LOGIC;
    hdmi_out_hsync : out STD_LOGIC;
    hdmi_out_vsync : out STD_LOGIC;
    hdmi_out_rgb : out STD_LOGIC_VECTOR ( 31 downto 0 );
    hdmi_out_de : out STD_LOGIC;
    h_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 );
    v_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of system_hdmi_ctrl_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of system_hdmi_ctrl_0_0 : entity is "system_hdmi_ctrl_0_0,hdmi_ctrl,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of system_hdmi_ctrl_0_0 : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of system_hdmi_ctrl_0_0 : entity is "module_ref";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of system_hdmi_ctrl_0_0 : entity is "hdmi_ctrl,Vivado 2019.2";
end system_hdmi_ctrl_0_0;

architecture STRUCTURE of system_hdmi_ctrl_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \^hdmi_in_clk\ : STD_LOGIC;
  signal \^hdmi_in_de\ : STD_LOGIC;
  signal \^hdmi_in_hsync\ : STD_LOGIC;
  signal \^hdmi_in_rgb\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \^hdmi_in_vsync\ : STD_LOGIC;
  signal \^sys_rst_n\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of hdmi_in_clk : signal is "xilinx.com:signal:clock:1.0 hdmi_in_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of hdmi_in_clk : signal is "XIL_INTERFACENAME hdmi_in_clk, ASSOCIATED_RESET hdmi_in_rst_n, FREQ_HZ 148500000, PHASE 0.000, CLK_DOMAIN system_hdmi_in_clk_0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of hdmi_in_rst_n : signal is "xilinx.com:signal:reset:1.0 hdmi_in_rst_n RST";
  attribute X_INTERFACE_PARAMETER of hdmi_in_rst_n : signal is "XIL_INTERFACENAME hdmi_in_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of hdmi_out_clk : signal is "xilinx.com:signal:clock:1.0 hdmi_out_clk CLK";
  attribute X_INTERFACE_PARAMETER of hdmi_out_clk : signal is "XIL_INTERFACENAME hdmi_out_clk, ASSOCIATED_RESET hdmi_out_rst_n, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_hdmi_ctrl_0_0_hdmi_out_clk, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of hdmi_out_rst_n : signal is "xilinx.com:signal:reset:1.0 hdmi_out_rst_n RST";
  attribute X_INTERFACE_PARAMETER of hdmi_out_rst_n : signal is "XIL_INTERFACENAME hdmi_out_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of sys_clk : signal is "xilinx.com:signal:clock:1.0 sys_clk CLK";
  attribute X_INTERFACE_PARAMETER of sys_clk : signal is "XIL_INTERFACENAME sys_clk, ASSOCIATED_RESET sys_rst_n, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN system_sys_clk, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of sys_rst_n : signal is "xilinx.com:signal:reset:1.0 sys_rst_n RST";
  attribute X_INTERFACE_PARAMETER of sys_rst_n : signal is "XIL_INTERFACENAME sys_rst_n, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  \^hdmi_in_clk\ <= hdmi_in_clk;
  \^hdmi_in_de\ <= hdmi_in_de;
  \^hdmi_in_hsync\ <= hdmi_in_hsync;
  \^hdmi_in_rgb\(23 downto 0) <= hdmi_in_rgb(23 downto 0);
  \^hdmi_in_vsync\ <= hdmi_in_vsync;
  \^sys_rst_n\ <= sys_rst_n;
  hdmi_in_rst_n <= \^sys_rst_n\;
  hdmi_out_clk <= \^hdmi_in_clk\;
  hdmi_out_de <= \^hdmi_in_de\;
  hdmi_out_hsync <= \^hdmi_in_hsync\;
  hdmi_out_rgb(31) <= \<const0>\;
  hdmi_out_rgb(30) <= \<const0>\;
  hdmi_out_rgb(29) <= \<const0>\;
  hdmi_out_rgb(28) <= \<const0>\;
  hdmi_out_rgb(27) <= \<const0>\;
  hdmi_out_rgb(26) <= \<const0>\;
  hdmi_out_rgb(25) <= \<const0>\;
  hdmi_out_rgb(24) <= \<const0>\;
  hdmi_out_rgb(23 downto 0) <= \^hdmi_in_rgb\(23 downto 0);
  hdmi_out_rst_n <= \^sys_rst_n\;
  hdmi_out_vsync <= \^hdmi_in_vsync\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.system_hdmi_ctrl_0_0_hdmi_ctrl
     port map (
      cfg_done_reg => cfg_done,
      ddc_scl => ddc_scl,
      ddc_sda => ddc_sda,
      fifo_wr_en => fifo_wr_en,
      h_cnt(31 downto 0) => h_cnt(31 downto 0),
      hdmi_in_clk => \^hdmi_in_clk\,
      hdmi_in_de => \^hdmi_in_de\,
      hdmi_in_vsync => \^hdmi_in_vsync\,
      sys_clk => sys_clk,
      sys_init_done => sys_init_done,
      sys_rst_n => \^sys_rst_n\,
      v_cnt(31 downto 0) => v_cnt(31 downto 0)
    );
end STRUCTURE;
