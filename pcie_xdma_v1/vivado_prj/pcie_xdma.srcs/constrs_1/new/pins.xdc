set_property PACKAGE_PIN W19 [get_ports sys_clk]
set_property IOSTANDARD LVCMOS33 [get_ports sys_clk]

#PCIe reference clock 100MHz
set_property PACKAGE_PIN F10 [get_ports {pcie_ref_clk_p[0]}]
# PCIe MGT interface
set_property LOC GTPE2_CHANNEL_X0Y7 [get_cells {system_i/xdma_0/inst/system_xdma_0_0_pcie2_to_pcie3_wrapper_i/pcie2_ip_i/inst/inst/gt_top_i/pipe_wrapper_i/pipe_lane[0].gt_wrapper_i/gtp_channel.gtpe2_channel_i}]
set_property PACKAGE_PIN D9 [get_ports {pcie_mgt_rxp[0]}]
set_property PACKAGE_PIN D7 [get_ports {pcie_mgt_txp[0]}]
set_property LOC GTPE2_CHANNEL_X0Y6 [get_cells {system_i/xdma_0/inst/system_xdma_0_0_pcie2_to_pcie3_wrapper_i/pcie2_ip_i/inst/inst/gt_top_i/pipe_wrapper_i/pipe_lane[1].gt_wrapper_i/gtp_channel.gtpe2_channel_i}]
set_property PACKAGE_PIN B10 [get_ports {pcie_mgt_rxp[1]}]
set_property PACKAGE_PIN B6 [get_ports {pcie_mgt_txp[1]}]
# PCIe rst signal
set_property -dict {PACKAGE_PIN F21 IOSTANDARD LVCMOS33} [get_ports pcie_rst_n]

# led interface
set_property -dict {PACKAGE_PIN M21 IOSTANDARD LVCMOS33} [get_ports {led_tri_o[0]}]
set_property -dict {PACKAGE_PIN L21 IOSTANDARD LVCMOS33} [get_ports {led_tri_o[1]}]
set_property -dict {PACKAGE_PIN K21 IOSTANDARD LVCMOS33} [get_ports {led_tri_o[2]}]
set_property -dict {PACKAGE_PIN K22 IOSTANDARD LVCMOS33} [get_ports lnk_up_led]
# key interface
set_property -dict {PACKAGE_PIN V17 IOSTANDARD LVCMOS33} [get_ports {key_tri_i[0]}]
set_property -dict {PACKAGE_PIN W17 IOSTANDARD LVCMOS33} [get_ports {key_tri_i[1]}]
set_property -dict {PACKAGE_PIN AA18 IOSTANDARD LVCMOS33} [get_ports {key_tri_i[2]}]
set_property -dict {PACKAGE_PIN AB18 IOSTANDARD LVCMOS33} [get_ports {key_tri_i[3]}]
# irq ack signal
# set_property -dict {PACKAGE_PIN V7 IOSTANDARD LVCMOS15} [get_ports irq_ack]


##ov5640
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets ov5640_pclk_IBUF]


set_property PACKAGE_PIN R17 [get_ports ov5640_pwdn]
set_property PACKAGE_PIN AA21 [get_ports ov5640_pclk]
set_property PACKAGE_PIN T21 [get_ports ov5640_href]
set_property PACKAGE_PIN P16 [get_ports ov5640_rst_n]
set_property PACKAGE_PIN T18 [get_ports ov5640_vsync]
set_property PACKAGE_PIN R14 [get_ports sccb_scl]
set_property PACKAGE_PIN P14 [get_ports sccb_sda]
set_property PACKAGE_PIN U21 [get_ports {ov5640_data[7]}]
set_property PACKAGE_PIN R18 [get_ports {ov5640_data[6]}]
set_property PACKAGE_PIN AA20 [get_ports {ov5640_data[5]}]
set_property PACKAGE_PIN U22 [get_ports {ov5640_data[4]}]
set_property PACKAGE_PIN P19 [get_ports {ov5640_data[3]}]
set_property PACKAGE_PIN R19 [get_ports {ov5640_data[2]}]
set_property PACKAGE_PIN N13 [get_ports {ov5640_data[1]}]
set_property PACKAGE_PIN N14 [get_ports {ov5640_data[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports ov5640_pwdn]
set_property IOSTANDARD LVCMOS33 [get_ports ov5640_pclk]
set_property IOSTANDARD LVCMOS33 [get_ports ov5640_href]
set_property IOSTANDARD LVCMOS33 [get_ports ov5640_rst_n]
set_property IOSTANDARD LVCMOS33 [get_ports ov5640_vsync]
set_property IOSTANDARD LVCMOS33 [get_ports sccb_sda]
set_property IOSTANDARD LVCMOS33 [get_ports sccb_scl]
set_property IOSTANDARD LVCMOS33 [get_ports {ov5640_data[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ov5640_data[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ov5640_data[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ov5640_data[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ov5640_data[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ov5640_data[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ov5640_data[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {ov5640_data[0]}]

set_property PACKAGE_PIN N15 [get_ports sys_rst_n]
set_property IOSTANDARD LVCMOS33 [get_ports sys_rst_n]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets ov5640_pclk_IBUF_BUFG]
