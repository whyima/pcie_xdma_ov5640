 `timescale 1ns/1ps
//核心代码
module AXI_DDR_RW_v1_0_M_AXI #
(
	parameter 			MAX_BURST_COUNTER 	= 14'd16_200,
	parameter  			C_M_TARGET_SLAVE_BASE_ADDR	= 32'h10_000_000,//! 操作内存起始地址
	parameter integer 	C_M_AXI_BURST_LEN	= 128,//! 突发长度取值范围1, 2, 4, 8, 16, 32, 64, 128, 256
	parameter integer 	C_M_AXI_ID_WIDTH	= 1,//! ID宽度（指定某些特殊任务传输顺序，未用）
	parameter integer	C_M_AXI_ADDR_WIDTH	= 32,//! 地址总线位宽
	parameter integer	C_M_AXI_DATA_WIDTH	= 32,//! 数据总线宽度
	// 以下为用户自定义数据位宽，均未用到
	parameter integer	C_M_AXI_ARUSER_WIDTH= 0,
	parameter integer	C_M_AXI_RUSER_WIDTH	= 0
)
(
	// 输入信号
	input  wire  							init_txn_pulse	, //! vsync 作为一次复位信号
	input  wire  							rd_prog_full	,
	//AXI主接口的信号
	// 全局时钟复位 
	input  wire  							M_AXI_ACLK		,
	input  wire  							M_AXI_ARESETN	,
	/**************************************读地址通道*********************************************/
	output wire [C_M_AXI_ADDR_WIDTH-1 : 0] 	M_AXI_ARADDR	,
	output wire [7 : 0] 					M_AXI_ARLEN		,
	output wire [2 : 0] 					M_AXI_ARSIZE	,
	output wire [1 : 0] 					M_AXI_ARBURST	,
	output wire  							M_AXI_ARVALID	,
	input  wire  							M_AXI_ARREADY	,
	output wire [C_M_AXI_ID_WIDTH-1 : 0] 	M_AXI_ARID		,
	output wire  							M_AXI_ARLOCK	,
	output wire [3 : 0] 					M_AXI_ARCACHE	,
	output wire [2 : 0] 					M_AXI_ARPROT	,
	output wire [3 : 0] 					M_AXI_ARQOS		,//! 指示A紅传输的优先级，以确保高优先级的传输可以优先处理
	output wire [C_M_AXI_ARUSER_WIDTH-1 : 0]M_AXI_ARUSER	,//! USER信号，在官方都没有明确定定义，牛！ 不使用
	/************************************************************************************************/

	/**************************************读数据通道*********************************************/
	output wire  							M_AXI_RREADY	,
	input  wire [C_M_AXI_DATA_WIDTH-1 : 0] 	M_AXI_RDATA		,
	input  wire [1 : 0] 					M_AXI_RRESP		,//! 读响应信号四种值对应四个响应，表示对错与否
	input  wire  							M_AXI_RLAST		,
	input  wire  							M_AXI_RVALID	,
	input  wire [C_M_AXI_ID_WIDTH-1 : 0] 	M_AXI_RID		,//! ID，可看作一个标记，未用到
	input  wire [C_M_AXI_RUSER_WIDTH-1 : 0] M_AXI_RUSER		
	/************************************************************************************************/
);


//输入的一个参数位深度，对输入的数据进行一个位宽的计算返回clogb2                     
function integer clogb2 (input integer bit_depth);//这个函数用来计算传入参数的位宽             
begin                                                           
for(clogb2=0; bit_depth>0; clogb2=clogb2+1)
	bit_depth = bit_depth >> 1;
end
endfunction                                                     

//一次读写突发传输计数位宽==4
localparam integer C_TRANSACTIONS_NUM = clogb2(C_M_AXI_BURST_LEN-1);

parameter [1:0] IDLE = 2'b00,
				INIT_READ = 2'b01; 



reg [1:0] 						mst_exec_state;
//AXI4关键接口的寄存
reg [C_M_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
reg  							axi_arvalid;
reg  							axi_rready;

//其他寄存器
wire [31 : 0] 	burst_size_bytes;//! 一次突发传输的字节数
// reg  [C_TRANSACTIONS_NUM : 0] 	read_index;//读计数（一次突发）
reg  [31 : 0] 	read_index; // 给32 方便调试
reg  [31 : 0] 	read_burst_counter;//! 读写突发计数
reg  			start_single_burst_read;//! 单次突发开始信号
reg  			reads_done;//! 读完成信号
reg  			error_reg;//! 错误信号
reg  			burst_read_active;//! 高表示突发正在进行
wire  			read_resp_error;//! 读写回应错误
wire  			rnext; //! 指示一个有效数据的到来

//
reg burst_vaild;

//信号的基本配置
assign M_AXI_ARID	= 'b0;
assign M_AXI_ARADDR	= C_M_TARGET_SLAVE_BASE_ADDR + axi_araddr;
assign M_AXI_ARLEN	= C_M_AXI_BURST_LEN - 1;
assign M_AXI_ARSIZE	= clogb2((C_M_AXI_DATA_WIDTH/8)-1);
assign M_AXI_ARBURST	= 2'b01;
assign M_AXI_ARLOCK	= 1'b0;
assign M_AXI_ARCACHE	= 4'b0010;//不缓存
assign M_AXI_ARPROT	= 3'h0;
assign M_AXI_ARQOS	= 4'h0;
assign M_AXI_ARUSER	= 'b1;
assign M_AXI_ARVALID	= axi_arvalid;
assign M_AXI_RREADY	= axi_rready;

assign burst_size_bytes	= C_M_AXI_BURST_LEN * C_M_AXI_DATA_WIDTH/8;//! 突发长度16*一个4个字节 = 突发传64个字节


//----------------------------
//用户代码
//----------------------------
always @(posedge M_AXI_ACLK) begin
	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)
		burst_vaild <= 1'b0;
	else if (!rd_prog_full) // 如果fifo没有"满"，就允许突发读ddr数据写进fifo
		burst_vaild <= 1'b1;
	else
		burst_vaild <= 1'b0;
end



//----------------------------
//读地址通道
//----------------------------
always @(posedge M_AXI_ACLK) begin                                                              
if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1 )               
	begin                                                          
	axi_arvalid <= 1'b0;
	end                                                            
// 读地址未有效且检测到单次读突发             
else if (~axi_arvalid && start_single_burst_read)
	begin
	axi_arvalid <= 1'b1;
	end
else if (M_AXI_ARREADY && axi_arvalid)
	begin                                                          
	axi_arvalid <= 1'b0;
	end                                                            
else                                                             
	axi_arvalid <= axi_arvalid;
end                                                                
																	
																	

always @(posedge M_AXI_ACLK) begin                                                              
if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)                
	begin                                                          
	axi_araddr <= 'b0;                                           
	end                                                            
else if (M_AXI_ARREADY && axi_arvalid)                           
	begin                                                          
	axi_araddr <= axi_araddr + burst_size_bytes;                 
	end                                                            
else                                                             
	axi_araddr <= axi_araddr;                                      
end                                                                


//--------------------------------
//读数据通道
//--------------------------------
assign rnext = M_AXI_RVALID && axi_rready;                            
																															
// 一次突发读次数计数即突发读长度
always @(posedge M_AXI_ACLK) begin                                                                 
if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1 || start_single_burst_read)                                                           
	read_index <= 0;                                                
else if (rnext && (read_index != C_M_AXI_BURST_LEN-1))                                                            
	read_index <= read_index + 1;                                   
else                                                                
	read_index <= read_index;                                         
end                                                                   
																		
																		
//读准备，只要从设备准备好了，就拉高
always @(posedge M_AXI_ACLK) begin                                                                 
if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1 )                  
	begin                                                             
	axi_rready <= 1'b0;                                             
	end                                                                                        
else if (M_AXI_RVALID) begin                                      
		if (M_AXI_RLAST && axi_rready)                                          
			axi_rready <= 1'b0;                                                     
		else                                                                   
			axi_rready <= 1'b1;                                                 
	end                                        
end                                            
																		                                                            
																		           
//! RRESP [1:0] 为响应信号
//! 00 OKAY  01 EXOKAY 表示成功 详见文档
//! 10 SLVERR 11 DECERR 表示失败                         
assign read_resp_error = axi_rready & M_AXI_RVALID & M_AXI_RRESP[1];  

//----------------------------------
//
//----------------------------------

always @(posedge M_AXI_ACLK)                                 
begin                                                              
if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)                                          
	begin                                                          
	error_reg <= 1'b0;                                           
	end                                                            
//else if (read_mismatch || write_resp_error || read_resp_error)   
// else if (write_resp_error || read_resp_error)   
else if (read_resp_error)   
	begin                                                          
	error_reg <= 1'b1;                                           
	end                                                            
else                                                             
	error_reg <= error_reg;                                        
end                                                                


//--------------------------------
//用户代码 -- 状态机 
//--------------------------------
					
always @(posedge M_AXI_ACLK)                                                                              
begin                                                                                                     
if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)                                                                                 
	begin                                                                                                 
	read_burst_counter <= 'b0;                                                                          
	end                                                                                                   
else if (M_AXI_ARREADY && axi_arvalid)                                                                  
	begin                                                                                                 
	if (read_burst_counter != MAX_BURST_COUNTER)                                                    
		begin                                                                                             
		read_burst_counter <= read_burst_counter + 1'b1;                                                
		end                                                                                               
	end                                                                                                   
else                                                                                                    
	read_burst_counter <= read_burst_counter;                                                             
end                                                                                                       

																											
always @ ( posedge M_AXI_ACLK) begin                                                                                                     
if (M_AXI_ARESETN == 1'b0 ) begin                                                                                                 
	mst_exec_state      <= IDLE;
	start_single_burst_read  <= 1'b0;
end                                                                                                   
else begin                                                                                                 
	// 状态机                                                                               
	case (mst_exec_state)                                                                               																									
		IDLE: begin
			if ( init_txn_pulse == 1'b1)                                                                                                                                             
				mst_exec_state  <= INIT_READ;
			else
				mst_exec_state  <= IDLE;
		end
		INIT_READ:                                                                                        
		if (reads_done) begin                                                                                         
			mst_exec_state <= IDLE;                                                             
		end                                                                                           
		else begin                                                                                         
			mst_exec_state  <= INIT_READ;
			//当一次突发读完成后，active将拉高，此时等待vaild信号进行下一次的突发读
			if (~axi_arvalid && ~burst_read_active && ~start_single_burst_read && burst_vaild)
				start_single_burst_read <= 1'b1; //这个信号拉一次就是一次突发，即128个xx位宽数据，我们控制这个信号
			else
				start_single_burst_read <= 1'b0; //Negate to generate a pulse
		end
		default: begin                                                                                           
			mst_exec_state  <= IDLE;                                                              
		end                                                                                             
	endcase
end                                                                                                   
end                                                                               

// 突发有效信号        
always @(posedge M_AXI_ACLK) begin                                                                                                     
if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)
	burst_read_active <= 1'b0;
else if (start_single_burst_read)
	burst_read_active <= 1'b1;
else if (M_AXI_RVALID && axi_rready && M_AXI_RLAST)
	burst_read_active <= 0;
end                                                                                                     
																			
always @(posedge M_AXI_ACLK) begin
if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)
	reads_done <= 1'b0;
else if (M_AXI_RVALID && axi_rready && (read_index == C_M_AXI_BURST_LEN-1) && (read_burst_counter ==  MAX_BURST_COUNTER))
	reads_done <= 1'b1;                                                                                   
else
	reads_done <= reads_done;
end                                                                                                     

//user logic

//////////////////////////////////////////////////////////////
/// DEBUG
//////////////////////////////////////////////////////////////

// reg [1:0] 						mst_exec_state;
// //AXI4关键接口的寄存
// reg [C_M_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr	;
// reg  							axi_arvalid	;
// reg  							axi_rready	;

// //其他寄存器
// wire [31 : 0] 	burst_size_bytes		;//! 一次突发传输的字节数
// reg  [31 : 0] 	read_index				;//读计数（一次突发）
// reg  [31 : 0] 	read_burst_counter		;//! 读写突发计数
// reg  			start_single_burst_read	;//! 单次突发开始信号
// reg  			reads_done				;//! 读完成信号
// reg  			error_reg				;//! 错误信号
// reg  			burst_read_active		;//! 高表示突发正在进行
// wire  			read_resp_error			;//! 读写回应错误
// wire  			rnext					; //! 指示一个有效数据的到来

// output wire  							M_AXI_ARVALID	,
// input  wire  							M_AXI_ARREADY	,
// output wire [C_M_AXI_ADDR_WIDTH-1 : 0] 	M_AXI_ARADDR	,
// input  wire  							M_AXI_RLAST		,
// input  wire  							M_AXI_RVALID	,
// output wire  							M_AXI_RREADY	,
// input  wire [C_M_AXI_DATA_WIDTH-1 : 0] 	M_AXI_RDATA		,

// input  wire  							init_txn_pulse	, //! vsync 作为一次复位信号
// input  wire  							rd_prog_full	,
// 											burst_vaild		


localparam DEBUG = 0;
generate
	// if (DEBUG == 1) begin
	// 	ila_axi_rw ila_axi_rw (
	// 		.clk		(M_AXI_ACLK				), // input wire clk
		
	// 		.probe0		(mst_exec_state			), // input wire [1:0]  probe0  
	// 		.probe1		(M_AXI_RDATA			), // input wire [31:0] probe1 
	// 		.probe2		(M_AXI_ARVALID			), // input wire [0:0]  probe2 
	// 		.probe3		(M_AXI_RREADY			), // input wire [0:0]  probe3 
	// 		.probe4		(burst_size_bytes		), // input wire [31:0] probe4 
	// 		.probe5		(read_index				), // input wire [31:0] probe5 
	// 		.probe6		(read_burst_counter		), // input wire [31:0] probe6 
	// 		.probe7		(start_single_burst_read), // input wire [0:0]  probe7 
	// 		.probe8		(reads_done				), // input wire [0:0]  probe8 
	// 		.probe9		(error_reg				), // input wire [0:0]  probe9 
	// 		.probe10	(burst_read_active		), // input wire [0:0]  probe10 
	// 		.probe11	(read_resp_error		), // input wire [0:0]  probe11 
	// 		.probe12	(rnext					), // input wire [0:0]  probe12 
	// 		.probe13	(M_AXI_ARREADY			), // input wire [0:0]  probe13 
	// 		.probe14	(M_AXI_RLAST			), // input wire [0:0]  probe14 
	// 		.probe15	(M_AXI_ARADDR			), // input wire [31:0] probe15 
	// 		.probe16	(M_AXI_RVALID			), // input wire [0:0]  probe16 
	// 		.probe17	(init_txn_pulse			), // input [0:0]
	// 		.probe18	(rd_prog_full			), // input [0:0]
	// 		.probe19	(burst_vaild			)  // input [0:0]
	// 	);
	// end
endgenerate

endmodule
