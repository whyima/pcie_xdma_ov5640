module ov5640_top
(

    input   wire            sys_clk         ,   //! 系统时钟
    input   wire            sys_rst_n       ,   //! 复位信号
	input   wire            sys_init_done   ,   //! 系统初始化完成(SDRAM cable done + 摄像头 cfg_done )


	// OV5640
    input   wire            ov5640_pclk     ,   //! 摄像头像素时钟
    input   wire            ov5640_href     ,   //! 摄像头行同步信号
    input   wire            ov5640_vsync    ,   //! 摄像头场同步信号
    input   wire    [ 7:0]  ov5640_data     ,   //! 摄像头图像数据

    output  wire            ov5640_rst_n	,  //摄像1与2头复位信号，低电平有效
    output  wire            ov5640_pwdn 	,  //摄像1与2头时钟选择信号

	// iic
	output  wire            sccb_scl        ,   //SCL
    inout   wire            sccb_sda        ,   //SDA
	//输出给fifo使用的信号

	output  wire            cfg_done        ,   //寄存器配置完成
	output  wire            ov5640_wr_en    ,   //! 图像数据有效使能信号
    output  wire    [31:0]  ov5640_data_out ,    //! 图像数据

	output  reg [31:0] 		href_cnt		,
	output  reg [31:0] 		href_v_cnt		
);
localparam ADDR_NUM 	= 1'b1; //iic 写地址字节数 1:2字节地址 0:1字节地址
localparam SLAVE_ADDR 	= 7'h3C; // 器件地址(SLAVE_ADDR)

wire            cfg_start;   //单个寄存器配置触发信号 配置好了，ov5640_cfg就发一下个
wire            i2c_end  ;   //! i2c一次读/写操作完成
wire    [23:0]  cfg_data ;   //ID,REG_ADDR,REG_VAL
wire            i2c_clk  ;   //! i2c驱动时钟

wire    [15:0]  cnt_wait ;   //寄存器配置等待计数器
///////////////////////////////////////////////////////////////////////
/// ov5640 寄存器配置
///////////////////////////////////////////////////////////////////////
iic_ctrl #(
    .DEVICE_ADDR     (SLAVE_ADDR		),   //i2c设备地址
    .SYS_CLK_FREQ    (26'd50_000_000  	),   //输入系统时钟频率
    .SCL_FREQ        (18'd250_000     	)    //i2c设备scl时钟频率
)
iic_ctrl
(
	.sys_clk     (sys_clk		),   //! 输入系统时钟,50MHz
    .sys_rst_n   (sys_rst_n		),   //! 输入复位信号,低电平有效
    .wr_en       (1'b1			),   //! 输入写使能信号
    .rd_en       (1'b0			),   //! 输入读使能信号
    .i2c_start   (cfg_start		),   //! 输入i2c触发信号
    .addr_num    (1'b1			),   //! 输入i2c字节地址字节数
    .byte_addr   (cfg_data[23:8]),   //! 输入i2c字节地址
    .wr_data     (cfg_data[7:0]	),   //! 输入i2c设备数据

    .i2c_clk     (i2c_clk		),   //! i2c驱动时钟 1M
    .i2c_end     (i2c_end		),   //! i2c一次读/写操作完成
    .rd_data     (),   				 //! 输出i2c设备读取数据
    .i2c_scl     (sccb_scl		),   //! 输出至i2c设备的串行时钟信号scl 250K
    .i2c_sda     (sccb_sda		)    //! 输出至i2c设备的串行数据信号sda
);

// 
ov5640_cfg ov5640_cfg
(
    .sys_clk     (i2c_clk	),   //系统时钟,由iic模块传入
    .sys_rst_n   (sys_rst_n	),   //系统复位,低有效
    .cfg_end     (i2c_end	),   //单个寄存器配置完成

    .cfg_start   (cfg_start	),   //单个寄存器配置触发信号
    .cfg_data    (cfg_data	),   //ID,REG_ADDR,REG_VAL
    .cfg_done    (cfg_done	),    //寄存器配置完成
	.cnt_wait 	 (cnt_wait	)
);



ov5640_data_in ov5640_data_in
(
	.sys_rst_n       (sys_rst_n & sys_init_done),   //复位信号 

	.ov5640_pclk     (ov5640_pclk 		),   //摄像头像素时钟
	.ov5640_href     (ov5640_href 		),   //摄像头行同步信号 , 此信号有效就代表像素点有效
	.ov5640_vsync    (ov5640_vsync		),   //摄像头场同步信号
	.ov5640_data     (ov5640_data 		),   //摄像头图像数据


	.ov5640_wr_en    (ov5640_wr_en   	),   //图像数据有效使能信号
    .ov5640_data_out (ov5640_data_out	)    //图像数据
);

assign  ov5640_rst_n = 1'b1;
assign  ov5640_pwdn = 1'b0;

// wire iic_sda_debug = sccb_sda;
/////////////////////////////////////////////////////////////
// debug 
/////////////////////////////////////////////////////////////

reg href_reg;
always @(posedge ov5640_pclk or negedge sys_rst_n) begin
	if(!sys_rst_n)
		href_cnt <= 1'b0;
	else if(ov5640_href)
		href_cnt <= href_cnt + 1'b1;
	else 
		href_cnt <= 1'b0;
end

always @(posedge ov5640_pclk) begin
	href_reg <= ov5640_href;
end

wire href_v_cnt_flag = ~href_reg & ov5640_href;
always @(posedge ov5640_pclk or negedge sys_rst_n) begin
	if(!sys_rst_n)
		href_v_cnt <= 1'b0;
	else if(ov5640_vsync)
		href_v_cnt <= 1'b0;
	else if(href_v_cnt_flag)
		href_v_cnt <= href_v_cnt + 1'b1;
	else 
		href_v_cnt <= href_v_cnt;
end


	// OV5640
// input   wire            ov5640_pclk     ,   //! 摄像头像素时钟
// input   wire            ov5640_href     ,   //! 摄像头行同步信号
// input   wire            ov5640_vsync    ,   //! 摄像头场同步信号
// input   wire    [ 7:0]  ov5640_data     ,   //! 摄像头图像数据

// output  wire            sccb_scl        ,   //SCL
// inout   wire            sccb_sda        ,   //SDA
// //输出给fifo使用的信号
// output  wire            ov5640_wr_en    ,   //! 图像数据有效使能信号
// output  wire    [15:0]  ov5640_data_out     //! 图像数据

// 					cfg_done
// wire            cfg_start
// wire            i2c_end  
// wire    [23:0]  cfg_data 


localparam Debug = 0;
generate
	if(Debug == 1) begin
		// ila_cam_interface ila_cam_interface (
		// 	.clk		(ov5640_pclk	), // input wire clk
		
		
		// 	.probe0		(ov5640_href    ), // input wire [0:0]  probe0  
		// 	.probe1		(ov5640_vsync   ), // input wire [0:0]  probe1 
		// 	.probe2		(ov5640_data	), // input wire [7:0]  probe2 
		// 	.probe3		(ov5640_wr_en	), // input wire [0:0]  probe3 
		// 	.probe4		(ov5640_data_out), // input wire [15:0]  probe4 
		// 	.probe5		(href_cnt		),  // input wire [31:0]  probe4 
		// 	.probe6		(href_v_cnt		)  // input wire [31:0]  probe4 
		// );

	end
endgenerate

endmodule 