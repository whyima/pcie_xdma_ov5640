//! { signal: [
//!   { name: "pclk",					wave: "p........|.." },
//!   { name: "ov5640_href", 			wave: "0.1...0..|.." },
//!   { name: "data_flag",				wave: "0..1010..|.." },
//!   { name: "ov5640_data[7:0]",		wave: "x.3456x..|..", data: ["8'h01", "8'h02", "8'h03", "8'h04"] },
//!   { name: "pic_data_reg[7:0]",		wave: "x..3456x.|..", data: ["8'h01", "8'h02", "8'h03", "8'h04"] },
//!   { name: "data_out_reg[16:0]",		wave: "x...3.4.x|..", data: ["16'h0102", "16'h0304", "tail", "data"] },
//!   { name: "data_flag_dly1",			wave: "0...1010.|.."},
//!   { name: "ov5640_data_out",		wave: "x...3.4.x|..", data: ["32'hxxxx_xxxx", "32'hxxxx_xxxx"] }
//! ]}
module ov5640_data_in (
	input   wire            sys_rst_n       ,   //复位信号
	// OV5640
	input   wire            ov5640_pclk     ,   //摄像头像素时钟
	input   wire            ov5640_href     ,   //摄像头行同步信号 , 此信号有效就代表像素点有效
	input   wire            ov5640_vsync    ,   //摄像头场同步信号
	input   wire    [ 7:0]  ov5640_data     ,   //摄像头图像数据

	// 连接fifo
	output  wire            ov5640_wr_en    ,   //图像数据有效使能信号
    output  wire    [31:0]  ov5640_data_out     //图像数据
);


reg 			ov5640_vsync_reg;
reg     [7:0]   pic_data_reg    ;   //输入8位图像数据缓存
reg     [15:0]  data_out_reg    ;   //输出16位图像数据缓存
reg             data_flag       ;   //数据有效信号
reg             data_flag_dly1  ;   //图像数据拼接标志信号打拍

wire pic_flag = ((ov5640_vsync_reg == 1'b0) && (ov5640_vsync == 1'b1)) ? 1'b1 : 1'b0; //应该作为输出fifo的复位触发信号

always@(posedge ov5640_pclk or negedge sys_rst_n) begin
    if(sys_rst_n == 1'b0)
		ov5640_vsync_reg    <=  1'b0;
    else
        ov5640_vsync_reg    <=  ov5640_vsync;
end


//data_out_reg,pic_data_reg,data_flag:输出16位图像数据缓冲
//输入8位图像数据缓存输入8位,图像数据缓存
always@(posedge ov5640_pclk or negedge sys_rst_n) begin
    if(sys_rst_n == 1'b0) begin
            data_out_reg    <=  16'd0;
            pic_data_reg    <=  8'd0;
            data_flag       <=  1'b0;
	end
    else if(ov5640_href == 1'b1) begin
            data_flag       <=  ~data_flag;
            pic_data_reg    <=  ov5640_data;
            data_out_reg    <=  data_out_reg;
        if(data_flag == 1'b1)
            data_out_reg    <=  {pic_data_reg,ov5640_data};
        else
            data_out_reg    <=  data_out_reg;
	end
    else begin
            data_flag       <=  1'b0;
            pic_data_reg    <=  8'd0;
            data_out_reg    <=  data_out_reg;
	end	
end

//data_flag_dly1:图像数据缓存打拍
always@(posedge ov5640_pclk or negedge sys_rst_n)
    if(sys_rst_n == 1'b0)
        data_flag_dly1  <=  1'b0;
    else
        data_flag_dly1  <=  data_flag;

assign ov5640_data_out = {8'b0,data_out_reg[15:11],3'b000,data_out_reg[10:5],2'b00,data_out_reg[4:0],3'b000};
assign ov5640_wr_en = data_flag_dly1;

endmodule