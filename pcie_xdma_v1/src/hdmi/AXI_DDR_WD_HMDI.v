`timescale 1 ns / 1 ps
module AXI_DDR_WD_HDMI #
(
	parameter 			MAX_BURST_COUNTER 			= 14'd16_200,
	parameter 			BURST_COUNTER_WIDTH 		= 14,
	parameter  			C_M_TARGET_SLAVE_BASE_ADDR	= 32'h10_000_000,//! 操作内存起始地址
	parameter integer 	C_M_AXI_BURST_LEN			= 128,//! 突发长度取值范围1, 2, 4, 8, 16, 32, 64, 128, 256
	parameter integer 	C_M_AXI_ID_WIDTH			= 1,//! ID宽度（指定某些特殊任务传输顺序，未用）
	parameter integer 	C_M_AXI_ADDR_WIDTH			= 32,//! 地址总线位宽
	parameter integer 	C_M_AXI_DATA_WIDTH			= 32,//! 数据总线宽度
	// 以下为用户自定义数据位宽，均未用到
	parameter integer 	C_M_AXI_AWUSER_WIDTH		= 0,
	parameter integer 	C_M_AXI_WUSER_WIDTH			= 0,
	parameter integer 	C_M_AXI_BUSER_WIDTH			= 0
)
(
	// input init_txn_pulse,
	input							almost_empty,
	input 							prog_empty,
	input  							init_txn_pulse,
	//AXI主接口的信号
	// 全局时钟复位 
	input wire  M_AXI_ACLK,
	input wire  M_AXI_ARESETN,
	/**************************************写地址通道*********************************************/
	output wire [C_M_AXI_ID_WIDTH-1 : 0] 		M_AXI_AWID,
	output wire [C_M_AXI_ADDR_WIDTH-1 : 0] 		M_AXI_AWADDR,//! 写地址
	//控制信号
	output wire [7 : 0] 						M_AXI_AWLEN,  //突发长度
	output wire [2 : 0] 						M_AXI_AWSIZE, //突发大小
	output wire [1 : 0] 						M_AXI_AWBURST,//突发类型
	output wire  								M_AXI_AWVALID, //写地址有效
	output wire  								M_AXI_AWLOCK, //锁 0：正常传输   1：独有传输
	output wire [3 : 0] 						M_AXI_AWCACHE,//总线存储类型 0010：不缓存
	output wire [2 : 0] 						M_AXI_AWPROT, //保护类型
	output wire [3 : 0] 						M_AXI_AWQOS,  //服务质量
	output wire [C_M_AXI_AWUSER_WIDTH-1 : 0]	M_AXI_AWUSER, //用户自定位
	input  wire  								M_AXI_AWREADY, //从机响应

	/**************************************写数据通道*********************************************/
	output wire [C_M_AXI_DATA_WIDTH-1 : 0] 		M_AXI_WDATA,
	output wire  								M_AXI_WLAST, //用来指示一次突发，最后一个数据的传输完成
	output wire  								M_AXI_WVALID,
	input  wire 	 							M_AXI_WREADY,

	output wire [C_M_AXI_DATA_WIDTH/8-1 : 0]	M_AXI_WSTRB,//频闪信号：用移位表示一个字节数据是否有效，如：32位的数据，则wstrb用四位分别表示每个byte是否有效
	output wire [C_M_AXI_WUSER_WIDTH-1 : 0] 	M_AXI_WUSER,

	/**************************************写响应通道*********************************************/
	output wire  								M_AXI_BREADY,
	input  wire [C_M_AXI_ID_WIDTH-1 : 0] 		M_AXI_BID,
	input  wire  								M_AXI_BVALID,
	input  wire [1 : 0] 						M_AXI_BRESP,//写响应信号，表示写响应的一个状态
	input  wire [C_M_AXI_BUSER_WIDTH-1 : 0] 	M_AXI_BUSER
);

//计算输入的一个参数位深度               
function integer clogb2 (input integer bit_depth); begin
    for(clogb2=0; bit_depth>0; clogb2=clogb2+1)
      bit_depth = bit_depth >> 1;
end
endfunction

//! 一次读写突发传输计数位宽==4
localparam integer C_TRANSACTIONS_NUM = clogb2(C_M_AXI_BURST_LEN-1);

//localparam integer C_MASTER_LENGTH	= 12;//这个参数用在写突发和读突发的计数设置上
// 总突发次数计数位宽
//==6 暂时不用
// localparam integer C_NO_BURSTS_REQ = BURST_COUNTER_WIDTH - 1;//14-1

//状态机
parameter [1:0] IDLE = 2'b00, 
				INIT_WRITE   = 2'b01;

reg [1:0] mst_exec_state;

//axi 协议输出信号寄存器
reg [C_M_AXI_ADDR_WIDTH-1 : 0] 	axi_awaddr;
reg  							axi_awvalid;
reg [C_M_AXI_DATA_WIDTH-1 : 0] 	axi_wdata = 32'h00_ff_00_00;
reg  							axi_wlast;
reg  							axi_wvalid;
reg  							axi_bready;
reg [C_TRANSACTIONS_NUM : 0] 	write_index;
wire [31:0] 					burst_size_bytes;
//读写突发计数
reg  							start_single_burst_write;
reg  							burst_write_active;
//Interface response error flags
wire  							wnext;
reg 							writes_done;
reg [31:0] 						write_burst_counter;

//user
wire [31:0] 					rgb_out;
reg     						burst_vaild;
// reg 							init_txn_pulse;

//信号的基本配置
// 写地址
assign M_AXI_AWID	 = 'b0;
assign M_AXI_AWADDR	 = C_M_TARGET_SLAVE_BASE_ADDR + axi_awaddr;//基地址+偏移地址
assign M_AXI_AWLEN	 = C_M_AXI_BURST_LEN - 1;
assign M_AXI_AWSIZE	 = clogb2((C_M_AXI_DATA_WIDTH/8)-1);
assign M_AXI_AWBURST = 2'b01;
assign M_AXI_AWLOCK	 = 1'b0;
assign M_AXI_AWCACHE = 4'b0010;
assign M_AXI_AWPROT	 = 3'h0;
assign M_AXI_AWQOS	 = 4'h0;
assign M_AXI_AWUSER	 = 'b1;
assign M_AXI_AWVALID = axi_awvalid;
// 写数据
assign M_AXI_WDATA	= axi_wdata;
assign M_AXI_WSTRB	= {(C_M_AXI_DATA_WIDTH/8){1'b1}};
assign M_AXI_WLAST	= axi_wlast;
assign M_AXI_WUSER	= 'b0;
assign M_AXI_WVALID	= axi_wvalid;
assign M_AXI_BREADY	= axi_bready;

	
assign burst_size_bytes	= C_M_AXI_BURST_LEN * C_M_AXI_DATA_WIDTH/8;//突发长度16*一个4个字节 = 突发传64个字节


//--------------------
//USER
//--------------------
always @(posedge M_AXI_ACLK) begin
    if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1 )
		burst_vaild <= 1'b0;
    else if (prog_empty||almost_empty)
        burst_vaild <= 1'b0;
    else
		burst_vaild <= 1'b1;
end


//--------------------
//写地址通道
//--------------------
always @(posedge M_AXI_ACLK) begin
    if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1 )
        axi_awvalid <= 1'b0;
    else if (~axi_awvalid && start_single_burst_write)                                                            
        axi_awvalid <= 1'b1;                                           
    else if (M_AXI_AWREADY && axi_awvalid)                                                             
        axi_awvalid <= 1'b0;
    else                                                               
      	axi_awvalid <= axi_awvalid;                                      
end
 
always @(posedge M_AXI_ACLK)begin
	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)
	    axi_awaddr <= 'b0;                                             
	else if (M_AXI_AWREADY && axi_awvalid)
	    axi_awaddr <= axi_awaddr + burst_size_bytes;                   
	else
	  	axi_awaddr <= axi_awaddr;                                        
end


//--------------------
//写数据通道
//--------------------
assign wnext = M_AXI_WREADY & axi_wvalid;                                   
                                                                                
always @(posedge M_AXI_ACLK)begin
  	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)                                                        
	  	axi_wvalid <= 1'b0;
	else if (~axi_wvalid && start_single_burst_write) begin
	  	axi_wvalid <= 1'b1;
	end
	else if (wnext && axi_wlast)
		axi_wvalid <= 1'b0;
  	else
		axi_wvalid <= axi_wvalid;
end                                                                               
				
// always @(posedge M_AXI_ACLK)begin
//   	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)                                                        
// 	  	axi_wvalid <= 1'b0;
// 	else if (burst_write_active) begin
// 		if (almost_empty)
// 			axi_wvalid <= 1'b0;
// 		else 
// 	  		axi_wvalid <= 1'b1;
// 	end
// 	else if (wnext && axi_wlast)
// 		axi_wvalid <= 1'b0;
//   	else
// 		axi_wvalid <= axi_wvalid;
// end      



always @(posedge M_AXI_ACLK) begin                                                                             
	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1 )                                                                         
		axi_wlast <= 1'b0;                                                          
	else if (((write_index == C_M_AXI_BURST_LEN-2 && C_M_AXI_BURST_LEN >= 2) && wnext) || (C_M_AXI_BURST_LEN == 1 ))
		axi_wlast <= 1'b1;                                                                                                                                     					
	else if (wnext)                                                                 
		axi_wlast <= 1'b0;
	else if (axi_wlast && C_M_AXI_BURST_LEN == 1)                                   
		axi_wlast <= 1'b0;                                                            
	else                                                                            
		axi_wlast <= axi_wlast;                                                       
end                                                                               
																																									   
//统计当前突发总共传输了多少次数据 方便拉高last
always @(posedge M_AXI_ACLK) begin                                                                             
	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1 || start_single_burst_write == 1'b1)    
		write_index <= 0;
	else if (wnext && (write_index != C_M_AXI_BURST_LEN-1))                                                                                            
		write_index <= write_index + 1;
	else                                                                            
	write_index <= write_index;                                                   
end                                                                               
																						  
always @(posedge M_AXI_ACLK) begin                                                                             
	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)
		axi_wdata <= ~axi_wdata;                                                                                    
	else if (wnext)                                                                 
		axi_wdata <= axi_wdata;                                                   
	else                                                                            
		axi_wdata <= axi_wdata;
end                                                                             
//   assign axi_wdata = rgb_out;

//----------------------------
//写响应通道.
//----------------------------

always @(posedge M_AXI_ACLK) begin
	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1 )
		axi_bready <= 1'b0;
	else if (M_AXI_BVALID && ~axi_bready)
		axi_bready <= 1'b1;
	else if (axi_bready)
		axi_bready <= 1'b0;                                             
	else                                                                
		axi_bready <= axi_bready;                                         
end                                                                   

//----------------------------
//其他
//----------------------------
reg [31:0]  cnt_cp; //由实际情况来看，差不多等一次突发要604个周期左右，用于最后一次突发的强行启动cvlll 


//状态机
always @ ( posedge M_AXI_ACLK) begin                                                                                                     
if (M_AXI_ARESETN == 1'b0 ) begin
	cnt_cp <= 'd603;                                                                                                                                
	mst_exec_state      <= IDLE;                                                                
	start_single_burst_write <= 1'b0;                                                                                                                                    
end                                                                                                   
else begin
	// init_txn_pulse <= 1'b0;
	case (mst_exec_state)
		IDLE:begin
			cnt_cp <= 603;
			if ( init_txn_pulse == 1'b1) begin                                                                                         
				mst_exec_state  <= IDLE;
				// init_txn_pulse <= 1'b1;
			end                                                                                           
			else begin                                                                                         
				mst_exec_state  <= INIT_WRITE;//无条件进入                                                      
			end				
		end
		INIT_WRITE: begin    
			if (writes_done)
				mst_exec_state <= IDLE;//                                                              
			else begin                                                                                       
				mst_exec_state  <= INIT_WRITE;                                                                                                                                                        
				if (~axi_awvalid && ~start_single_burst_write && ~burst_write_active) begin 
					//if(rd_en_0_reg1 == 1'b0 && rd_en_1_reg1 == 1'b0)     
					if (burst_vaild == 1'b1  || ((write_burst_counter == MAX_BURST_COUNTER - 1) && cnt_cp == 'd0)) //最后一次时若突发强行读，忽略fifo的
						start_single_burst_write <= 1'b1;                                                                          
					else 
						start_single_burst_write <= 1'b0;

					if (write_burst_counter == MAX_BURST_COUNTER - 1) 
						cnt_cp <= cnt_cp - 'd1;
				end                                                                                       
				else begin
					start_single_burst_write <= 1'b0;                        
				end
			end
		end
		default: begin                                                                                           
			mst_exec_state  <= IDLE;                                                              
		end                                                                                             
	endcase
	end                                                                                                   
end

always @(posedge M_AXI_ACLK) begin
	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)     //这个信号为高表示一次突发正在进行                                                                              
		burst_write_active <= 1'b0;                                                                           
	else if (start_single_burst_write)                                                                      
		burst_write_active <= 1'b1;                                                                           
	else if (M_AXI_BVALID && axi_bready)                                                                    
		burst_write_active <= 0;
end                                                                                                       
	                                                                                                            
always @(posedge M_AXI_ACLK) begin                                                                                                     
	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1)                                                                                 
		writes_done <= 1'b0;                                                                                  
	else if (M_AXI_BVALID && (write_burst_counter  == MAX_BURST_COUNTER) && axi_bready)                                
		writes_done <= 1'b1;                                                                                  
	else                                                                                                    
		writes_done <= writes_done;                                                                           
end

always @(posedge M_AXI_ACLK) begin                                                                                                     
	if (M_AXI_ARESETN == 0 || init_txn_pulse == 1'b1 ) begin                                                                                                 
		write_burst_counter <= 'b0;
	end                                                                                                   
	else if (M_AXI_AWREADY && axi_awvalid) begin                                                                                                 
		if (write_burst_counter != MAX_BURST_COUNTER) begin                                                                                             
			write_burst_counter <= write_burst_counter + 1'b1;
			//write_burst_counter[C_NO_BURSTS_REQ] <= 1'b1;                                                 
		end                                                                                               
	end                                                                                                   
	else                                                                                                    
		write_burst_counter <= write_burst_counter;
end     
		
///////////////////////////////////////////////////////////////////
/// DEBUG
///////////////////////////////////////////////////////////////////

// reg [1:0] mst_exec_state;

// reg [C_M_AXI_ADDR_WIDTH-1 : 0] 	axi_awaddr;
// reg  	axi_awvalid;
// reg [C_M_AXI_DATA_WIDTH-1 : 0] 	axi_wdata;
// reg  	axi_wlast	;
// reg  	axi_wvalid	;
// reg  	axi_bready	;
// reg [C_TRANSACTIONS_NUM : 0] 	write_index;
// wire [C_TRANSACTIONS_NUM+2 : 0] 	burst_size_bytes;
// //读写突发计数
// reg  	start_single_burst_write;
// reg  	burst_write_active;
// wire  	wnext;
// reg 	writes_done;
// reg [31:0] write_burst_counter;
localparam DEBUG = 0;
generate
	if (DEBUG == 1) begin
		// ila_axi_wd ila_axi_wd (
		// 	.clk		(M_AXI_ACLK					), // input wire clk
		
		// 	.probe0		(mst_exec_state				), // input wire [1:0]  probe0  
		// 	.probe1		(axi_awaddr					), // input wire [31:0]  probe1 
		// 	.probe2		(axi_awvalid				), // input wire [0:0]  probe2 
		// 	.probe3		(axi_wdata					), // input wire [31:0]  probe3 
		// 	.probe4		(axi_wlast					), // input wire [0:0]  probe4 
		// 	.probe5		(axi_wvalid					), // input wire [0:0]  probe5 
		// 	.probe6		(axi_bready					), // input wire [0:0]  probe6 
		// 	.probe7		(write_index				), // input wire [6:0]  probe7 
		// 	.probe8		(burst_size_bytes			), // input wire [31:0]  probe8 
		// 	.probe9		(start_single_burst_write	), // input wire [0:0]  probe9 
		// 	.probe10	(burst_write_active			), // input wire [0:0]  probe10 
		// 	.probe11	(wnext						), // input wire [0:0]  probe11 
		// 	.probe12	(writes_done				), // input wire [0:0]  probe12 
		// 	.probe13	(write_burst_counter		), // input wire [31:0]  probe13
		// 	.probe14	(init_txn_pulse				), // input wire [0:0]  probe14 
		// 	.probe15	(prog_empty					), // input wire [0:0]  probe15
		// 	.probe16	(M_AXI_AWREADY				), // input wire [0:0]  probe14 
		// 	.probe17	(M_AXI_BVALID				),  // input wire [0:0]  probe15
		// 	.probe18	(burst_vaild				)
		// );
	end
endgenerate
endmodule
