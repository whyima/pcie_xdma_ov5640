module  hdmi_ctrl
(
    input   wire            sys_clk     	,   //输入工作时钟,频率50MHz
    input   wire            sys_rst_n   	,   //输入复位信号,低电平有效
	input   wire            sys_init_done	,   //! 系统初始化完成(SDRAM cable done + 摄像头 cfg_done )

    output  wire            ddc_scl     	,
    inout   wire            ddc_sda     	,
    
    input   wire            hdmi_in_clk   	,
    output  wire            hdmi_in_rst_n 	,
    input   wire            hdmi_in_hsync 	,   //输出行同步信号
    input   wire            hdmi_in_vsync 	,   //输出场同步信号
    input   wire    [23:0]  hdmi_in_rgb   	,   //输出像素信息
    input   wire            hdmi_in_de    	,
	output	wire			fifo_wr_en		,

    output  wire  			cfg_done 		,
    output  wire            hdmi_out_clk   	,
    output  wire            hdmi_out_rst_n 	,
    output  wire            hdmi_out_hsync 	,   //输出行同步信号
    output  wire            hdmi_out_vsync 	,   //输出场同步信号
    output  wire    [31:0]  hdmi_out_rgb	,   //输出像素信息
    output  wire            hdmi_out_de		,

	output	reg  	[31:0]	h_cnt			,
	output	reg		[31:0]	v_cnt 			

);

//********************************************************************//
//****************** Parameter and Internal Signal *******************//
//********************************************************************//
assign  hdmi_out_clk   = hdmi_in_clk;
assign  hdmi_out_hsync = hdmi_in_hsync;
assign  hdmi_out_vsync = hdmi_in_vsync;
assign  hdmi_out_rgb   = {8'd0,hdmi_in_rgb};
assign  hdmi_out_de    = hdmi_in_de;
assign  hdmi_in_rst_n  = sys_rst_n;
assign  hdmi_out_rst_n = sys_rst_n;


always @(posedge hdmi_in_clk or negedge sys_rst_n) begin
	if (!sys_rst_n) 
		h_cnt <= 'd0;
	else if (hdmi_in_de)
		h_cnt <= h_cnt + 1'b1;
	else 
		h_cnt <= 'd0;
end

always @(posedge hdmi_in_clk or negedge sys_rst_n) begin
	if (!sys_rst_n) 
		v_cnt <= 'd0;
	else if(hdmi_in_vsync)
		v_cnt <= 'd0;
	else if (h_cnt=='d1919)
		v_cnt <= v_cnt + 1'b1;
	else 
		v_cnt <= v_cnt;
end

assign fifo_wr_en = v_cnt[0]?hdmi_in_de:1'b0;
//********************************************************************//
//*************************** Instantiation **************************//
//********************************************************************//

hdmi_i2c hdmi_i2c_inst(
 .sys_clk   (sys_clk    )   ,   //系统时钟
 .sys_rst_n (sys_rst_n & sys_init_done  )   ,   //复位信号
 .cfg_done  (cfg_done   )   ,   //寄存器配置完成
 .sccb_scl  (ddc_scl    )   ,   //SCL
 .sccb_sda  (ddc_sda    )       //SDA

);



endmodule
