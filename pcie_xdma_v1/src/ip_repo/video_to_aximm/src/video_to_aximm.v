module video_to_aximm 
#(
			////
	parameter MAX_BURST_COUNTER 	= 'd3750, 
	parameter BURST_COUNTER_WIDTH 	= 14,
	parameter MAX_FIFO_RST_CNT 		= 9'd300,
	//操作内存起始地址
	parameter  C_M_TARGET_SLAVE_BASE_ADDR	= 32'h00_000_000,
	//突发长度取值范围1, 2, 4, 8, 16, 32, 64, 128, 256   
	parameter integer C_M_AXI_BURST_LEN	= 128,
	// ID宽度（指定某些特殊任务传输顺序，未用）
	parameter integer C_M_AXI_ID_WIDTH	= 1,
	// 地址总线位宽
	parameter integer C_M_AXI_ADDR_WIDTH	= 32,
	// 数据总线宽度
	parameter integer C_M_AXI_DATA_WIDTH	= 32,
	// 以下为用户自定义数据位宽，均未用到
	parameter integer C_M_AXI_AWUSER_WIDTH	= 0,
	parameter integer C_M_AXI_ARUSER_WIDTH	= 0,
	parameter integer C_M_AXI_WUSER_WIDTH	= 0,
	parameter integer C_M_AXI_RUSER_WIDTH	= 0,
	parameter integer C_M_AXI_BUSER_WIDTH	= 0
)
(
	input 									M_AXI_ACLK		,
	input 	 								ui_rst	     	,  //外部复位
	//ov5650数据有效信号
	input 									ov5640_vsync	,

	/**************************************fifo*********************************************/
	output  								rd_wdfifo_en 	,
	input   [31:0]							fifo_data_rd	,
	input 									almost_empty	,	
	input 									prog_empty		,		

	/**************************************写地址通道*********************************************/
	// ID
	output  [C_M_AXI_ID_WIDTH-1 : 0] 		M_AXI_AWID		,
	// 写地址
	output  [C_M_AXI_ADDR_WIDTH-1 : 0] 		M_AXI_AWADDR	,
	//控制信号
	output  [7 : 0] 						M_AXI_AWLEN		,  	//突发长度
	output  [2 : 0] 						M_AXI_AWSIZE	, 	//突发大小
	output  [1 : 0] 						M_AXI_AWBURST	,	//突发类型
	output   		 						M_AXI_AWVALID	,	//写地址有效
	output   								M_AXI_AWLOCK	,	//锁 0：正常传输   1：独有传输
	output  [3 : 0] 						M_AXI_AWCACHE	,	//总线存储类型 0010：不缓存
	output  [2 : 0] 						M_AXI_AWPROT	, 	//保护类型
	output  [3 : 0] 						M_AXI_AWQOS		,  	//服务质量
	output  [C_M_AXI_AWUSER_WIDTH-1 : 0] 	M_AXI_AWUSER	,	//用户自定位
	input    							  	M_AXI_AWREADY	,	//从机响应
	/************************************************************************************************/

	/**************************************写数据通道*********************************************/
	output  [C_M_AXI_DATA_WIDTH-1 : 0] 		M_AXI_WDATA		,
	output   								M_AXI_WLAST		,	//用来指示一次突发，最后一个数据的传输完成
	output   								M_AXI_WVALID	,
	input    								M_AXI_WREADY	,
	output  [C_M_AXI_DATA_WIDTH/8-1 : 0] 	M_AXI_WSTRB		,//频闪信号：用移位表示一个字节数据是否有效，如：32位的数据，则wstrb用四位分别表示每个byte是否有效

	/************************************************************************************************/

	/**************************************写响应通道*********************************************/
	output   								M_AXI_BREADY	,
	input   [C_M_AXI_ID_WIDTH-1 : 0] 		M_AXI_BID		,
	input    								M_AXI_BVALID	,
	input   [1 : 0] 						M_AXI_BRESP		,//写响应信号，表示写响应的一个状态
	input   [C_M_AXI_BUSER_WIDTH-1 : 0] 	M_AXI_BUSER		,
	/************************************************************************************************/

	// /**************************************读地址通道*********************************************/
	output  [C_M_AXI_ADDR_WIDTH-1 : 0] 		M_AXI_ARADDR	,
	output  [7 : 0] 						M_AXI_ARLEN		,
	output  [2 : 0] 						M_AXI_ARSIZE	,
	output  [1 : 0] 						M_AXI_ARBURST	,
	output   								M_AXI_ARVALID	,
	input    								M_AXI_ARREADY	,
	output  [C_M_AXI_ID_WIDTH-1 : 0] 		M_AXI_ARID		,
	output   								M_AXI_ARLOCK	,
	output  [3 : 0] 						M_AXI_ARCACHE	,
	output  [2 : 0] 						M_AXI_ARPROT	,
	output  [3 : 0] 						M_AXI_ARQOS		,
	output  [C_M_AXI_ARUSER_WIDTH-1 : 0] 	M_AXI_ARUSER	,
	// /************************************************************************************************/

	// /**************************************读数据通道*********************************************/
	output   								M_AXI_RREADY	,
	input   [C_M_AXI_DATA_WIDTH-1 : 0] 		M_AXI_RDATA		,
	input   [1 : 0] 						M_AXI_RRESP		,
	input    								M_AXI_RLAST		,
	input    								M_AXI_RVALID	,
	input   [C_M_AXI_ID_WIDTH-1 : 0] 		M_AXI_RID		



);
	
wire  [C_M_AXI_WUSER_WIDTH-1 : 0] 	M_AXI_WUSER		; //没有
wire  [C_M_AXI_RUSER_WIDTH-1 : 0] 	M_AXI_RUSER		;





// /***********************************************************************************/
reg ov5640_vsync_reg;
reg ov5640_vsync_reg1;
always @(posedge M_AXI_ACLK) begin
	ov5640_vsync_reg <= ov5640_vsync;
	ov5640_vsync_reg1 <= ov5640_vsync_reg;
end
wire init_txn_pulse = (~ov5640_vsync_reg & ov5640_vsync_reg1) ? 1'b1 : 1'b0;
AXI_DDR_WD_A_v1_0_SA_AXI #(
	.MAX_BURST_COUNTER			(MAX_BURST_COUNTER			),
	.BURST_COUNTER_WIDTH		(BURST_COUNTER_WIDTH		),
	.C_M_TARGET_SLAVE_BASE_ADDR	(C_M_TARGET_SLAVE_BASE_ADDR	),
	.C_M_AXI_BURST_LEN			(C_M_AXI_BURST_LEN			),
	.C_M_AXI_ID_WIDTH			(C_M_AXI_ID_WIDTH			),
	.C_M_AXI_ADDR_WIDTH			(C_M_AXI_ADDR_WIDTH			),
	.C_M_AXI_DATA_WIDTH			(C_M_AXI_DATA_WIDTH			),
	.C_M_AXI_AWUSER_WIDTH	 	(C_M_AXI_AWUSER_WIDTH		),
	.C_M_AXI_WUSER_WIDTH	 	(C_M_AXI_WUSER_WIDTH		),
	.C_M_AXI_BUSER_WIDTH	 	(C_M_AXI_BUSER_WIDTH		)
)
AXI_DDR_WD_A_v1_0_SA_AXI
(
	//AXI主接口的信号
	.almost_empty	(almost_empty	),
	.prog_empty		(prog_empty		),
	.init_txn_pulse	(init_txn_pulse	),
	// 全局时钟复位 
	.M_AXI_ACLK		(M_AXI_ACLK		),
	.M_AXI_ARESETN	(~ui_rst		),
	/**************************************写地址通道*********************************************/
	.M_AXI_AWID		(M_AXI_AWID		),
	.M_AXI_AWADDR	(M_AXI_AWADDR	),
	.M_AXI_AWLEN	(M_AXI_AWLEN	), //突发长度
	.M_AXI_AWSIZE	(M_AXI_AWSIZE	), //突发大小 
	.M_AXI_AWBURST	(M_AXI_AWBURST	), //突发类型
	.M_AXI_AWVALID	(M_AXI_AWVALID	), //写地址有效
	.M_AXI_AWLOCK	(M_AXI_AWLOCK	), //锁 0：正常传输   1：独有传输
	.M_AXI_AWCACHE	(M_AXI_AWCACHE	), //总线存储类型 0010：不缓存
	.M_AXI_AWPROT	(M_AXI_AWPROT	), //保护类型
	.M_AXI_AWQOS	(M_AXI_AWQOS	), //服务质量
	.M_AXI_AWUSER	(M_AXI_AWUSER	), //用户自定位
	.M_AXI_AWREADY	(M_AXI_AWREADY	), //从机响应
	/**************************************写数据通道*********************************************/
	.M_AXI_WDATA	(), //数据改为从fifo里取
	.M_AXI_WLAST	(M_AXI_WLAST	),	//用来指示一次突发，最后一个数据的传输完成
	.M_AXI_WVALID	(M_AXI_WVALID	),
	.M_AXI_WREADY	(M_AXI_WREADY	),
	.M_AXI_WSTRB	(M_AXI_WSTRB	),  //频闪信号：用移位表示一个字节数据是否有效，如：32位的数据，则wstrb用四位分别表示每个byte是否有效
	.M_AXI_WUSER	(M_AXI_WUSER	),

	/**************************************写响应通道*********************************************/
	.M_AXI_BREADY	(M_AXI_BREADY	),               
	.M_AXI_BID		(M_AXI_BID		),
	.M_AXI_BVALID	(M_AXI_BVALID	),
	.M_AXI_BRESP	(M_AXI_BRESP	),//写响应信号，表示写响应的一个状态
	.M_AXI_BUSER	(M_AXI_BUSER	)
);
///////////////////////////////////////////////////////////////////////////////////////////////
/// 写fifo连hdmi输入
///////////////////////////////////////////////////////////////////////////////////////////////

// 要非常注意 ，AXI_WD 信号的是当fifo里面有一次突发所需的数据就会读(本工程设的prog 为128) 以此保证突发能把数据都读完
// fifo_ddr_wd fifo_ddr_wd (
// 	.rst			(ov5640_vsync	), // input wire rst
// 	.wr_clk			(ov5640_pclk	), // input wire wr_clk
// 	.rd_clk			(M_AXI_ACLK		), // input wire rd_clk
// 	.din			(ov5640_data	), // input wire [31 : 0] din 
// 	.wr_en			(ov5640_wr_en	), // input wire wr_en
// 	.rd_en			(rd_wdfifo_en	), // input wire rd_en
// 	.dout			(M_AXI_WDATA	), // output wire [31 : 0] dout。。
// 	.almost_empty	(almost_empty	),
// 	.prog_empty		(prog_empty		),

// 	.full			(), // output wire full
// 	.empty			(), // output wire empty
// 	.wr_rst_busy	(), // output wire wr_rst_busy
// 	.rd_rst_busy	()  // output wire rd_rst_busy
// );



AXI_DDR_RW_v1_0_M_AXI #(
	.MAX_BURST_COUNTER 			(MAX_BURST_COUNTER			),
	.C_M_TARGET_SLAVE_BASE_ADDR	(C_M_TARGET_SLAVE_BASE_ADDR	),
	.C_M_AXI_BURST_LEN			(C_M_AXI_BURST_LEN			),
	.C_M_AXI_ID_WIDTH			(C_M_AXI_ID_WIDTH			),
	.C_M_AXI_ADDR_WIDTH			(C_M_AXI_ADDR_WIDTH			),
	.C_M_AXI_DATA_WIDTH			(C_M_AXI_DATA_WIDTH			),
	.C_M_AXI_ARUSER_WIDTH		(C_M_AXI_ARUSER_WIDTH		),
	.C_M_AXI_RUSER_WIDTH		(C_M_AXI_RUSER_WIDTH		)
)
AXI_DDR_RW_v1_0_M_AXI
(
	.init_txn_pulse		(1'b0			), //产生一次pulse 让模块恢复状态
	.rd_prog_full		(1'b0			),
	.M_AXI_ACLK			(M_AXI_ACLK		),
	.M_AXI_ARESETN		(1'b1			),
	/**************************************读地址通道*********************************************/
	.M_AXI_ARADDR		(M_AXI_ARADDR	),
	.M_AXI_ARLEN		(M_AXI_ARLEN	),
	.M_AXI_ARSIZE		(M_AXI_ARSIZE	),
	.M_AXI_ARBURST		(M_AXI_ARBURST	),
	.M_AXI_ARVALID		(M_AXI_ARVALID	),
	.M_AXI_ARREADY		(M_AXI_ARREADY	),
	.M_AXI_ARID			(M_AXI_ARID		),
	.M_AXI_ARLOCK		(M_AXI_ARLOCK	),
	.M_AXI_ARCACHE		(M_AXI_ARCACHE	),
	.M_AXI_ARPROT		(M_AXI_ARPROT	),
	.M_AXI_ARQOS		(M_AXI_ARQOS	),
	.M_AXI_ARUSER		(M_AXI_ARUSER	),
	/************************************************************************************************/

	/**************************************读数据通道*********************************************/
	.M_AXI_RREADY		(M_AXI_RREADY	),
	.M_AXI_RDATA		(M_AXI_RDATA	),
	.M_AXI_RRESP		(M_AXI_RRESP	),
	.M_AXI_RLAST		(M_AXI_RLAST	),
	.M_AXI_RVALID		(M_AXI_RVALID	),
	.M_AXI_RID			(M_AXI_RID		),
	.M_AXI_RUSER		(M_AXI_RUSER	)
	/************************************************************************************************/
);


assign rd_wdfifo_en = M_AXI_WVALID & M_AXI_WREADY;
assign fifo_data_rd = M_AXI_WDATA;
endmodule