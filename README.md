# pcie_xdma_ov5640

#### 介绍
> 利用xdma ip 实现了pcie传输视频数据上传到pc，上位机实现了qt显示视频数据，驱动用的官方的xdma驱动，
> 开发板用的野火升腾pro，100t


#### 软件架构
**软件架构说明**

> Vivado 2019.2
>
> Qt 6.6 (MSVC 2019, qml)

软件搭配本工程直接使用，驱动使用官方的XDMA驱动，

#### 效果展示

> 下面分别是启动页面，GPIO控制，读写测试，速度测速

<img src="./assets/image-20231226163820007.png" alt="image-20231226163820007" style="zoom: 25%;" /><img src="./assets/image-20231226111212390.png" alt="image-20231226111212390" style="zoom:25%;" /><img src="./assets/image-20231226111148473.png" alt="image-20231226111148473" style="zoom:25%;" /><img src="./assets/image-20231226111103495.png" alt="image-20231226111103495" style="zoom:25%;" />

> 视频采集，两路视频传输，ov5640一路，hdmi一路

![image-20231226111232914](./assets/image-20231226111232914-1703580129436-3.png)



#### 参考

> 驱动的使用参考了正点原子的 [3_达芬奇Pro之PCIe开发指南(Windows系统版)-V1.2.pdf](C:\Users\Lee\Desktop\常用文档\3_达芬奇Pro之PCIe开发指南(Windows系统版)-V1.2.pdf) 
>
> Qt 设计借用了 B站up[**诺谦君**](https://space.bilibili.com/82157618/channel/collectiondetail?sid=1360570&amp%3Bctype=0)的[炫酷Qt qml开源界面框架demo-V1](https://github.com/nuoqian-lgtm/QianWindow)。在此基础上完成的设计
>





